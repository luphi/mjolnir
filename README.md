# Mjolnir

Mjolnir is a 2D trajectory in the style of Gunbound: Thor's Hammer with singleplayer and multiplayer.

It is or will be available for Windows, macOS, Linux, iOS, and Android.

## Screenshots

![](Content/Screenshots/root_menu.png)
![](Content/Screenshots/lobby.png)
![](Content/Screenshots/pregame.png)
![](Content/Screenshots/ingame.png)

## License

This project is licensed under the GNU Public License v3 - see the [COPYING](COPYING) file for details.