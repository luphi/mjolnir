﻿using Mjolnir.Common.Disk;
using Mjolnir.Common.Models;
using Mjolnir.Common.Server;
using Mjolnir.Server.Disk;
using Microsoft.Extensions.Hosting;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Mjolnir.Server
{
   public class ServerService : BackgroundService
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IContentWrapper mContentWrapper = new ContentWrapper();
      private ServerBootstrap mBootstrap;

      protected override Task ExecuteAsync(CancellationToken aCancellationToken)
      {
         try
         {
            // Read the configuration from disk or use a default one failing that.
            Configuration configuration = GetConfiguration();

            // Load all stages. If the returned list is empty, 1+ stage(s) failed to
            // load. This server cannot operate without all of them.
            List<Stage> stages = VerifyStages();
            if (stages.Count == 0)
            {
               mLog.Error("At least one stage load failed - cannot continue");
               return Task.CompletedTask;
            }

            mBootstrap = new ServerBootstrap(mContentWrapper,
                                             configuration.Port,
                                             configuration.RegistryHostname,
                                             configuration.RegistryPort,
                                             configuration.IsPrivate,
                                             configuration.IsVoiceChatEnabled,
                                             configuration.AreBotsEnabled,
                                             configuration.Name,
                                             configuration.MessageOfTheDay,
                                             configuration.AdminPassword,
                                             configuration.JoinPassword,
                                             configuration.BanList);
            mBootstrap.Start();
         }
         catch (Exception e)
         {
            mLog.Error(e, "Exception thrown while starting the server");
         }

         return Task.CompletedTask;
      }

      public override Task StopAsync(CancellationToken aCancellationToken)
      {
         try
         {
            // Stop the server through the boostrap and provide a reason. Clients will
            // display this reason, hopefully reducing the confusion from the apparently
            // sudden connection loss.
            mBootstrap?.Stop("The server was manually shut down.");
         }
         catch (Exception e)
         {
            mLog.Error(e, "Exception thrown while stopping the server");
         }

         return Task.CompletedTask;
      }

      private Configuration GetConfiguration()
      {
         // Prioritize the configuration saved on disk. Attempt to read it.
         Configuration configuration = Configuration.Load();

         // If there was no configuration on disk or parsing failed for any reason.
         if (configuration == null)
         {
            mLog.Warn("No configuration file was found or parsing failed. A default one will be used and saved.");
            configuration = new Configuration();
            Configuration.Store(configuration);
         }

         mLog.Info($"Hosting on port {configuration.Port}");
         mLog.Info($"Using name \"{configuration.Name}\"");
         if (!string.IsNullOrEmpty(configuration.MessageOfTheDay))
            mLog.Info($"Using message of the day \"{configuration.MessageOfTheDay}\"");
         mLog.Info($"The server is {(configuration.IsPrivate ? "private" : "public")}");
         if (configuration.IsPrivate)
            mLog.Info($"Using join password \"{configuration.JoinPassword}\"");
         mLog.Info($"Using admin password \"{configuration.AdminPassword}\"");
         mLog.Info($"Voice chat is {(configuration.IsVoiceChatEnabled ? "enabled" : "disabled")}");
         if (configuration.BanList.Count > 0)
            mLog.Info($"Will reject connections from: {string.Join(", ", configuration.BanList)}");

         return configuration;
      }

      private List<Stage> VerifyStages()
      {
         // Get a list of stage models.
         List<Stage> stages = Stage.List;
         // Prepare a list that will contain only successfully-loaded models.
         List<Stage> loaded = new List<Stage>();

         foreach (Stage stage in stages)
         {
            if (stage.Load(mContentWrapper) == true)
            {
               // If loading the stage (specifically, generating the collision mask from the foreground
               // image) succeeded.

               loaded.Add(stage);
            }
            else
            {
               // If loading failed, log a warning so the user/admin knows which stage to investigate.

               mLog.Warn($"Failed to load stage {stage.Name}");
            }
         }

         // If not all stages could be loaded.
         if (stages.Count != loaded.Count)
         {
            return new List<Stage>(); // Return an empty list to indicate a failure.
         }

         mLog.Info($"Loaded all {loaded.Count} stage(s)");
         return stages;
      }
   }
}
