﻿using Mjolnir.Common;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Mjolnir.Server.Disk
{
   [Serializable]
   public class Configuration
   {
      private static readonly ILogger sLog = LogManager.GetCurrentClassLogger();
      private static readonly string CONFIG_FILENAME = "server_config.xml";

      /// <summary>
      /// Public name of this game server. This settings doesn't affect behavior
      /// in any way. It is only handed to the registry, ultimately being shown to
      /// players when searching for servers in multiplayer.
      /// </summary>
      public string Name { get; set; } = "Unnamed";
      
      /// <summary>
      /// A message to be displayed to a player upon joining. This isn't necessarily
      /// a description although it could be. If omitted from the configuration or
      /// just an empty string, no message will be shown to the player.
      /// Note: this message is limited to 256 characters.
      /// </summary>
      public string MessageOfTheDay { get; set; } = string.Empty;
      
      /// <summary>
      /// Password used to join the server as a player iff the server is also
      /// configured to be private. If the server is not private, this value will
      /// have no effect whatsoever.
      /// Note: this password is case INsensitive.
      /// </summary>
      public string JoinPassword { get; set; } = "marcus aurelius";
      
      /// <summary>
      /// Password used to grant admin privileges. Users requesting these
      /// privileges must configure their clients to send this password when
      /// joining this server.
      /// Note: this password is case INsensitive.
      /// </summary>
      public string AdminPassword { get; set; } = "correct horse battery staple";
      
      /// <summary>
      /// Port on which to host this server.
      /// </summary>
      public int Port { get; set; } = Constants.DefaultServerPort;
      
      /// <summary>
      /// Flag to enable/disable password protection on the server. If true, players
      /// wanting to join the server will be prompted to enter the JoinPassword.
      /// </summary>
      public bool IsPrivate { get; set; } = false;
      
      /// <summary>
      /// Flag to enable/disable the voice chat feature. Voice chat uses a relatively
      /// high amount of network bandwidth so servers hosted at home may benefit
      /// from disabling it.
      /// </summary>
      public bool IsVoiceChatEnabled { get; set; } = true;
      
      /// <summary>
      /// Flag to enable/disable the use of bots when an actual player is not available
      /// to fill a slot. Bots are added and removed from the server as slots become
      /// available or taken, respectively.
      /// </summary>
      public bool AreBotsEnabled { get; set; } = true;
      
      /// <summary>
      /// Hostname or IP address of the, or a, registry. This server will register
      /// itself with this registry at startup thereby making itself visible to
      /// clients looking for servers via the registry.
      /// </summary>
      public string RegistryHostname { get; set; } = "127.0.0.1";

      /// <summary>
      /// Port of the, or a, registry. It's recommended that the default value be used
      /// at all times.
      /// </summary>
      public int RegistryPort { get; set; } = 51510;
      
      /// <summary>
      /// A list of addresses that are prohibited from joining this server.
      /// </summary>
      public List<string> BanList { get; } = new List<string>();

      public static Configuration Load()
      {
         if (File.Exists(CONFIG_FILENAME) == false)
         {
            return null;
         }

         try
         {
            XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
            StreamReader reader = File.OpenText(CONFIG_FILENAME);
            Configuration serializable = (Configuration)serializer.Deserialize(reader);
            reader.Close();
            return serializable;
         }
         catch (Exception e)
         {
            sLog.Error(e, $"Exception thrown while loading server configuration file " +
                          $"{Path.Combine(Directory.GetCurrentDirectory(), CONFIG_FILENAME)}");
            return null;
         }
      }

      public static void Store(Configuration aConfiguration)
      {
         if (aConfiguration == null)
         {
            return;
         }

         try
         {
            XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
            TextWriter writer = new StreamWriter(CONFIG_FILENAME);
            serializer.Serialize(writer, aConfiguration);
            writer.Close();
         }
         catch (Exception e)
         {
            sLog.Error(e, $"Exception thronw while storing a server configurtion file to " +
                          $"{Path.Combine(Directory.GetCurrentDirectory(), CONFIG_FILENAME)}");
         }
      }
   }
}
