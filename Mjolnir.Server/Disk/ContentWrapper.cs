﻿using Mjolnir.Common.Disk;
using Mjolnir.Common.Models;
using NLog;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace Mjolnir.Server.Disk
{
   public class ContentWrapper : IContentWrapper
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();

      public bool[,] LoadStage(Stage aStage)
      {
         if (aStage == null)
         {
            return null;
         }

         string foregroundPath = StageForegroundPath(aStage);
         if (!File.Exists(foregroundPath))
         {
            mLog.Error($"Could not find foreground image for {aStage.Name} at {foregroundPath}");
            return null;
         }

         try
         {
            Bitmap bitmap = new Bitmap(foregroundPath);
            Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            BitmapData bitmapData = bitmap.LockBits(rect, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            int length = bitmapData.Stride * bitmapData.Height;
            byte[] argbData = new byte[length];
            Marshal.Copy(bitmapData.Scan0, argbData, 0, length);
            bitmap.UnlockBits(bitmapData);
            bool[,] mask = new bool[bitmap.Width, bitmap.Height];
            for (int y = 0; y < bitmapData.Height; y++)
            {
               for (int x = 0; x < bitmapData.Width; x++)
               {
                  mask[x, y] = argbData[(y * bitmapData.Stride) + (x * 4) + 3] != Color.Transparent.A;
               }
            }

            return mask;
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while creating a mask from image {foregroundPath}");
            return null;
         }
      }

      public string StageForegroundPath(Stage aStage)
      {
         // The Stages directory contains one directory per stage with each directory being
         // named after the stage (the Name property) and holding background and foreground images.
         // The server needs the foreground image to build the mask (a 2D array representing
         // solid pixels, AKA the terrain). The Foreground property excludes the file extension
         // but it will always be a PNG.
         return Path.Combine(StagesDirectory, aStage?.Name ?? string.Empty, $"{aStage?.Foreground}.png");
      }

      public string StageBackgroundPath(Stage aStage)
      {
         return Path.Combine(StagesDirectory, aStage?.Name ?? string.Empty, $"{aStage?.Background}.png");
      }

      // We need to load the foreground image which is assumed to be located in a directory
      // named "Stages" under the "Content" directory that is adjacent to this executable.
      private static string StagesDirectory => Path.Combine(Directory.GetCurrentDirectory(), "Content", "Stages");
   }
}
