These were the original spritesheets and references for mobiles.
They were created in what could be called a pixel art style with coloring that
differed too much from the other content for them to remain.
They're archived here just in case the art direction changes (back) to this,
and just because these took time and effort and it's hard to just throw
something away, you know?