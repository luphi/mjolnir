﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Mjolnir.Registry
{
    class Program
    {
        static void Main(string[] aArgs)
        {
            Host.CreateDefaultBuilder(aArgs)
                // Instruct the host to act as a Windows service.
                // Note: If not executing in the correct environment (as a service),
                // this method will be a no-op and do nothing.
                .UseWindowsService()
                // Instruct the host to act as a Systemd daemon.
                // Note: Like the above, this will be affected by the runtime environment.
                .UseSystemd()
                // Modify the logging behavior.
                .ConfigureLogging(loggingBuilder =>
                {
                    // Add filters to messages from the Microsoft and System packages
                    // such that only warnings and errors are shown.
                    loggingBuilder.AddFilter("Microsoft", LogLevel.Warning)
                                  .AddFilter("System", LogLevel.Warning);
                })
                .ConfigureServices(services =>
                {
                    // Add ServerService as a service to be hosted.
                    services.AddHostedService<RegistryService>();
                })
                // With configuration(s) out of the way, link everything into a single
                // object (applies dependency injection, etc.).
                .Build()
                // Begin running the host. This will continue until instructed to stop,
                // depending on the environment. For a console (e.g. Windows Command
                // Prompt), this means waiting unti the user presses Ctrl+C.
                .Run();
        }
    }
}
