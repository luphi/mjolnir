﻿using Microsoft.Extensions.Hosting;
using Mjolnir.Common;
using Mjolnir.Common.Messages;
using Mjolnir.Common.Network;
using Mjolnir.Registry.Disk;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Mjolnir.Registry
{
   public class RegistryService : BackgroundService
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly ConcurrentDictionary<IPEndPoint, ServerRegistrationMessage> mServerMap =
                   new ConcurrentDictionary<IPEndPoint, ServerRegistrationMessage>();
      private TcpServerSocket mSocket = null;

      protected override Task ExecuteAsync(CancellationToken aCancellationToken)
      {
         try
         {
            Configuration configuration = GetConfiguration();

            mSocket = new TcpServerSocket(IPAddress.Any.ToString(), configuration.Port);
            mSocket.DisconnectEvent += OnDisconnect;
            mSocket.ReceiveEvent += OnReceive;
            mSocket.Listen(out int _, configuration.MaxBacklog);
         }
         catch (Exception e)
         {
            mLog.Error(e, "Exception thrown while starting the registry.");
         }

         return Task.CompletedTask;
      }

      public override Task StopAsync(CancellationToken aCancellationToken)
      {
         try
         {
            mSocket?.Close();
         }
         catch (Exception e)
         {
            mLog.Error(e, "Exception thrown while stopping the registry.");
         }

         return Task.CompletedTask;
      }

      private Configuration GetConfiguration()
      {
         Configuration configuration = Configuration.Load();

         if (configuration == null)
         {
            mLog.Warn("No configuration file was found or parsing failed. A default one will be used and saved.");
            configuration = new Configuration();
            Configuration.Store(configuration);
         }

         mLog.Info($"Hosting on port {configuration.Port}.");
         mLog.Info($"Allowing a maximum listen backlog of {configuration.MaxBacklog} clients.");

         return configuration;
      }

      private void OnDisconnect(object aSender, IPEndPoint aEndPoint)
      {
         if (mServerMap.TryRemove(aEndPoint, out ServerRegistrationMessage registration) == true)
         {
            mLog.Info($"Unregistered {registration} due to a lost connection.");
         }
      }

      private void OnReceive(object aSender, Reception aReception)
      {
         try
         {
            Message message = Message.Deserialize(aReception.Data);
            switch (message.Op)
            {
               case Message.Opcode.SERVER_REGISTER:
                  ServerRegistrationMessage serverRegistrationMessage = message as ServerRegistrationMessage;
                  serverRegistrationMessage.Address = aReception.EndPoint.Address.ToString();
                  mServerMap[aReception.EndPoint] = serverRegistrationMessage;
                  mLog.Info($"Registered {serverRegistrationMessage}");
                  break;
               case Message.Opcode.SERVER_UNREGISTER:
                  if (mServerMap.TryRemove(aReception.EndPoint, out ServerRegistrationMessage registration))
                     mLog.Info($"Unregistered {registration}");
                  break;
               case Message.Opcode.SERVER_LIST_REQUEST:
                  ServerListMessage serverListMessage = new ServerListMessage();
                  serverListMessage.Servers.AddRange(mServerMap.Values.Where(s => s.State == StateEnum.LOBBY));
                  byte[] data = Message.Serialize(serverListMessage);
                  mSocket?.SendTo(data, aReception.EndPoint);
                  break;
            }
         }
         catch (Exception e)
         {
            mLog.Warn(e, $"{aReception.EndPoint} sent something unknown/dangerous.");
         }
      }
   }
}
