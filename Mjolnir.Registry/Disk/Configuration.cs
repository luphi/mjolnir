﻿using Mjolnir.Common;
using System;
using System.IO;
using System.Xml.Serialization;

namespace Mjolnir.Registry.Disk
{
    [Serializable]
    public class Configuration
    {
        private static readonly string CONFIG_FILENAME = "registry_config.xml";

        /// <summary>
        /// Port on which to host this server. It is recommended that the default
        /// value be used at all times.
        /// </summary>
        public int Port { get; set; } = Constants.DefaultRegistryPort;
        /// <summary>
        /// Maximum number of pending connections. Connections *should* be established
        /// almost instantaneously but this value is configurable in the off chance
        /// the registry becomes very busy.
        /// </summary>
        public int MaxBacklog { get; set; } = 4;

        public static Configuration Load()
        {
            if (!File.Exists(CONFIG_FILENAME))
                return null;

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
                StreamReader reader = File.OpenText(CONFIG_FILENAME);
                Configuration serializable = (Configuration)serializer.Deserialize(reader);
                reader.Close();
                return serializable;
            }
            catch
            {
                return null;
            }
        }

        public static void Store(Configuration aConfiguration)
        {
            if (aConfiguration == null)
                return;

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
                TextWriter writer = new StreamWriter(CONFIG_FILENAME);
                serializer.Serialize(writer, aConfiguration);
                writer.Close();
            }
            catch { }
        }
    }
}
