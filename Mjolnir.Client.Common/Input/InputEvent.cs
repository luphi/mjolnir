﻿using Microsoft.Xna.Framework.Input;
using Mjolnir.Common.Models;

namespace Mjolnir.Client.Common.Input
{
    /// <summary>
    /// A single, platform-independent user input event with associated information depending on the
    /// specific type of event.
    /// </summary>
    public class InputEvent
    {
        public enum Opcode
        {
            // A "go back one step" event. On a desktop, this would be a press of the escape key.
            // On Android, it would be the system's back event. Etc.
            // No other properties of this object are set with this opcode.
            BACK,
            // A single character (letter) specifically entered for the purpose of writing a message.
            // The Character property is set along with this opcode.
            CHAR,
            // Mouse hover event specifying the current location only. This does not imply a click,
            // scroll, or any other mouse/cursor actions.
            // The Position property is set along with this opcode.
            HOVER,
            // Press of a single keyboard key.
            // The Key property is set along with this opcode.
            KEY_DOWN,
            // Relase of a single keyboard key.
            // The Key proeprty is set along with this opcode.
            KEY_UP,
            // Press of a mouse's left button.
            // The Position property is set along with this opcode.
            MOUSE_DOWN,
            // Release of a mouse's left button.
            // The Position property is set along with this opcode.
            MOUSE_UP,
            // Rotation of a mouse's scroll wheel one more detent.
            // The value, that is to say the amount of scrolling, is derived directly from Monogame.
            // The Delta property is set along with this opcode.
            SCROLL,
            // A touchscreen-based swipe action caused by a single finger. This could also be called
            // a drag, flick, or even a pinch (if two fingers are used).
            // The Delta property is set along with this opcode.
            SWIPE
        }

        public Opcode Op { get; }

        public Keys? Key { get; set; } = null;

        public char? Character { get; set; } = null;

        public Vector Position { get; set; } = null;

        public Vector Delta { get; set; } = null;

        public InputEvent(Opcode aOp)
        {
            Op = aOp;
        }

        public override string ToString()
        {
            switch (Op)
            {
                case Opcode.BACK:
                default:
                    return $"{{ {nameof(Op)} = {Op} }}";
                case Opcode.CHAR:
                    return $"{{ {nameof(Op)} = {Op}, {nameof(Character)} = \"{Character}\" }}";
                case Opcode.HOVER:
                case Opcode.MOUSE_DOWN:
                case Opcode.MOUSE_UP:
                    return $"{{ {nameof(Op)} = {Op}, {nameof(Position)} = {Position} }}";
                case Opcode.KEY_DOWN:
                case Opcode.KEY_UP:
                    return $"{{ {nameof(Op)} = {Op}, {nameof(Key)} = {Key} }}";
                case Opcode.SCROLL:
                case Opcode.SWIPE:
                    return $"{{ {nameof(Op)} = {Op}, {nameof(Delta)} = {Delta} }}";
            }
        }
    }
}
