﻿namespace Mjolnir.Client.Common.Input
{
    /// <summary>
    /// Defines the behavior expected of components that respond to and do something
    /// with user input.
    /// </summary>
    public interface IControl
    {

        void Respond(InputEvent aInput);
    }
}
