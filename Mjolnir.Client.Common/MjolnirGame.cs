﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.Screens;
using Mjolnir.Client.Common.UI;
using Mjolnir.Client.Common.Views;
using Mjolnir.Common.Disk;
using Mjolnir.Common.Network;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Threading;
using Mjolnir.Client.Common.Screens.Dev;

namespace Mjolnir.Client.Common
{
   public class MjolnirGame : Game
   {
      protected readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      protected readonly IServiceCollection mServiceCollection = new ServiceCollection();
      protected readonly ConcurrentQueue<InputEvent> mInputEventsQueue = new ConcurrentQueue<InputEvent>();
      private readonly Configuration mConfiguration;
      private readonly GraphicsDeviceManager mGraphicsDeviceManager;
      private readonly ConcurrentQueue<Action> mUIThreadActionQueue = new ConcurrentQueue<Action>();

      private Window mWindow = null;
      private Camera mCamera = null;
      private ResolutionIndependentViewport mViewport;

      private int? mUIThreadID = null;

      /// <summary>
      /// Flag set at startup indicating, if true, that this game is running on a desktop.
      /// </summary>
      public static bool IsDesktop { get; private set; }

      public MjolnirGame(bool aIsDesktop, Configuration aConfiguration)
      {
         IsDesktop = aIsDesktop;
         mConfiguration = aConfiguration;

         mServiceCollection.AddSingleton(this);
         mServiceCollection.AddSingleton<Game>(x => x.GetRequiredService<MjolnirGame>());
         mServiceCollection.AddSingleton(aConfiguration);

         // Finish the typical MonoGame construction.
         mGraphicsDeviceManager = new GraphicsDeviceManager(this);
         Content.RootDirectory = "Content";
      }

      /// <summary>
      /// Invoke the given void delegate on the UI thread at the earliest opportunity.
      /// If invoked on the UI thread, it will be invoked immediately before returning. If not,
      /// it will be queued and invoked on the next frame.
      /// </summary>
      /// <param name="aAction">A runnable action to be performed.</param>
      public void InvokeOnUIThread(Action aAction)
      {
         if (Thread.CurrentThread.ManagedThreadId == mUIThreadID)
         {
            // If this method was called from the UI thread.

            try
            {
               // Immediately invoke the action before this method returns.
               aAction.Invoke();
            }
            catch (Exception e)
            {
               mLog.Error(e, "Exception thrown while (immediately) executing a delegate on the UI thread");
            }
         }
         else
         {
            // If this method was NOT called from the UI thread.

            // Enqueue the action resulting in it being called on the next Draw().
            mUIThreadActionQueue.Enqueue(aAction);
         }
      }

      /// <summary>
      /// Called by Monogame once in order to perform whatever non-graphical initialization
      /// we see fit.
      /// </summary>
      protected override void Initialize()
      {
         if (mConfiguration.IsWindowed == false)
         {
            // If running in fullscreen mode (i.e. if running on a mobile device or if running
            // on a desktop with the windowed mode disabled), tell MonoGame.

            mGraphicsDeviceManager.IsFullScreen = true;
         }
         else
         {
            // If running in windowed mode, request/set the viewport's resolution.

            mGraphicsDeviceManager.PreferredBackBufferWidth = mConfiguration.ViewportWidth;
            mGraphicsDeviceManager.PreferredBackBufferHeight = mConfiguration.ViewportHeight;
         }
         mGraphicsDeviceManager.ApplyChanges();

         // With the viewport's resolution known, create the adapter that will maintain the
         // virtual/internal resolution and assist, in conjunction with the Camera, in scaling
         // all graphics.
         mViewport = new ResolutionIndependentViewport(Window,
                                                       GraphicsDevice,
                                                       mConfiguration.PreferredVirtualWidth,
                                                       mConfiguration.PreferredVirtualHeight);

         // Use the viewport's (virtual) dimensions to determine an appropriate default text height.
         // The idea here is to make text and, by extension, text buttons a reasoanble height to be
         // legible but no larger than necessary while also, for buttons, being easy enough to select.

         if (IsDesktop == true)
         {
            // If running on a desktop where lower pixel densities and the use of mice are assumed.

            Text.DefaultHeight = mViewport.VirtualHeight / 21.0; // Relatively smaller text.
         }
         else
         {
            // If running on a mobile device where higher pixel densities and finger-based input are assumed.

            Text.DefaultHeight = mViewport.VirtualHeight / 14.0; // Relatively larger text.
         }

         // Register the viewport and some other fundamental services.
         mServiceCollection.AddSingleton(mViewport);
         mServiceCollection.AddSingleton<GameClient>();
         mServiceCollection.AddSingleton(new RegistryClient(mConfiguration.RegistryHostname,
                                                            mConfiguration.RegistryPort));

         // Let MonoGame do its pre-graphics initialization.
         base.Initialize();
      }

      /// <summary>
      /// Called by Monogame once, after the graphical subsystem has been readied, in order to load
      /// content that will be displayed.
      /// Here, this means registering the graphical classes as services for the service provider
      /// and buildling said service provider.
      /// </summary>
      protected override void LoadContent()
      {
         // MonoGame calls this method from the UI thread. Given that, now is a good time to remember
         // the UI thread's ID.
         mUIThreadID = Thread.CurrentThread.ManagedThreadId;

         // Register MonoGame's graphical and related objects.
         mServiceCollection.AddSingleton(GraphicsDevice);
         mServiceCollection.AddSingleton(Content);
         mServiceCollection.AddSingleton<SpriteBatch>();
         mServiceCollection.AddSingleton<ContentWrapper>();
         mServiceCollection.AddSingleton<IContentWrapper>(x => x.GetRequiredService<ContentWrapper>());

         // Register some graphical objects specific to this game.
         mServiceCollection.AddSingleton<Window>();
         mServiceCollection.AddTransient<Transition>();
         mServiceCollection.AddSingleton<Camera>();

         // Register UI classes.
         mServiceCollection.AddTransient<Image>();
         mServiceCollection.AddTransient<Quad>();
         mServiceCollection.AddTransient<Text>();
         mServiceCollection.AddTransient<TextButton>();
         mServiceCollection.AddTransient<ImageButton>();
         mServiceCollection.AddTransient<ButtonBase>();
         mServiceCollection.AddTransient<Scrollbar>();
         mServiceCollection.AddTransient<Table>();
         mServiceCollection.AddTransient<Border>();
         mServiceCollection.AddTransient<ProgressBar>();
         mServiceCollection.AddSingleton<ChatLog>(); // Singleton to retain previous messages between screens.
         mServiceCollection.AddTransient<AlertModalWindow>();
         mServiceCollection.AddTransient<ConfirmationModalWindow>();
         mServiceCollection.AddTransient<InputModalWindow>();
         mServiceCollection.AddTransient<Switch>();
         mServiceCollection.AddTransient<Carousel>();
         mServiceCollection.AddTransient<MobileTile>();
         mServiceCollection.AddTransient<SplitTable>();
         mServiceCollection.AddTransient<WindVane>();
         mServiceCollection.AddTransient<ItemTile>();
         mServiceCollection.AddTransient<ItemTray>();
         mServiceCollection.AddTransient<PowerBar>();

         // Register view model classes.
         mServiceCollection.AddTransient<StageView>();
         mServiceCollection.AddTransient<PlayerView>();
         mServiceCollection.AddTransient<ProjectileView>();
         mServiceCollection.AddTransient<TurnListView>();
         mServiceCollection.AddTransient<SatelliteView>();

         // Register Screen classes.
         mServiceCollection.AddTransient<DirectConnectScreen>();
         mServiceCollection.AddTransient<ExitScreen>();
         mServiceCollection.AddTransient<IngameScreen>();
         mServiceCollection.AddTransient<LobbyScreen>();
         mServiceCollection.AddTransient<MainMenuScreen>();
         mServiceCollection.AddTransient<MultiplayerScreen>();
         mServiceCollection.AddTransient<PostgameScreen>();
         mServiceCollection.AddTransient<PregameScreen>();
         mServiceCollection.AddTransient<SingleplayerScreen>();
         if (mConfiguration.IsDeveloper == true)
         {
            mServiceCollection.AddTransient<DevMenuScreen>();
            mServiceCollection.AddTransient<DevStageScreen>();
            mServiceCollection.AddTransient<DevPlayerScreen>();
            mServiceCollection.AddTransient<DevWorldScreen>();
            mServiceCollection.AddTransient<DevWalkScreen>();
            mServiceCollection.AddTransient<DevBotScreen>();
         }

         // At this point, all necessary services are registered so the service provider can be built.
         IServiceProvider serviceProvider = mServiceCollection.BuildServiceProvider();

         // Get the window (screen manager) and effectively hand control over to it.
         mWindow = serviceProvider.GetService<Window>();
         mWindow.LoadScreenImmediate(serviceProvider.GetService<MainMenuScreen>());

         // Get the camera so that is may be used on input events with viewport coordinates.
         mCamera = serviceProvider.GetService<Camera>();
      }

      /// <summary>
      /// Called by Monogame periodically and very frequently. This method is responsible for responding
      /// to user input. That said, the method of this base class is largely limited to relaying input
      /// events to the active screen. The events themselves are pushed by child classes from
      /// platform-specific actions (e.g. DesktopGame will respond to keyboard input).
      /// </summary>
      /// <param name="aGameTime"></param>
      protected override void Update(GameTime aGameTime)
      {
         // For each input event pushed to the queue.
         while (mInputEventsQueue.TryDequeue(out InputEvent input) == true)
         {
            // Some event types include viewport-related properties, like the position of a user's
            // mouse click. Because these values come from the system, they are pushed with numbers
            // in the viewport's measurements. Before handing them to the active screen, they must
            // be changed to use the virtual equivalent.
            switch (input.Op)
            {
               // These events pass a position value.
               case InputEvent.Opcode.HOVER:
               case InputEvent.Opcode.MOUSE_DOWN:
               case InputEvent.Opcode.MOUSE_UP:
                  input.Position = mCamera.ViewportToVirtual(input.Position);
                  break;
               // These events pass a delta (i.e. a span of pixels) value.
               case InputEvent.Opcode.SCROLL:
               case InputEvent.Opcode.SWIPE:
                  input.Delta = mCamera.ViewportToVirtual(input.Delta);
                  break;
            }

            // Hand the input event to the window which hands the event to the active screen.
            mWindow?.Respond(input);
         }

         base.Update(aGameTime);
      }

      /// <summary>
      /// Called by Monogame periodically and very frequently. This method is responsible for
      /// clearing the screen and drawing the current visual state of the game.
      /// This method is executed on the UI thread.
      /// </summary>
      /// <param name="aGameTime"></param>
      protected override void Draw(GameTime aGameTime)
      {
         // Paint it black.
         GraphicsDevice.Clear(Color.Black);

         // For all enqueued items in the UI thread action queue.
         while (mUIThreadActionQueue.IsEmpty == false)
         {
            // Dequeue a single action.
            if (mUIThreadActionQueue.TryDequeue(out Action action) == true)
            {
               try
               {
                  // Run it.
                  action.Invoke();
               }
               catch (Exception e)
               {
                  mLog.Error(e, "Exception thrown while executing a delegate on the UI thread");
               }
            }
         }

         // Redraw the active screen.
         mWindow?.Draw(aGameTime);

         base.Draw(aGameTime);
      }

      /// <summary>
      /// Called by Monogame once, only when the game is exiting.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      protected override void OnExiting(object aSender, EventArgs aArgs)
      {
         mWindow?.Dispose();
         mViewport?.Dispose();

         base.OnExiting(aSender, aArgs);
      }
   }
}
