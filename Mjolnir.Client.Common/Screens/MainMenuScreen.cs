﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.UI;
using Mjolnir.Common.Collections;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mjolnir.Client.Common.Screens
{
   public class MainMenuScreen : Screen
   {
      public class Options
      {
         public List<Type> Entries;
      }

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly Window mWindow;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly Camera mCamera;
      private readonly Options mOptions;
      private readonly Configuration mConfiguration;
      private readonly IServiceProvider mServiceProvider;
      private readonly ConcurrentList<TextButton> mButtons = new ConcurrentList<TextButton>();
      private readonly SpriteBatch mSpriteBatch;

      public override string Name => "Main Menu";

      public MainMenuScreen(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mWindow = mServiceProvider.GetService<Window>();
         mViewport = mServiceProvider.GetService<ResolutionIndependentViewport>();
         mCamera = mServiceProvider.GetService<Camera>();
         mOptions = mServiceProvider.GetService<Options>();
         mConfiguration = mServiceProvider.GetService<Configuration>();
         mSpriteBatch = mServiceProvider.GetService<SpriteBatch>();
      }

      public override void LoadContent()
      {
         double padding = mViewport.VirtualHeight * 0.01;
         Rectangle paddedBounds = new Rectangle((int)padding,
                                                (int)padding,
                                                (int)(mViewport.VirtualWidth - (2.0 * padding)),
                                                (int)(mViewport.VirtualHeight - (2.0 * padding)));

         // This case should be impossible given the main menu entries are hardcoded into
         // the Game's constructor parameters, but just in case...
         if (mOptions.Entries.Count == 0)
         {
            // Display an exit button and nothing more. Hopefully the user can take the hint.
            Screen screen = mServiceProvider.GetService<ExitScreen>();
            TextButton button = mServiceProvider.GetService<TextButton>();
            button.Text = screen.Name;
            button.X = paddedBounds.X;
            button.Y = paddedBounds.Y;
            button.Height = paddedBounds.Height;
            // Make sure the button's full text is visible by fitting it to the (virtual) viewport
            // width if it exceeds it.
            if (button.Width > paddedBounds.Width)
            {
               button.Width = paddedBounds.Width;
            }
            button.ClickEvent += delegate (object aSender, EventArgs aArgs)
            {
               mWindow.LoadScreen(screen);
            };
            mButtons.Add(button);

            return;
         }

         // Create and size the buttons such that all of them are fitted to the screen height-wise.
         int buttonHeight = paddedBounds.Height / mOptions.Entries.Count;
         foreach (Type screenType in mOptions.Entries)
         {
            Screen screen = (Screen)mServiceProvider.GetService(screenType);
            TextButton button = mServiceProvider.GetService<TextButton>();
            button.Text = screen?.Name?.ToUpper() ?? "NULL";
            button.X = paddedBounds.X;
            button.Y = paddedBounds.Y + buttonHeight * mButtons.Count;
            button.Height = buttonHeight;
            button.ClickEvent += delegate (object aSender, EventArgs aArgs)
            {
               mWindow.LoadScreen(screen);
            };
            mButtons.Add(button);
         }

         double maxButtonWidth;
         maxButtonWidth = mButtons.Max(b => b.Width);
         if (maxButtonWidth > paddedBounds.Width)
         {
            // Resize using the viewport's width to determine button sizes.
            int index = 0;
            foreach (TextButton button in mButtons)
            {
               button.Width = (int)(paddedBounds.Width * (button.Width / maxButtonWidth));
               button.Y = button.Height * index++;
            }
         }

         // If the configuration is being used by a developer, or dishonest user.
         if (mConfiguration.IsDeveloper == true)
         {
            // Display a button in the bottom-right corner to access the developer tools.
            Screen screen = mServiceProvider.GetService<DevMenuScreen>();
            TextButton devButton = mServiceProvider.GetService<TextButton>();
            devButton.Text = screen?.Name?.ToUpper() ?? "NULL";
            devButton.Height = mButtons[0].Height;

            // Position the button in the bottom right using its bounds to determine the exact X, Y.
            Rectangle devButtonBounds = devButton.Bounds;
            devButton.X = paddedBounds.Width - devButtonBounds.Width;
            devButton.Y = paddedBounds.Height - devButtonBounds.Height;
            devButton.ClickEvent += delegate (object aSender, EventArgs aArgs)
            {
               mWindow.LoadScreen(screen);
            };
            mButtons.Add(devButton);
         }
      }

      public override void Draw(GameTime aGameTime)
      {
         mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix);

         foreach (TextButton button in mButtons)
         {
            button.Draw();
         }

         mSpriteBatch.End();
      }

      public override void Respond(InputEvent aInput)
      {
         // If a back event came in on a desktop (Windows, Linux, etc.) platform.
         if ((aInput.Op == InputEvent.Opcode.BACK) && (MjolnirGame.IsDesktop == true))
         {
            // Transition to the sort of contradictory exit screen that initializes by
            // exiting the game and, therefore, program. This is not done on mobile
            // platforms as those programs are more accuractely described as asleep
            // rather than closed even when Monogame is instructed to exit.
            mWindow.LoadScreen(mServiceProvider.GetService<ExitScreen>());
         }

         foreach (TextButton button in mButtons)
         {
            button.Respond(aInput);
         }
      }
   }
}
