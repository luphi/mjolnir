﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.UI;
using NLog;
using System;

namespace Mjolnir.Client.Common.Screens
{
   internal class Transition
   {
      private enum TransitionState
      {
         OUT,
         IN
      }

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly MjolnirGame mGame;
      private readonly SpriteBatch mSpriteBatch;
      private readonly Camera mCamera;
      private readonly Quad mQuad;
      private TransitionState mState;
      private double mHalfDurationInSeconds;
      private double mCurrentSeconds;

      public event EventHandler StateChangeEvent;
      public event EventHandler DoneEvent;

      public double Duration
      {
         get => mDurationInSeconds;
         set
         {
            if (value >= 0.0)
            {
               mDurationInSeconds = value;
               mHalfDurationInSeconds = value / 2.0;
            }
         }
      }
      private double mDurationInSeconds = 0.5;

      public Transition(IServiceProvider aServiceProvider)
      {
         mGame = aServiceProvider.GetService<MjolnirGame>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mCamera = aServiceProvider.GetService<Camera>();
         mQuad = aServiceProvider.GetService<Quad>();

         ResolutionIndependentViewport viewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mQuad.X = 0.0;
         mQuad.Y = 0.0;
         mQuad.Width = viewport.VirtualWidth;
         mQuad.Height = viewport.VirtualHeight;

         mHalfDurationInSeconds = mDurationInSeconds / 2.0;
         mState = TransitionState.OUT;
      }

      public void Update(double aElapsedSeconds)
      {
         switch (mState)
         {
            case TransitionState.OUT:
               mCurrentSeconds += aElapsedSeconds;
               // If the time has surpassed the half duration OR is effectively equal to
               // the half duration, allowing for floating point error.
               if ((mCurrentSeconds > mHalfDurationInSeconds) ||
                   (Math.Abs(mCurrentSeconds - mHalfDurationInSeconds) <= 0.0001))
               {
                  mState = TransitionState.IN;
                  mCurrentSeconds = mHalfDurationInSeconds;

                  // Invoke the state change event from the UI thread. We want this invoke from the UI
                  // thread as the Window is expected to change screens and, therefore, perform a lot of
                  // graphical operations that are either best done on the UI thread, or must be done on it.
                  mGame.InvokeOnUIThread(() =>
                  {
                     try
                     {
                        StateChangeEvent?.Invoke(this, EventArgs.Empty);
                     }
                     catch (Exception e)
                     {
                        mLog.Error(e, "Exception thrown while propagating a transition state change event");
                     }
                  });
               }
               break;
            case TransitionState.IN:
               mCurrentSeconds -= aElapsedSeconds;
               // If the time, now counting down, has passed zero or is effectively equal
               // to zero, allowing for floating point error.
               if (mCurrentSeconds <= 0.0001)
               {
                  // Invoke the done event from the UI thread for the same reasons as the above case.
                  mGame.InvokeOnUIThread(() =>
                  {
                     try
                     {
                        DoneEvent?.Invoke(this, EventArgs.Empty);
                     }
                     catch (Exception e)
                     {
                        mLog.Error(e, "Exception thrown while propagating a transition done event");
                     }
                  });
               }
               break;
         }
      }

      public void Draw()
      {
         mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix);
         // Set the quad's color with to black with an oppacity that essentially hides the quad
         // at the very start or end of the transition but is completely opaque at the half duration.
         mQuad.Color = Color.Black * (float)(1.0 -
                       (Math.Abs(mHalfDurationInSeconds - mCurrentSeconds) / mHalfDurationInSeconds));
         // Draw the quad, overlapping the whole viewport.
         mQuad.Draw();
         mSpriteBatch.End();
      }
   }
}
