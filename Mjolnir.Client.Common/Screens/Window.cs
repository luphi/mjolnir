﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using NLog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Mjolnir.Client.Common.Screens
{
   public class Window : IControl, IDisposable
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly MjolnirGame mGame;
      private readonly Camera mCamera;
      private Transition mActiveTransition = null;

      public Screen ActiveScreen { get; private set; } = null;

      public bool IsTransitioning => mActiveTransition != null;

      public Window(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mGame = aServiceProvider.GetService<MjolnirGame>();
         mCamera = aServiceProvider.GetService<Camera>();
      }

      public void Dispose()
      {
         ActiveScreen?.Dispose();
      }

      public void LoadScreen(Screen aScreen)
      {
         if (IsTransitioning == false)
         {
            mActiveTransition = mServiceProvider.GetService<Transition>();
            CancellationTokenSource tokenSource = new CancellationTokenSource();
            CancellationToken token = tokenSource.Token;

            mActiveTransition.StateChangeEvent += delegate (object aSender, EventArgs aArgs)
            {
               LoadScreenImmediate(aScreen);
            };

            mActiveTransition.DoneEvent += delegate (object aSender, EventArgs aArgs)
            {
               // Cancel any ongoing transition.
               tokenSource.Cancel();
               mActiveTransition = null;
            };

            Task.Run(async () =>
            {
               int delayInMilliseconds = 50;
               while (token.IsCancellationRequested == false)
               {
                  await Task.Delay(delayInMilliseconds);

                  mActiveTransition?.Update(delayInMilliseconds / 1000.0);
               }
            });
         }
      }

      public void LoadScreenImmediate(Screen aScreen)
      {
         if (aScreen != null)
         {
            mCamera.Reset();

            // Close the old screen.
            ActiveScreen?.Dispose();

            // Open the new screen.
            aScreen.Initialize(); // Might run on the UI thread. Depends on called LoadScreenImmediate().
            mGame.InvokeOnUIThread(() =>
            {
               aScreen.LoadContent(); // Will definitely run on the UI thread.
            });

            // Set the active screen to the new one without any transition.
            ActiveScreen = aScreen;
         }
      }

      public void Respond(InputEvent aInput)
      {
         // If not in the middle of a transition, pass the event to the active screen.
         if (IsTransitioning == false)
         {
            ActiveScreen?.Respond(aInput);
         }
      }

      public void Draw(GameTime aGameTime)
      {
         ActiveScreen?.Draw(aGameTime);
         mActiveTransition?.Draw();
      }
   }
}
