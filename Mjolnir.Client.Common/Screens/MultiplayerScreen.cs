﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.UI;
using Mjolnir.Common;
using Mjolnir.Common.Messages;
using Mjolnir.Common.Network;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mjolnir.Client.Common.Screens
{
   public class MultiplayerScreen : ConnectScreenBase
   {
      private readonly RegistryClient mRegistryClient;
      private Table mServersTable = null;
      private TextButton mRefreshButton = null;
      private TextButton mDirectionConnectButton = null;
      private string mServerAddress = null;
      private int mServerPort = Constants.DefaultServerPort;

      protected override string Address => mServerAddress;

      protected override int Port => mServerPort;

      public override string Name => "Multiplayer";

      public MultiplayerScreen(IServiceProvider aServiceProvider) : base(aServiceProvider)
      {
         mRegistryClient = aServiceProvider.GetService<RegistryClient>();
      }

      public override void Dispose()
      {
         mRegistryClient.RetrievalEvent -= OnServerRegistrations;

         base.Dispose();
      }

      public override void Initialize()
      {
         mRegistryClient.RetrievalEvent += OnServerRegistrations;

         base.Initialize();
      }

      public override void LoadContent()
      {
         double padding = mViewport.VirtualHeight * 0.01;
         Rectangle paddedBounds = new Rectangle((int)padding,
                                                (int)padding,
                                                (int)(mViewport.VirtualWidth - (2.0 * padding)),
                                                (int)(mViewport.VirtualHeight - (2.0 * padding)));

         mServersTable = mServiceProvider.GetService<Table>();
         mServersTable.SelectEvent += OnServerSelect;
         mServersTable.Headers = new List<string>
         {
            "NAME",
            "PLAYERS",
            "PRIVATE?"
         };
         mServersTable.Weights = new List<int>
         {
            4,
            1,
            1 
         };

         if (MjolnirGame.IsDesktop == true)
         {
            // If running on a desktop with very fine cursor accuracy thanks to the availability of a mouse.

            // Allow for a large-ish number of visible rows.
            mServersTable.VisibleRows = 20;
         }
         else
         {
            // If running on a mobile device where tap accuracy is limited due to using fingers on a small screen.

            // Limit the number of visible rows so that they are larger (in height) and easier to select.
            mServersTable.VisibleRows = 8;
         }
         mServersTable.X = paddedBounds.X;
         mServersTable.Y = paddedBounds.Y;
         mServersTable.Width = paddedBounds.Width;
         mServersTable.Height = paddedBounds.Height - Text.DefaultHeight;
         mVisuals.Add(mServersTable);
         mControls.Add(mServersTable);

         double buttonSpacing = paddedBounds.Width / 50.0;
         mBackButton = mServiceProvider.GetService<TextButton>();
         mBackButton.Text = "BACK";
         mBackButton.X = paddedBounds.X + buttonSpacing;
         mBackButton.Y = paddedBounds.Y + paddedBounds.Height - mBackButton.Height;
         mBackButton.ClickEvent += OnBackClick;
         mVisuals.Add(mBackButton);
         mControls.Add(mBackButton);

         mRefreshButton = mServiceProvider.GetService<TextButton>();
         mRefreshButton.Text = "REFRESH";
         mRefreshButton.X = mBackButton.X + mBackButton.Width + buttonSpacing;
         mRefreshButton.Y = mBackButton.Y;
         mRefreshButton.ClickEvent += OnRefreshClick;
         mVisuals.Add(mRefreshButton);
         mControls.Add(mRefreshButton);

         mDirectionConnectButton = mServiceProvider.GetService<TextButton>();
         mDirectionConnectButton.Text = "DIRECT CONNECT";
         mDirectionConnectButton.X = mRefreshButton.X + mRefreshButton.Width + buttonSpacing;
         mDirectionConnectButton.Y = mRefreshButton.Y;
         mDirectionConnectButton.ClickEvent += OnDirectConnectClick;
         mVisuals.Add(mDirectionConnectButton);
         mControls.Add(mDirectionConnectButton);

         mConnectButton = mServiceProvider.GetService<TextButton>();
         mConnectButton.Text = "CONNECT";
         mConnectButton.X = mDirectionConnectButton.X + mDirectionConnectButton.Width + buttonSpacing;
         mConnectButton.Y = mDirectionConnectButton.Y;
         mConnectButton.ClickEvent += OnConnectClick;
         mConnectButton.IsEnabled = false;
         mVisuals.Add(mConnectButton);
         mControls.Add(mConnectButton);

         mRegistryClient.Retrieve();
      }

      public override void Respond(InputEvent aInput)
      {
         if ((aInput.Op == InputEvent.Opcode.BACK) && (mPasswordModalWindow == null) &&
             (mAlertModalWindow == null))
         {
            // If a back event came in AND neither modal window is taking priority.

            OnBackClick(this, EventArgs.Empty);
         }
         else if ((mPasswordModalWindow != null) || (mAlertModalWindow != null))
         {
            mPasswordModalWindow?.Respond(aInput);
            mAlertModalWindow?.Respond(aInput);
         }
         else
         {
            foreach (IControl control in mControls)
            {
               control.Respond(aInput);
            }
         }
      }


      #region Event handlers

      protected override void OnBackClick(object aSender, EventArgs aArgs)
      {
         mWindow.LoadScreen(mServiceProvider?.GetService<MainMenuScreen>());
      }

      /// <summary>
      /// Invoked by the server list when the user selects a row.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aKey"></param>
      private void OnServerSelect(object aSender, string aKey)
      {
         if (string.IsNullOrEmpty(aKey) == false)
         {
            // The key should in a the typical "127.0.0.1:51509" format.
            // Split it into "127.0.0.1" and "51509".
            string[] split = aKey.Split(':');

            // If the key split into exactly two parts and the latter can be parsed
            // as an integer.
            if ((split.Length == 2) && (int.TryParse(split[1], out mServerPort) == true))
            {
               mServerAddress = split[0];

               // At this point, the server's address and port are known so we can
               // enable the "CONNECT" button and the user can click it if he/she wants.
               mConnectButton.IsEnabled = true;
            }
         }
      }

      /// <summary>
      /// Invoked when the "REFRESH" button is clicked.
      /// Clears the list of servers and asks the registry to send the list of
      /// servers to us again.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnRefreshClick(object aSender, EventArgs aArgs)
      {
         // Clear the list of all previously-received servers. This causes the list to
         // forget any that had been selected.
         mServersTable.Clear();

         // Ask the registry to send the current list of servers.
         mRegistryClient.Retrieve();

         // Forget any server address and port that may have been remembered when a row
         // was selected.
         mServerAddress = null;
         mServerPort = Constants.DefaultServerPort;

         // With no server row selected, disable the connect button.
         mConnectButton.IsEnabled = false;
      }

      /// <summary>
      /// Invoked when the "DIRECT CONNECT" button is clicked.
      /// Loads the direct connect screen.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnDirectConnectClick(object aSender, EventArgs aArgs)
      {
         mWindow.LoadScreen(mServiceProvider.GetService<DirectConnectScreen>());
      }

      /// <summary>
      /// Invoked when the current list of servers is received from a registry.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aRegistrations">A list of metadata objects descrbing the servers.</param>
      private void OnServerRegistrations(object aSender, List<ServerRegistrationMessage> aRegistrations)
      {
         IOrderedEnumerable<ServerRegistrationMessage> sortedRegistrations = aRegistrations.OrderBy(r => r.Address);

         // Create a list of lists with the outer list being the list of rows to add to the table and the
         // inner list being the columns of the row.
         List<List<string>> rows = new List<List<string>>();
         foreach (ServerRegistrationMessage registration in sortedRegistrations)
         {
            rows.Add(new List<string>
            {
               $"{registration.Address}:{registration.Port}", // The row's key. Not displayed.
               registration.Name, // First column. Server's name.
               $"{registration.PlayerCount} / 8", // Second column. Number of players, including spectators.
               registration.IsPrivate ? "Y" : "N" // Third column. Y or N, if the server is private.
            });
         }

         mServersTable.AddRows(rows);
      }

      #endregion Event handlers
   }
}
