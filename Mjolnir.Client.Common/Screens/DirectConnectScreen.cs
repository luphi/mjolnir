﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.UI;
using Mjolnir.Common;
using NLog;
using System;

namespace Mjolnir.Client.Common.Screens
{

   public class DirectConnectScreen : ConnectScreenBase
   {
      private TextInput mAddressInput = null;
      private TextInput mPortInput = null;

      protected override string Address => mAddressInput?.Value;
      // Try to parse the port by grabbing the text in the port text input.
      // If parsing it as an integer fails, use the default server port value.
      protected override int Port => int.TryParse(mPortInput.Value, out int port) == false ?
                                     port :
                                     Constants.DefaultServerPort;

      public override string Name => "DIRECT CONNECT";

      public DirectConnectScreen(IServiceProvider aServiceProvider) : base(aServiceProvider) { }

      public override void LoadContent()
      {
         double screenPadding = mViewport.VirtualHeight * 0.1;
         double controlPadding = mViewport.VirtualHeight * 0.01;
         Rectangle paddedBounds = new Rectangle((int)screenPadding,
                                                (int)screenPadding,
                                                (int)(mViewport.VirtualWidth - (2.0 * screenPadding)),
                                                (int)(mViewport.VirtualHeight - (2.0 * screenPadding)));

         Text header = mServiceProvider.GetService<Text>();
         header.Value = Name;
         header.Y = paddedBounds.Y;
         header.X = paddedBounds.X + (paddedBounds.Width / 2) - (header.Width / 2);
         mVisuals.Add(header);

         mBackButton = mServiceProvider.GetService<TextButton>();
         mBackButton.ClickEvent += OnBackClick;
         mBackButton.Text = "BACK";
         mBackButton.Width = paddedBounds.Width;
         mBackButton.Padding = (uint)controlPadding;
         mBackButton.X = paddedBounds.X + (paddedBounds.Width / 2) - (mBackButton.Width / 2);
         mBackButton.Y = paddedBounds.Y + paddedBounds.Height - mBackButton.Height;
         mBackButton.HasBorder = true;
         mVisuals.Add(mBackButton);
         mControls.Add(mBackButton);

         mConnectButton = mServiceProvider.GetService<TextButton>();
         mConnectButton.ClickEvent += OnConnectClick;
         mConnectButton.Text = "CONNECT";
         mConnectButton.Width = paddedBounds.Width;
         mBackButton.Padding = (uint)controlPadding;
         mConnectButton.X = paddedBounds.X + (paddedBounds.Width / 2) - (mConnectButton.Width / 2.0);
         mConnectButton.Y = mBackButton.Y - mConnectButton.Height - controlPadding;
         mConnectButton.HasBorder = true;
         mVisuals.Add(mConnectButton);
         mControls.Add(mConnectButton);

         double midpoint = mConnectButton.Y - ((mConnectButton.Y - header.Y - header.Height) / 2.0);
         mAddressInput = mServiceProvider.GetService<TextInput>();
         mAddressInput.EnterEvent += OnConnectClick;
         mAddressInput.Width = paddedBounds.Width;
         mAddressInput.X = paddedBounds.X;
         mAddressInput.Y = midpoint - mAddressInput.Height;
         mVisuals.Add(mAddressInput);
         mControls.Add(mAddressInput);

         Text addressLabel = mServiceProvider.GetService<Text>();
         addressLabel.Value = "SERVER ADDRESS";
         addressLabel.X = paddedBounds.X;
         addressLabel.Y = mAddressInput.Y - addressLabel.Height;
         mVisuals.Add(addressLabel);

         Text portLabel = mServiceProvider.GetService<Text>();
         portLabel.Value = "SERVER PORT";
         portLabel.X = paddedBounds.X;
         portLabel.Y = midpoint + controlPadding;
         mVisuals.Add(portLabel);

         mPortInput = mServiceProvider.GetService<TextInput>();
         mPortInput.EnterEvent += OnConnectClick;
         mPortInput.Value = $"{Constants.DefaultServerPort}";
         mPortInput.Width = paddedBounds.Width;
         mPortInput.X = paddedBounds.X;
         mPortInput.Y = portLabel.Y + portLabel.Height;
         mVisuals.Add(mPortInput);
         mControls.Add(mPortInput);
      }

      public override void Respond(InputEvent aInput)
      {
         // If a back event came in AND the user isn't currently entering text into either
         // the address or port inputs AND neither modal window is taking priority.
         if ((aInput.Op == InputEvent.Opcode.BACK) &&
             (((mAddressInput?.IsFocused == true) || (mPortInput?.IsFocused == true)) == false) &&
             (mPasswordModalWindow == null) && (mAlertModalWindow == null))
         {
            OnBackClick(this, EventArgs.Empty);
         }
         else if ((mPasswordModalWindow != null) || (mAlertModalWindow != null))
         {
            mPasswordModalWindow?.Respond(aInput);
            mAlertModalWindow?.Respond(aInput);
         }
         else
         {
            foreach (IControl control in mControls)
            {
               control.Respond(aInput);
            }
         }
      }

      protected override void OnBackClick(object aSender, EventArgs aArgs)
      {
         mWindow.LoadScreen(mServiceProvider?.GetService<MultiplayerScreen>());
      }
   }
}
