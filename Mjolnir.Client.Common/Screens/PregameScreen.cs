﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.UI;
using Mjolnir.Common;
using Mjolnir.Common.Collections;
using Mjolnir.Common.Disk;
using Mjolnir.Common.Messages;
using Mjolnir.Common.Models;
using Mjolnir.Common.Network;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mjolnir.Client.Common.Screens
{
   public class PregameScreen : Screen
   {
      // The list of eight colors to choose from when assigning colors to players' names
      // and their corresponding spawn point on the stage preview.
      private static readonly Color[] sSpawnPointColors = new Color[]
      {
         Color.Aqua,
         Color.Lime,
         Color.Violet,
         Color.Yellow,
         Color.Orange,
         Color.Wheat,
         Color.Gainsboro, // "Mine is special. It's good for absolutely nothing!"
         Color.Crimson
      };

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly IContentWrapper mContentWrapper;
      private readonly Configuration mConfiguration;
      private readonly Window mWindow;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly Camera mCamera;
      private readonly SpriteBatch mSpriteBatch;
      private readonly GameClient mGameClient;
      private bool mIsExiting = false;

      // Generic UI variables.
      private readonly ConcurrentList<IVisual> mVisuals = new ConcurrentList<IVisual>();
      private Image mStagePreviewImage = null;
      private ChatLog mChatLog = null;
      private TextInput mChatInput = null;
      private ProgressBar mProgressBar = null;
      private AlertModalWindow mAlertOverlay = null;
      private TextButton mBackButton = null;

      // Player/team lists variables.
      private readonly Dictionary<uint, Text> mTeamANamesMap = new Dictionary<uint, Text>();
      private readonly Dictionary<uint, Text> mTeamBNamesMap = new Dictionary<uint, Text>();
      private Rectangle mTeamAPlayerListBounds = Rectangle.Empty;
      private Rectangle mTeamBPlayerListBounds = Rectangle.Empty;
      private Text mTeamALabel = null;
      private Text mTeamBLabel = null;

      // Starting location indicator variables.
      private readonly Dictionary<uint, SpawnIndicator> mPlayerSpawnMap =
          new Dictionary<uint, SpawnIndicator>();

      // Server settings variables.
      private Rectangle mServerSettingsBounds = Rectangle.Empty;
      private Rectangle mServerSettingsStagePreviewBounds = Rectangle.Empty;
      private Text mSettingsStageLabel = null;
      private Text mSettingsStageName = null;
      private Text mSettingsSuddenDeathLabel = null;
      private Text mSettingsSuddenDeathMode = null;

      public override string Name => "Pre-game";

      public PregameScreen(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mContentWrapper = aServiceProvider.GetService<IContentWrapper>();
         mConfiguration = aServiceProvider.GetService<Configuration>();
         mWindow = aServiceProvider.GetService<Window>();
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mCamera = aServiceProvider.GetService<Camera>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mGameClient = aServiceProvider.GetService<GameClient>();
      }

      public override void Initialize()
      {
         mGameClient.DisconnectEvent += OnDisconnect;
         mGameClient.MessageEvent += OnMessage;
      }

      public override void Dispose()
      {
         mGameClient.DisconnectEvent -= OnDisconnect;
         mGameClient.MessageEvent -= OnMessage;
      }

      public override void LoadContent()
      {
         double padding = mViewport.VirtualHeight * 0.01;
         Rectangle paddedBounds = new Rectangle((int)padding,
                                                (int)padding,
                                                (int)(mViewport.VirtualWidth - (2.0 * padding)),
                                                (int)(mViewport.VirtualHeight - (2.0 * padding)));


         #region Bounds - locations and sizes of the various regions

         // Regions are laid out like so:
         // (If this appears nonsensical to you, it's due to editor/system differences. Sorry.)
         //
         //   ---------------------------------------------------------------------------------------------
         //   |                      |                     |                                              |
         //   |        team A        |        team B       |                                              |
         //   |        names         |        names        |                                              |
         //   |                      |                     |                                              |
         //   |                      |                     |                    chat                      |
         //   |--------------------------------------------|                                              |
         //   |                   server                   |                                              |
         //   |                  settings                  |                                              |
         //   |--------------------------------------------|                                              |
         //   | "BACK" |          progress bar             |                                              |
         //   |-------------------------------------------------------------------------------------------|
         //   |                                                                                           |
         //   |                                                                                           |
         //   |                                                                                           |
         //   |                                                                                           |
         //   |                                                                                           |
         //   |                                         stage                                             |
         //   |                                        preview                                            |
         //   |                                                                                           |
         //   |                                                                                           |
         //   |                                                                                           |
         //   |                                                                                           |
         //   |                                                                                           |
         //   ---------------------------------------------------------------------------------------------

         // Prepare some bounds to help organize the screen.
         Rectangle stageBounds = new Rectangle(paddedBounds.X,
            (int)(paddedBounds.Y + paddedBounds.Height - (paddedBounds.Height * 0.6)),
            paddedBounds.Width,
            (int)(paddedBounds.Height * 0.6));
         Rectangle quadrant2Bounds = new Rectangle(paddedBounds.X,
            paddedBounds.Y,
            (int)(paddedBounds.Width * 0.5),
            paddedBounds.Height - stageBounds.Height);
         mTeamAPlayerListBounds = new Rectangle((int)(quadrant2Bounds.X + padding),
            (int)(quadrant2Bounds.Y + padding),
            (int)((quadrant2Bounds.Width / 2) - (2.0 * padding)),
            quadrant2Bounds.Height / 2);
         mTeamBPlayerListBounds = new Rectangle(
            (int)(mTeamAPlayerListBounds.X + mTeamAPlayerListBounds.Width + padding),
            mTeamAPlayerListBounds.Y,
            mTeamAPlayerListBounds.Width,
            mTeamAPlayerListBounds.Height);
         Rectangle progressBarAndBackBounds = new Rectangle(paddedBounds.X,
            (int)(stageBounds.Y - Text.DefaultHeight),
            quadrant2Bounds.Width,
            (int)Text.DefaultHeight);
         mServerSettingsBounds = new Rectangle(paddedBounds.X,
            (int)(paddedBounds.Y + mTeamAPlayerListBounds.Height + (2.0 * padding)),
            quadrant2Bounds.Width,
            (int)(quadrant2Bounds.Height - mTeamAPlayerListBounds.Height -
               progressBarAndBackBounds.Height - (4.0 * padding)));
         Rectangle chatBounds = new Rectangle(paddedBounds.X + quadrant2Bounds.Width,
            paddedBounds.Y,
            paddedBounds.Width - quadrant2Bounds.Width,
            quadrant2Bounds.Height);

         #endregion Bounds - locations and sizes of the various regions


         #region Stage preview and color assignments

         mStagePreviewImage = mServiceProvider.GetService<Image>();
         if (Context.Stage != null)
         {
            mStagePreviewImage.Content = mContentWrapper.StageForegroundPath(Context.Stage);
         }

         // Create the final bounds for the stage preview with some padding.
         mServerSettingsStagePreviewBounds = new Rectangle((int)(stageBounds.X + padding),
                                                        (int)(stageBounds.Y + padding),
                                                        (int)(stageBounds.Width - (2.0 * padding)),
                                                        (int)(stageBounds.Height - (2.0 * padding)));
         mStagePreviewImage.CenterInBounds(mServerSettingsStagePreviewBounds);
         mVisuals.Add(mStagePreviewImage);

         for (int i = 0; i < Context.Players.Count; i++)
         {
            SpawnIndicator indicator = new SpawnIndicator(mServiceProvider)
            {
               Color = sSpawnPointColors[i],
               Width = stageBounds.Height / 10.0
            };
            indicator.Height = indicator.Width;
            Player player = Context.Players[i];
            mPlayerSpawnMap[player.ID] = indicator;
            mVisuals.Add(indicator);
            PlaceSpawnPointIndicator(player);
         }

         #endregion Stage preview and color assignments


         #region Player lists

         double namesTextHeight = Math.Min(Text.DefaultHeight, mTeamAPlayerListBounds.Height / 5.0);
         // The player lists simply shows the players' names under a "Team A" or "Team B" label.
         // Create team A's list (to the left of team B's).
         mTeamALabel = mServiceProvider.GetService<Text>();
         mTeamALabel.Value = "TEAM A";
         mTeamALabel.Color = mConfiguration.SecondaryColor.ToColor();
         mTeamALabel.Height = namesTextHeight;
         mTeamALabel.X = mTeamAPlayerListBounds.X + (mTeamAPlayerListBounds.Width / 2.0) - (mTeamALabel.Width / 2.0);
         mTeamALabel.Y = mTeamAPlayerListBounds.Y;
         mVisuals.Add(mTeamALabel);

         List<Player> teamAPlayers = Context.Players.Where(p => p.Team == TeamEnum.A).ToList();
         for (int i = 0; i < teamAPlayers.Count; i++)
         {
            Text name = mServiceProvider.GetService<Text>();
            name.Value = teamAPlayers[i].Name;
            name.Height = namesTextHeight; ; // Also sets width.
            name.X = mTeamAPlayerListBounds.X + (mTeamAPlayerListBounds.Width / 2.0) - (name.Width / 2.0);
            name.Y = mTeamALabel.Y + mTeamALabel.Height + (i * namesTextHeight);
            if (mPlayerSpawnMap.TryGetValue(teamAPlayers[i].ID, out SpawnIndicator indicator) == true)
            {
               name.Color = indicator.Color;
            }
            mVisuals.Add(name);
         }

         mTeamBLabel = mServiceProvider.GetService<Text>();
         mTeamBLabel.Value = "TEAM B";
         mTeamBLabel.Color = mConfiguration.SecondaryColor.ToColor();
         mTeamBLabel.Height = namesTextHeight;
         mTeamBLabel.X = mTeamBPlayerListBounds.X + (mTeamBPlayerListBounds.Width / 2.0) - (mTeamBLabel.Width / 2.0);
         mTeamBLabel.Y = mTeamBPlayerListBounds.Y;
         mVisuals.Add(mTeamBLabel);

         List<Player> teamBPlayers = Context.Players.Where(p => p.Team == TeamEnum.B).ToList();
         for (int i = 0; i < teamBPlayers.Count; i++)
         {
            Text name = mServiceProvider.GetService<Text>();
            name.Value = teamBPlayers[i].Name;
            name.Height = namesTextHeight; // Also sets width.
            name.X = mTeamBPlayerListBounds.X + (mTeamBPlayerListBounds.Width / 2.0) - (name.Width / 2.0);
            name.Y = mTeamBLabel.Y + mTeamBLabel.Height + (i * namesTextHeight);
            if (mPlayerSpawnMap.TryGetValue(teamBPlayers[i].ID, out SpawnIndicator indicator) == true)
            {
               name.Color = indicator.Color;
            }
            mVisuals.Add(name);
         }

         #endregion Player lists


         #region Back button, progress bar, and server settings

         // A typical back button placed in the bottom left of the screen.
         mBackButton = mServiceProvider.GetService<TextButton>();
         mBackButton.Text = "BACK";
         mBackButton.X = progressBarAndBackBounds.X;
         mBackButton.Y = progressBarAndBackBounds.Y;
         mBackButton.ClickEvent += OnBackClick;
         mVisuals.Add(mBackButton);

         mProgressBar = mServiceProvider.GetService<ProgressBar>();
         mProgressBar.Width = progressBarAndBackBounds.Width - mBackButton.Width - (2.0 * padding);
         mProgressBar.Height = Text.DefaultHeight;
         mProgressBar.X = mBackButton.X + mBackButton.Width + padding;
         mProgressBar.Y = progressBarAndBackBounds.Y;
         mVisuals.Add(mProgressBar);

         double settingsTextHeight = Math.Min(Text.DefaultHeight, mServerSettingsBounds.Height / 2.0);
         mSettingsStageLabel = mServiceProvider.GetService<Text>();
         mSettingsStageLabel.Value = "STAGE";
         mSettingsStageLabel.Color = mConfiguration.SecondaryColor.ToColor();
         mSettingsStageLabel.Height = settingsTextHeight;
         mVisuals.Add(mSettingsStageLabel);

         mSettingsStageName = mServiceProvider.GetService<Text>();
         mSettingsStageName.Height = settingsTextHeight;
         mVisuals.Add(mSettingsStageName);

         mSettingsSuddenDeathLabel = mServiceProvider.GetService<Text>();
         mSettingsSuddenDeathLabel.Value = "SUDDEN DEATH";
         mSettingsSuddenDeathLabel.Color = mConfiguration.SecondaryColor.ToColor();
         mSettingsSuddenDeathLabel.Height = settingsTextHeight;
         mVisuals.Add(mSettingsSuddenDeathLabel);

         mSettingsSuddenDeathMode = mServiceProvider.GetService<Text>();
         mSettingsSuddenDeathMode.Height = settingsTextHeight;
         mVisuals.Add(mSettingsSuddenDeathMode);

         double remainingWidth = mServerSettingsBounds.Width -
                                 (mSettingsStageLabel.Width + mSettingsSuddenDeathLabel.Width);
         double settingsSpacing = remainingWidth / 3.0;
         mSettingsStageLabel.X = mServerSettingsBounds.X + settingsSpacing;
         mSettingsStageLabel.Y = mServerSettingsBounds.Y + (mServerSettingsBounds.Height / 2.0) -
                                 mSettingsStageLabel.Height;
         mSettingsSuddenDeathLabel.X = mServerSettingsBounds.X + mServerSettingsBounds.Width -
                                       settingsSpacing - mSettingsSuddenDeathLabel.Width;
         mSettingsSuddenDeathLabel.Y = mSettingsStageLabel.Y;
         mSettingsStageName.Y = mSettingsStageLabel.Y + mSettingsStageLabel.Height;
         mSettingsSuddenDeathMode.Y = mSettingsSuddenDeathLabel.Y + mSettingsSuddenDeathLabel.Height;

         // Use a helper method to assign the Texts' values and horizontally align them.
         UpdateAndDisplayServerSettings();

         #endregion Back button, progress bar, and server settings


         #region Chat and its input

         // Create the chat input, placed along the bounds' bottom edge.
         mChatInput = mServiceProvider.GetService<TextInput>();
         mChatInput.EnterEvent += OnChatMessageEnter;
         mChatInput.Width = chatBounds.Width - (2.0 * padding);
         mChatInput.X = chatBounds.X + padding;
         mChatInput.Y = chatBounds.Y + chatBounds.Height - mChatInput.Height;
         mVisuals.Add(mChatInput);

         // Create the chat log, placed above the chat input.
         mChatLog = mServiceProvider.GetService<ChatLog>();
         mChatLog.MaxLines = 10;
         mChatLog.X = chatBounds.X;
         mChatLog.Y = chatBounds.Y;
         mChatLog.Width = chatBounds.Width;
         mChatLog.Height = chatBounds.Height - mChatInput.Height;
         mVisuals.Add(mChatLog);

         #endregion Chat and its input


         // Process any backlogged messages (that may have been queued while changing screens) now that
         // all the UI components are available.
         while (mGameClient.HasMessage == true)
         {
            HandleMessage(mGameClient.Head);
         }
      }

      public override void Respond(InputEvent aInput)
      {
         if ((aInput.Op == InputEvent.Opcode.BACK) && (mChatInput?.IsFocused == false))
         {
            // If a back event came in AND the user isn't currently entering a chat message.

            OnBackClick(this, EventArgs.Empty);
         }
         else if (mAlertOverlay != null)
         {
            mAlertOverlay?.Respond(aInput);
         }
         else
         {
            mChatInput?.Respond(aInput);
            mBackButton?.Respond(aInput);
         }
      }

      public override void Draw(GameTime aGameTime)
      {
         mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix);

         foreach (IVisual visual in mVisuals)
         {
            visual.Draw();
         }

         mAlertOverlay?.Draw();

         mSpriteBatch.End();
      }

      #region Event handlers

      private void OnDisconnect(object aSender, EventArgs aArgs)
      {
         AlertThenExit("The connection to the server was lost without explanation.");
      }

      /// <summary>
      /// Invoked when enter is pressed while the chat input is focused.
      /// In other words, the user finished typing a message and hit enter to send it.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnChatMessageEnter(object aSender, EventArgs aArgs)
      {
         // If the user didn't actually enter a message, don't send anything.
         if (string.IsNullOrEmpty(mChatInput.Value) == true)
         {
            return;
         }

         mGameClient.Send(new ChatMessage(Context.LocalPlayerID, false, mChatInput.Value));

         mChatInput.IsFocused = true;
         mChatInput.Value = string.Empty;
      }

      private void OnBackClick(object aSender, EventArgs aArgs)
      {
         mGameClient.Disconnect();
         bool isSingleplayer = Context.IsSingleplayer;
         Context.Reset();
         if (isSingleplayer == true)
         {
            mWindow.LoadScreen(mServiceProvider?.GetService<MainMenuScreen>());
         }
         else
         {
            mWindow.LoadScreen(mServiceProvider?.GetService<MultiplayerScreen>());
         }
      }

      #endregion Event handlers


      #region Helper methods

      private void PlaceSpawnPointIndicator(Player aPlayer)
      {
         if ((aPlayer == null) || (mStagePreviewImage == null))
         {
            return;
         }

         if (mPlayerSpawnMap.TryGetValue(aPlayer.ID, out SpawnIndicator indicator) == true)
         {
            Vector spawnPoint = GetNeareastSpawnPoint(aPlayer);
            Vector scaledPoint = new Vector(
               spawnPoint.X * (mStagePreviewImage.Width / mStagePreviewImage.NativeWidth),
               spawnPoint.Y * (mStagePreviewImage.Height / mStagePreviewImage.NativeHeight));
            indicator.X = mStagePreviewImage.X + scaledPoint.X - (indicator.Width / 2.0);
            indicator.Y = mStagePreviewImage.Y + scaledPoint.Y - (indicator.Height / 2.0);
         }
         else
         {
            mLog.Warn($"Could not place a spawn indicator for player \"{aPlayer.Name}\"({aPlayer.ID}): " +
                      $"the player's ID was not found in the map of spawn points");
         }
      }

      private Vector GetNeareastSpawnPoint(Player aPlayer)
      {
         if (Context.Stage == null)
         {
            return Vector.Zero;
         }

         Vector closestLocation = Vector.Zero;
         double closestDistance = float.MaxValue;
         List<Vector> startingLocations = aPlayer.Team == TeamEnum.A ?
                                          Context.Stage.SpawnPointsA :
                                          Context.Stage.SpawnPointsB;

         foreach (Vector location in startingLocations)
         {
            Vector playerCenter = aPlayer.Center;
            if (Vector.Distance(playerCenter, location) < closestDistance)
            {
               closestLocation = location;
               closestDistance = Vector.Distance(playerCenter, location);
            }
         }

         return closestLocation;
      }

      private void RemovePlayer(Player aPlayer)
      {
         // Start by getting the name Text, team label Text, and list of others Texts
         // for the whole team so we can adjust them rather than leave an empty space.
         Text name;
         Text label;
         List<Text> names;
         if ((aPlayer.Team == TeamEnum.A) && (mTeamANamesMap.ContainsKey(aPlayer.ID) == true))
         {
            name = mTeamANamesMap[aPlayer.ID];
            mTeamANamesMap.Remove(aPlayer.ID);
            label = mTeamALabel;
            names = mTeamANamesMap.Values.ToList();
         }
         else if (mTeamBNamesMap.ContainsKey(aPlayer.ID) == true)
         {
            name = mTeamBNamesMap[aPlayer.ID];
            mTeamBNamesMap.Remove(aPlayer.ID);
            label = mTeamBLabel;
            names = mTeamBNamesMap.Values.ToList();
         }
         else
         {
            mLog.Warn($"Could not remove a player during the pre-game: player ID {aPlayer.ID} " +
                      $"was not used as a key in either team map");
            return;
         }

         // Shift the remaining players' names by stacking them below the team's label.
         for (int i = 0; i < names.Count; i++)
         {
            names[i].Y = label.Y + label.Height + (i * names[i].Height);
         }

         mVisuals.Remove(name);
      }

      private void UpdateAndDisplayServerSettings()
      {
         if ((Context.ServerSettings == null) || (mServerSettingsBounds == Rectangle.Empty))
         {
            return;
         }

         // If the stage has changed. Unlikely to happen in this screen but maybe possible.
         if (Context.Stage?.ID != Context.ServerSettings.StageID)
         {
            // Grab a model of the new stage.
            Stage stage = Stage.List.FirstOrDefault(s => s.ID == Context.ServerSettings.StageID);
            if (stage != null)
            {
               Context.Stage = stage;

               // Update the preview image with the new stage's foreground.
               if (mStagePreviewImage != null)
               {
                  mStagePreviewImage.Content = mContentWrapper.StageForegroundPath(stage);
                  mStagePreviewImage.CenterInBounds(mServerSettingsStagePreviewBounds);
               }

               // Update each player's spawn indicator.
               foreach (Player player in Context.Players)
               {
                  // Move the starting location indicator.
                  PlaceSpawnPointIndicator(player);
               }
            }
         }

         if (mSettingsStageName != null)
         {
            // Display the stage's name, horizontally centered below the "STAGE" label.
            mSettingsStageName.Value = Stage.List.FirstOrDefault(
                s => s.ID == Context.ServerSettings.StageID)?.Name ?? "????";
            mSettingsStageName.X = mSettingsStageLabel.X + (mSettingsStageLabel.Width / 2.0) -
                                   (mSettingsStageName.Width / 2.0);
         }

         if (mSettingsSuddenDeathMode != null)
         {
            // Display the sudden death mode, horizontally centered below the "SUDDEN DEATH" label.
            mSettingsSuddenDeathMode.Value = Context.ServerSettings.SuddenDeathMode.ToString();
            mSettingsSuddenDeathMode.X = mSettingsSuddenDeathLabel.X +
                                        (mSettingsSuddenDeathLabel.Width / 2.0) -
                                        (mSettingsSuddenDeathMode.Width / 2.0);
         }
      }

      private void AlertThenExit(string aMessage)
      {
         // If already alerting then exiting.
         if (mIsExiting == true)
         {
            return;
         }

         // Set a flag indicating we're in the process of exiting.
         mIsExiting = true;

         // Display a message to the user, then leave the screen when the message is acknowledged.
         mAlertOverlay = mServiceProvider.GetService<AlertModalWindow>();
         mAlertOverlay.Message = aMessage;
         mAlertOverlay.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
         {
            mAlertOverlay = null;
            OnBackClick(this, EventArgs.Empty);
         };
      }

      #endregion Helper methods


      #region Message handling

      private void OnMessage(object aSender, EventArgs aArgs)
      {
         while ((mGameClient.HasMessage == true) && (mWindow.IsTransitioning == false))
         {
            HandleMessage(mGameClient.Head);
         }
      }


      private void HandleMessage(Message aMessage)
      {
         if (mIsExiting == true)
         {
            return;
         }

         switch (aMessage.Op)
         {
            case Message.Opcode.CHAT:
               if (aMessage is ChatMessage chatMessage)
               {
                  Player player = Context.Players.FirstOrDefault(p => p.ID == chatMessage.PlayerID);
                  mChatLog.Add(player?.Name ?? "????", chatMessage.Message);
               }
               break;
            case Message.Opcode.COUNTDOWN:
               if (aMessage is CountdownMessage countdownMessage)
               {
                  if (mProgressBar != null)
                  {
                     mProgressBar.Percentage = countdownMessage.CountdownAsPercent;
                  }
               }
               break;
            case Message.Opcode.PLAYERS:
               if (aMessage is PlayersMessage playersMessage)
               {
                  // Get a list of players that are in the current list but not the incoming list.
                  List<Player> departingPlayers = Context.Players.Except(playersMessage.Players,
                                                                         Player.Comparer).ToList();
                  foreach (Player player in departingPlayers)
                  {
                     RemovePlayer(player);
                     Context.Players.Remove(player);
                  }

                  // Sync the players with the updates from the server.
                  foreach (Player updatedPlayer in playersMessage.Players)
                  {
                     Player localPlayer = Context.Players.FirstOrDefault(p => p.ID == updatedPlayer.ID);
                     // If the local instance of the player was found and syncing it with the
                     // update modified the player in any way.
                     if (localPlayer?.Sync(updatedPlayer) == true)
                     {
                        // Move the starting location indicator.
                        PlaceSpawnPointIndicator(localPlayer);
                     }
                  }
               }
               break;
            case Message.Opcode.SERVER_SETTINGS:
               if (aMessage is ServerSettingsMessage serverSettingsMessage)
               {
                  // The server settings should only change in the lobby but, just in case one last
                  // update was sent before or during the state change, we'll allow for the possibility
                  // that one will be processed during the pre-game state.
                  Context.ServerSettings = serverSettingsMessage;
                  UpdateAndDisplayServerSettings();
               }
               break;
            case Message.Opcode.SERVER_SHUTDOWN:
               if (aMessage is ServerShutdownMessage serverShutdownMessage)
               {
                  if (string.IsNullOrEmpty(serverShutdownMessage.Reason) == false)
                  {
                     // If a reason for the server's shutdown was provided.

                     // Display a message including the reason before leaving the screen.
                     AlertThenExit($"The server shut down: {serverShutdownMessage.Reason}");
                  }
                  else
                  {
                     // If no reason for the shutdown was provided.

                     // In practice, this will only happen when smoothly exiting a single-player
                     // game and displaying a message to the user could only be confusing.
                     // Display nothing and immediately exit the screen.
                     OnBackClick(this, EventArgs.Empty);
                  }
               }
               break;
            case Message.Opcode.SERVER_STATE:
               if (aMessage is ServerStateMessage serverStateMessage)
               {
                  if (serverStateMessage.State == StateEnum.IN_GAME)
                  {
                     // If the server is announcing its transition to the in-game state.

                     // Follow the server's lead by loading the in-game screen.
                     mWindow.LoadScreen(mServiceProvider?.GetService<IngameScreen>());
                  }
                  else
                  {
                     // If the server is transitioning to any other state.

                     // It doesn't work like that. The server should only ever transition to the
                     // in-game state from the pre-game state. If it's going somewhere else, we'll
                     // consider it modified and likely hostile.
                     AlertThenExit("The server acted irregularly. Disconnecting.");
                  }
               }
               break;
         }
      }

      #endregion Message handling


      #region Spawn indicator class

      /// <summary>
      /// Private helper class for visually indicating where a player will start the game.
      /// This is essentially just a (UI) Quad with a black border.
      /// </summary>
      private class SpawnIndicator : IVisual
      {
         private readonly Quad mBorder;
         private readonly Quad mFill;

         public double X
         {
            get => mX;
            set
            {
               mX = value;
               Resize();
            }
         }
         private double mX = 0.0;

         public double Y
         {
            get => mY;
            set
            {
               mY = value;
               Resize();
            }
         }
         private double mY = 0.0;

         public double Width
         {
            get => mWidth;
            set
            {
               mWidth = value;
               Resize();
            }
         }
         private double mWidth = 0.0;

         public double Height
         {
            get => mHeight;
            set
            {
               mHeight = value;
               Resize();
            }
         }
         private double mHeight = 0.0;

         public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

         public Rectangle Bounds => mBorder.Bounds;

         internal Color Color
         {
            get => mFill.Color;
            set => mFill.Color = value;
         }

         internal SpawnIndicator(IServiceProvider aProvider)
         {
            mBorder = aProvider.GetService<Quad>();
            mFill = aProvider.GetService<Quad>();

            mBorder.Color = Color.Black;
         }

         public void Draw()
         {
            mBorder.Draw();
            mFill.Draw();
         }

         private void Resize()
         {
            mBorder.X = mX;
            mBorder.Y = mY;
            mBorder.Width = mWidth;
            mBorder.Height = mHeight;

            mFill.X = mX + (mWidth * 0.1);
            mFill.Y = mY + (mHeight * 0.1);
            mFill.Width = mWidth * 0.8;
            mFill.Height = mHeight * 0.8;
         }
      }

      #endregion Spawn indicator class
   }
}
