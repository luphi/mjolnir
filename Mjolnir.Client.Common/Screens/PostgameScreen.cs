﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.UI;
using Mjolnir.Common;
using Mjolnir.Common.Collections;
using Mjolnir.Common.Messages;
using Mjolnir.Common.Models;
using Mjolnir.Common.Network;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mjolnir.Client.Common.Screens
{
   public class PostgameScreen : Screen
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly Configuration mConfiguration;
      private readonly Window mWindow;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly Camera mCamera;
      private readonly SpriteBatch mSpriteBatch;
      private readonly GameClient mGameClient;
      private bool mIsExiting = false;

      // Generic UI variables.
      private readonly ConcurrentList<IVisual> mVisuals = new ConcurrentList<IVisual>();
      private ChatLog mChatLog = null;
      private TextInput mChatInput = null;
      private ProgressBar mProgressBar = null;
      private AlertModalWindow mAlertOverlay = null;
      private TextButton mBackButton = null;

      // Scoreboard variables.
      private Rectangle mTeamABounds = Rectangle.Empty;
      private Rectangle mTeamBBounds = Rectangle.Empty;
      private Text mTeamALabel = null;
      private Text mTeamBLabel = null;
      private SplitTable mTeamATable = null;
      private SplitTable mTeamBTable = null;

      public override string Name => "Post-game";

      public PostgameScreen(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mConfiguration = aServiceProvider.GetService<Configuration>();
         mWindow = aServiceProvider.GetService<Window>();
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mCamera = aServiceProvider.GetService<Camera>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mGameClient = aServiceProvider.GetService<GameClient>();
      }

      public override void Initialize()
      {
         mGameClient.DisconnectEvent += OnDisconnect;
         mGameClient.MessageEvent += OnMessage;
      }

      public override void Dispose()
      {
         mGameClient.DisconnectEvent -= OnDisconnect;
         mGameClient.MessageEvent -= OnMessage;

         // The winning team only matters to the preceding round. Set it to null now that it's moot.
         Context.WinningTeam = null;
      }

      public override void LoadContent()
      {
         double padding = mViewport.VirtualHeight * 0.01;
         Rectangle paddedBounds = new Rectangle((int)padding,
                                                (int)padding,
                                                (int)(mViewport.VirtualWidth - (2.0 * padding)),
                                                (int)(mViewport.VirtualHeight - (2.0 * padding)));


         #region Bounds - locations and sizes of the various regions

         // Regions are laid out like so:
         // (If this appears nonsensical to you, it's due to editor/system differences. Sorry.)
         //
         //   ---------------------------------------------------------------------------------------------
         //   |             "TEAM A - [WIN/LOSE]"           |             "TEAM B - [WIN/LOSE]"           |
         //   |---------------------------------------------|---------------------------------------------|
         //   | "PLAYER"                            "SCORE" | "PLAYER"                            "SCORE" |
         //   | <player name>                             # | <player name>                             # |
         //   | <player name>                             # | <player name>                             # |
         //   | <player name>                             # | <player name>                             # |
         //   | <player name>                             # | <player name>                             # |
         //   |-------------------------------------------------------------------------------------------|
         //   |                                                                                           |
         //   |                                                                                           |
         //   |                                          chat log                                         |
         //   |                                                                                           |
         //   |                                                                                           |
         //   |                                                                                           |
         //   |                                                                                           |
         //   |-------------------------------------------------------------------------------------------|
         //   | "BACK" |     progress bar      |                      chat input                          |
         //   ---------------------------------------------------------------------------------------------

         // Prepare some bounds to help organize the screen.
         mTeamABounds = new Rectangle(paddedBounds.X,
            paddedBounds.Y,
            (int)((paddedBounds.Width / 2.0) - padding),
            (int)(paddedBounds.Height * 0.4));
         mTeamBBounds = new Rectangle((int)(paddedBounds.X + mTeamABounds.Width + (2.0 * padding)),
            paddedBounds.Y,
            mTeamABounds.Width,
            mTeamABounds.Height);
         Rectangle bottomRowBounds = new Rectangle(paddedBounds.X,
            (int)(paddedBounds.Y + paddedBounds.Height - Text.DefaultHeight),
            paddedBounds.Width,
            (int)Text.DefaultHeight);
         Rectangle chatBounds = new Rectangle(paddedBounds.X,
            paddedBounds.Y + mTeamABounds.Height,
            paddedBounds.Width,
            paddedBounds.Height - mTeamABounds.Height - bottomRowBounds.Height);

         #endregion Bounds - locations and sizes of the various regions


         #region Team A and B labels

         // Create a label to appear about the team A list identifying the team (A) and the team's
         // win/lose status.
         mTeamALabel = mServiceProvider.GetService<Text>();
         mTeamALabel.Value = "TEAM A - ????";
         mTeamALabel.Color = mConfiguration.SecondaryColor.ToColor();
         mTeamALabel.Y = mTeamABounds.Y;
         mVisuals.Add(mTeamALabel);

         // Create the equivalent label for team B.
         mTeamBLabel = mServiceProvider.GetService<Text>();
         mTeamBLabel.Value = "TEAM B - ????";
         mTeamBLabel.Color = mConfiguration.SecondaryColor.ToColor();
         mTeamBLabel.Y = mTeamABounds.Y;
         mVisuals.Add(mTeamBLabel);

         // Use a helper method to assign the team labels' more useful values and horizontally align them.
         UpdateGameStatus();

         #endregion Team A and B labels


         #region Team A and B lists

         // Create a list with two columns to display team A's players alongside their scores.
         mTeamATable = mServiceProvider.GetService<SplitTable>();
         mTeamATable.LeftHeader = "PLAYER";
         mTeamATable.RightHeader = "SCORE";
         mTeamATable.X = mTeamABounds.X;
         mTeamATable.Y = mTeamABounds.Y + mTeamALabel.Height;
         mTeamATable.Width = mTeamABounds.Width;
         mTeamATable.Height = mTeamABounds.Height - mTeamALabel.Height;
         mVisuals.Add(mTeamATable);

         // Do the same thing for team B.
         mTeamBTable = mServiceProvider.GetService<SplitTable>();
         mTeamBTable.LeftHeader = "PLAYER";
         mTeamBTable.RightHeader = "SCORE";
         mTeamBTable.X = mTeamBBounds.X;
         mTeamBTable.Y = mTeamBBounds.Y + mTeamBLabel.Height;
         mTeamBTable.Width = mTeamBBounds.Width;
         mTeamBTable.Height = mTeamBBounds.Height - mTeamBLabel.Height;
         mVisuals.Add(mTeamBTable);

         // Use a helper method to fill the above lists.
         UpdateScoreboard();

         #endregion Team A and B lists


         // Create the chat log, placed above the back button, progress bar, and chat input.
         mChatLog = mServiceProvider.GetService<ChatLog>();
         mChatLog.MaxLines = 10;
         mChatLog.X = chatBounds.X;
         mChatLog.Y = chatBounds.Y;
         mChatLog.Width = chatBounds.Width;
         mChatLog.Height = chatBounds.Height;
         mVisuals.Add(mChatLog);


         #region Bottom row - back button, progress bar, and chat input

         // A typical back button placed in the bottom left of the screen.
         mBackButton = mServiceProvider.GetService<TextButton>();
         mBackButton.Text = "BACK";
         mBackButton.Height = bottomRowBounds.Height;
         mBackButton.X = bottomRowBounds.X;
         mBackButton.Y = bottomRowBounds.Y;
         mBackButton.ClickEvent += OnBackClick;
         mVisuals.Add(mBackButton);

         // Create a progress bar to show the countdown until returning to the lobby.
         // This is placed on the bottom edge of the screen between the back button and chat input.
         mProgressBar = mServiceProvider.GetService<ProgressBar>();
         mProgressBar.Width = (bottomRowBounds.Width * 0.25) - padding;
         mProgressBar.X = bottomRowBounds.X + mBackButton.Width;
         mProgressBar.Y = bottomRowBounds.Y;
         mVisuals.Add(mProgressBar);

         // Create a chat text input. This takes up the majority of the width of this bottom row.
         // It's snapped to the bottom right corner of the screen.
         mChatInput = mServiceProvider.GetService<TextInput>();
         mChatInput.EnterEvent += OnChatMessageEntered;
         mChatInput.Width = bottomRowBounds.Width - mBackButton.Width - mProgressBar.Width - padding;
         mChatInput.X = bottomRowBounds.X + bottomRowBounds.Width - mChatInput.Width + padding;
         mChatInput.Y = bottomRowBounds.Y;
         mVisuals.Add(mChatInput);

         #endregion Bottom row - back button, progress bar, and chat input


         // Process any backlogged messages (that may have been queued while changing screens) now that
         // all the UI components are available.
         while (mGameClient.HasMessage == true)
         {
            HandleMessage(mGameClient.Head);
         }
      }


      public override void Respond(InputEvent aInput)
      {
         if ((aInput.Op == InputEvent.Opcode.BACK) && (mChatInput?.IsFocused == false))
         {
            // If a back event came in AND the user isn't currently entering a chat message.

            OnBackClick(this, EventArgs.Empty);
         }
         else if (mAlertOverlay != null)
         {
            mAlertOverlay?.Respond(aInput);
         }
         else
         {
            mChatInput?.Respond(aInput);
            mBackButton?.Respond(aInput);
         }
      }

      public override void Draw(GameTime aGameTime)
      {
         mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix);

         foreach (IVisual visual in mVisuals)
         {
            visual.Draw();
         }

         mAlertOverlay?.Draw();

         mSpriteBatch.End();
      }

      #region Event handlers

      private void OnDisconnect(object aSender, EventArgs aArgs)
      {
         AlertThenExit("The connection to the server was lost without explanation.");
      }

      /// <summary>
      /// Invoked when enter is pressed while the chat input is focused.
      /// In other words, the user finished typing a message and hit enter to send it.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnChatMessageEntered(object aSender, EventArgs aArgs)
      {
         // If the user didn't actually enter a message, don't send anything.
         if (string.IsNullOrEmpty(mChatInput.Value) == true)
         {
            return;
         }

         mGameClient.Send(new ChatMessage(Context.LocalPlayerID, false, mChatInput.Value));

         mChatInput.IsFocused = true;
         mChatInput.Value = string.Empty;
      }

      private void OnBackClick(object aSender, EventArgs aArgs)
      {
         mGameClient.Disconnect();
         bool isSingleplayer = Context.IsSingleplayer;
         Context.Reset();
         if (isSingleplayer == true)
         {
            mWindow.LoadScreen(mServiceProvider?.GetService<MainMenuScreen>());
         }
         else
         {
            mWindow.LoadScreen(mServiceProvider?.GetService<MultiplayerScreen>());
         }
      }

      #endregion Event handlers


      #region Helper methods

      private void UpdateGameStatus()
      {
         if ((Context.WinningTeam == null) || (mTeamALabel == null) || (mTeamBLabel == null))
         {
            return;
         }

         mTeamALabel.Value = $"TEAM A - {(Context.WinningTeam == TeamEnum.A ? "WIN" : "LOSE")}";
         mTeamALabel.X = mTeamABounds.X + (mTeamABounds.Width / 2.0) - (mTeamALabel.Width / 2.0);

         mTeamBLabel.Value = $"TEAM B - {(Context.WinningTeam == TeamEnum.B ? "WIN" : "LOSE")}";
         mTeamBLabel.X = mTeamBBounds.X + (mTeamBBounds.Width / 2.0) - (mTeamBLabel.Width / 2.0);
      }

      private void UpdateScoreboard()
      {
         if ((mTeamATable == null) || (mTeamBTable == null))
         {
            return;
         }

         List<List<string>> teamARows = new List<List<string>>();
         List<List<string>> teamBRows = new List<List<string>>();
         foreach (Player player in Context.Players)
         {
            List<string> row = new List<string>
            {
               player.Name, player.Score.ToString()
            };

            if (player.Team == TeamEnum.A)
            {
               teamARows.Add(row);
            }
            else
            {
               teamBRows.Add(row);
            }
         }

         mTeamATable.Content = teamARows;
         mTeamBTable.Content = teamBRows;
      }

      private void AlertThenExit(string aMessage)
      {
         // If already alerting then exiting.
         if (mIsExiting == true)
         {
            return;
         }

         // Set a flag indicating we're in the process of exiting.
         mIsExiting = true;
         // Display a message to the user, then leave the screen when the message is acknowledged.
         mAlertOverlay = mServiceProvider.GetService<AlertModalWindow>();
         mAlertOverlay.Message = aMessage;
         mAlertOverlay.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
         {
            mAlertOverlay = null;
            OnBackClick(this, EventArgs.Empty);
         };
      }

      #endregion Helper methods


      #region Message handling

      private void OnMessage(object aSender, EventArgs aArgs)
      {
         while ((mGameClient.HasMessage == true) && (mWindow.IsTransitioning == false))
         {
            HandleMessage(mGameClient.Head);
         }
      }

      private void HandleMessage(Message aMessage)
      {
         if (mIsExiting == true)
         {
            return;
         }

         switch (aMessage.Op)
         {
            case Message.Opcode.CHAT:
               if (aMessage is ChatMessage chatMessage)
               {
                  Player player = Context.Players.FirstOrDefault(p => p.ID == chatMessage.PlayerID);
                  mChatLog.Add(player?.Name ?? "????", chatMessage.Message);
               }
               break;
            case Message.Opcode.COUNTDOWN:
               if (aMessage is CountdownMessage countdownMessage)
               {
                  if (mProgressBar != null)
                  {
                     mProgressBar.Percentage = countdownMessage.CountdownAsPercent;
                  }
               }
               break;
            case Message.Opcode.SERVER_SHUTDOWN:
               if (aMessage is ServerShutdownMessage serverShutdownMessage)
               {
                  if (string.IsNullOrEmpty(serverShutdownMessage.Reason) == false)
                  {
                     // If a reason for the server's shutdown was provided.

                     // Display a message including the reason before leaving the screen.
                     AlertThenExit($"The server shut down: {serverShutdownMessage.Reason}");
                  }
                  else
                  {
                     // If no reason for the shutdown was provided.

                     // In practice, this will only happen when smoothly exiting a single-player
                     // game and displaying a message to the user could only be confusing.
                     // Display nothing and immediately exit the screen.
                     OnBackClick(this, EventArgs.Empty);
                  }
               }
               break;
            case Message.Opcode.SERVER_STATE:
               if (aMessage is ServerStateMessage serverStateMessage)
               {
                  if (serverStateMessage.State == StateEnum.LOBBY)
                  {
                     // If the server is announcing its transition to the lobby state.

                     // Follow the server's lead by loading the lobby screen.
                     mWindow.LoadScreen(mServiceProvider?.GetService<LobbyScreen>());
                  }
                  else
                  {
                     // If the server is transitioning to any other state.

                     // It doesn't work like that. The server should only ever transition to the
                     // lobby state from the post-game state. If it's going somewhere else, we'll
                     // consider it modified and likely hostile.
                     AlertThenExit("The server acted irregularly. Disconnecting.");
                  }
               }
               break;
         }
      }

      #endregion Message handling
   }
}
