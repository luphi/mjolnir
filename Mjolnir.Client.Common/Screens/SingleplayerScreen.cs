﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.UI;
using Mjolnir.Common;
using Mjolnir.Common.Messages;
using Mjolnir.Common.Models;
using Mjolnir.Common.Network;
using Mjolnir.Common.Server;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NLog;
using System;

namespace Mjolnir.Client.Common.Screens
{
   /// <summary>
   /// This Screen acts as a mezzanine between the main menu and singleplayer lobby.
   /// It initializes by configuring a server on localhost, a game client to talk to it, and
   /// by finally transitioning to the lobby screen after a quick handshake between them.
   /// </summary>

   public class SingleplayerScreen : Screen
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly Window mWindow;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly Camera mCamera;
      private readonly SpriteBatch mSpriteBatch;
      private readonly GameClient mGameClient;
      private readonly Configuration mConfiguration;
      private readonly Text mLoadingText;
      private AlertModalWindow mAlertOverlay = null;

      public override string Name => "Singleplayer";

      public SingleplayerScreen(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mWindow = aServiceProvider.GetService<Window>();
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mCamera = aServiceProvider.GetService<Camera>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mGameClient = aServiceProvider.GetService<GameClient>();
         mConfiguration = aServiceProvider.GetService<Configuration>();
         mLoadingText = aServiceProvider.GetService<Text>();
      }

      public override void Dispose()
      {
         mGameClient.ConnectRefusalEvent -= OnConnectFailed;
         mGameClient.MessageEvent -= OnMessage;
      }

      public override void Initialize()
      {
         // Create a text object to display a loading message, centered and with a width
         // equal to half the display's.
         // Although the server boots up fairly quickly, it will likely take at least a
         // few seconds on even the fastest of computers.
         mLoadingText.Value = "LOADING...";
         mLoadingText.Width = mViewport.VirtualWidth * 0.5;
         mLoadingText.X = mViewport.VirtualCenter.X - (mLoadingText.Width / 2.0);
         mLoadingText.Y = mViewport.VirtualCenter.Y - (mLoadingText.Height / 2.0);

         // Create a server. This will behave exactly as any other server would, being the
         // object, but the default boostrap constructor used here will configure it for
         // usage on localhost.
         Context.ServerBootstrap = new ServerBootstrap(mServiceProvider.GetService<ContentWrapper>());

         // Try to start the server.
         // Start() returns the port on which the server is listening, or zero if there was a problem.
         if (Context.ServerBootstrap.Start() == 0)
         {
            // Starting the server failed so display an alert to tell the user and
            // transition back to the main menu when done.
            mAlertOverlay = mServiceProvider.GetService<AlertModalWindow>();
            mAlertOverlay.Message = "Failed to start a localhost server";
            mAlertOverlay.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
            {
               mAlertOverlay = null;
               mGameClient.Disconnect();
               Context.Reset();
               mWindow.LoadScreen(mServiceProvider.GetService<MainMenuScreen>());
            };
            return;
         }

         // Hook a couple game client events and then connect to the server. Once connected,
         // the server will send some message that perform a handshake and we'll respond as
         // those messages come in.
         mGameClient.ConnectRefusalEvent += OnConnectFailed;
         mGameClient.MessageEvent += OnMessage;
         mGameClient.Connect("127.0.0.1", Constants.DefaultServerPort);

         while (mGameClient.HasMessage == true)
         {
            HandleMessage(mGameClient.Head);
         }
      }

      public override void Draw(GameTime aGameTime)
      {
         mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix);

         if (mAlertOverlay != null)
         {
            mAlertOverlay.Draw();
         }
         else
         {
            mLoadingText.Draw();
         }

         mSpriteBatch.End();
      }

      /// <summary>
      /// Invoked when an attempt to connect to a server is unsuccessful.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnConnectFailed(object aSender, EventArgs aArgs)
      {
         // If the server started without issue, it's very unlikely that client will fail
         // to open the connection but it's a case that should be handled.
         // Display an alert to tell the user about this failure and transition back to
         // the main menu when done.
         mAlertOverlay = mServiceProvider.GetService<AlertModalWindow>();
         mAlertOverlay.Message = "Failed to connect to a singleplayer server on localhost";
         mAlertOverlay.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
         {
            mAlertOverlay = null;
            mGameClient.Disconnect();
            Context.Reset();
            mWindow.LoadScreen(mServiceProvider.GetService<MainMenuScreen>());
         };
      }

      /// <summary>
      /// Invoked when a message is received from the server.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnMessage(object aSender, EventArgs aArgs)
      {
         while (mGameClient.HasMessage == true)
         {
            HandleMessage(mGameClient.Head);
         }
      }

      /// <summary>
      /// 
      /// Handles only the messages relevant to this screen.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      protected void HandleMessage(Message aMessage)
      {
         switch (aMessage.Op)
         {
            case Message.Opcode.AHOY:
               // The server saw us and said "Hello." Send it the player's name as well
               // as (empty) join and admin passwords.
               mGameClient.Send(new AhoyHoyMessage(mConfiguration.PlayerName, string.Empty, string.Empty));
               break;
            case Message.Opcode.PLAYERS:
               if (aMessage is PlayersMessage playersMessage)
               {
                  // Add a new instance for each player.
                  // This being singleplayer, those players are all bots.
                  foreach (Player player in playersMessage.Players)
                  {
                     Context.Players.Add(new Player(player));
                  }
               }
               break;
            case Message.Opcode.WELCOME:
               if (aMessage is WelcomeMessage welcomeMessage)
               {
                  // The server says we can come in. With this message, it will tell us the
                  // ID assign to the player and whether or not the player is an admin.
                  // (For singleplayer, the player will be an admin.)
                  Context.LocalPlayerID = welcomeMessage.PlayerID;
                  Context.IsAdmin = welcomeMessage.IsAdmin;

                  // It's safe to assume the server is in its lobby state so transition to
                  // the lobby screen so the player can choose a mobile and such things.
                  mWindow.LoadScreenImmediate(mServiceProvider.GetService<LobbyScreen>());
               }
               break;
         }
      }
   }
}
