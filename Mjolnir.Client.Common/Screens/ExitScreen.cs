﻿using Microsoft.Xna.Framework;

namespace Mjolnir.Client.Common.Screens
{
   /// <summary>
   /// This Screen is transient in that it is not intended to be interacted with or seen.
   /// It initializes by exiting the game, and application by extension.
   /// </summary>
   public class ExitScreen : Screen
   {
      private readonly Game mGame;

      public override string Name => "Exit";

      public ExitScreen(Game aGame)
      {
         mGame = aGame;
      }

      public override void Initialize()
      {
         mGame.Exit();
      }
   }
}
