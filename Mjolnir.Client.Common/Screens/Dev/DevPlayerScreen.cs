﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.UI;
using Mjolnir.Client.Common.Views;
using Mjolnir.Common;
using Mjolnir.Common.Models;
using NLog;
using System;
using System.Collections.Generic;

namespace Mjolnir.Client.Common.Screens
{
   public class DevPlayerScreen : Screen
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly Window mWindow;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly Camera mCamera;
      private readonly SpriteBatch mSpriteBatch;
      private readonly List<TextButton> mButtons = new List<TextButton>();
      private readonly PlayerView mPlayerView;
      private readonly Player mPlayer;

      public override string Name => "Dev Player";

      public DevPlayerScreen(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mWindow = aServiceProvider.GetService<Window>();
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mCamera = aServiceProvider.GetService<Camera>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mPlayerView = aServiceProvider.GetService<PlayerView>();
         mPlayer = new Player(0, "luphi", true)
         {
            MobileID = MobileEnum.LARVA
         };
      }

      public override void Dispose()
      {
         mPlayerView?.Dispose();
      }

      public override void Initialize()
      {
      }

      public override void LoadContent()
      {
         mPlayerView.Model = mPlayer;

         mCamera.LookAt(mPlayerView);

         double padding = mViewport.VirtualHeight * 0.01;
         Rectangle paddedBounds = new Rectangle((int)padding,
                                                (int)padding,
                                                (int)(mViewport.VirtualWidth - (2 * padding)),
                                                (int)(mViewport.VirtualHeight - (2 * padding)));
         double ySpacing = paddedBounds.Height / 4.0;

         // Create and position the buttons that go on the left side of the screen.

         TextButton button = mServiceProvider.GetService<TextButton>();
         button.Text = "AIM -";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            mPlayer.AimInRadians -= Math.PI / 32.0;
            if (mPlayer.AimInRadians < mPlayer.MinAimInRadians)
            {
               mPlayer.AimInRadians = mPlayer.MinAimInRadians;
            }
         };
         button.X = paddedBounds.X;
         button.Y = paddedBounds.Y + (1.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "ROTATE <-";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            mPlayer.Pitch -= Math.PI / 16.0;
         };
         button.X = paddedBounds.X;
         button.Y = paddedBounds.Y + (3.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "CYCLE";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            MobileEnum nextID = mPlayer.MobileID.Next();
            if (nextID == MobileEnum.NONE)
            {
               nextID = nextID.Next();
            }
            mPlayer.MobileID = nextID;
            mLog.Debug($"Cycled to mobile {mPlayer.Mobile.ID}");
            Reset();
            mCamera.LookAt(mPlayerView);
         };
         button.X = paddedBounds.X;
         button.Y = paddedBounds.Y + (5.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "MIRROR";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            mPlayer.Direction = mPlayer.Direction == Direction.LEFT ? Direction.RIGHT : Direction.LEFT;
         };
         button.X = paddedBounds.X;
         button.Y = paddedBounds.Y + (7.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }

         // Create and position the buttons that go on the right side of the screen.

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "AIM +";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            mPlayer.AimInRadians += Math.PI / 32.0;
            if (mPlayer.AimInRadians > mPlayer.MaxAimInRadians)
            {

               mPlayer.AimInRadians = mPlayer.MaxAimInRadians;
            }
         };
         button.X = paddedBounds.X + paddedBounds.Width - button.Width;
         button.Y = paddedBounds.Y + (1.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "ROTATE ->";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            mPlayer.Pitch += Math.PI / 16.0;
         };
         button.X = paddedBounds.X + paddedBounds.Width - button.Width;
         button.Y = paddedBounds.Y + (3.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "RESET";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            Reset();
         };
         button.X = paddedBounds.X + paddedBounds.Width - button.Width;
         button.Y = paddedBounds.Y + (5.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "DEBUG";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            mPlayerView.IsDebugEnabled = !mPlayerView.IsDebugEnabled;
         };
         button.X = paddedBounds.X + paddedBounds.Width - button.Width;
         button.Y = paddedBounds.Y + (7.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }
      }

      public override void Respond(InputEvent aInput)
      {
         if (aInput.Op == InputEvent.Opcode.BACK)
         {
            mWindow.LoadScreen(mServiceProvider?.GetService<DevMenuScreen>());
         }

         lock (mButtons)
         {
            foreach (TextButton button in mButtons)
            {
               button.Respond(aInput);
            }
         }
      }

      public override void Draw(GameTime aGameTime)
      {
         mSpriteBatch.Begin(transformMatrix: mCamera.WorldViewMatrix);
         mPlayerView?.Draw();
         mSpriteBatch.End();

         mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix);
         lock (mButtons)
         {
            foreach (TextButton button in mButtons)
            {
               button.Draw();
            }
         }
         mSpriteBatch.End();
      }

      private void Reset()
      {
         mPlayer.Pitch = Vector.UpInRadians;
         mPlayer.AimInRadians = Player.CenterAimInRadians;
         mPlayer.Direction = Direction.LEFT;
      }
   }
}
