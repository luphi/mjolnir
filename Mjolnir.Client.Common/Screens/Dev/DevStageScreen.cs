﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.UI;
using Mjolnir.Client.Common.Views;
using Mjolnir.Common;
using Mjolnir.Common.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mjolnir.Client.Common.Screens
{
   public class DevStageScreen : Screen
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly Window mWindow;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly Camera mCamera;
      private readonly ContentWrapper mContentWrapper;
      private readonly SpriteBatch mSpriteBatch;
      private readonly StageView mStageView;
      private readonly List<TextButton> mButtons = new List<TextButton>();
      private Stage mStage = null;
      private Text mHelpText = null;
      private Keys mHeldKey = Keys.None;
      private bool mIsSwiping = false;

      public override string Name => "Dev Stage";

      public DevStageScreen(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mWindow = aServiceProvider.GetService<Window>();
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mCamera = aServiceProvider.GetService<Camera>();
         mContentWrapper = aServiceProvider.GetService<ContentWrapper>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mStageView = aServiceProvider.GetService<StageView>();
      }

      public override void Dispose()
      {
         mStageView.Dispose();
      }

      public override void Initialize()
      {
      }

      public override void LoadContent()
      {
         mStage = new Stage(StageEnum.DEV);
         mStage.Load(mContentWrapper);
         mStageView.Model = mStage;
         // Restrain the camera to within the bounds defiend by the stage view.
         mCamera.Bounds = mStageView.CameraBounds;
         // Position the camera such that the bottom edge of the viewport snaps to the bottom
         // edge of the stage.
         Rectangle foregroundBounds = mStageView.ForegroundBounds;
         mCamera.LookAt(new Vector(foregroundBounds.X + (foregroundBounds.Width / 2.0),
                                   foregroundBounds.Y + foregroundBounds.Height - (mViewport.VirtualHeight / 2.0)));

         double padding = mViewport.VirtualHeight * 0.01;
         Rectangle paddedBounds = new Rectangle((int)padding,
                                                (int)padding,
                                                (int)(mViewport.VirtualWidth - (2 * padding)),
                                                (int)(mViewport.VirtualHeight - (2 * padding)));

         mHelpText = mServiceProvider.GetService<Text>();
         mHelpText.Value = "ARROWS/SWIPING = TRANSLATE, CLICK/TAP = DIG";
         mHelpText.Width = paddedBounds.Width * 0.75;
         mHelpText.X = paddedBounds.X + (paddedBounds.Width / 2.0) - (mHelpText.Width / 2.0);
         mHelpText.Y = paddedBounds.Y;

         TextButton button = mServiceProvider.GetService<TextButton>();
         button.Text = "CYCLE";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            StageEnum nextID = mStage.ID.Next();
            if (nextID == StageEnum.NONE)
            {
               nextID = nextID.Next();
            }
            mStage = new Stage(nextID);
            mLog.Debug($"Cycled to stage {mStage.ID}");
            mStage.Load(mContentWrapper);
            mStageView.Model = mStage;
            mCamera.Bounds = mStageView.CameraBounds;
            Rectangle foregroundBounds = mStageView.ForegroundBounds;
            mCamera.LookAt(new Vector(foregroundBounds.X + (foregroundBounds.Width / 2.0),
                                      foregroundBounds.Y + foregroundBounds.Height - (mViewport.VirtualHeight / 2.0)));
         };
         button.Height = paddedBounds.Height / 10.0;
         button.X = paddedBounds.X + (paddedBounds.Width / 4.0) - (button.Width / 2.0);
         button.Y = paddedBounds.Y + paddedBounds.Height - button.Height;
         lock (mButtons)
         {
            mButtons.Add(button);
         }

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "RESET";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            mStage?.Reset();
         };
         button.Height = paddedBounds.Height / 10.0;
         button.X = paddedBounds.X + (3.0 * paddedBounds.Width / 4.0) - (button.Width / 2.0);
         button.Y = paddedBounds.Y + paddedBounds.Height - button.Height;
         lock (mButtons)
         {
            mButtons.Add(button);
         }
      }

      public override void Respond(InputEvent aInput)
      {
         if (aInput.Op == InputEvent.Opcode.BACK)
         {
            mWindow.LoadScreen(mServiceProvider?.GetService<DevMenuScreen>());
         }

         if (aInput.Op == InputEvent.Opcode.KEY_DOWN)
         {
            switch (aInput.Key)
            {
               case Keys.Down:
               case Keys.Up:
               case Keys.Right:
               case Keys.Left:
                  mHeldKey = (Keys)aInput.Key;
                  break;
            }
         }

         if ((aInput.Op == InputEvent.Opcode.KEY_UP) && (aInput.Key == mHeldKey))
         {
            mHeldKey = Keys.None;
         }

         if (aInput.Op == InputEvent.Opcode.SWIPE)
         {
            mIsSwiping = true;
            mCamera.Translate(-aInput.Delta);
         }

         if (aInput.Op == InputEvent.Opcode.HOVER)
         {
            lock (mButtons)
            {
               foreach (TextButton button in mButtons)
               {
                  button.Respond(aInput);
               }
            }
         }

         if (aInput.Op == InputEvent.Opcode.MOUSE_DOWN)
         {
            bool isConsumed = false;
            lock (mButtons)
            {
               foreach (TextButton button in mButtons)
               {
                  if (button.Bounds.Contains(aInput.Position.ToXna()))
                  {
                     button.Respond(aInput);
                     isConsumed = true;
                  }
               }
            }

            if (isConsumed == false)
            {
               // Spawn a task that will wait very briefly and check to see if the mouse down event
               // resulted in a swipe. We do this to avoid digging holes unnecessarily as swiping
               // on a touch device is always preceded by a mouse down (finger press) event.
               Task.Run(async () =>
               {
                  await Task.Delay(100);
                  // If not swiping.
                  if (mIsSwiping == false)
                  {
                     // Determine the internal coordinate equivalent to the click's position
                     mStage.Dig(mCamera.VirtualToWorld(aInput.Position), 100.0, 75.0);
                  }
               });
            }
         }
         else if (aInput.Op == InputEvent.Opcode.MOUSE_UP)
         {
            mIsSwiping = false;
         }
      }

      public override void Draw(GameTime aGameTime)
      {
         if (mHeldKey != Keys.None)
         {
            double distance = 1000.0 * aGameTime.ElapsedGameTime.TotalSeconds;
            switch (mHeldKey)
            {
               case Keys.Down:
                  mCamera.Translate(new Vector(0.0, distance));
                  break;
               case Keys.Up:
                  mCamera.Translate(new Vector(0.0, -distance));
                  break;
               case Keys.Right:
                  mCamera.Translate(new Vector(distance, 0.0));
                  break;
               case Keys.Left:
                  mCamera.Translate(new Vector(-distance, 0.0));
                  break;
            }
         }

         mSpriteBatch.Begin(transformMatrix: mCamera.ParallaxViewMatrix);
         mStageView?.DrawBackground();
         mSpriteBatch.End();

         mSpriteBatch.Begin(transformMatrix: mCamera.WorldViewMatrix);
         mStageView?.DrawForeground();
         mSpriteBatch.End();

         mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix);
         mHelpText?.Draw();
         lock (mButtons)
         {
            foreach (TextButton button in mButtons)
            {
               button.Draw();
            }
         }
         mSpriteBatch.End();
      }
   }
}
