﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.UI;
using Mjolnir.Client.Common.Views;
using Mjolnir.Common;
using Mjolnir.Common.Models;
using Mjolnir.Common.Physics;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mjolnir.Client.Common.Screens
{
   /// <summary>
   /// For developing and testing the "player walking on the stage" method, and related
   /// things like calculating stage normals and applying them to player views.
   /// </summary>
   public class DevWalkScreen : Screen
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly Window mWindow;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly Camera mCamera;
      private readonly ContentWrapper mContentWrapper;
      private readonly SpriteBatch mSpriteBatch;
      private readonly List<TextButton> mButtons = new List<TextButton>();
      private readonly Stage mStage;
      private readonly World mWorld;
      private readonly Player mPlayer;
      private StageView mStageView = null;
      private PlayerView mPlayerView = null;
      private int mSpawnPointIndex = 0;
      private bool mIsWalkHeld = false;

      public override string Name => "Dev Walk";

      public DevWalkScreen(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mWindow = aServiceProvider.GetService<Window>();
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mCamera = aServiceProvider.GetService<Camera>();
         mContentWrapper = aServiceProvider.GetService<ContentWrapper>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();

         mStage = new Stage(StageEnum.DEV);
         mWorld = new World(mStage);
         mPlayer = new Player(0, "luphi", true)
         {
            MobileID = MobileEnum.LARVA
         };
         mWorld.Add(mPlayer);
      }

      public override void Dispose()
      {
         mWorld.LandingEvent -= OnLandingEvent;
         mWorld.DeathEvent -= OnDeathEvent;
         mWorld.HaltEvent -= OnHaltedEvent;

         mStageView?.Dispose();
         mPlayerView?.Dispose();
      }

      public override void Initialize()
      {
         mWorld.LandingEvent += OnLandingEvent;
         mWorld.DeathEvent += OnDeathEvent;
         mWorld.HaltEvent += OnHaltedEvent;
      }

      public override void LoadContent()
      {
         mStage.Load(mContentWrapper);
         mStageView = mServiceProvider.GetService<StageView>();
         mStageView.Model = mStage;

         mPlayerView = mServiceProvider.GetService<PlayerView>();
         mPlayerView.Model = mPlayer;

         Spawn(false);

         mCamera.Bounds = mStageView.CameraBounds;

         double padding = mViewport.VirtualHeight * 0.01;
         Rectangle paddedBounds = new Rectangle((int)padding,
                                                (int)padding,
                                                (int)(mViewport.VirtualWidth - (2.0 * padding)),
                                                (int)(mViewport.VirtualHeight - (2.0 * padding)));

         // Create and position the buttons that go along the top edge of the screen.

         TextButton button = mServiceProvider.GetService<TextButton>();
         button.Text = "<-";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            mIsWalkHeld = true;
            DoWalk(Direction.LEFT);
         };
         button.Height = Text.DefaultHeight * 2.0;
         button.X = paddedBounds.X + (1.0 * paddedBounds.Width / 8.0) - (button.Width / 2.0);
         button.Y = paddedBounds.Y + (1.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "->";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            mIsWalkHeld = true;
            DoWalk(Direction.RIGHT);
         };
         button.Height = Text.DefaultHeight * 2.0;
         button.X = paddedBounds.X + (7.0 * paddedBounds.Width / 8.0) - (button.Width / 2.0);
         button.Y = paddedBounds.Y + (1.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }

         // Create and position the buttons that go along the bottom edge of the screen.

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "DIG";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            Vector point = mPlayer.Footing;
            point.Y += 1.0;
            mStage.Dig(point, 100.0, 75.0);
            mWorld.Start();
         };
         button.X = paddedBounds.X + (1.0 * paddedBounds.Width / 5.0) - (button.Width / 2.0);
         button.Y = paddedBounds.Y + (7.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "RESPAWN";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            mStage.Reset();
            Spawn(true);
         };
         button.X = paddedBounds.X + (2.0 * paddedBounds.Width / 5.0) - (button.Width / 2.0);
         button.Y = paddedBounds.Y + (7.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "DEBUG";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            if (mPlayerView != null)
            {
               mPlayerView.IsDebugEnabled = !mPlayerView.IsDebugEnabled;
            }
         };
         button.X = paddedBounds.X + (3.0 * paddedBounds.Width / 5.0) - (button.Width / 2.0);
         button.Y = paddedBounds.Y + (7.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "CYCLE";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            MobileEnum nextID = mPlayer.MobileID.Next();
            if (nextID == MobileEnum.NONE)
            {
               nextID = nextID.Next();
            }
            mPlayer.MobileID = nextID;
            mLog.Debug($"Cycled to mobile {mPlayer.Mobile.ID}");
            Spawn(false);
         };
         button.X = paddedBounds.X + (4.0 * paddedBounds.Width / 5.0) - (button.Width / 2.0);
         button.Y = paddedBounds.Y + (7.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         lock (mButtons)
         {
            mButtons.Add(button);
         }
      }

      public override void Respond(InputEvent aInput)
      {
         if (aInput.Op == InputEvent.Opcode.BACK)
         {
            mWindow.LoadScreen(mServiceProvider?.GetService<DevMenuScreen>());
         }

         if (aInput.Op == InputEvent.Opcode.MOUSE_UP)
         {
            mIsWalkHeld = false;
         }

         if (aInput.Op == InputEvent.Opcode.SWIPE)
         {
            mCamera.Translate(aInput.Delta);
         }

         lock (mButtons)
         {
            foreach (TextButton button in mButtons)
            {
               button.Respond(aInput);
            }
         }
      }

      public override void Draw(GameTime aGameTime)
      {
         if (mPlayerView != null)
         {
            mCamera.LookAt(mPlayerView);
         }

         // Parallaxed draw.
         mSpriteBatch.Begin(transformMatrix: mCamera.ParallaxViewMatrix);
         mStageView?.DrawBackground();
         mSpriteBatch.End();

         // World draw.
         mSpriteBatch.Begin(transformMatrix: mCamera.WorldViewMatrix);
         mStageView?.DrawForeground();
         mPlayerView?.Draw();
         mSpriteBatch.End();

         // UI draw.
         mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix);
         lock (mButtons)
         {
            foreach (TextButton button in mButtons)
            {
               button.Draw();
            }
         }
         mSpriteBatch.End();
      }

      private void DoWalk(Direction aDirection)
      {
         if (mPlayer.Direction != aDirection)
         {
            mPlayer.Direction = aDirection;
            Task.Run(async () =>
            {
               await Task.Delay(250);
               if ((mIsWalkHeld == true) && (mPlayer.Direction == aDirection) &&
                   (mWorld.Walk(mPlayer) == true))
               {
                  mWorld.Start();
               }
            });
         }
         else if (mWorld.Walk(mPlayer) == true)
         {
            mWorld.Start();
         }
      }

      private void Spawn(bool aShouldCycleSpawnPoint)
      {
         mWorld.Stop();

         if (aShouldCycleSpawnPoint)
         {
            mSpawnPointIndex = (mSpawnPointIndex + 1) % 8;
         }

         Vector spawnPoint;
         if (mSpawnPointIndex < 4)
         {
            spawnPoint = mStage.SpawnPointsA[mSpawnPointIndex];
         }
         else
         {
            spawnPoint = mStage.SpawnPointsB[mSpawnPointIndex - 4];
         }

         mPlayer.X = spawnPoint.X - mPlayer.Radius;
         mPlayer.Y = spawnPoint.Y - (mPlayer.Radius * 2.0);
         mPlayer.Pitch = mStage.NormalAt(mPlayer.Footing).AngleInRadians;
         mPlayer.IsWalking = false;
         mPlayer.IsSettled = true;

         mWorld.Start();
      }

      private void OnLandingEvent(object aSender, Landing aLanding)
      {
         mLog.Debug($"{aLanding.Player.Name} landed at {aLanding.Point}");
      }

      private void OnDeathEvent(object aSender, Player aBody)
      {
         if (aBody is Player player)
         {
            mLog.Debug($"Lost player {player.Name}");
            Spawn(false);
         }
      }

      private void OnHaltedEvent(object aSender, Body aBody)
      {
         if ((mIsWalkHeld == true) && (mWorld.Walk(aBody) == true))
         {
            mWorld.Start();
         }
      }
   }
}
