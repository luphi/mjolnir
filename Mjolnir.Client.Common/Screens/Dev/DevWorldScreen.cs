﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.UI;
using Mjolnir.Client.Common.Views;
using Mjolnir.Common;
using Mjolnir.Common.Collections;
using Mjolnir.Common.Models;
using Mjolnir.Common.Physics;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Mjolnir.Client.Common.Screens
{
   /// <summary>
   /// For developing and testing the functions of the World and bodies within it.
   /// To a lesser extent, this is also does the same for related views (e.g. ProjectileView)
   /// that are intended to observe and visually represent their bodies.
   /// </summary>
   public class DevWorldScreen : Screen
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly Window mWindow;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly Camera mCamera;
      private readonly ContentWrapper mContentWrapper;
      private readonly SpriteBatch mSpriteBatch;
      private readonly ConcurrentList<TextButton> mButtons = new ConcurrentList<TextButton>();
      private readonly WindVane mWindVane;
      private readonly StageView mStageView;
      private readonly PlayerView mPlayerView1;
      private readonly PlayerView mPlayerView2;
      private readonly ConcurrentList<ProjectileView> mProjectileViews = new ConcurrentList<ProjectileView>();
      private readonly SatelliteView mThorView;
      private readonly World mWorld;
      private readonly Stage mStage;
      private Player mPlayer1;
      private Player mPlayer2;
      private IVisual mLookAt = null;
      private CancellationTokenSource mTokenSource = null;
      private TextButton mStartStopButton = null;

      public override string Name => "Dev World";

      public DevWorldScreen(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mWindow = aServiceProvider.GetService<Window>();
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mCamera = aServiceProvider.GetService<Camera>();
         mContentWrapper = aServiceProvider.GetService<ContentWrapper>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mWindVane = aServiceProvider.GetService<WindVane>();
         mStageView = aServiceProvider.GetService<StageView>();
         mPlayerView1 = aServiceProvider.GetService<PlayerView>();
         mPlayerView2 = aServiceProvider.GetService<PlayerView>();
         mThorView = aServiceProvider.GetService<SatelliteView>();

         mStage = new Stage(StageEnum.PIDGIN_ROOT);
         mPlayer1 = new Player(/* aID = */ 0, /* aName = */ "luphi", /* aIsAdmin = */ true)
         {
            MobileID = MobileEnum.AKUDA,
            ShotID = ShotEnum.SHOT_1,
            Team = TeamEnum.A
         };
         mPlayer2 = new Player(1, "ihpul", false)
         {
            MobileID = MobileEnum.WIZARD,
            Team = TeamEnum.B
         };

         Random random = new Random();
         mWorld = new World(mStage)
         {
            //Wind = Vector.FromRadians(random.NextDouble() * 2.0 * Math.PI) *
            //       Constants.MaxWindAcceleration * (1.0 - random.NextDouble())
            Wind = new Vector(15.0, -15.0)
         };
         mWorld.Add(mPlayer1);
         mWorld.Add(mPlayer2);
      }

      public override void Dispose()
      {
         mWorld.LandingEvent -= OnLandingEvent;
         mWorld.DeathEvent -= OnDeathEvent;
         mWorld.ShootingEvent -= OnShootingEvent;
         mWorld.SettledEvent -= OnSettledEvent;

         mPlayerView1.Dispose();
         mPlayerView2.Dispose();
         foreach (ProjectileView view in mProjectileViews)
         {
            view.Dispose();
         }
         mThorView.Dispose();
      }

      public override void Initialize()
      {
         mWorld.LandingEvent += OnLandingEvent;
         mWorld.DeathEvent += OnDeathEvent;
         mWorld.ShootingEvent += OnShootingEvent;
         mWorld.SettledEvent += OnSettledEvent;
      }

      public override void LoadContent()
      {
         mStage.Load(mContentWrapper);
         mStageView.Model = mStage;

         double padding = mViewport.VirtualHeight * 0.01;
         Rectangle paddedBounds = new Rectangle((int)padding,
                                                (int)padding,
                                                (int)(mViewport.VirtualWidth - (2.0 * padding)),
                                                (int)(mViewport.VirtualHeight - (2.0 * padding)));

         mWindVane.Height = mViewport.VirtualHeight / 10.0;
         mWindVane.X = paddedBounds.X + (paddedBounds.Width / 2.0) - (mWindVane.Width / 2.0);
         mWindVane.Y = paddedBounds.Y;
         mWindVane.Wind = mWorld.Wind;

         mThorView.Model = mWorld.Thor;

         mStartStopButton = mServiceProvider.GetService<TextButton>();
         mStartStopButton.Text = "START";
         mStartStopButton.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            if (mTokenSource == null)
            {
               Reset();
               mStartStopButton.Text = "STOP";
               mTokenSource = new CancellationTokenSource();
               CancellationToken token = mTokenSource.Token;
               Task.Run(() => Run(token));
            }
            else
            {
               mTokenSource?.Cancel();
               mTokenSource = null;
               mStartStopButton.Text = "START";
            }
         };
         // Place the "START"/"STOP" button in the top left of the screen.
         mStartStopButton.X = paddedBounds.X;
         mStartStopButton.Y = paddedBounds.Y + (1.0 * paddedBounds.Height / 8.0) - (mStartStopButton.Height / 2.0);
         mButtons.Add(mStartStopButton);

         TextButton button = mServiceProvider.GetService<TextButton>();
         button.Text = "DEBUG";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            mPlayerView1.IsDebugEnabled = !mPlayerView1.IsDebugEnabled;
            mPlayerView2.IsDebugEnabled = !mPlayerView2.IsDebugEnabled;
            foreach (ProjectileView projectileView in mProjectileViews)
            {
               projectileView.IsDebugEnabled = mPlayerView1.IsDebugEnabled;
            }
         };
         button.X = paddedBounds.X;
         button.Y = paddedBounds.Y + (7.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         mButtons.Add(button);

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "CYCLE MOBILE";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            MobileEnum nextID = mPlayer1.MobileID.Next();
            if (nextID == MobileEnum.NONE)
            {
               nextID = nextID.Next();
            }
            mPlayer1.MobileID = nextID;
            mLog.Debug($"Cycled to mobile {mPlayer1.Mobile.ID}");
            Spawn(mPlayer1, mStage.SpawnPointsA[0]);
            mCamera.LookAt(mPlayerView1);
         };
         // Place the "CYCLE MOBILE" button in the top right of the screen.
         button.X = paddedBounds.X + paddedBounds.Width - button.Width;
         button.Y = paddedBounds.Y + (1.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         mButtons.Add(button);

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "CYCLE SHOT";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            mPlayer1.ShotID = mPlayer1.ShotID switch
            {
               ShotEnum.SHOT_2 => ShotEnum.SPECIAL_SHOT,
               ShotEnum.SPECIAL_SHOT => ShotEnum.SHOT_1,
               _ => ShotEnum.SHOT_2
            };
            mLog.Debug($"Cycled to shot {mPlayer1.ShotID}");
         };
         // Place the "CYCLE SHOT" button in the bottom right of the screen.
         button.X = paddedBounds.X + paddedBounds.Width - button.Width;
         button.Y = paddedBounds.Y + (7.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         mButtons.Add(button);

         Spawn(mPlayer1, mStage.SpawnPointsA[0]);
         Spawn(mPlayer2, mStage.SpawnPointsB[0]);

         mCamera.Bounds = mStageView.CameraBounds;
         mPlayerView1.Model = mPlayer1;
         mPlayerView2.Model = mPlayer2;
         mLookAt = mPlayerView1;
      }

      public override void Respond(InputEvent aInput)
      {
         if (aInput.Op == InputEvent.Opcode.BACK)
         {
            mWindow.LoadScreen(mServiceProvider?.GetService<DevMenuScreen>());
            return;
         }

         foreach (TextButton button in mButtons)
         {
            button.Respond(aInput);
         }
      }

      public override void Draw(GameTime aGameTime)
      {
         if (mLookAt != null)
         {
            mCamera.LookAt(mLookAt);
         }

         // Parallaxed draw.
         mSpriteBatch.Begin(transformMatrix: mCamera.ParallaxViewMatrix);
         mStageView.DrawBackground();
         mSpriteBatch.End();

         // World draw.
         mSpriteBatch.Begin(transformMatrix: mCamera.WorldViewMatrix);
         mStageView.DrawForeground();
         mPlayerView1.Draw();
         mPlayerView2.Draw();
         foreach (ProjectileView view in mProjectileViews)
         {
            view.Draw();
         }
         mThorView.Draw();
         mSpriteBatch.End();

         // UI draw.
         mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix);
         foreach (TextButton button in mButtons)
         {
            button.Draw();
         }
         mWindVane.Draw();
         mSpriteBatch.End();
      }

      private void OnLandingEvent(object aSender, Landing aLanding)
      {
         mLog.Debug($"{aLanding.Player.Name} landed at {aLanding.Point} from " +
                    $"{(ReferenceEquals(aSender, mWorld) == true ? "original" : "clone")} world");
      }

      private void OnDeathEvent(object aSender, Player aPlayer)
      {
         mLog.Debug($"Player {aPlayer.Name} died in " +
                    $"{(ReferenceEquals(aSender, mWorld) == true ? "original" : "clone")} world");
      }

      private void OnShootingEvent(object aSender, Shooting aShooting)
      {
         mLog.Debug($"Shooting at {aShooting.Center} in " +
                    $"{(ReferenceEquals(aSender, mWorld) == true ? "original" : "clone")} world");

         if (aShooting.Behavior == ProjectileBehaviorEnum.THOR)
         {
            mThorView.Fire(aShooting.Center);
         }
         else if (aShooting.Behavior == ProjectileBehaviorEnum.SATELLITE)
         {
            mPlayerView1.SatelliteView.Fire(aShooting.Center);
         }
      }

      private void OnSettledEvent(object aSender, Events aEvents)
      {
         mLog.Debug($"{(ReferenceEquals(aSender, mWorld) == true ? "Original" : "Clone")} world settled");

         mLookAt = null; // Keep the camera still for now.

         List<ProjectileView> toRemove = new List<ProjectileView>();
         foreach (ProjectileView view in mProjectileViews)
         {
            if (view.Model?.IsDone ?? true)
            {
               toRemove.Add(view);
            }
         }
         foreach (ProjectileView view in toRemove)
         {
            view.Dispose();
            mProjectileViews.Remove(view);
         }
      }

      private void Reset()
      {
         mPlayer1.AimInRadians = Player.CenterAimInRadians;

         // Stop the task, if it's running.
         mTokenSource?.Cancel();
         mTokenSource = null;
         if (mStartStopButton != null)
         {
            mStartStopButton.Text = "START";
         }

         // Stop the world and clear lists.
         mWorld.Stop();
         mWorld.Clear();
         mWorld.Thor.Reset();
         mStage.Reset();
         foreach (ProjectileView view in mProjectileViews)
         {
            view.Dispose();
         }
         mProjectileViews.Clear();

         // Reset the players to their initial states.
         mPlayer1.HP = mPlayer1.Mobile?.HP ?? 0;
         mPlayer2.HP = mPlayer2.Mobile?.HP ?? 0;
         Spawn(mPlayer1, mStage.SpawnPointsA[0]);
         Spawn(mPlayer2, mStage.SpawnPointsB[0]);
         mWorld.Add(mPlayer1);
         mWorld.Add(mPlayer2);

         mLookAt = mPlayerView1;
      }

      private void Spawn(Player aPlayer, Vector aSpawnPoint)
      {
         aPlayer.X = aSpawnPoint.X - aPlayer.Radius;
         aPlayer.Y = aSpawnPoint.Y - (2.0 * aPlayer.Radius);
         aPlayer.Pitch = mStage.NormalAt(aPlayer.Footing).AngleInRadians;
      }

      private async Task Run(CancellationToken aToken)
      {
         uint phase = 0;
         World clone = null;
         bool isDone = false;

         while ((aToken.IsCancellationRequested == false) && (isDone == false))
         {
            switch (phase)
            {
               case 0:
                  ///////////////////////////////////////////////////////////////////////////////////////////////
                  /// Spawn, turn, and look at player 1 for a second.
                  mLookAt = mPlayerView1;
                  Spawn(mPlayer1, mStage.SpawnPointsA[0]);
                  mPlayer1.Direction = Direction.RIGHT;
                  mPlayer1.AimInRadians = Player.CenterAimInRadians;
                  await Task.Delay(1000, aToken);
                  break;

               case 1:
                  ///////////////////////////////////////////////////////////////////////////////////////////////
                  /// Fire at the default aim and watch the first or only projectile.
                  if (mWorld.Fire(mPlayer1, 0.75, out List<Projectile> projectiles) == true)
                  {
                     foreach (Projectile projectile in projectiles)
                     {
                        ProjectileView view = mServiceProvider.GetService<ProjectileView>();
                        view.Model = projectile;
                        view.IsDebugEnabled = mPlayerView1.IsDebugEnabled;
                        mProjectileViews.Add(view);
                     }

                     mLookAt = mProjectileViews.LastOrDefault();

                     mWorld.Start();
                  }
                  await Task.Delay(750, aToken);
                  break;

               case 2:
                  mWorld.Pause();
                  await Task.Delay(1500, aToken);
                  break;

               case 3:
                  mWorld.Resume();
                  await Task.Delay(3000, aToken);
                  break;

               case 4:
                  ///////////////////////////////////////////////////////////////////////////////////////////////
                  /// Spawn again, turn, and look at player 1 for a second.
                  mLookAt = mPlayerView1;
                  Spawn(mPlayer1, mStage.SpawnPointsB[3]);
                  mPlayer1.Direction = Direction.LEFT;
                  await Task.Delay(1000, aToken);
                  break;

               case 5:
                  ///////////////////////////////////////////////////////////////////////////////////////////////
                  /// Pre-turn check. The vast majority of shots do not use this.
                  await DoAllPreTurnsAsync(aToken);
                  break;

               case 6:
                  ///////////////////////////////////////////////////////////////////////////////////////////////
                  /// Now that the world is settled and idle, create a clone that will mirror all tests going
                  /// forward in the hopes of receiving identical results.
                  try
                  {
                     clone = mWorld.Clone();
                     clone.LandingEvent += OnLandingEvent;
                     clone.DeathEvent += OnDeathEvent;
                     clone.ShootingEvent += OnShootingEvent;
                     clone.SettledEvent += OnSettledEvent;

                     // Reassign or recreate the views so we can see the new world. These lines aren't
                     // safe but, if the cloned World is truly identical, there will be no problems.
                     // This is a development screen after all.
                     mPlayer1 = mWorld.Players.First();
                     mPlayer2 = mWorld.Players.Last();
                     mPlayerView1.Model = mPlayer1;
                     mPlayerView2.Model = mPlayer2;
                     List<Projectile> newProjectiles = mWorld.Projectiles.ToList();
                     for (int i = 0; i < newProjectiles.Count; i++)
                     {
                        mProjectileViews[i].Model = newProjectiles[i];
                     }
                  }
                  catch (Exception e)
                  {
                     mLog.Error(e);
                  }
                  break;

               case 7:
                  ///////////////////////////////////////////////////////////////////////////////////////////////
                  /// Fire to the left.
                  if ((mWorld.Fire(mPlayer1, 1.0, out projectiles) == true) &&
                      (clone.Fire(mPlayer1, 1.0, out _) == true))
                  {
                     foreach (Projectile projectile in projectiles)
                     {
                        ProjectileView view = mServiceProvider.GetService<ProjectileView>();
                        view.Model = projectile;
                        view.IsDebugEnabled = mPlayerView1.IsDebugEnabled;
                        mProjectileViews.Add(view);
                     }

                     mLookAt = mProjectileViews.LastOrDefault();

                     mWorld.Start();
                     clone.Start();
                  }
                  await Task.Delay(4000, aToken);
                  break;

               case 8:
                  ///////////////////////////////////////////////////////////////////////////////////////////////
                  /// Turn to the right.
                  mLookAt = mPlayerView1;
                  mPlayer1.Direction = Direction.RIGHT;
                  break;

               case 9:
                  ///////////////////////////////////////////////////////////////////////////////////////////////
                  /// Fire to the right.
                  if ((mWorld.Fire(mPlayer1, 0.85, out projectiles) == true) &&
                      (clone.Fire(mPlayer1, 0.85, out _) == true))
                  {
                     foreach (Projectile projectile in projectiles)
                     {
                        ProjectileView view = mServiceProvider.GetService<ProjectileView>();
                        view.Model = projectile;
                        view.IsDebugEnabled = mPlayerView1.IsDebugEnabled;
                        mProjectileViews.Add(view);
                     }

                     mLookAt = mProjectileViews.LastOrDefault();

                     mWorld.Start();
                     clone.Start();
                  }
                  await Task.Delay(4000, aToken);
                  break;

               case 10:
                  ///////////////////////////////////////////////////////////////////////////////////////////////
                  /// Pre-turn check. The vast majority of shots do not use this.
                  await DoAllPreTurnsAsync(aToken);
                  break;

               case 11:
                  ///////////////////////////////////////////////////////////////////////////////////////////////
                  /// Spawn both players at new locations.
                  Spawn(mPlayer1, mStage.SpawnPointsA[3]);
                  Spawn(mPlayer2, mStage.SpawnPointsB[1]);
                  mLookAt = mPlayerView1;
                  mPlayer1.AimInRadians = Player.CenterAimInRadians + (mPlayer1.TrueAngleRangeInRadians / 4.0);
                  await Task.Delay(1000, aToken);
                  break;

               case 12:
                  ///////////////////////////////////////////////////////////////////////////////////////////////
                  /// Pre-turn check. The vast majority of shots do not use this.
                  await DoAllPreTurnsAsync(aToken);
                  break;

               case 13:
                  ///////////////////////////////////////////////////////////////////////////////////////////////
                  /// Fire at an aim halfway between the default and the max. Also bring player 2 to the verge
                  /// of death.
                  if ((mWorld.Fire(mPlayer1, 0.95, out projectiles) == true) &&
                      (clone.Fire(mPlayer1, 0.95, out _) == true))
                  {
                     foreach (Projectile projectile in projectiles)
                     {
                        ProjectileView view = mServiceProvider.GetService<ProjectileView>();
                        view.Model = projectile;
                        view.IsDebugEnabled = mPlayerView1.IsDebugEnabled;
                        mProjectileViews.Add(view);
                     }

                     mLookAt = mProjectileViews.LastOrDefault();
                     mPlayer2.HP = 1;

                     mWorld.Start();
                     clone.Start();
                  }
                  await Task.Delay(4000, aToken);
                  break;

               case 14:
                  ///////////////////////////////////////////////////////////////////////////////////////////////
                  /// Pre-turn check. The vast majority of shots do not use this.
                  await DoAllPreTurnsAsync(aToken);
                  break;

               case 15:
                  isDone = true;
                  break;
            }

            phase += 1;
         }

         if (clone != null)
         {
            clone.LandingEvent -= OnLandingEvent;
            clone.DeathEvent -= OnDeathEvent;
            clone.ShootingEvent -= OnShootingEvent;
            clone.SettledEvent -= OnSettledEvent;
         }
      }

      private async Task DoAllPreTurnsAsync(CancellationToken aToken)
      {
         Queue<Projectile> preturns = new Queue<Projectile>(mWorld.PreTurns.Where(l => l.PlayerID == mPlayer1.ID)
                                                                           .Select(l => l.Projectile));
         while ((aToken.IsCancellationRequested == false) && (preturns.Count > 0))
         {
            await DoSinglePreTurnAsync(preturns.Dequeue(), aToken);
         }
      }

      private async Task DoSinglePreTurnAsync(Projectile aProjectile, CancellationToken aToken)
      {
         aProjectile.OnPreTurn();
         if (aProjectile.CorporealBodies.Any(b => (b.IsSettled == false) || (b.IsWalking == true)) == true)
         {
            ProjectileView view;
            view = mProjectileViews.FirstOrDefault(v => ReferenceEquals(v.Model, aProjectile) == true);

            if (view != null)
            {
               mLookAt = view;
            }

#pragma warning disable CS4014 // Suppress the "please use 'await'" warning
            mWorld.Start();
#pragma warning restore CS4014 // Unsuppress the "please use 'await'" warning
            await Task.Delay(4000, aToken);
         }
      }
   }
}
