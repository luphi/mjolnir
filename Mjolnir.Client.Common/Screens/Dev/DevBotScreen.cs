﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.UI;
using Mjolnir.Client.Common.Views;
using Mjolnir.Common;
using Mjolnir.Common.Collections;
using Mjolnir.Common.Messages;
using Mjolnir.Common.Models;
using Mjolnir.Common.Physics;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Mjolnir.Client.Common.Screens.Dev
{
   public class DevBotScreen : Screen
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly Window mWindow;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly Camera mCamera;
      private readonly ContentWrapper mContentWrapper;
      private readonly SpriteBatch mSpriteBatch;
      private readonly ConcurrentList<TextButton> mButtons = new ConcurrentList<TextButton>();
      private readonly TextButton mStartStopButton = null;
      private readonly WindVane mWindVane;
      private readonly StageView mStageView;
      private readonly SatelliteView mThorView;
      private readonly World mWorld;
      private readonly Stage mStage;
      private readonly Bot mBot;
      private readonly ConcurrentList<Player> mFodder = new ConcurrentList<Player>();
      private readonly ConcurrentList<PlayerView> mPlayerViews = new ConcurrentList<PlayerView>();
      private readonly ConcurrentList<ProjectileView> mProjectileViews = new ConcurrentList<ProjectileView>();
      private readonly ConcurrentBag<int> mOccupiedSpawnPoints = new ConcurrentBag<int>();
      private IVisual mLookAt = null;
      private CancellationTokenSource mTokenSource = null;
      private bool mIsTargetingBest = true;

      public override string Name => "Dev Bot";

      public DevBotScreen(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mWindow = aServiceProvider.GetService<Window>();
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mCamera = aServiceProvider.GetService<Camera>();
         mContentWrapper = aServiceProvider.GetService<ContentWrapper>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mWindVane = aServiceProvider.GetService<WindVane>();
         mStageView = aServiceProvider.GetService<StageView>();
         mThorView = aServiceProvider.GetService<SatelliteView>();
         mStartStopButton = mServiceProvider.GetService<TextButton>();

         mStage = new Stage(StageEnum.PIDGIN_ROOT);

         uint id = 0;
         mBot = new Bot(id++)
         {
            MobileID = MobileEnum.LARVA,
            ShotID = ShotEnum.SHOT_2,
            Team = TeamEnum.A
         };

         for (int i = 0; i < 4; i++)
         {
            Player target = new Player(aID: id++, aName: $"Target{i}", aIsAdmin: false)
            {
               MobileID = MobileEnum.AKUDA,
               ShotID = ShotEnum.SHOT_1,
               Team = TeamEnum.B
            };
            mFodder.Add(target);
         }

         for (int i = 0; i < 3; i++)
         {
            Player ally = new Player(aID: id++, aName: $"Ally{i}", aIsAdmin: false)
            {
               MobileID = MobileEnum.THUNDER,
               ShotID = ShotEnum.SHOT_1,
               Team = TeamEnum.A
            };
            mFodder.Add(ally);
         }

         mWorld = new World(mStage)
         {
            //Wind = Vector.FromRadians(random.NextDouble() * 2.0 * Math.PI) *
            //       Constants.MaxWindAcceleration * (1.0 - random.NextDouble())
            Wind = new Vector(15.0, -15.0)
         };

         mWorld.Add(mBot);
         foreach (Player target in mFodder)
         {
            mWorld.Add(target);
         }
      }

      public override void Dispose()
      {
         mWorld.LandingEvent -= OnLandingEvent;
         mWorld.DeathEvent -= OnDeathEvent;
         mWorld.ShootingEvent -= OnShootingEvent;
         mWorld.SettledEvent -= OnSettledEvent;

         foreach (PlayerView view in mPlayerViews)
         {
            view.Dispose();
         }
         foreach (ProjectileView view in mProjectileViews)
         {
            view.Dispose();
         }
         mThorView.Dispose();
      }

      public override void Initialize()
      {
         mWorld.LandingEvent += OnLandingEvent;
         mWorld.DeathEvent += OnDeathEvent;
         mWorld.ShootingEvent += OnShootingEvent;
         mWorld.SettledEvent += OnSettledEvent;
      }

      public override void LoadContent()
      {
         mStage.Load(mContentWrapper);
         mStageView.Model = mStage;

         double padding = mViewport.VirtualHeight * 0.01;
         Rectangle paddedBounds = new Rectangle((int)padding,
                                                (int)padding,
                                                (int)(mViewport.VirtualWidth - (2.0 * padding)),
                                                (int)(mViewport.VirtualHeight - (2.0 * padding)));

         mWindVane.Height = mViewport.VirtualHeight / 10.0;
         mWindVane.X = paddedBounds.X + (paddedBounds.Width / 2.0) - (mWindVane.Width / 2.0);
         mWindVane.Y = paddedBounds.Y;
         mWindVane.Wind = mWorld.Wind;

         mThorView.Model = mWorld.Thor;

         mStartStopButton.Text = "START";
         mStartStopButton.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            if (mTokenSource == null)
            {
               Reset();
               mStartStopButton.Text = "STOP";
               RunTask(BotTurnAsync);
            }
            else
            {
               mTokenSource?.Cancel();
               mTokenSource = null;
               mStartStopButton.Text = "START";
            }
         };
         // Place the "START"/"STOP" button in the top left of the screen.
         mStartStopButton.X = paddedBounds.X;
         mStartStopButton.Y = paddedBounds.Y + (1.0 * paddedBounds.Height / 8.0) - (mStartStopButton.Height / 2.0);
         mButtons.Add(mStartStopButton);

         TextButton button = mServiceProvider.GetService<TextButton>();
         button.Text = "DEBUG";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            bool isDebugEnabled = !mPlayerViews[0].IsDebugEnabled;
            foreach (PlayerView playerView in mPlayerViews)
            {
               playerView.IsDebugEnabled = isDebugEnabled;
            }
            foreach (ProjectileView projectileView in mProjectileViews)
            {
               projectileView.IsDebugEnabled = isDebugEnabled;
            }
         };
         // Place the "DEBUG" button in the bottom left of the screen.
         button.X = paddedBounds.X;
         button.Y = paddedBounds.Y + (7.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         mButtons.Add(button);

         button = mServiceProvider.GetService<TextButton>();
         button.Text = "RESPAWN";
         button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            Respawn();
            RunTask(StagePanAsync);
         };
         // Place the "RESPAWN" button in the top right of the screen.
         button.X = paddedBounds.X + paddedBounds.Width - button.Width;
         button.Y = paddedBounds.Y + (1.0 * paddedBounds.Height / 8.0) - (button.Height / 2.0);
         mButtons.Add(button);

         TextButton modeButton = mServiceProvider.GetService<TextButton>();
         modeButton.Text = "TARGET ALL";
         modeButton.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            if (modeButton.Text == "TARGET ALL")
            {
               modeButton.Text = "TARGET BEST";
               mIsTargetingBest = false;
            }
            else
            {
               modeButton.Text = "TARGET ALL";
               mIsTargetingBest = true;
            }
            modeButton.X = paddedBounds.X + paddedBounds.Width - modeButton.Width;
         };
         // Place the "TARGET ALL"/"TARGET BEST" button in the bottom right of the screen.
         modeButton.X = paddedBounds.X + paddedBounds.Width - modeButton.Width;
         modeButton.Y = paddedBounds.Y + (7.0 * paddedBounds.Height / 8.0) - (modeButton.Height / 2.0);
         mButtons.Add(modeButton);

         mCamera.Bounds = mStageView.CameraBounds;

         PlayerView playerView = mServiceProvider.GetService<PlayerView>();
         playerView.Model = mBot;
         mPlayerViews.Add(playerView);
         foreach (Player target in mFodder)
         {
            playerView = mServiceProvider.GetService<PlayerView>();
            playerView.Model = target;
            mPlayerViews.Add(playerView);
         }

         // Place the bot and targets at spawn points on the stage.
         Respawn();

         // Clear the targets, views, etc. and add new ones as appropriate. (Since nothing has been
         // added yet, this is effectively initialization.)
         Reset();
      }

      public override void Respond(InputEvent aInput)
      {
         if (aInput.Op == InputEvent.Opcode.BACK)
         {
            mWindow.LoadScreen(mServiceProvider?.GetService<DevMenuScreen>());
            return;
         }

         foreach (TextButton button in mButtons)
         {
            button.Respond(aInput);
         }
      }

      public override void Draw(GameTime aGameTime)
      {
         if (mLookAt != null)
         {
            mCamera.LookAt(mLookAt);
         }

         // Parallaxed draw.
         mSpriteBatch.Begin(transformMatrix: mCamera.ParallaxViewMatrix);
         mStageView.DrawBackground();
         mSpriteBatch.End();

         // World draw.
         mSpriteBatch.Begin(transformMatrix: mCamera.WorldViewMatrix);
         mStageView.DrawForeground();
         foreach (PlayerView playerView in mPlayerViews)
         {
            playerView.Draw();
         }
         foreach (ProjectileView projectileView in mProjectileViews)
         {
            projectileView.Draw();
         }
         mThorView.Draw();
         mSpriteBatch.End();

         // UI draw.
         mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix);
         foreach (TextButton button in mButtons)
         {
            button.Draw();
         }
         mWindVane.Draw();
         mSpriteBatch.End();
      }

      private void OnLandingEvent(object aSender, Landing aLanding)
      {
         mLog.Debug($"{aLanding.Player.Name} landed at {aLanding.Point}");
      }

      private void OnDeathEvent(object aSender, Player aPlayer)
      {
         mLog.Debug($"Player {aPlayer.Name} died");
      }

      private void OnShootingEvent(object aSender, Shooting aShooting)
      {
         mLog.Debug($"Shooting at {aShooting.Center}");
      }

      private void OnSettledEvent(object aSender, Events aEvents)
      {
         mLog.Debug($"World settled");

         mLookAt = null; // Keep the camera still for now.

         List<ProjectileView> toRemove = new List<ProjectileView>();
         foreach (ProjectileView projectileView in mProjectileViews)
         {
            if (projectileView.Model?.IsDone ?? true)
            {
               toRemove.Add(projectileView);
            }
         }
         foreach (ProjectileView projectileView in toRemove)
         {
            projectileView.Dispose();
            mProjectileViews.Remove(projectileView);
         }
      }

      private void Spawn(Player aPlayer)
      {
         if (mOccupiedSpawnPoints.Count < 8)
         {
            // Get a random index, 0 to 7, that isn't being used.
            IEnumerable<int> unoccupied = Enumerable.Except(new int[] { 0, 1, 2, 3, 4, 5, 6, 7 },
                                                            mOccupiedSpawnPoints);
            int index = new Random().Next(unoccupied.Count()); // 'index' is a random index of 'unoccupied'.
            index = unoccupied.ElementAt(index); // 'index' is now a random unused spawn point index.

            Vector aSpawnPoint = index < 4 ? mStage.SpawnPointsA[index] : mStage.SpawnPointsB[index - 4];
            mOccupiedSpawnPoints.Add(index);

            aPlayer.X = aSpawnPoint.X - aPlayer.Radius;
            aPlayer.Y = aSpawnPoint.Y - (2.0 * aPlayer.Radius);
            aPlayer.Pitch = mStage.NormalAt(aPlayer.Footing).AngleInRadians;
         }
      }

      private void Respawn()
      {
         try
         {
            mOccupiedSpawnPoints.Clear();

            Spawn(mBot);
            foreach (Player target in mFodder)
            {
               Spawn(target);
            }
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while attempting to respawn the bot and targets");
         }
      }

      private void Reset()
      {
         try
         {
            // Stop the task, if one is running.
            mTokenSource?.Cancel();
            mTokenSource = null;

            // Whether the task was stopped or not, it's safe to change the start/stop button to "START."
            if (mStartStopButton != null)
            {
               mStartStopButton.Text = "START";
            }

            // Stop the world and reset a couple things within it.
            mWorld.Stop();
            mWorld.Clear();
            mWorld.Thor.Reset();
            mStage.Reset();

            // Add the bot and allies/targets back into the world.
            mWorld.Add(mBot);
            foreach (Player target in mFodder)
            {
               mWorld.Add(target);
            }

            // Reset the bot's properties.
            mBot.AimInRadians = Player.CenterAimInRadians;
            mBot.HP = mBot.Mobile?.HP ?? 0;

            // Look at the bot.
            mLookAt = mPlayerViews.FirstOrDefault();
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while attempting to reset the the \"{Name}\" screen");
         }
      }

      private void RunTask(Func<CancellationToken, Task> aMethod)
      {
         CancellationTokenSource tokenSource = new CancellationTokenSource();
         mTokenSource = tokenSource;
         Task.Run(async () => await aMethod(tokenSource.Token))
             .ContinueWith(task => Reset());
      }

      private async Task StagePanAsync(CancellationToken aToken)
      {
         // Nullify the "look at" visual so the camera is directed at anything during
         // the Draw() calls.
         mLookAt = null;

         // Create a modifiable vector for the camera to look at. It will be placed at
         // the Y of the bot's center point and its X value will begin towards the left
         // of the stage such that the left edge of the viewport lines up with the left
         // edge of the stage.
         Vector lookAt = new Vector(mViewport.VirtualWidth / 2.0, mBot.Center.Y);

         // Calculate the rate at which the camera will pan across the stage and begin
         // tracking time.
         double durationInSeconds = 3.0;
         double distanceInPixels = mStage.Width - mViewport.VirtualWidth;
         double dpdt = distanceInPixels / durationInSeconds;
         double elapsedSecondsLastIteration = 0.0;
         Stopwatch stopwatch = Stopwatch.StartNew();

         try
         {
            while ((stopwatch.Elapsed.TotalSeconds <= durationInSeconds) &&
                   (aToken.IsCancellationRequested == false))
            {
               await Task.Delay(10, aToken);

               double elapsedSeconds = stopwatch.Elapsed.TotalSeconds;
               double dt = elapsedSeconds - elapsedSecondsLastIteration;
               elapsedSecondsLastIteration = elapsedSeconds;

               lookAt.X += dpdt * dt;

               mCamera.LookAt(lookAt);
            }
         }
         catch (TaskCanceledException)
         {
            mLog.Debug($"{nameof(BotTurnAsync)} task canceled");
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrwon in {nameof(StagePanAsync)}");
         }

         mLookAt = mPlayerViews.FirstOrDefault();
      }

      private async Task BotTurnAsync(CancellationToken aToken)
      {
         try
         {
            IEnumerable<Player> targets = mFodder.Where(p => p.Team != mBot.Team);

            if (mIsTargetingBest == true)
            {
               List<Message> messages = await mBot.OnTurnAsync(aToken, mWorld, targets);

               foreach (Message message in messages)
               {
                  switch (message.Op)
                  {
                     case Message.Opcode.PLAYER:
                        if (message is PlayerMessage playerMessage)
                        {
                           mLog.Warn($"Updating the bot via player message");
                           mBot.Sync(playerMessage.Player);
                        }
                        break;
                     case Message.Opcode.FIRE:
                        if (message is FireMessage fireMessage)
                        {
                           mLog.Debug($"Firing with aim {mBot.AimInRadians} to the " +
                                      $"{mBot.Direction.ToString().ToLower()}");
                           await FireAtAsync(aToken, fireMessage.PowerAsPercentage);
                        }
                        break;
                     case Message.Opcode.SKIP:
                        mLog.Debug($"The bot opted to skip its turn");
                        break;
                     default:
                        mLog.Warn($"The bot returned an unexpected message: {message}");
                        break;
                  }
               }
            }
            else
            {
               foreach (Player target in targets)
               {
                  if (aToken.IsCancellationRequested == true)
                  {
                     break;
                  }

                  for (double apexInPixels = 1000.0; apexInPixels >= 50.0; apexInPixels -= 50.0)
                  {
                     if (mBot.TryGetFireArgsForPoint(target.Center,
                                                     mWorld.Acceleration,
                                                     apexInPixels,
                                                     out Direction direction,
                                                     out double aimInRadians,
                                                     out double powerAsPercent) == true)
                     {
                        mBot.Direction = direction;
                        mBot.AimInRadians = aimInRadians;
                        await FireAtAsync(aToken, powerAsPercent);
                        break;
                     }
                  }
               }
            }

            // Pause for a bit so the camera stays in place and we can get a look at the results.
            await Task.Delay(500, aToken);
         }
         catch (TaskCanceledException)
         {
            mLog.Debug($"{nameof(BotTurnAsync)} task canceled");
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown in {nameof(BotTurnAsync)}");
         }

         mLookAt = mPlayerViews.FirstOrDefault();
      }

      private async Task FireAtAsync(CancellationToken aToken, double aPowerAsPercent)
      {
         if (mWorld.Fire(mBot, aPowerAsPercent, out List<Projectile> projectiles) == true)
         {
            foreach (Projectile projectile in projectiles)
            {
               ProjectileView view = mServiceProvider.GetService<ProjectileView>();
               view.Model = projectile;
               view.IsDebugEnabled = mPlayerViews.FirstOrDefault()?.IsDebugEnabled == true;
               mProjectileViews.Add(view);
            }

            mLookAt = mProjectileViews.LastOrDefault();

            Events events = await mWorld.StartAsync(aToken);
            mLog.Debug($"World run events: {events}");

            // Wait a second just to get a view of hte results.
            await Task.Delay(1000);
         }
      }
   }
}
