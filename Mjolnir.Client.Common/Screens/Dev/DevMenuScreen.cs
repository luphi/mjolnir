﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.Screens.Dev;
using Mjolnir.Client.Common.UI;
using Mjolnir.Client.Common.Views;
using Mjolnir.Common;
using Mjolnir.Common.Collections;
using Mjolnir.Common.Messages;
using Mjolnir.Common.Models;
using Mjolnir.Common.Network;
using Mjolnir.Common.Threading;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Mjolnir.Client.Common.Screens
{
   public class DevMenuScreen : Screen
   {
      private const int TASK_DELAY = 2000; // Milliseconds

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly Window mWindow;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly Camera mCamera;
      private readonly IServiceProvider mServiceProvider;
      private readonly SpriteBatch mSpriteBatch;
      private readonly AsyncManualResetEvent mResetEvent = new AsyncManualResetEvent();
      private readonly ConcurrentList<IVisual> mVisuals = new ConcurrentList<IVisual>();
      private readonly ConcurrentList<IControl> mControls = new ConcurrentList<IControl>();
      private Table mOptionsTable;
      private TextButton mRunButton = null;
      private TextButton mBackButton = null;
      private string mSelectedRoutine = string.Empty;

      // Variables potentially used by dev routines
      private TextInput mTextInput = null;
      private ConfirmationModalWindow mConfirmationModalWindow = null;
      private AlertModalWindow mAlertModalWindow = null;
      private InputModalWindow mInputModalWindow = null;

      public override string Name => "Dev";

      public DevMenuScreen(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mWindow = aServiceProvider.GetService<Window>();
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mCamera = aServiceProvider.GetService<Camera>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
      }

      public override void Dispose()
      {
         mResetEvent.Set();
      }

      public override void Initialize()
      {
         ResetResetEvent();
      }


      #region LoadContent()

      public override void LoadContent()
      {
         double padding = mViewport.VirtualHeight * 0.01;
         Rectangle paddedBounds = new Rectangle((int)padding,
                                                (int)padding,
                                                (int)(mViewport.VirtualWidth - (2 * padding)),
                                                (int)(mViewport.VirtualHeight - (2 * padding)));

         mOptionsTable = mServiceProvider.GetService<Table>();
         mOptionsTable.SelectEvent += OnOptionSelect;
         mOptionsTable.Headers = new List<string>
         {
            "DEV ROUTINE",
            "SCREEN?"
         };
         mOptionsTable.Weights = new List<int>
         {
            5,
            1
         };

         if (MjolnirGame.IsDesktop == true)
         {
            mOptionsTable.VisibleRows = 20;
         }
         else
         {
            mOptionsTable.VisibleRows = 8;
         }
         mOptionsTable.X = paddedBounds.X;
         mOptionsTable.Y = paddedBounds.Y;
         mOptionsTable.Width = paddedBounds.Width;
         mOptionsTable.Height = paddedBounds.Height - Text.DefaultHeight;
         mVisuals.Add(mOptionsTable);
         mControls.Add(mOptionsTable);

         double buttonSpacing = paddedBounds.Width / 50.0;
         mBackButton = mServiceProvider.GetService<TextButton>();
         mBackButton.Text = "BACK";
         mBackButton.X = paddedBounds.X + buttonSpacing;
         mBackButton.Y = paddedBounds.Y + paddedBounds.Height - mBackButton.Height;
         mBackButton.ClickEvent += OnBackClick;
         mVisuals.Add(mBackButton);
         mControls.Add(mBackButton);

         mRunButton = mServiceProvider.GetService<TextButton>();
         mRunButton.Text = "RUN";
         mRunButton.IsEnabled = false; // Disabled until something is selected.
         mRunButton.X = mBackButton.X + mBackButton.Width + buttonSpacing;
         mRunButton.Y = mBackButton.Y;
         mRunButton.ClickEvent += OnRunClick;
         mVisuals.Add(mRunButton);
         mControls.Add(mRunButton);

         mOptionsTable.AddRows(new List<List<string>>
         {
            new List<string>
            {
               "Send/receive large and small messages over sockets", // Invisible key.
               "Send/receive large and small messages over sockets", // Visible first column.
               "No" // Does the routine proceed to another screen?
            },
            new List<string>
            {
               "Create a confirmation modal window",
               "Create a confirmation modal window",
               "No"
            },
            new List<string>
            {
               "Create an input modal window",
               "Create an input modal window",
               "No"
            },
            new List<string>
            {
               "Stage view test",
               "Stage view test",
               "Yes"
            },
            new List<string>
            {
               "Player view test",
               "Player view test",
               "Yes"
            },
            new List<string>
            {
               "World test",
               "World test",
               "Yes"
            },
            new List<string>
            {
               "Walk test",
               "Walk test",
               "Yes"
            },
            new List<string>
            {
               "Bot test",
               "Bot test",
               "Yes"
            },
            new List<string>
            {
               "Receive server details from a registry",
               "Receive server details from a registry",
               "No"
            },
            new List<string>
            {
               "Turn list from random player delays",
               "Turn list from random player delays",
               "No"
            },
            new List<string>
            {
               "Display and rotate a Thor satellite view",
               "Display and rotate a Thor satellite view",
               "No"
            },
            new List<string>
            {
               "Display and rotate a B.Seat satellite view",
               "Display and rotate a B.Seat satellite view",
               "No"
            },
            new List<string>
            {
               "Text input",
               "Text input",
               "No"
            }
         });
      }

      #endregion LoadContent()


      public override void Respond(InputEvent aInput)
      {
         try
         {
            if ((aInput.Op == InputEvent.Opcode.BACK) && (mTextInput?.IsFocused != true) &&
                (mInputModalWindow == null) && (mInputModalWindow == null) && (mConfirmationModalWindow == null))
            {
               // If there was a back event AND the text input and neither modal window is in focus.
               // (If they are, we want to let them respond rather than exit this screen.)

               OnBackClick(this, EventArgs.Empty);
            }
            else if ((mInputModalWindow != null) || (mConfirmationModalWindow != null) ||
                     (mInputModalWindow != null) || (mTextInput != null))
            {
               // If any of the above objects are not null, give them priority with input events.

               mInputModalWindow?.Respond(aInput);
               mConfirmationModalWindow?.Respond(aInput);
               mInputModalWindow?.Respond(aInput);
               mTextInput?.Respond(aInput);
            }
            else
            {
               // For all other cases...

               foreach (IControl control in mControls)
               {
                  control.Respond(aInput);
               }
            }
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Unexpected {nameof(Respond)}() exception thrown");
         }
      }

      public override void Draw(GameTime aGameTime)
      {
         try
         {
            mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix,
                               blendState: BlendState.AlphaBlend);

            foreach (IVisual visual in mVisuals)
            {
               visual.Draw();
            }

            mAlertModalWindow?.Draw();
            mConfirmationModalWindow?.Draw();
            mInputModalWindow?.Draw();

            mSpriteBatch.End();
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Unexpected {nameof(Draw)}() exception thrown");
         }
      }

      private void OnOptionSelect(object aSender, string aKey)
      {
         mSelectedRoutine = aKey;
         mRunButton.IsEnabled = true; // Enable the "RUN" button now that something was selected.
      }

      private void OnBackClick(object aSender, EventArgs aArgs)
      {
         mWindow.LoadScreen(mServiceProvider?.GetService<MainMenuScreen>());
      }

      private void OnRunClick(object aSender, EventArgs aArgs)
      {
         try
         {
            switch (mSelectedRoutine)
            {
               case "Send/receive large and small messages over sockets":
                  DoSendAndReceive(); // Gets its own method because it's a lot of code.
                  break;

               case "Create a confirmation modal window":
                  ResetResetEvent();
                  mConfirmationModalWindow = mServiceProvider.GetService<ConfirmationModalWindow>();
                  mConfirmationModalWindow.Message = "This is an overly-long modal window message to test " +
                                                     "line wrapping";
                  mConfirmationModalWindow.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
                  {
                     mConfirmationModalWindow = null;
                  };
                  mConfirmationModalWindow.CancelEvent += delegate (object aSender, EventArgs aArgs)
                  {
                     mConfirmationModalWindow = null;
                  };
                  break;

               case "Create an input modal window":
                  ResetResetEvent();
                  mInputModalWindow = mServiceProvider.GetService<InputModalWindow>();
                  mInputModalWindow.Message = "This is an overly-long modal window message to test line wrapping. " +
                                              "Enter something:";
                  mInputModalWindow.CancelEvent += delegate (object aSender, EventArgs aArgs)
                  {
                     mInputModalWindow = null;
                  };
                  mInputModalWindow.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
                  {
                     string entered = mInputModalWindow.Value;
                     mInputModalWindow = null;
                     mAlertModalWindow = mServiceProvider.GetService<AlertModalWindow>();
                     mAlertModalWindow.Message = $"You entered: \"{entered}\"";
                     mAlertModalWindow.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
                     {
                        mAlertModalWindow = null;
                     };
                  };
                  break;

               case "Stage view test":
                  mWindow.LoadScreen(mServiceProvider?.GetService<DevStageScreen>());
                  break;

               case "Player view test":
                  mWindow.LoadScreen(mServiceProvider?.GetService<DevPlayerScreen>());
                  break;

               case "World test":
                  mWindow.LoadScreen(mServiceProvider?.GetService<DevWorldScreen>());
                  break;

               case "Walk test":
                  mWindow.LoadScreen(mServiceProvider?.GetService<DevWalkScreen>());
                  break;

               case "Bot test":
                  mWindow.LoadScreen(mServiceProvider?.GetService<DevBotScreen>());
                  break;

               case "Receive server details from a registry":
                  ResetResetEvent();
                  RegistryClient registryClient = mServiceProvider.GetService<RegistryClient>();
                  registryClient.RetrievalEvent += delegate (object aSender,
                                                             List<ServerRegistrationMessage> aRegistrations)
                  {
                     foreach (ServerRegistrationMessage registration in aRegistrations)
                     {
                        mLog.Debug(registration);
                     }
                  };
                  registryClient.Retrieve();
                  break;

               case "Turn list from random player delays":
                  DoTurnQueue(); // Gets its own method because it's a lot of code.
                  break;

               case "Display and rotate a Thor satellite view":
                  Thor thor = new Thor(null);
                  thor.X = (mViewport.VirtualWidth / 2.0) - thor.Radius;
                  thor.Y = (mViewport.VirtualHeight / 2.0) - thor.Radius;
                  SatelliteView thorView = mServiceProvider.GetService<SatelliteView>();
                  thorView.Model = thor;
                  mVisuals.Add(thorView);

                  Task.Run(async () =>
                  {
                     await mResetEvent.WaitAsync();
                     mVisuals.Remove(thorView);
                  });

                  Task.Run(async () =>
                  {
                     Vector target = null;
                     for (int i = 0; i < 7; i++)
                     {
                        switch (i)
                        {
                           case 1:
                              target = thor.Center + new Vector(500.0, -500.0); // Right and up.
                              break;
                           case 2:
                              target = thor.Center + new Vector(500.0, 0.0); // Right.
                              break;
                           case 3:
                              target = thor.Center + new Vector(500.0, 500.0); // Right and down.
                              break;
                           case 4:
                              target = thor.Center + new Vector(0.0, 500.0); // Down.
                              break;
                           case 5:
                              target = thor.Center + new Vector(-500.0, 500.0); // Left and down.
                              break;
                           case 6:
                              target = thor.Center + new Vector(-500.0, 0.0); // Left.
                              break;
                        }

                        if (target != null)
                        {
                           thor.Charge(300);
                           thor.Fire();
                           // Use both fire methods. This way, we can easily spot any difference between them.
                           thorView.Fire(target);
                           thorView.Fire((target - thor.Center).AngleInRadians);
                        }

                        await Task.Delay(TASK_DELAY);
                     }

                     ResetResetEvent();
                  });
                  break;

               case "Display and rotate a B.Seat satellite view":
                  Body satellite = new Body { Radius = 20.0 };
                  satellite.X = (mViewport.VirtualWidth / 2.0) - satellite.Radius;
                  satellite.Y = (mViewport.VirtualHeight / 2.0) - satellite.Radius;
                  SatelliteView satelliteView = mServiceProvider.GetService<SatelliteView>();
                  satelliteView.Model = satellite;
                  mVisuals.Add(satelliteView);

                  Task.Run(async () =>
                  {
                     await mResetEvent.WaitAsync();
                     mVisuals.Remove(satelliteView);
                  });

                  Task.Run(async () =>
                  {
                     Vector target = null;
                     for (int i = 0; i < 7; i++)
                     {
                        switch (i)
                        {
                           case 1:
                              target = satellite.Center + new Vector(500.0, -500.0); // Right and up.
                              break;
                           case 2:
                              target = satellite.Center + new Vector(500.0, 0.0); // Right.
                              break;
                           case 3:
                              target = satellite.Center + new Vector(500.0, 500.0); // Right and down.
                              break;
                           case 4:
                              target = satellite.Center + new Vector(0.0, 500.0); // Down.
                              break;
                           case 5:
                              target = satellite.Center + new Vector(-500.0, 500.0); // Left and down.
                              break;
                           case 6:
                              target = satellite.Center + new Vector(-500.0, 0.0); // Left.
                              break;
                        }

                        if (target != null)
                        {
                           // Use both fire methods. This way, we can easily spot any difference between them.
                           satelliteView.Fire(target);
                           satelliteView.Fire((target - satellite.Center).AngleInRadians);
                        }

                        await Task.Delay(TASK_DELAY);
                     }

                     ResetResetEvent();
                  });
                  break;

               case "Text input":
                  TextInput input = mServiceProvider.GetService<TextInput>();
                  input.HasBackground = true;
                  input.Width = mViewport.VirtualWidth / 2.0;
                  input.Height = mViewport.VirtualHeight / 10.0;
                  input.X = mViewport.VirtualCenter.X - (input.Width / 2.0);
                  input.Y = 0;
                  mVisuals.Add(input);

                  mTextInput = input;
                  input.EscapeEvent += delegate (object aSender, EventArgs aArgs)
                  {
                     mTextInput = null;
                     mVisuals.Remove(input);
                  };
                  input.EnterEvent += delegate (object aSender, EventArgs aArgs)
                  {
                     mLog.Debug($"{nameof(TextInput)} enter event: \"{input.Value}\"");
                     mTextInput = null;
                     mVisuals.Remove(input);
                     input.IsFocused = false;
                  };
                  input.IsFocused = true;
                  break;
            }
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Unexpected {nameof(OnRunClick)}() exception thrown");
         }
      }

      private void ResetResetEvent()
      {
         mResetEvent.Set();
         mResetEvent.Reset();
      }


      #region Helper methods for large content initializations

      private void DoSendAndReceive()
      {
         ResetResetEvent();

         List<Player> players = new List<Player>();
         uint id = 0;
         players.Add(new Player(id++, "Apollo", false) { Team = TeamEnum.A, IsVeteran = true });
         players.Add(new Player(id++, "Zeus", false) { Team = TeamEnum.A });
         players.Add(new Player(id++, "Aphrodite", false) { Team = TeamEnum.A });
         players.Add(new Player(id++, "Hades", false) { Team = TeamEnum.A });
         players.Add(new Player(id++, "Galatea", false) { Team = TeamEnum.B });
         players.Add(new Player(id++, "Achilles", false) { Team = TeamEnum.B });
         players.Add(new Player(id++, "Homer", false) { Team = TeamEnum.B });
         players.Add(new Player(id++, "luphi", true) { Team = TeamEnum.B });
         PlayersMessage largeMessage = new PlayersMessage(players);

         string loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
         "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim " +
         "veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo " +
         "consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore " +
         "eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa " +
         "qui officia deserunt mollit anim id est laborum";
         Random random = new Random();
         Player player = players[random.Next(players.Count)];
         List<string> loremIpsumList = new List<string>(loremIpsum.Split(' '));
         ChatMessage smallMessage = new ChatMessage(player.ID, false, string.Join(
            " ", loremIpsumList.OrderBy(s => random.Next()).Take(random.Next(5, 15))));

         TcpServerSocket serverSocket = new TcpServerSocket("127.0.0.1", 51509);
         TcpClientSocket clientSocket = new TcpClientSocket("127.0.0.1", 51509);
         clientSocket.ReceiveEvent += delegate (object aSender, byte[] aData)
         {
            if (mConfirmationModalWindow != null)
            {
               mConfirmationModalWindow.Message = $"Client received {aData.Length} bytes";
            }
         };
         serverSocket.ReceiveEvent += delegate (object aSender, Reception aReception)
         {
            if (mConfirmationModalWindow != null)
            {
               mConfirmationModalWindow.Message = $"Server received {aReception.Data.Length} bytes";
            }
         };

         int phase = 0;
         byte[] largeData = Message.Serialize(largeMessage);
         byte[] smallData = Message.Serialize(smallMessage);
         mConfirmationModalWindow = mServiceProvider.GetService<ConfirmationModalWindow>();
         mConfirmationModalWindow.Message = $"Will send {largeData.Length} bytes from server to client";
         mConfirmationModalWindow.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
         {
            switch (phase)
            {
               case 0:
                  serverSocket.SendAll(largeData);
                  break;
               case 1:
                  mConfirmationModalWindow.Message = $"Will send {smallData.Length} bytes from server to client";
                  break;
               case 2:
                  serverSocket.SendAll(smallData);
                  break;
               case 3:
                  mConfirmationModalWindow.Message = $"Will send {largeData.Length} bytes from client to server";
                  break;
               case 4:
                  clientSocket.Send(largeData);
                  break;
               case 5:
                  mConfirmationModalWindow.Message = $"Will send {smallData.Length} bytes from client to server";
                  break;
               case 6:
                  clientSocket.Send(smallData);
                  break;
               case 7:
                  clientSocket.Disconnect();
                  serverSocket.Close();
                  mConfirmationModalWindow = null;
                  break;
            }
            phase += 1;
         };
         mConfirmationModalWindow.CancelEvent += delegate (object aSender, EventArgs aArgs)
         {
            clientSocket.Disconnect();
            serverSocket.Close();
            mConfirmationModalWindow = null;
         };

         serverSocket.Listen(out _);
         clientSocket.Connect();
      }

      private void DoTurnQueue()
      {
         List<Player> originalList = new List<Player>();
         Random random = new Random();
         // The first three will be given randomized values.
         for (int i = 0; i < 3; i++)
         {
            originalList.Add(new Player((uint)i, $"Player {i}", false)
            {
               Delay = (uint)random.Next(0, 1000)
            });
         }
         // The next three will be given the same randomized value.
         uint equal = (uint)random.Next(0, 1000);
         for (int i = 3; i < 6; i++)
         {
            originalList.Add(new Player((uint)i, $"Player {i}", false)
            {
               Delay = equal
            });
         }
         // The final two will be given randomized values again.
         for (int i = 6; i < 8; i++)
         {
            originalList.Add(new Player((uint)i, $"Player {i}", false)
            {
               Delay = (uint)random.Next(0, 1000)
            });
         }
         TurnList turnList = new TurnList();
         foreach (Player player in originalList)
         {
            turnList.Add(player);
         }

         double padding = mViewport.VirtualWidth / 100.0;
         Quad background = mServiceProvider.GetService<Quad>();
         background.Color = Color.SkyBlue;
         background.Width = mViewport.VirtualWidth * 0.75;
         background.Height = (Text.DefaultHeight * 8.0) + (padding * 9.0);
         background.X = (mViewport.VirtualWidth / 2.0) - (background.Width / 2.0);
         background.Y = (mViewport.VirtualHeight / 2.0) - (background.Height / 2.0);
         Rectangle backgroundBounds = background.Bounds;

         List<Text> originalTexts = new List<Text>();
         for (int i = 0; i < originalList.Count; i++)
         {
            Text text = mServiceProvider.GetService<Text>();
            text.Color = Color.Black;
            text.Value = $"{originalList[i].Name} -> {originalList[i].Delay}";
            text.X = backgroundBounds.X + (1.0 * backgroundBounds.Width / 3.0) - (text.Width / 2.0);
            text.Y = backgroundBounds.Y + (Text.DefaultHeight * i) + (padding * (i + 1));
            originalTexts.Add(text);
         }

         TurnListView turnListView = mServiceProvider.GetService<TurnListView>();
         turnListView.Model = turnList;
         //turnListView.Height = Text.DefaultHeight * 8.0;
         turnListView.Width = backgroundBounds.Width / 4.0;
         turnListView.X = backgroundBounds.X + (2.0 * backgroundBounds.Width / 3.0) - (turnListView.Width / 2.0);
         turnListView.Y = backgroundBounds.Y + (backgroundBounds.Height / 2.0) - (turnListView.Height / 2.0);

         lock (mVisuals)
         {
            mVisuals.Add(background);
            foreach (Text text in originalTexts)
            {
               mVisuals.Add(text);
            }
            mVisuals.Add(turnListView);
         }

         Task.Run(async () =>
         {
            await mResetEvent.WaitAsync();
            mVisuals.Clear();
         });

         Task.Run(async () =>
         {
            await Task.Delay(TASK_DELAY * 3);
            ResetResetEvent();
         });
      }

      #endregion Helper methods for large content initializations
   }
}
