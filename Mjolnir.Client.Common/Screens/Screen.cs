﻿using Microsoft.Xna.Framework;
using Mjolnir.Client.Common.Input;
using System;

namespace Mjolnir.Client.Common.Screens
{
   public abstract class Screen : IControl, IDisposable
   {
      public abstract string Name { get; }

      public virtual void Dispose()
      {
      }

      public virtual void Initialize()
      {
      }

      public virtual void LoadContent()
      {
      }

      public virtual void Respond(InputEvent aInput)
      {
      }

      public virtual void Draw(GameTime aGameTime)
      {
      }
   }
}
