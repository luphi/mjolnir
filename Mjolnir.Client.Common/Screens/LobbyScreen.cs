﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.UI;
using Mjolnir.Common;
using Mjolnir.Common.Collections;
using Mjolnir.Common.Disk;
using Mjolnir.Common.Messages;
using Mjolnir.Common.Models;
using Mjolnir.Common.Network;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Mjolnir.Client.Common.Screens
{
   public class LobbyScreen : Screen
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly IContentWrapper mContentWrapper;
      private readonly Configuration mConfiguration;
      private readonly Window mWindow;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly Camera mCamera;
      private readonly SpriteBatch mSpriteBatch;
      private readonly GameClient mGameClient;
      private bool mIsExiting = false;

      // UI components.
      private readonly ConcurrentList<IVisual> mVisuals = new ConcurrentList<IVisual>();
      private readonly ConcurrentList<IControl> mControls = new ConcurrentList<IControl>();
      private readonly Dictionary<uint, Text> mTeamANamesMap = new Dictionary<uint, Text>();
      private readonly Dictionary<uint, Text> mTeamBNamesMap = new Dictionary<uint, Text>();
      private Rectangle mPlayerListBounds = Rectangle.Empty;
      private Rectangle mPreviewBounds = Rectangle.Empty;
      private Text mTeamALabel = null;
      private Text mTeamBLabel = null;
      private Image mStagePreviewImage = null;
      private Carousel mStagePreviewCarousel = null;
      private Carousel mTeamSizesCarousel = null;
      private Carousel mSuddenDeathCarousel = null;
      private ChatLog mChatLog = null;
      private ItemTray mItemTray = null;
      private TextInput mChatInput = null;
      private TextButton mBackButton = null;
      private TextButton mChangeTeamButton = null;
      private TextButton mReadyButton = null;
      private TextButton mStartButton = null;
      private AlertModalWindow mAlertOverlay = null;

      // Settings-related properties.
      private readonly Stage mNoneStage = new Stage(StageEnum.NONE);
      private Stage mSelectedStage = null;
      private uint mMaxPlayerCount = 8;
      private SuddenDeathModeEnum mSuddenDeathMode = SuddenDeathModeEnum.DOUBLE;

      // Player's selections.
      private MobileTile mSelectedMobileTile = null;
      private MobileTile mPendingMobileTile = null;

      public override string Name => "Lobby";

      public LobbyScreen(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mContentWrapper = aServiceProvider.GetService<IContentWrapper>();
         mConfiguration = aServiceProvider.GetService<Configuration>();
         mWindow = aServiceProvider.GetService<Window>();
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mCamera = aServiceProvider.GetService<Camera>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mGameClient = aServiceProvider.GetService<GameClient>();
      }

      public override void Dispose()
      {
         mGameClient.DisconnectEvent -= OnDisconnect;
         mGameClient.MessageEvent -= OnMessage;

         if (Context.LocalPlayer != null)
         {
            Context.LocalPlayer.PropertyChanged -= OnLocalPlayerPropertyChanged;
         }
      }

      public override void Initialize()
      {
         mGameClient.DisconnectEvent += OnDisconnect;
         mGameClient.MessageEvent += OnMessage;

         if (Context.LocalPlayer != null)
         {
            Context.LocalPlayer.PropertyChanged += OnLocalPlayerPropertyChanged;
         }
      }


      #region LoadContent()

      public override void LoadContent()
      {
         double padding = mViewport.VirtualHeight * 0.01;
         Rectangle paddedBounds = new Rectangle((int)padding,
                                                (int)padding,
                                                (int)(mViewport.VirtualWidth - (2.0 * padding)),
                                                (int)(mViewport.VirtualHeight - (2.0 * padding)));


         #region Bounds - locations and sizes of the various regions

         // Regions are laid out like so:
         // (If this appears nonsensical to you, it's due to editor/system differences. Sorry.)
         //
         //   ---------------------------------------------------------------------------------------------
         //   |                     |                     |                                               |
         //   |                     |                     |                                               |
         //   |                     |                     |                                               |
         //   |       server        |       player        |                                               |
         //   |      settings       |        list         |                    chat                       |
         //   |                     |                     |                                               |
         //   |                     |                     |                                               |
         //   |                     |                     |                                               |
         //   ---------------------------------------------------------------------------------------------
         //   |                                                          |                                |
         //   |                                                          |                                |
         //   |                       mobile                             |             item               |
         //   |                      selection                           |           selection            |
         //   |                                                          |                                |
         //   |                                                          |                                |
         //   |                                                          |                                |
         //   ---------------------------------------------------------------------------------------------
         //   |  BACK                                                            CHANGE TEAM       READY  |
         //   ---------------------------------------------------------------------------------------------

         // Prepare some bounds to help organize the screen.
         Rectangle buttonBounds = new Rectangle(paddedBounds.X,
            (int)(paddedBounds.Y + paddedBounds.Height - Text.DefaultHeight),
            paddedBounds.Width,
            (int)Text.DefaultHeight);
         Rectangle serverSettingsAndPlayerListBounds = new Rectangle(paddedBounds.X,
            paddedBounds.Y,
            (int)(paddedBounds.Width * 0.5),
            (int)(paddedBounds.Height * 0.5));
         Rectangle settingsBounds = new Rectangle(serverSettingsAndPlayerListBounds.X,
            serverSettingsAndPlayerListBounds.Y,
            (int)(serverSettingsAndPlayerListBounds.Width / 2.0),
            serverSettingsAndPlayerListBounds.Height);
         mPlayerListBounds = new Rectangle(serverSettingsAndPlayerListBounds.X + settingsBounds.Width,
            serverSettingsAndPlayerListBounds.Y,
            (int)(serverSettingsAndPlayerListBounds.Width / 2.0),
            serverSettingsAndPlayerListBounds.Height);
         Rectangle chatBounds = new Rectangle(
            serverSettingsAndPlayerListBounds.X + serverSettingsAndPlayerListBounds.Width,
            serverSettingsAndPlayerListBounds.Y,
            paddedBounds.Width - serverSettingsAndPlayerListBounds.Width,
            serverSettingsAndPlayerListBounds.Height);
         Rectangle mobileSelectionBounds = new Rectangle(paddedBounds.X,
            (int)(chatBounds.Y + chatBounds.Height + padding),
            (int)((paddedBounds.Width * 0.6) - padding),
            (int)(buttonBounds.Y - (chatBounds.Y + chatBounds.Height + padding)));
         Rectangle itemSelectionBounds = new Rectangle(
            (int)(mobileSelectionBounds.X + mobileSelectionBounds.Width + padding),
            mobileSelectionBounds.Y,
            (int)(paddedBounds.Width - mobileSelectionBounds.Width - padding),
            mobileSelectionBounds.Height);

         #endregion Bounds - locations and sizes of the various regions


         #region Player list

         // Create the player list.
         // The player list simply shows the players' names under a "Team A" or "Team B" label.
         mTeamALabel = mServiceProvider.GetService<Text>();
         mTeamALabel.Value = "TEAM A";
         mTeamALabel.Color = mConfiguration.SecondaryColor.ToColor();
         mTeamALabel.X = mPlayerListBounds.X + (mPlayerListBounds.Width / 2.0) -
                         (mTeamALabel.Width / 2.0); // Centered.
         mTeamALabel.Y = mPlayerListBounds.Y;
         mVisuals.Add(mTeamALabel);

         mTeamBLabel = mServiceProvider.GetService<Text>();
         mTeamBLabel.Value = "TEAM B";
         mTeamBLabel.Color = mConfiguration.SecondaryColor.ToColor();
         mTeamBLabel.X = mPlayerListBounds.X + (mPlayerListBounds.Width / 2.0) -
                         (mTeamBLabel.Width / 2.0); // Centered.
         mTeamBLabel.Y = mPlayerListBounds.Y + (mPlayerListBounds.Height / 2.0);
         mVisuals.Add(mTeamBLabel);

         // Add texts for the current players.
         foreach (Player player in Context.Players)
         {
            AddPlayer(player);
         }

         #endregion Player list


         #region Server settings (stage, team size, and sudden death mode selection/display)

         // Create the settings.
         // The settings are carousels that control some aspects of the upcoming game. They are
         // only editable to an admin or the veteran.
         // Start with the stage carousel that cycles amongst the available stages and shows
         // preview images (of their foregrounds) above the carousel.
         mStagePreviewImage = mServiceProvider.GetService<Image>();
         mSelectedStage = Context.Stage;
         mStagePreviewImage.Content = mContentWrapper.StageForegroundPath(mSelectedStage ?? mNoneStage);
         mPreviewBounds = new Rectangle(settingsBounds.X,
                                        settingsBounds.Y,
                                        settingsBounds.Width,
                                        settingsBounds.Width / 2);
         mStagePreviewImage.CenterInBounds(mPreviewBounds);
         mVisuals.Add(mStagePreviewImage);

         double settingsTextHeight = Math.Min(Text.DefaultHeight,
                                              (settingsBounds.Height - mPreviewBounds.Height) / 5.0);
         mStagePreviewCarousel = mServiceProvider.GetService<Carousel>();
         mStagePreviewCarousel.IsEnabled = (Context.IsAdmin == true) || (Context.IsVeteran == true);
         mStagePreviewCarousel.Width = mPreviewBounds.Width * 0.95;
         mStagePreviewCarousel.Height = settingsTextHeight;
         mStagePreviewCarousel.X = mPreviewBounds.X + (mPreviewBounds.Width / 2.0) -
                                  (mStagePreviewCarousel.Width / 2.0);
         mStagePreviewCarousel.Y = mPreviewBounds.Y + mPreviewBounds.Height;
         List<string> entries = new List<string>
         {
            mNoneStage.Name
         };
         entries.AddRange(Stage.List.Select(s => s.Name).ToList());
         mStagePreviewCarousel.Entries = entries;
         mStagePreviewCarousel.Selected = mSelectedStage?.Name ?? mNoneStage.Name;
         mStagePreviewCarousel.SwivelEvent += delegate (object aSender, string aSelected)
         {
            mGameClient.Send(new ServerSettingsMessage(
               Stage.List.FirstOrDefault(s => s.Name == aSelected)?.ID ?? StageEnum.NONE,
               mMaxPlayerCount,
               mSuddenDeathMode));
         };
         mVisuals.Add(mStagePreviewCarousel);
         mControls.Add(mStagePreviewCarousel);

         mSuddenDeathCarousel = mServiceProvider.GetService<Carousel>();
         mSuddenDeathCarousel.IsEnabled = (Context.IsAdmin == true) || (Context.IsVeteran == true);
         mSuddenDeathCarousel.Width = mStagePreviewCarousel.Width;
         mSuddenDeathCarousel.Height = settingsTextHeight;
         mSuddenDeathCarousel.X = settingsBounds.X + (settingsBounds.Width / 2.0) -
                                  (mSuddenDeathCarousel.Width / 2.0);
         mSuddenDeathCarousel.Y = settingsBounds.Y + settingsBounds.Height - mSuddenDeathCarousel.Height;
         mSuddenDeathCarousel.Entries = new List<string>
         {
            "DOUBLE SHOT",
            "INFINITE SS",
            "BIGBOMB",
            "NONE"
         };
         mSuddenDeathCarousel.Selected = "DOUBLE SHOT";
         mSuddenDeathCarousel.SwivelEvent += delegate (object aSender, string aSelected)
         {
            SuddenDeathModeEnum mode = aSelected switch
            {
               "DOUBLE SHOT" => SuddenDeathModeEnum.DOUBLE,
               "INFINITE SS" => SuddenDeathModeEnum.SS,
               "BIGBOMB" => SuddenDeathModeEnum.BIGBOMB,
               _ => SuddenDeathModeEnum.NONE
            };
            mGameClient.Send(new ServerSettingsMessage(mSelectedStage?.ID ?? StageEnum.NONE,
                                                       mMaxPlayerCount,
                                                       mode));
         };
         mVisuals.Add(mSuddenDeathCarousel);
         mControls.Add(mSuddenDeathCarousel);

         Text suddenDeathLabel = mServiceProvider.GetService<Text>();
         suddenDeathLabel.Value = "SUDDEN DEATH";
         suddenDeathLabel.Color = mConfiguration.SecondaryColor.ToColor();
         suddenDeathLabel.Height = settingsTextHeight;
         suddenDeathLabel.X = settingsBounds.X;
         suddenDeathLabel.Y = mSuddenDeathCarousel.Y - suddenDeathLabel.Height;
         mVisuals.Add(suddenDeathLabel);

         // Create a carousel, and label, for the possible team sizes. That is, a one-on-one battle,
         // two-on-two battle, and so on. This is effectively the setting for the maximum number of
         // players as well.
         // This gets placed halfway between the sudden death carousel and the stage preview.
         double midY = suddenDeathLabel.Y -
                       ((suddenDeathLabel.Y - mStagePreviewCarousel.Y - mStagePreviewCarousel.Height) / 2.0);
         Text teamSizesLabel = mServiceProvider.GetService<Text>();
         teamSizesLabel.Value = "TEAM SIZES";
         teamSizesLabel.Color = mConfiguration.SecondaryColor.ToColor();
         teamSizesLabel.Height = settingsTextHeight;
         teamSizesLabel.X = settingsBounds.X;
         teamSizesLabel.Y = midY - teamSizesLabel.Height;
         mVisuals.Add(teamSizesLabel);

         mTeamSizesCarousel = mServiceProvider.GetService<Carousel>();
         mTeamSizesCarousel.IsEnabled = (Context.IsAdmin == true) || (Context.IsVeteran == true);
         mTeamSizesCarousel.Width = mStagePreviewCarousel.Width;
         mTeamSizesCarousel.Height = settingsTextHeight;
         mTeamSizesCarousel.X = settingsBounds.X +
                                (settingsBounds.Width / 2.0) - (mTeamSizesCarousel.Width / 2.0);
         mTeamSizesCarousel.Y = midY;
         mTeamSizesCarousel.Entries = new List<string>
         {
            "1:1",
            "2:2",
            "3:3",
            "4:4"
         };
         mTeamSizesCarousel.Selected = "4:4";
         mTeamSizesCarousel.SwivelEvent += delegate (object aSender, string aSelected)
         {
            uint count = aSelected switch
            {
               "1:1" => 2,
               "2:2" => 4,
               "3:3" => 6,
               _ => 8
            };
            mGameClient.Send(new ServerSettingsMessage(mSelectedStage?.ID ?? StageEnum.NONE,
                                                       count,
                                                       mSuddenDeathMode));
         };
         mVisuals.Add(mTeamSizesCarousel);
         mControls.Add(mTeamSizesCarousel);

         #endregion Server settings (stage, team size, and sudden death mode selection/display)


         #region Chat

         // Create the chat components.
         // This includes a chat log (new and past messages) and an input for the user.
         mChatInput = mServiceProvider.GetService<TextInput>();
         mChatInput.EnterEvent += OnChatMessageEnter;
         mChatInput.Width = chatBounds.Width;
         mChatInput.X = chatBounds.X;
         mChatInput.Y = chatBounds.Y + chatBounds.Height - mChatInput.Height;
         mVisuals.Add(mChatInput);
         mControls.Add(mChatInput);

         mChatLog = mServiceProvider.GetService<ChatLog>();
         mChatLog.MaxLines = 10;
         mChatLog.X = chatBounds.X;
         mChatLog.Y = chatBounds.Y;
         mChatLog.Width = chatBounds.Width;
         mChatLog.Height = chatBounds.Height - mChatInput.Height;
         mVisuals.Add(mChatLog);

         #endregion Chat


         #region Back, change team, ready, and (possibly hidden) start buttons

         // Create and position the "BACK" button.
         // This is positioned in the bottom left corner of the screen.
         mBackButton = mServiceProvider.GetService<TextButton>();
         mBackButton.Text = "BACK";
         mBackButton.X = buttonBounds.X;
         mBackButton.Y = buttonBounds.Y;
         mBackButton.ClickEvent += OnBackClick;
         mVisuals.Add(mBackButton);
         mControls.Add(mBackButton);

         // Create and postiion the "READY" button.
         // This is positioned in the bottom right corner of the screen.
         mReadyButton = mServiceProvider.GetService<TextButton>();
         mReadyButton.Text = "READY";
         mReadyButton.X = buttonBounds.X + buttonBounds.Width - mReadyButton.Width;
         mReadyButton.Y = buttonBounds.Y;
         mReadyButton.ClickEvent += OnReadyClick;
         mVisuals.Add(mReadyButton);
         mControls.Add(mReadyButton);

         // Create and position the "CHANGE TEAM" button.
         // This button's position is relative to (left of) the "READY" button.
         mChangeTeamButton = mServiceProvider.GetService<TextButton>();
         mChangeTeamButton.Text = "CHANGE TEAM";
         mChangeTeamButton.Y = buttonBounds.Y;
         mChangeTeamButton.X = mReadyButton.X - mChangeTeamButton.Width - padding;
         mChangeTeamButton.ClickEvent += OnChangeTeamClick;
         mVisuals.Add(mChangeTeamButton);
         mControls.Add(mChangeTeamButton);

         // Create and position the "START" button.
         // This button's position is relative to (left of) the "CHANGE TEAM" button.
         // This button will only be displayed if the local player is a veteran or admin and is only
         // enabled when the local player is ready (IsReady == true);
         mStartButton = mServiceProvider.GetService<TextButton>();
         mStartButton.Text = "START";
         mStartButton.IsEnabled = false;
         mStartButton.Y = buttonBounds.Y;
         mStartButton.X = mChangeTeamButton.X - mStartButton.Width - padding;
         mStartButton.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            mGameClient.Send(new Message(Message.Opcode.START_GAME));
         };

         #endregion Back, change team, ready, and (possibly hidden) start buttons


         #region Mobile selection tiles

         // Create the tiles for the mobile selections.
         // There are 14 mobiles but we'll also include one more tile for a random selection
         // resulting in a total of 15 tiles. We'll display them as three rows of five columns,
         // or five rows of three colums, depending on the aspect ratio of the allotted bounds.
         bool isWider = ((float)mobileSelectionBounds.Width / mobileSelectionBounds.Height) > 1.0f;

         // If the bounds' aspect ratio is greater (wider) than 1:1, use a 5 x 3 (row x columns)
         // layout. Otherwise, use a 3 x 5 layout.
         int columns = isWider == true ? 5 : 3;
         int rows = isWider == true ? 3 : 5;

         // Calculate the appropriate tile dimensions. Tile dimensions are not 1:1 but the
         // mobile images inside them will be.
         double tileWidth = mobileSelectionBounds.Width / (double)columns;
         double tileHeight = mobileSelectionBounds.Height / (double)rows;
         List<Mobile> mobiles = Mobile.List; // Get the full list of mobile models.
         for (int row = 0; row < rows; row++)
         {
            for (int column = 0; column < columns; column++)
            {
               MobileTile tile = mServiceProvider.GetService<MobileTile>();
               tile.Width = tileWidth;
               tile.Height = tileHeight;
               tile.X = mobileSelectionBounds.X + (column * tileWidth);
               tile.Y = mobileSelectionBounds.Y + (row * tileHeight);

               if ((row == rows - 1) && (column == columns - 1))
               {
                  // If this is the last (bottom right) tile, use it for random mobile selection.

                  tile.Mobile = new Mobile(MobileEnum.NONE);
               }
               else
               {
                  tile.Mobile = mobiles[row * columns + column];
               }

               tile.ClickEvent += OnMobileTileClick;

               mVisuals.Add(tile);
               mControls.Add(tile);
            }
         }

         #endregion Mobile selection tiles


         #region Item selection (tray and tiles)

         mItemTray = mServiceProvider.GetService<ItemTray>();
         mItemTray.Width = itemSelectionBounds.Width;
         mItemTray.X = itemSelectionBounds.X;
         mItemTray.Y = itemSelectionBounds.Y;
         mItemTray.ClickEvent += OnItemTrayClick;
         mVisuals.Add(mItemTray);
         mControls.Add(mItemTray);

         // Create the tiles for the mobile selections.
         // There are 10 items that use up 15 slots in total. The tiles will have an aspect
         // ratio matching their slot count (i.e. 1:1 or 2:1 for single- and double-slot items,
         // respectively). Like the mobile tiles, this results in a grid of 15 total squares.
         // And like the mobile tiles, we'll display them as three rows of five columns,
         // or five rows of three colums, depending on the aspect ratio of the allotted bounds.
         isWider = ((double)itemSelectionBounds.Width / itemSelectionBounds.Height) > 1.0;

         // If the bounds' aspect ratio is greater (wider) than 1:1, use a 5 x 3 (row x columns)
         // layout. Otherwise, use a 3 x 5 layout.
         columns = isWider == true ? 5 : 3;
         rows = isWider == true ? 3 : 5;

         // Calculate the appropriate tile dimensions. Tile dimensions are not 1:1 but the
         // mobile images inside them will be.
         tileWidth = itemSelectionBounds.Width / (float)columns;
         tileHeight = (itemSelectionBounds.Height - mItemTray.Height - padding) / rows;
         uint[] slotSums = new uint[rows];
         for (int i = 0; i < slotSums.Length; i++)
         {
            slotSums[i] = 0;
         }
         // Cycle through the items being added as tiles.
         foreach (Item item in Item.List)
         {
            // Descend (visually speaking) through the rows and look for the first row in which
            // the item will fit given its number of slots.
            for (int row = 0; row < rows; row++)
            {
               // If placing the item in this row would not exceed the greatest allowable slots.
               if (slotSums[row] + item.Slots <= columns)
               {
                  ItemTile tile = mServiceProvider.GetService<ItemTile>();
                  tile.Width = item.Slots == 1 ? tileWidth : tileWidth * 2.0; // 1x width or 2x width depending on slots.
                  tile.Height = tileHeight;
                  tile.X = itemSelectionBounds.X + (slotSums[row] * tileWidth);
                  tile.Y = itemSelectionBounds.Y + mItemTray.Height + padding + (row * tileHeight);
                  tile.Item = item;
                  tile.ClickEvent += OnItemTileClick;

                  mVisuals.Add(tile);
                  mControls.Add(tile);

                  slotSums[row] += item.Slots;
                  break;
               }
            }
         }

         #endregion Item selection (tray and tiles)


         // Process any backlogged messages (that may have been queued while changing screens) now that
         // all the UI components are available.
         while (mGameClient.HasMessage == true)
         {
            HandleMessage(mGameClient.Head);
         }
      }

      #endregion LoadContent()


      public override void Respond(InputEvent aInput)
      {
         if ((aInput.Op == InputEvent.Opcode.BACK) && (mChatInput?.IsFocused == false))
         {
            // If a back event came in AND the user isn't currently entering a chat message.

            OnBackClick(this, EventArgs.Empty);
         }
         else if (mAlertOverlay != null)
         {
            mAlertOverlay?.Respond(aInput);
         }
         else
         {
            foreach (IControl control in mControls)
            {
               control.Respond(aInput);
            }

            if ((Context.IsAdmin == true) || (Context.IsVeteran == true))
            {
               mStartButton?.Respond(aInput);
            }
         }
      }

      public override void Draw(GameTime aGameTime)
      {
         mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix);

         foreach (IVisual visual in mVisuals)
         {
            visual.Draw();
         }

         if ((Context.IsAdmin == true) || (Context.IsVeteran == true))
         {
            mStartButton?.Draw();
         }

         mAlertOverlay?.Draw();

         mSpriteBatch.End();
      }


      #region Event handlers

      private void OnDisconnect(object aSender, EventArgs aArgs)
      {
         AlertThenExit("The connection to the server was lost without explanation.");
      }

      private void OnLocalPlayerPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
      {
         if (aSender is Player player)
         {
            switch (aArgs.PropertyName)
            {
               case nameof(Player.MobileID):
                  // If another tile is alrighted selected/highlighted, deselect it.
                  if (mSelectedMobileTile != null)
                  {
                     mSelectedMobileTile.IsSelected = false;
                  }

                  if (mPendingMobileTile != null)
                  {
                     // Flag the mobile tile associated with this mobile as selected and remember it.
                     mPendingMobileTile.IsSelected = true;
                     mSelectedMobileTile = mPendingMobileTile;
                     mPendingMobileTile = null;
                  }
                  break;
               case nameof(Player.IsReady):
                  if (mReadyButton != null)
                  {
                     // Add a border to the READY button if the player is ready, or remove it if not.
                     mReadyButton.HasBorder = player.IsReady;
                  }

                  if ((mStartButton != null) && ((Context.IsAdmin == true) || (Context.IsVeteran == true)))
                  {
                     // Enable the start button if the local player is an admin or veteran, and is
                     // now ready. Disable it if not ready.
                     mStartButton.IsEnabled = player.IsReady;
                  }
                  break;
               case nameof(Player.Items):
                  // Tell the item tray to display the player's updated list of items.
                  mItemTray?.Sync(player.Items);
                  break;
            }
         }
      }

      /// <summary>
      /// Invoked when enter is pressed while the chat input is focused.
      /// In other words, the user finished typing a message and hit enter to send it.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnChatMessageEnter(object aSender, EventArgs aArgs)
      {
         // If the user didn't actually enter a message, don't send anything.
         if (string.IsNullOrEmpty(mChatInput.Value) == true)
         {
            return;
         }

         mGameClient.Send(new ChatMessage(Context.LocalPlayerID, false, mChatInput.Value));

         mChatInput.IsFocused = true;
         mChatInput.Value = string.Empty;
      }

      private void OnMobileTileClick(object aSender, EventArgs aArgs)
      {
         if (aSender is MobileTile tile)
         {
            mPendingMobileTile = tile;

            if (Context.LocalPlayer != null)
            {
               // Create a clone of the player and change its mobile ID.
               Player clone = Context.LocalPlayer.Clone() as Player;

               if (tile.Mobile.ID == MobileEnum.NONE)
               {
                  // Type "none" is treated as a random. In other words, the user doesn't care
                  // which mobile they get so we'll choose a random one.
                  clone.MobileID = Mobile.RandomID;
               }
               else
               {
                  clone.MobileID = tile.Mobile.ID;
               }

               // Send the cloned player with a new mobile ID to the server. If the server finds
               // this acceptable, it will respond with the same updated player model.
               mGameClient.Send(new PlayerMessage(clone));
            }
         }
      }

      private void OnItemTrayClick(object aSender, ItemTray.TrayData aData)
      {
         mLog.Debug($"aData = {{ FirstIndex = {aData.FirstIndex}, Item = {aData.Item} }}");

         // Quick null and list size check for a likely impossible case, but just to be safe.
         if (Context.LocalPlayer?.Items.Length > 0)
         {
            // Create a new list of items for the player by getting a copy of the current
            // list, removing the given indexes, and shifting the remaining items to the left.
            Item[] originalItems = (Item[])Context.LocalPlayer.Items.Clone();
            Item[] shiftedItems = new Item[6];

            // Remove the given indexes from the list by setting them to null.
            mLog.Debug($"originalItems[{aData.FirstIndex}] = null");
            originalItems[aData.FirstIndex] = null;
            if (aData.Item.Slots == 2)
            {
               originalItems[aData.FirstIndex + 1] = null;
            }
            int nextOpenIndex = 0;
            for (int i = 0; i < 6; i++)
            {
               Item item = originalItems[i];
               // If this index holds an item.
               if (item != null)
               {
                  shiftedItems[nextOpenIndex++] = item;
                  if (item.Slots > 1)
                  {
                     shiftedItems[nextOpenIndex++] = item;
                  }
                  i += (int)item.Slots - 1; // Increments i an extra index if the item uses two slots.
               }
            }

            // Send a cloned player with the shifted items to the server. If the server finds
            // this acceptable, it will respond with the same updated player model.
            Player clone = Context.LocalPlayer.Clone() as Player;
            clone.Items = shiftedItems;
            mGameClient.Send(new PlayerMessage(clone));
         }
      }

      private void OnItemTileClick(object aSender, EventArgs aArgs)
      {
         if ((aSender is ItemTile tile) && (Context.LocalPlayer != null))
         {
            // Calculate the would-be sum of item slots used if the item represented by this
            // tile is added to the player's items.
            uint itemSlotSum = tile.Item.Slots;
            for (int i = 0; i < 6; i++)
            {
               Item item = Context.LocalPlayer.Items[i];
               if (item != null)
               {
                  itemSlotSum += item.Slots;
                  i += (int)item.Slots - 1; // Increments i an extra index if the item uses two slots.
               }
            }

            // If adding the item would not exceed the maximum allowable slots.
            if (itemSlotSum <= 6)
            {
               // Create a clone of the player and add the item to the clone's items.
               Player clone = Context.LocalPlayer.Clone() as Player;
               // Cycle through the clone's items to find the first open (null) index.
               for (int i = 0; i < 6; i++)
               {
                  // If this index is open.
                  if (clone.Items[i] == null)
                  {
                     // Add the item to this index.
                     clone.Items[i] = new Item(tile.Item);

                     // If this item uses two slots, point the next index to it as well.
                     if (tile.Item.Slots > 1)
                     {
                        clone.Items[i + 1] = clone.Items[i];
                     }

                     // We placed the item at an open index so stop cycling.
                     break;
                  }
               }
               // Send the cloned player with a new item to the server. If the server finds
               // this acceptable, it will respond with the same updated player model.
               mGameClient.Send(new PlayerMessage(clone));
            }
         }
      }

      private void OnBackClick(object aSender, EventArgs aArgs)
      {
         mGameClient.Disconnect();
         bool isSingleplayer = Context.IsSingleplayer;
         Context.Reset();
         if (isSingleplayer == true)
         {
            mWindow.LoadScreen(mServiceProvider?.GetService<MainMenuScreen>());
         }
         else
         {
            mWindow.LoadScreen(mServiceProvider?.GetService<MultiplayerScreen>());
         }
      }

      private void OnReadyClick(object aSender, EventArgs aArgs)
      {
         // If the player is clearly not ready yet.
         if (mSelectedMobileTile == null)
         {
            // Display a message regarding the need for a mobile selection.
            mAlertOverlay = mServiceProvider.GetService<AlertModalWindow>();
            mAlertOverlay.Message = $"Select a mobile first.";
            mAlertOverlay.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
            {
               mAlertOverlay = null;
            };
            return;
         }

         if (Context.LocalPlayer != null)
         {
            // Clone the local player's model. This is done to change one property of the
            // model without modifying the actual player.
            Player clone = Context.LocalPlayer.Clone() as Player;

            // Flip the model's "is ready" flag.
            clone.IsReady = !Context.LocalPlayer.IsReady;

            // Form and send a player message from the modified model. If the server deems
            // this acceptable, the local player will be synced with the model the server
            // sends as a response.
            mGameClient.Send(new PlayerMessage(clone));
         }
      }

      private void OnChangeTeamClick(object aSender, EventArgs aArgs)
      {
         if (Context.LocalPlayer != null)
         {
            // Clone the local player's model. This is done to change one property of the
            // model without modifying the actual player.
            Player clone = Context.LocalPlayer.Clone() as Player;

            // Flip the model's team.
            clone.Team = Context.LocalPlayer.Team == TeamEnum.A ? TeamEnum.B : TeamEnum.A;

            // Form and send a player message from the modified model. If the server deems
            // this acceptable, the local player will be synced with the model the server
            // sends as a response.
            mGameClient.Send(new PlayerMessage(clone));
         }
      }

      #endregion Event handlers


      #region Helper methods

      private void AddPlayer(Player aPlayer)
      {
         double maxTextHeight = (mPlayerListBounds.Height - (mTeamALabel.Height + mTeamBLabel.Height)) / 9.0;

         Text name = mServiceProvider.GetService<Text>();
         name.Value = aPlayer.Name;
         name.Height = Math.Min(mTeamALabel.Height, maxTextHeight);
         if (name.Width > mPlayerListBounds.Width)
         {
            name.Truncate((uint)mPlayerListBounds.Width);
         }
         name.X = mPlayerListBounds.X + (mPlayerListBounds.Width / 2.0) - (name.Width / 2.0); // Centered.

         if (aPlayer.Team == TeamEnum.A)
         {
            name.Y = mTeamALabel.Y + mTeamALabel.Height + (mTeamANamesMap.Count * name.Height);
            mTeamANamesMap[aPlayer.ID] = name;
         }
         else
         {
            name.Y = mTeamBLabel.Y + mTeamBLabel.Height + (mTeamBNamesMap.Count * name.Height);
            mTeamBNamesMap[aPlayer.ID] = name;
         }

         mVisuals.Add(name);
      }

      private void RemovePlayer(Player aPlayer)
      {
         // Start by getting the name Text, team label Text, and list of others Texts
         // for the whole team so we can adjust them rather than leave an empty space.
         Text name;
         Text label;
         List<Text> names;
         if (aPlayer.Team == TeamEnum.A)
         {
            name = mTeamANamesMap[aPlayer.ID];
            mTeamANamesMap.Remove(aPlayer.ID);
            label = mTeamALabel;
            names = mTeamANamesMap.Values.ToList();
         }
         else
         {
            name = mTeamBNamesMap[aPlayer.ID];
            mTeamBNamesMap.Remove(aPlayer.ID);
            label = mTeamBLabel;
            names = mTeamBNamesMap.Values.ToList();
         }

         // Shift the remaining players' names by stacking them below the team's label.
         for (int i = 0; i < names.Count; i++)
         {
            names[i].Y = label.Y + label.Height + (i * names[i].Height);
         }

         mVisuals.Remove(name);
      }

      private void AlertThenExit(string aMessage)
      {
         // If already alerting then exiting.
         if (mIsExiting == true)
         {
            return;
         }

         // Set a flag indicating we're in the process of exiting.
         mIsExiting = true;
         // Display a message to the user, then leave the screen when the message is acknowledged.
         mAlertOverlay = mServiceProvider.GetService<AlertModalWindow>();
         mAlertOverlay.Message = aMessage;
         mAlertOverlay.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
         {
            mAlertOverlay = null;
            OnBackClick(this, EventArgs.Empty);
         };
      }

      #endregion Helper methods


      #region Message handling

      private void OnMessage(object aSender, EventArgs aArgs)
      {
         while ((mGameClient.HasMessage == true) && (mWindow.IsTransitioning == false))
         {
            HandleMessage(mGameClient.Head);
         }
      }

      private void HandleMessage(Message aMessage)
      {
         if (mIsExiting == true)
         {
            return;
         }

         switch (aMessage.Op)
         {
            case Message.Opcode.CHAT:
               if (aMessage is ChatMessage chatMessage)
               {
                  Player player = Context.Players.FirstOrDefault(p => p.ID == chatMessage.PlayerID);
                  mChatLog.Add(player?.Name ?? "????", chatMessage.Message);
               }
               break;
            case Message.Opcode.PLAYER:
               if (aMessage is PlayerMessage playerMessage)
               {
                  Player player = Context.Players.FirstOrDefault(p => p.ID == playerMessage.Player.ID);
                  if (player != null)
                  {
                     TeamEnum oldTeam = player.Team;

                     if (player.Sync(playerMessage.Player) == true)
                     {
                        // If the player moved to the other team.
                        if (oldTeam != player.Team)
                        {
                           RemovePlayer(player);
                           AddPlayer(player);
                        }
                     }
                  }
               }
               break;
            case Message.Opcode.PLAYERS:
               if (aMessage is PlayersMessage playersMessage)
               {
                  List<Player> originalList = new List<Player>(Context.Players);

                  // Get a list of players that are in the incoming list but not the current list.
                  List<Player> joiningPlayers = playersMessage.Players.Except(originalList,
                                                                              Player.Comparer).ToList();
                  foreach (Player player in joiningPlayers)
                  {
                     Context.Players.Add(player.Clone() as Player);
                     AddPlayer(player);

                     // If we're receiving the local player's model for the first time.
                     if ((player.ID == Context.LocalPlayerID) && (Context.LocalPlayer != null))
                     {
                        // The local player model should not have its PropertyChanged event hooked yet
                        // but attempt to unhook it just in case. If not hooked, this will do nothing.
                        Context.LocalPlayer.PropertyChanged -= OnLocalPlayerPropertyChanged;

                        // Hook the PropertyChanged event in order to update UI elements derived from
                        // some of the model's properties.
                        Context.LocalPlayer.PropertyChanged += OnLocalPlayerPropertyChanged;
                     }
                  }

                  // Get a list of players that are in the current list but not the incoming list.
                  List<Player> departingPlayers = originalList.Except(playersMessage.Players,
                                                                      Player.Comparer).ToList();
                  foreach (Player player in departingPlayers)
                  {
                     RemovePlayer(player);
                     Context.Players.Remove(player);
                  }

                  // Sync the players with the updates from the server.
                  foreach (Player updatedPlayer in playersMessage.Players)
                  {
                     Context.Players.FirstOrDefault(p => p.ID == updatedPlayer.ID)?.Sync(updatedPlayer);
                  }
               }
               break;
            case Message.Opcode.SERVER_SETTINGS:
               if (aMessage is ServerSettingsMessage serverSettingsMessage)
               {
                  Context.ServerSettings = serverSettingsMessage;

                  // Update the state preview image and carousel to match the selected stage.
                  if (serverSettingsMessage.StageID != StageEnum.NONE)
                  {
                     // If a specific stage has beene selected.

                     mSelectedStage = new Stage(serverSettingsMessage.StageID);
                     Context.Stage = mSelectedStage;
                  }
                  else
                  {
                     // If no stage has been selected, treat it as a random selection.

                     mSelectedStage = null;
                     Context.Stage = null;
                  }

                  if (mStagePreviewImage != null)
                  {
                     mStagePreviewImage.Content = mContentWrapper.StageForegroundPath(mSelectedStage ?? mNoneStage);
                     mStagePreviewImage.CenterInBounds(mPreviewBounds);
                  }

                  if (mStagePreviewCarousel != null)
                  {
                     mStagePreviewCarousel.Selected = mSelectedStage?.Name ?? mNoneStage.Name;
                  }

                  // Update the team size carousel to match the max player count.
                  mMaxPlayerCount = serverSettingsMessage.MaxPlayerCount;
                  if (mTeamSizesCarousel != null)
                  {
                     mTeamSizesCarousel.Selected = serverSettingsMessage.MaxPlayerCount switch
                     {
                        2 => "1:1",
                        4 => "2:2",
                        6 => "3:3",
                        _ => "4:4"
                     };
                  }

                  // Update the sudden death mode carousel to match the selected mode.
                  mSuddenDeathMode = serverSettingsMessage.SuddenDeathMode;
                  if (mSuddenDeathCarousel != null)
                  {
                     mSuddenDeathCarousel.Selected = serverSettingsMessage.SuddenDeathMode switch
                     {
                        SuddenDeathModeEnum.DOUBLE => "DOUBLE SHOT",
                        SuddenDeathModeEnum.SS => "INFINITE SS",
                        SuddenDeathModeEnum.BIGBOMB => "BIGBOMB",
                        _ => "NONE"
                     };
                  }
               }
               break;
            case Message.Opcode.SERVER_SHUTDOWN:
               if (aMessage is ServerShutdownMessage serverShutdownMessage)
               {
                  if (string.IsNullOrEmpty(serverShutdownMessage.Reason) == false)
                  {
                     // If a reason for the server's shutdown was provided.

                     // Display a message including the reason before leaving the screen.
                     AlertThenExit($"The server shut down: {serverShutdownMessage.Reason}");
                  }
                  else
                  {
                     // If no reason for the shutdown was provided.

                     // In practice, this will only happen when smoothly exiting a single-player
                     // game and displaying a message to the user could only be confusing.
                     // Display nothing and immediately exit the screen.
                     OnBackClick(this, EventArgs.Empty);
                  }
               }
               break;
            case Message.Opcode.SERVER_STATE:
               if (aMessage is ServerStateMessage serverStateMessage)
               {
                  if (serverStateMessage.State == StateEnum.PRE_GAME)
                  {
                     // If the server is announcing its transition to the pre-game state.

                     // Follow the server's lead by loading the pre-game screen.
                     mWindow.LoadScreen(mServiceProvider?.GetService<PregameScreen>());
                  }
                  else
                  {
                     // If the server is transitioning to any other state.

                     // It doesn't work like that. The server should only ever transition to the
                     // pre-game state from the lobby state. If it's going somewhere else, we'll
                     // consider it modified and likely hostile.
                     AlertThenExit("The server acted irregularly. Disconnecting.");
                  }
               }
               break;
         }
      }

      #endregion Message handling
   }
}
