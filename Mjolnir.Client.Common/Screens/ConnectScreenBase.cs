﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.UI;
using Mjolnir.Common;
using Mjolnir.Common.Collections;
using Mjolnir.Common.Messages;
using Mjolnir.Common.Models;
using Mjolnir.Common.Network;
using NLog;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Mjolnir.Client.Common.Screens
{
   /// <summary>
   /// This class provides common code for the Multiplayer and Direct Connect screens.
   /// This Screen is not registered with the service collection and should not be instantiated.
   /// </summary>
   public abstract class ConnectScreenBase : Screen
   {
      protected readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      protected readonly IServiceProvider mServiceProvider;
      protected readonly Window mWindow;
      protected readonly ResolutionIndependentViewport mViewport;
      protected readonly Camera mCamera;
      protected readonly SpriteBatch mSpriteBatch;
      protected readonly GameClient mGameClient;
      protected readonly ConcurrentList<IVisual> mVisuals = new ConcurrentList<IVisual>();
      protected readonly ConcurrentList<IControl> mControls = new ConcurrentList<IControl>();
      protected TextButton mBackButton = null;
      protected TextButton mConnectButton = null;
      protected InputModalWindow mPasswordModalWindow = null;
      protected AlertModalWindow mAlertModalWindow = null;
      protected bool mIsJoining = false;

      /// <summary>
      /// Getter to be implemented that returns the address/hostname string to connect to.
      /// </summary>
      protected abstract string Address { get; }
      /// <summary>
      /// Getter to be implemented that returns the port to connect to.
      /// </summary>
      protected abstract int Port { get; }

      public override string Name => string.Empty; // Overridden by the child Screens.

      public ConnectScreenBase(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mWindow = aServiceProvider.GetService<Window>();
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mCamera = aServiceProvider.GetService<Camera>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mGameClient = aServiceProvider.GetService<GameClient>();
      }

      public override void Dispose()
      {
         mGameClient.ConnectEvent -= OnConnect;
         mGameClient.ConnectRefusalEvent -= OnConnectFailed;
         mGameClient.MessageEvent -= OnMessage;
      }

      public override void Initialize()
      {
         mGameClient.ConnectEvent += OnConnect;
         mGameClient.ConnectRefusalEvent += OnConnectFailed;
         mGameClient.MessageEvent += OnMessage;
      }

      public override void Draw(GameTime aGameTime)
      {
         mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix);

         foreach (IVisual visual in mVisuals)
         {
            visual.Draw();
         }

         mPasswordModalWindow?.Draw();
         mAlertModalWindow?.Draw();

         mSpriteBatch.End();
      }


      #region Event handlers

      /// <summary>
      /// Invoked when escape is pressed or the "BACK" button is clicked.
      /// Returns to the previous screen.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      protected abstract void OnBackClick(object aSender, EventArgs aArgs);

      /// <summary>
      /// Invoked when the "CONNECT" button is clicked or when enter is pressed while
      /// the address input is focused.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      protected void OnConnectClick(object aSender, EventArgs aArgs)
      {
         // Disable the join button while the connection is attempted and have it display
         // "CONNECTING..." to indicate to the user that the client is working on something.
         mConnectButton.IsEnabled = false;
         mConnectButton.Text = "CONNECTING...";

         // Attempt the connection. This will soon either invoke the ConnectEvent or
         // ConnectFailedEvent, which will be handled by other methods.
         mGameClient.Connect(Address, Port);
      }


      /// <summary>
      /// Invoked when a connection is successfully made to the selected server.
      /// Begins the join process.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      protected void OnConnect(object aSender, EventArgs aArgs)
      {
         mIsJoining = true;

         // Spawn a task to handle a timeout. The server is expected to send an
         // "ahoy" message which we'll respond to. However, it's also possible we
         // connected to something other than a game server, it's malfunctioning,
         // or modified.
         Task.Run(async () =>
         {
            await Task.Delay(2000);

            // If the client is still attempting to join (awaiting an "ahoy" message).
            if (mIsJoining == true)
            {
               mIsJoining = false;
               OnConnectFailed(this, EventArgs.Empty);
            }
         });
      }

      /// <summary>
      /// Invoked when an attempt to connect to a server is unsuccessful.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      protected void OnConnectFailed(object aSender, EventArgs aArgs)
      {
         // Return the join button to its initial state.
         mConnectButton.Text = "CONNECT";
         mConnectButton.IsEnabled = true;

         // Give up on the connection.
         mGameClient.Disconnect();
      }

      #endregion Event handlers


      #region Message handler

      /// <summary>
      /// Invoked when a message is received from the server.
      /// Handles only the messages relevant to this screen.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      protected void OnMessage(object aSender, EventArgs aArgs)
      {
         if (mWindow.IsTransitioning == true)
         {
            return;
         }

         while (mGameClient.HasMessage == true)
         {
            Message message = mGameClient.Head;
            switch (message.Op)
            {
               case Message.Opcode.AHOY:
                  if (message is AhoyMessage ahoyMessage)
                  {
                     mIsJoining = false;

                     // Grab the configuration and try to find an address-password pair for the
                     // server being joined.
                     Configuration config = mServiceProvider.GetService<Configuration>();
                     Configuration.HostnamePasswordPair addressPasswordPair =
                         config.AddressPasswordPairs.FirstOrDefault(p => p.Hostname == Address);

                     // If the server is private and requires a password.
                     if (ahoyMessage.IsPrivate == true)
                     {
                        // Create a modal window with text input in order to ask for the password.
                        mPasswordModalWindow = mServiceProvider.GetService<InputModalWindow>();
                        mPasswordModalWindow.Message = $"\"{ahoyMessage.ServerName}\" requires a password:";
                        mPasswordModalWindow.CancelEvent += delegate (object aSender, EventArgs aArgs)
                        {
                           mPasswordModalWindow = null;
                           OnConnectFailed(this, EventArgs.Empty);
                        };
                        mPasswordModalWindow.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
                        {
                           // Send the response message to the server. It will either respond with a
                           // welcome message or a kick message. Both cases are handled by another method.
                           mGameClient.Send(new AhoyHoyMessage(config.PlayerName,
                                                               mPasswordModalWindow.Value,
                                                               addressPasswordPair?.AdminPassword ?? string.Empty));
                           mPasswordModalWindow = null;
                        };
                     }
                     // If the server is public and we don't need any information from the user.
                     else
                     {
                        // Immediately send a response to the "ahoy." It will either respond with a
                        // welcome message or a kick message. Both cases are handled by another method.
                        mGameClient.Send(new AhoyHoyMessage(config.PlayerName,
                                                            string.Empty,
                                                            addressPasswordPair?.AdminPassword ?? string.Empty));
                     }
                  }
                  break;
               case Message.Opcode.KICK:
                  if (message is KickMessage kickMessage)
                  {
                     // Nullify the password modal window just in case. It may already be null but,
                     // in the event that the user is entering a password and took an excessively
                     // long time, they will have timed out and should be told.
                     mPasswordModalWindow = null;

                     // Create the alert and choose a message to display based on the reason
                     // the player was kicked from the server.
                     mAlertModalWindow = mServiceProvider.GetService<AlertModalWindow>();
                     switch (kickMessage.Reason)
                     {
                        case KickReasonEnum.TIMEOUT:
                           mAlertModalWindow.Message = "Timed out.";
                           break;
                        case KickReasonEnum.INCORRECT_PASSWORD:
                           mAlertModalWindow.Message = "Incorrect password.";
                           break;
                        case KickReasonEnum.BAN:
                           mAlertModalWindow.Message = "You are banned from this server.";
                           break;
                        case KickReasonEnum.KICK:
                           mAlertModalWindow.Message = "Another person kicked you from this server.";
                           break;
                        case KickReasonEnum.NOT_NOW:
                           mAlertModalWindow.Message = "This server's game has already started.";
                           break;
                     }
                     mAlertModalWindow.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
                     {
                        mAlertModalWindow = null;
                        OnConnectFailed(this, EventArgs.Empty);
                     };

                     // The server dumped us. Time to move on.
                     mGameClient.Disconnect();
                  }
                  break;
               case Message.Opcode.PLAYERS:
                  if (message is PlayersMessage playersMessage)
                  {
                     // Add a new instance for each player.
                     foreach (Player player in playersMessage.Players)
                     {
                        Context.Players.Add(new Player(player));
                     }
                  }
                  break;
               case Message.Opcode.WELCOME:
                  if (message is WelcomeMessage welcomeMessage)
                  {
                     Context.LocalPlayerID = welcomeMessage.PlayerID;
                     Context.IsAdmin = welcomeMessage.IsAdmin;
                     switch (welcomeMessage.StateCode)
                     {
                        case StateEnum.LOBBY:
                           mWindow.LoadScreen(mServiceProvider.GetService<LobbyScreen>());
                           break;
                     }
                  }
                  break;
            }
         }
      }

      #endregion Message handler
   }
}
