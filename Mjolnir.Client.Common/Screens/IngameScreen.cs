﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Client.Common.UI;
using Mjolnir.Client.Common.Views;
using Mjolnir.Common;
using Mjolnir.Common.Collections;
using Mjolnir.Common.Messages;
using Mjolnir.Common.Models;
using Mjolnir.Common.Network;
using Mjolnir.Common.Physics;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Mjolnir.Client.Common.Screens
{
   public class IngameScreen : Screen
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly Configuration mConfiguration;
      private readonly Window mWindow;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly Camera mCamera;
      private readonly ContentWrapper mContentWrapper;
      private readonly SpriteBatch mSpriteBatch;
      private readonly GameClient mGameClient;
      private readonly World mWorld;
      private readonly TurnList mTurnList = new TurnList();
      private string mDoAReason = string.Empty; // Dead on arrival (DoA) reason.
      private bool mIsDeadOnArrival = false;
      private bool mIsExiting = false;

      // Status variables.
      private uint mTurnCount = 0; // Continuously incremented value that tracks the number of turns.
      private uint mActivePlayerID = uint.MaxValue; // ID number of the player currently taking his/her turn.

      // Input variables.
      private bool mIsWalkHeld = false;
      private bool mIsUpHeld = false;
      private bool mIsDownHeld = false;
      private bool mIsSpaceHeld = false;

      // Generic UI variables.
      private readonly ConcurrentList<IVisual> mWorldVisuals = new ConcurrentList<IVisual>();
      private readonly ConcurrentList<IVisual> mInterfaceVisuals = new ConcurrentList<IVisual>();
      private readonly ConcurrentList<ButtonBase> mButtons = new ConcurrentList<ButtonBase>();
      private ConfirmationModalWindow mConfirmationModalWindow = null;
      private AlertModalWindow mAlertModalWindow = null;
      private WindVane mWindVane = null;
      private double mPadding = 0.0;

      // Top taskbar UI variables.
      private ItemTray mItemTray = null;
      private TextButton mSkipButton = null;

      // Bottom taskbar UI variables.
      private TextButton mShot1Button;
      private TextButton mShot2Button;
      private TextButton mShotSSButton;
      private Quad mSelectedShotIndicator;
      private Text mFireAngleText = null;
      private Text mMoveCountdownText = null;
      private Text mTeamAScoreText = null;
      private Text mTeamBScoreText = null;
      private PowerBar mFirePowerBar = null;

      // Chat UI variables.
      private ChatLog mChatLog = null;
      private TextInput mChatInput = null;
      private TextButton mChatPrivacyButton = null;
      private bool mIsChatTeamPrivate = false;

      // Model view variables (stage, players, projectiles, etc.).
      private readonly Dictionary<uint, PlayerView> mPlayerViewsMap = new Dictionary<uint, PlayerView>();
      private readonly ConcurrentList<ProjectileView> mProjectileViews = new ConcurrentList<ProjectileView>();
      private StageView mStageView = null;
      private TurnListView mTurnListView = null;

      public override string Name => "In-game";

      public IngameScreen(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mConfiguration = aServiceProvider.GetService<Configuration>();
         mWindow = aServiceProvider.GetService<Window>();
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mCamera = aServiceProvider.GetService<Camera>();
         mContentWrapper = aServiceProvider.GetService<ContentWrapper>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mGameClient = aServiceProvider.GetService<GameClient>();

         mWorld = new World(Context.Stage);
      }

      public override void Dispose()
      {
         if (mIsDeadOnArrival == false)
         {
            mGameClient.DisconnectEvent -= OnDisconnect;
            mGameClient.MessageEvent -= OnMessage;
            mWorld.LandingEvent -= OnLandingEvent;
            mWorld.HaltEvent -= OnHaltedEvent;
            mWorld.DeathEvent -= OnDeathEvent;
            mWorld.ShootingEvent -= OnShootingEvent;

            mWorld.Clear();
            mTurnList.Clear();
         }

         mStageView?.Dispose();
         mTurnListView?.Dispose();
         lock (mPlayerViewsMap)
         {
            foreach (PlayerView playerView in mPlayerViewsMap.Values)
            {
               playerView.Dispose();
            }
         }
         foreach (ProjectileView projectileView in mProjectileViews)
         {
            projectileView.Dispose();
         }

         if (Context.LocalPlayer != null)
         {
            Context.LocalPlayer.PropertyChanged -= OnLocalPlayerPropertyChanged;
         }
      }

      public override void Initialize()
      {
         // If the stage is either null or the failed to load.
         // Note: the stage *should* be set by the lobby or pre-game Screens already but we
         //       shouldn't assume the server is friendly nor that the connection is reliable.
         // Note: loading the stage (reading its foreground image) *should* always succeed if
         //       this game is unmodified but, like the other case, that shouldn't be assumed.
         if (!Context.Stage?.Load(mContentWrapper) ?? true)
         {
            string message;
            if (Context.Stage == null)
            {
               message = "No stage was set when entering the in-game screen";
            }
            else
            {
               message = $"Failed to load the stage {Context.Stage.Name} from disk";
            }

            mLog.Error(message);
            mDoAReason = $"{message}. The game cannot continue.";

            // Set a flag to alert the user to the problem and, following that, return
            // to the previous Screen. This will be done after LoadContent() completes
            // in order to maintain the Screen initialize/load conventions.
            mIsDeadOnArrival = true;
            return; // No need to initialize any further.
         }

         mGameClient.DisconnectEvent += OnDisconnect;
         mGameClient.MessageEvent += OnMessage;
         mWorld.LandingEvent += OnLandingEvent;
         mWorld.HaltEvent += OnHaltedEvent;
         mWorld.DeathEvent += OnDeathEvent;
         mWorld.ShootingEvent += OnShootingEvent;

         foreach (Player player in Context.Players)
         {
            mWorld.Add(player);
            mTurnList.Add(player);
         }

         if (Context.LocalPlayer != null)
         {
            Context.LocalPlayer.PropertyChanged += OnLocalPlayerPropertyChanged;
         }
      }

      public override void LoadContent()
      {
         mPadding = mViewport.VirtualHeight * 0.01;
         double horizontalPadding = mViewport.VirtualHeight * 0.05;
         // Calculate bounds to be applied to the UI components. These bounds are not applied to the
         // task bars' backgrounds which will extend to the edges of the viewport. And, unlike other
         // screens, the padding is only applied horizontally here.
         Rectangle paddedBounds = new Rectangle((int)horizontalPadding,
                                                0,
                                                (int)(mViewport.VirtualWidth - (2.0 * horizontalPadding)),
                                                mViewport.VirtualHeight);


         #region Stage view

         mStageView = mServiceProvider.GetService<StageView>();
         mStageView.Model = Context.Stage;
         mCamera.Bounds = mStageView.CameraBounds;

         #endregion Stage view


         #region Player views

         foreach (Player player in Context.Players)
         {
            PlayerView playerView = mServiceProvider.GetService<PlayerView>();
            playerView.Model = player;
            mWorldVisuals.Add(playerView);
            lock (mPlayerViewsMap)
            {
               mPlayerViewsMap.Add(player.ID, playerView);
            }
         }

         #endregion Player views


         #region Top task bar

         Quad topTaskBar = mServiceProvider.GetService<Quad>();
         topTaskBar.Width = mViewport.VirtualWidth;
         topTaskBar.Height = Math.Ceiling(Text.DefaultHeight);
         topTaskBar.X = 0;
         topTaskBar.Y = 0;
         topTaskBar.Color = Color.Black;
         mInterfaceVisuals.Add(topTaskBar);


         #region Back button

         TextButton backButton = mServiceProvider.GetService<TextButton>();
         backButton.Text = "BACK";
         backButton.Height = topTaskBar.Height;
         backButton.X = paddedBounds.X;
         backButton.Y = topTaskBar.Y;
         backButton.ClickEvent += OnBackClick;
         mInterfaceVisuals.Add(backButton);
         mButtons.Add(backButton);

         #endregion Top task bar


         #region Item tray

         mItemTray = mServiceProvider.GetService<ItemTray>();
         mItemTray.Height = topTaskBar.Height; // Also sets the width.
         mItemTray.X = paddedBounds.X + (paddedBounds.Width / 2.0) - (mItemTray.Width / 2.0);
         mItemTray.Y = paddedBounds.Y;
         mItemTray.Sync(Context.LocalPlayer?.Items);
         mItemTray.ClickEvent += OnItemTrayClick;
         mInterfaceVisuals.Add(mItemTray);

         #endregion Item tray


         #region Skip button

         mSkipButton = mServiceProvider.GetService<TextButton>();
         mSkipButton.Text = "SKIP";
         mSkipButton.Height = topTaskBar.Height;
         mSkipButton.X = paddedBounds.X + paddedBounds.Width - mSkipButton.Width;
         mSkipButton.Y = topTaskBar.Y;
         mSkipButton.IsEnabled = false;
         mSkipButton.ClickEvent += OnSkipClick;
         mInterfaceVisuals.Add(mSkipButton);
         mButtons.Add(mSkipButton);

         #endregion Skip button


         #endregion Top task bar


         #region Wind vane

         mWindVane = mServiceProvider.GetService<WindVane>();
         mWindVane.Height = mViewport.VirtualHeight / 10.0;
         // Horizontally center the wind vane.
         mWindVane.X = paddedBounds.X + (paddedBounds.Width / 2.0) - (mWindVane.Width / 2.0);
         // Place the wind vane just below the top task bar.
         mWindVane.Y = topTaskBar.Y + topTaskBar.Height + mPadding;
         // Give the wind vane an initial speed and direction.
         mWindVane.Wind = mWorld.Wind;
         mInterfaceVisuals.Add(mWindVane);

         #endregion Wind vanes


         #region Bottom task bar

         Quad bottomTaskBar = mServiceProvider.GetService<Quad>();
         bottomTaskBar.Width = mViewport.VirtualWidth;
         bottomTaskBar.Height = topTaskBar.Height;
         bottomTaskBar.X = 0;
         bottomTaskBar.Y = mViewport.VirtualHeight - bottomTaskBar.Height;
         bottomTaskBar.Color = Color.Black;
         mInterfaceVisuals.Add(bottomTaskBar);


         #region Shot selection buttons

         mShot1Button = mServiceProvider.GetService<TextButton>();
         mShot2Button = mServiceProvider.GetService<TextButton>();
         mShotSSButton = mServiceProvider.GetService<TextButton>();
         mSelectedShotIndicator = mServiceProvider.GetService<Quad>();
         mShot1Button.Text = "1";
         mShot1Button.Height = bottomTaskBar.Height;
         mShot1Button.X = paddedBounds.X;
         mShot1Button.Y = bottomTaskBar.Y;
         mShot1Button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            if (Context.LocalPlayer != null)
            {
               // Create a clone of the player and change its selected shot.
               Player clone = new Player(Context.LocalPlayer)
               {
                  ShotID = ShotEnum.SHOT_1
               };
               // Send the cloned player with a new selected shot to the server. If the server finds
               // this acceptable, it will respond with the same updated player model.
               mGameClient.Send(new PlayerMessage(clone));
            }
         };
         mInterfaceVisuals.Add(mShot1Button);
         mButtons.Add(mShot1Button);

         mShot2Button.Text = "2";
         mShot2Button.Height = bottomTaskBar.Height;
         mShot2Button.X = mShot1Button.X + mShot1Button.Width;
         mShot2Button.Y = bottomTaskBar.Y;
         mShot2Button.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            if (Context.LocalPlayer != null)
            {
               Player clone = new Player(Context.LocalPlayer)
               {
                  ShotID = ShotEnum.SHOT_2
               };
               mGameClient.Send(new PlayerMessage(clone));
            }
         };
         mInterfaceVisuals.Add(mShot2Button);
         mButtons.Add(mShot2Button);

         mShotSSButton.Text = "SS";
         mShotSSButton.Height = bottomTaskBar.Height;
         mShotSSButton.X = mShot2Button.X + mShot2Button.Width;
         mShotSSButton.Y = bottomTaskBar.Y;
         mShotSSButton.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            if (Context.LocalPlayer != null)
            {
               Player clone = new Player(Context.LocalPlayer)
               {
                  ShotID = ShotEnum.SPECIAL_SHOT
               };
               mGameClient.Send(new PlayerMessage(clone));
            }
         };
         mInterfaceVisuals.Add(mShotSSButton);
         mButtons.Add(mShotSSButton);

         // Create the selected shot indicator (just a contrasting bar that appears at the top of the button)
         // with a static height and Y position.
         mSelectedShotIndicator.Color = mConfiguration.SecondaryColor.ToColor();
         mSelectedShotIndicator.Height = mShot1Button.Height * 0.1;
         mSelectedShotIndicator.Y = mShot1Button.Y;
         // Initially place the indicator above the initial selected shot and match its width.
         TextButton selectedShotButton = Context.LocalPlayer?.ShotID switch
         {
            ShotEnum.SHOT_2 => mShot2Button,
            ShotEnum.SPECIAL_SHOT => mShotSSButton,
            _ => mShot1Button,
         };
         mSelectedShotIndicator.Width = selectedShotButton.Width;
         mSelectedShotIndicator.X = selectedShotButton.X;
         mInterfaceVisuals.Add(mSelectedShotIndicator);

         #endregion Shot selection buttons


         #region Team B score

         mTeamBScoreText = mServiceProvider.GetService<Text>();
         mTeamBScoreText.Value = "B: 0";
         mTeamBScoreText.X = paddedBounds.X + paddedBounds.Width - mTeamBScoreText.Width;
         mTeamBScoreText.Y = bottomTaskBar.Y + (bottomTaskBar.Height / 2.0) - (mTeamBScoreText.Height / 2.0);
         mInterfaceVisuals.Add(mTeamBScoreText);

         #endregion Team B score


         #region Team A score

         mTeamAScoreText = mServiceProvider.GetService<Text>();
         mTeamAScoreText.Value = "A: 0";
         mTeamAScoreText.X = mTeamBScoreText.X - mTeamAScoreText.Width - horizontalPadding;
         mTeamAScoreText.Y = bottomTaskBar.Y + (bottomTaskBar.Height / 2.0) - (mTeamAScoreText.Height / 2.0);
         mInterfaceVisuals.Add(mTeamAScoreText);

         #endregion Team A score


         #region Move countdown text

         mMoveCountdownText = mServiceProvider.GetService<Text>();
         mMoveCountdownText.Value = "00.0s";
         mMoveCountdownText.X = mTeamAScoreText.X - mMoveCountdownText.Width - horizontalPadding;
         mMoveCountdownText.Y = bottomTaskBar.Y + (bottomTaskBar.Height / 2.0) - (mMoveCountdownText.Height / 2.0);
         mInterfaceVisuals.Add(mMoveCountdownText);

         #endregion Move countdown text


         #region Fire angle and power

         Rectangle fireBounds = new Rectangle((int)(mShotSSButton.X + mShotSSButton.Width),
                                              (int)bottomTaskBar.Y,
                                              (int)(mMoveCountdownText.X - mShotSSButton.X - mShotSSButton.Width),
                                              (int)bottomTaskBar.Height);
         // Apply some padding by increasing the X and decreasing the width by twice the X offset.
         fireBounds.X += (int)(1.0 * horizontalPadding);
         fireBounds.Width -= (int)(2.0 * horizontalPadding);


         #region Fire angle text

         mFireAngleText = mServiceProvider.GetService<Text>();
         mFireAngleText.Value = "45";
         mFireAngleText.X = fireBounds.X;
         mFireAngleText.Y = fireBounds.Y + (bottomTaskBar.Height / 2.0) - (mFireAngleText.Height / 2.0);
         mInterfaceVisuals.Add(mFireAngleText);

         #endregion Fire angle text


         #region Fire power bar

         mFirePowerBar = mServiceProvider.GetService<PowerBar>();
         mFirePowerBar.HasBorder = true;
         mFirePowerBar.Width = fireBounds.Width - mFireAngleText.Width - horizontalPadding;
         mFirePowerBar.Height = fireBounds.Height / 2.0;
         mFirePowerBar.X = mFireAngleText.X + mFireAngleText.Width + horizontalPadding;
         mFirePowerBar.Y = fireBounds.Y + (fireBounds.Height / 2.0) - (mFirePowerBar.Height / 2.0);
         mFirePowerBar.Percentage = 0.0;
         mInterfaceVisuals.Add(mFirePowerBar);

         #endregion Fire power bar


         #endregion Fire angle and power


         #endregion Bottom task bar


         #region Turn list view

         mTurnListView = mServiceProvider.GetService<TurnListView>();
         mTurnListView.Model = mTurnList;
         mTurnListView.Height = paddedBounds.Height / 4.0;
         mTurnListView.X = mViewport.VirtualWidth - mTurnListView.Width - mPadding;
         mTurnListView.Y = topTaskBar.Y + topTaskBar.Height + mPadding;
         mInterfaceVisuals.Add(mTurnListView);

         #endregion Turn list view


         #region Chat input

         mChatPrivacyButton = mServiceProvider.GetService<TextButton>();
         // The default privacy for chat will be 'all' but 'team' is used here for sizing.
         // Once the privacy button and adjacent chat input are placed and sized, the default
         // text will be changed.
         mChatPrivacyButton.Text = "TEAM";
         mChatPrivacyButton.X = mPadding;
         //mChatPrivacyButton.Y = bottomTaskBar.Y - mChatPrivacyButton.Height - padding;
         mChatPrivacyButton.ClickEvent += OnChatPrivacyClick;
         mInterfaceVisuals.Add(mChatPrivacyButton);
         mButtons.Add(mChatPrivacyButton);

         mChatInput = mServiceProvider.GetService<TextInput>();
         // The privacy button and chat input combined will use half the viewport width.
         mChatInput.Width = (paddedBounds.Width / 2.0) - mChatPrivacyButton.Width;
         // Place the chat input to the right of the chat privacy button, at the same Y.
         mChatInput.X = mChatPrivacyButton.X + mChatPrivacyButton.Width;
         mChatInput.Y = bottomTaskBar.Y - mChatInput.Height - mPadding;
         mChatInput.EnterEvent += OnChatMessageEnter;
         mInterfaceVisuals.Add(mChatInput);

         // With sizing done, set the privacy button's text to its actual default privacy.
         mChatPrivacyButton.Text = "ALL";
         mChatPrivacyButton.Y = mChatInput.Y + (mChatInput.Height / 2.0) - (mChatPrivacyButton.Height / 2.0);

         #endregion Chat input


         #region Chat log

         mChatLog = mServiceProvider.GetService<ChatLog>();
         mChatLog.MaxLines = 8;
         mChatLog.Width = mChatInput.X - mChatPrivacyButton.X + mChatInput.Width; // Width of the button + input.
         mChatLog.Height = paddedBounds.Height / 4.0;
         mChatLog.X = mChatPrivacyButton.X;
         mChatLog.Y = mChatInput.Y - mChatLog.Height - mPadding;
         mInterfaceVisuals.Add(mChatLog);

         #endregion Chat log


         // If the game cannot continue.
         if (mIsDeadOnArrival == true)
         {
            // Display an alert modal window notifying the user that a fatal error occurred then
            // leave the game when acknowledged.
            AlertThenExit(mDoAReason);
         }
         else
         {
            // Process any backlogged messages (that may have been queued while changing screens) now that
            // all the UI components are available.
            while (mGameClient.HasMessage == true)
            {
               HandleMessage(mGameClient.Head);
            }
         }
      }

      public override void Respond(InputEvent aInput)
      {
         if ((aInput.Op == InputEvent.Opcode.BACK) && (mChatInput?.IsFocused == false))
         {
            // If a back event came in AND the user isn't currently entering a chat message.

            OnBackClick(this, EventArgs.Empty);
         }
         else if ((mConfirmationModalWindow != null) || (mAlertModalWindow != null))
         {
            mConfirmationModalWindow?.Respond(aInput);
            mAlertModalWindow?.Respond(aInput);
         }
         else
         {
            foreach (ButtonBase button in mButtons)
            {
               button.Respond(aInput);
            }

            mItemTray?.Respond(aInput);
            mChatInput?.Respond(aInput);
            if (mChatInput?.IsFocused == true)
            {
               mChatInput.HasBackground = true;
            }
            else if (mChatInput != null)
            {
               mChatInput.HasBackground = false;
            }


            #region Desktop keyboard input response

            // If running on a desktop with a keyboard.
            if (MjolnirGame.IsDesktop == true)
            {
               switch (aInput.Op)
               {
                  case InputEvent.Opcode.KEY_DOWN:
                     switch (aInput.Key)
                     {
                        case Keys.Left:
                           mIsWalkHeld = true;
                           DoWalk(Direction.LEFT);
                           break;
                        case Keys.Right:
                           mIsWalkHeld = true;
                           DoWalk(Direction.RIGHT);
                           break;
                        case Keys.Up:
                           mIsUpHeld = true;
                           DecrementAim();
                           Task.Run(async () =>
                           {
                              await Task.Delay(250);
                              while (mIsUpHeld == true)
                              {
                                 DecrementAim();
                                 await Task.Delay(50);
                              }
                           });
                           break;
                        case Keys.Down:
                           mIsDownHeld = true;
                           IncrementAim();
                           Task.Run(async () =>
                           {
                              await Task.Delay(250);
                              while (mIsDownHeld == true)
                              {
                                 IncrementAim();
                                 await Task.Delay(50);
                              }
                           });
                           break;
                        case Keys.Space:
                           mIsSpaceHeld = true;
                           DoCharge();
                           break;
                     }
                     break;
                  case InputEvent.Opcode.KEY_UP:
                     switch (aInput.Key)
                     {
                        case Keys.Left:
                        case Keys.Right:
                           mIsWalkHeld = false;
                           break;
                        case Keys.Up:
                           mIsUpHeld = false;
                           break;
                        case Keys.Down:
                           mIsDownHeld = false;
                           break;
                        case Keys.Space:
                           mIsSpaceHeld = false;
                           break;
                     }
                     break;
               }
            }

            #endregion Desktop keyboard input response
         }
      }

      public override void Draw(GameTime aGameTime)
      {
         // Parallaxed draw.
         mSpriteBatch.Begin(transformMatrix: mCamera.ParallaxViewMatrix);
         mStageView?.DrawBackground();
         mSpriteBatch.End();

         // World draw.
         mSpriteBatch.Begin(transformMatrix: mCamera.WorldViewMatrix);
         mStageView?.DrawForeground();
         foreach (IVisual visual in mWorldVisuals)
         {
            visual.Draw();
         }
         mSpriteBatch.End();

         // UI draw.
         mSpriteBatch.Begin(transformMatrix: mCamera.InterfaceViewMatrix);
         foreach (IVisual visual in mInterfaceVisuals)
         {
            visual.Draw();
         }
         mConfirmationModalWindow?.Draw();
         mAlertModalWindow?.Draw();

         mSpriteBatch.End();
      }


      #region UI event handlers

      private void OnDisconnect(object aSender, EventArgs aArgs)
      {
         AlertThenExit("The connection to the server was lost without explanation.");
      }

      private void OnLocalPlayerPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
      {
         if (aSender is Player player)
         {
            switch (aArgs.PropertyName)
            {
               case nameof(Player.Items):
                  mItemTray?.Sync(player.Items);
                  break;
               case nameof(Player.SelectedItem):
                  if (player.SelectedItem.HasValue == false)
                  {
                     // If an item was deselected.

                     // Clear any highlights from the item tray.
                     mItemTray?.HighlightIndexes(null);
                  }
                  else if ((player.SelectedItem >= 0) && (player.SelectedItem < 6))
                  {
                     // If an item was selected, plus a bounds check.

                     // Highlight the index(es) of the item that was previously clicked.
                     int[] indexes;

                     if (player.Items[player.SelectedItem.Value].Slots == 1)
                     {
                        // If the item uses just one slot, we'll only highlight one index in the tray.

                        indexes = new int[] { player.SelectedItem.Value };
                     }
                     else
                     {
                        // If the item uses two slots, we'll highlight the selected index and the one
                        // following it.

                        indexes = new int[] { player.SelectedItem.Value, player.SelectedItem.Value + 1 };
                     }

                     mItemTray?.HighlightIndexes(indexes);
                  }
                  break;
               case nameof(Player.ShotID):
                  if (mSelectedShotIndicator != null)
                  {
                     // Determine which button to place the indicator over based on the shot selected.
                     // Note: the indicator and buttons are created at the same time so we can assume
                     // that, if the indicator isn't null, the buttons aren't either.
                     TextButton selectedShotButton = player.ShotID switch
                     {
                        ShotEnum.SHOT_2 => mShot2Button,
                        ShotEnum.SPECIAL_SHOT => mShotSSButton,
                        _ => mShot1Button,
                     };

                     // Place the indicator at the top edge of the button for the selected shot and
                     // match the button's width.
                     mSelectedShotIndicator.X = selectedShotButton.X;
                     mSelectedShotIndicator.Width = selectedShotButton.Width;
                  }
                  break;
            }
         }
      }

      private void OnItemTrayClick(object aSender, ItemTray.TrayData aData)
      {
         // Quick null and list size check for a likely impossible case, but just to be safe.
         if (Context.LocalPlayer?.Items.Length > 0)
         {
            // Create a clone of the player and change its selected item.
            Player clone = new Player(Context.LocalPlayer);

            if (clone.SelectedItem == aData.FirstIndex)
            {
               // If the user clicked on an item that was already selected.

               // Deselect it.
               clone.SelectedItem = null;
            }
            else
            {
               // If the user clicked on an item with the intention of using it during the next shot.

               // Select it by remembering its (first or only) index in the player's Items list.
               clone.SelectedItem = aData.FirstIndex;
            }

            // Send the cloned player with a new item to the server. If the server finds
            // this acceptable, it will respond with the same updated player model.
            mGameClient.Send(new PlayerMessage(clone));
         }
      }

      /// <summary>
      /// Invoked when enter is pressed while the chat input is focused.
      /// In other words, the user finished typing a message and hit enter to send it.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnChatMessageEnter(object aSender, EventArgs aArgs)
      {
         // If the user didn't actually enter a message, don't send anything.
         if (string.IsNullOrEmpty(mChatInput.Value) == true)
         {
            return;
         }

         mGameClient.Send(new ChatMessage(Context.LocalPlayerID, mIsChatTeamPrivate, mChatInput.Value));

         mChatInput.IsFocused = true;
         mChatInput.Value = string.Empty;
      }

      private void OnBackClick(object aSender, EventArgs aArgs)
      {
         // Create a modal window with a confirmation to ask the user if he/she is sure.
         mConfirmationModalWindow = mServiceProvider.GetService<ConfirmationModalWindow>();
         mConfirmationModalWindow.Message = $"Are you sure you want to leave the game?";
         mConfirmationModalWindow.CancelEvent += delegate (object aSender, EventArgs aArgs)
         {
            mConfirmationModalWindow = null;
         };
         mConfirmationModalWindow.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
         {
            LeaveGame();
         };
      }

      private void OnSkipClick(object aSender, EventArgs aArgs)
      {
         // Create a modal window with a confirmation to ask the user if he/she is sure.
         mConfirmationModalWindow = mServiceProvider.GetService<ConfirmationModalWindow>();
         mConfirmationModalWindow.Message = $"Are you sure you want to skip this turn?";
         mConfirmationModalWindow.CancelEvent += delegate (object aSender, EventArgs aArgs)
         {
            mConfirmationModalWindow = null;
         };
         mConfirmationModalWindow.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
         {
            mConfirmationModalWindow = null;
            mGameClient.Send(new Message(Message.Opcode.SKIP));
         };
      }

      private void OnChatPrivacyClick(object aSender, EventArgs aArgs)
      {
         if (mIsChatTeamPrivate == true)
         {
            mChatPrivacyButton.Text = "ALL";
            mIsChatTeamPrivate = false;
         }
         else
         {
            mChatPrivacyButton.Text = "TEAM";
            mIsChatTeamPrivate = true;
         }
      }

      #endregion UI event handlers


      #region World event handlers

      private void OnLandingEvent(object aSender, Landing aLanding)
      {
         mLog.Debug("OnLandingEvent");
      }
      private void OnHaltedEvent(object aSender, Body aBody)
      {
         if ((mIsWalkHeld == true) && (mWorld.Walk(aBody) == true))
         {
            mGameClient.Send(new WalkMessage(Context.LocalPlayerID, aBody.Direction));

            mWorld.Start();
         }
      }

      private void OnDeathEvent(object aSender, Player aBody)
      {
         mLog.Debug("OnDeathEvent");
      }

      private void OnShootingEvent(object aSender, Shooting aDetonation)
      {
         mLog.Debug("OnShootingEvent");
      }

      #endregion World event handlers


      #region Helper methods

      private void LeaveGame()
      {
         mConfirmationModalWindow = null;
         mGameClient.Disconnect();
         bool isSingleplayer = Context.IsSingleplayer;
         Context.Reset();
         if (isSingleplayer == true)
         {
            mWindow.LoadScreen(mServiceProvider?.GetService<MainMenuScreen>());
         }
         else
         {
            mWindow.LoadScreen(mServiceProvider?.GetService<MultiplayerScreen>());
         }
      }

      private void RemovePlayer(Player aPlayer)
      {
         mTurnList.Remove(aPlayer);

         lock (mPlayerViewsMap)
         {
            uint playerID = aPlayer?.ID ?? uint.MaxValue;
            if (mPlayerViewsMap.ContainsKey(playerID) == true)
            {
               mPlayerViewsMap.Remove(playerID);
            }
         }

         // TODO?
      }

      private void DoWalk(Direction aDirection)
      {
         if (Context.LocalPlayer != null)
         {
            if (Context.LocalPlayer.Direction != aDirection)
            {
               Context.LocalPlayer.Direction = aDirection;
               mGameClient.Send(new PlayerMessage(Context.LocalPlayer));

               Task.Run(async () =>
               {
                  // Wait a quarter of a second and see if the player still wants to walk.
                  await Task.Delay(250);

                  // If the player still wants to walk, is facing the same direction that launched
                  // this delayed task, is currently taking his/her turn, and the world says it's
                  // okay to walk (e.g. the player's path isn't obstructed).
                  if ((mIsWalkHeld == true) &&
                      (Context.LocalPlayer.Direction == aDirection) &&
                      (mActivePlayerID == Context.LocalPlayerID) &&
                      (mWorld.Walk(Context.LocalPlayer) == true))
                  {
                     mGameClient.Send(new WalkMessage(Context.LocalPlayerID, aDirection));

                     mWorld.Start();
                  }
               });
            }
            else if ((mActivePlayerID == Context.LocalPlayerID) &&
                     (mWorld.Walk(Context.LocalPlayer) == true))
            {
               // If the player is currently taking his/her turn and the world says it's okay to walk
               // (e.g. the player's path isn't obstructed).

               mGameClient.Send(new WalkMessage(Context.LocalPlayerID, aDirection));

               mWorld.Start();
            }
         }
      }

      private void DoCharge()
      {
         // If it's the local player's turn and the space key is held.
         if ((mActivePlayerID == Context.LocalPlayerID) && (mIsSpaceHeld == true))
         {
            if (Context.LocalPlayer != null)
            {
               Context.LocalPlayer.IsCharging = true;
               mGameClient.Send(new PlayerMessage(Context.LocalPlayer));
            }

            Task.Run(async () =>
            {
               Stopwatch stopwatch = new Stopwatch();
               double percentage = 0.0;
               uint cycles = 0;
               stopwatch.Start();
               while ((mIsSpaceHeld == true) && (cycles < 3))
               {
                  TimeSpan cycleTimespan = stopwatch.Elapsed -
                          TimeSpan.FromMilliseconds(cycles * Constants.ChargePeriodInMilliseconds);
                  percentage = Math.Clamp(cycleTimespan.TotalMilliseconds / Constants.ChargePeriodInMilliseconds,
                          0.0, 1.0);

                  if (percentage == 1.0)
                  {
                     cycles += 1;
                  }

                  if (mFirePowerBar != null)
                  {
                     mFirePowerBar.Percentage = percentage;
                  }

                  await Task.Delay(50);
               }

               // Remember this shot's power (i.e. display a marker at the final percentage) and
               // then reset the percentage to 0%.
               if (mFirePowerBar != null)
               {
                  mFirePowerBar.RememberPower();
                  mFirePowerBar.Percentage = 0.0;
               }

               DoFire(percentage);
            });
         }
      }

      private void DoFire(double aPowerAsPercentage)
      {
         // If it's the local player's turn and the local player model is available.
         if ((mActivePlayerID == Context.LocalPlayerID) && (Context.LocalPlayer != null))
         {
            mGameClient.Send(new FireMessage(Context.LocalPlayerID, aPowerAsPercentage));
         }

         // TODO?
      }

      private void DecrementAim()
      {
         if (Context.LocalPlayer != null)
         {
            double originalAim = Context.LocalPlayer.AimInRadians;
            Context.LocalPlayer.AimInRadians += Math.PI / 64.0;

            if (Context.LocalPlayer.AimInRadians > Context.LocalPlayer.MaxAimInRadians)
            {
               Context.LocalPlayer.AimInRadians = Context.LocalPlayer.MaxAimInRadians;
            }

            if (originalAim != Context.LocalPlayer.AimInRadians)
            {
               mGameClient.Send(new PlayerMessage(Context.LocalPlayer));
            }
         }
      }

      private void IncrementAim()
      {
         if (Context.LocalPlayer != null)
         {
            double originalAim = Context.LocalPlayer.AimInRadians;
            Context.LocalPlayer.AimInRadians -= Math.PI / 64.0;

            if (Context.LocalPlayer.AimInRadians < Context.LocalPlayer.MinAimInRadians)
            {
               Context.LocalPlayer.AimInRadians = Context.LocalPlayer.MinAimInRadians;
            }

            if (originalAim != Context.LocalPlayer.AimInRadians)
            {
               mGameClient.Send(new PlayerMessage(Context.LocalPlayer));
            }
         }
      }

      private void AlertThenExit(string aMessage)
      {
         // If already alerting then exiting.
         if (mIsExiting == true)
         {
            return;
         }

         // Set a flag indicating we're in the process of exiting.
         mIsExiting = true;
         // Display a message to the user, then leave the screen when the message is acknowledged.
         mAlertModalWindow = mServiceProvider.GetService<AlertModalWindow>();
         mAlertModalWindow.Message = aMessage;
         mAlertModalWindow.AcknowledgeEvent += delegate (object aSender, EventArgs aArgs)
         {
            mAlertModalWindow = null;
            LeaveGame();
         };
      }

      #endregion Helper methods


      #region Message handling

      private void OnMessage(object aSender, EventArgs aArgs)
      {
         while ((mGameClient.HasMessage == true) && (mWindow.IsTransitioning == false))
         {
            HandleMessage(mGameClient.Head);
         }
      }

      private void HandleMessage(Message aMessage)
      {
         if (mIsExiting == true)
         {
            return;
         }

         if (aMessage.Op != Message.Opcode.CHAT)
         {
            mLog.Debug($"client {aMessage}");
         }

         switch (aMessage.Op)
         {
            case Message.Opcode.CHAT:
               if (aMessage is ChatMessage chatMessage)
               {
                  Player player = Context.Players.FirstOrDefault(p => p.ID == chatMessage.PlayerID);
                  mChatLog?.Add(player?.Name ?? "????", chatMessage.Message, chatMessage.IsPrivate);
               }
               break;
            case Message.Opcode.EVENTS:
               if (aMessage is EventsMessage eventsMessage)
               {
                  // TODO check against the local events
               }
               break;
            case Message.Opcode.GAME_BEGIN:
               if (aMessage is GameBeginMessage gameBeginMessage)
               {
                  mTurnList.Sync(gameBeginMessage.TurnList);
               }
               break;
            case Message.Opcode.GAME_END:
               if (aMessage is GameEndMessage gameEndMessage)
               {
                  Context.WinningTeam = gameEndMessage.WinningTeam;

                  // TODO game win text of some kind?
               }
               break;
            case Message.Opcode.LIVES:
               if (aMessage is LivesMessage livesMessage)
               {
                  if (mTeamAScoreText != null)
                  {
                     mTeamAScoreText.Value = $"A: {livesMessage.TeamARemainingLives}";
                  }

                  if (mTeamBScoreText != null)
                  {
                     mTeamBScoreText.Value = $"B: {livesMessage.TeamBRemainingLives}";
                  }
               }
               break;
            case Message.Opcode.PLAYER:
               if (aMessage is PlayerMessage playerMessage)
               {
                  Player player = Context.Players.FirstOrDefault(p => p.ID == playerMessage.Player.ID);
                  if (player != null)
                  {
                     uint originalDelay = player.Delay;

                     if (player.Sync(playerMessage.Player) == true)
                     {
                        // If the player's delay changed and triggered a change in the turn list.
                        if ((player.Delay != originalDelay) && (mTurnListView != null))
                        {
                           // The turn list view probably has a different width now.
                           // Adjust the view's X such that it's snapped to the right edge, with padding.
                           mTurnListView.X = mViewport.VirtualWidth - mPadding - mTurnListView.Width;
                        }

                        // Some of the local player's properties are monitored with its PropertyChanged event
                        // and affect UI elements through an event handler (OnLocalPlayerPropertyChanged).
                     }
                  }
               }
               break;
            case Message.Opcode.PLAYERS:
               if (aMessage is PlayersMessage playersMessage)
               {
                  // Get a list of players that are in the current list but not the incoming list.
                  List<Player> departingPlayers = Context.Players.Except(playersMessage.Players,
                                                                         Player.Comparer).ToList();
                  foreach (Player player in departingPlayers)
                  {
                     RemovePlayer(player);
                     Context.Players.Remove(player);
                  }

                  // Sync the players with the updates from the server.
                  foreach (Player updatedPlayer in playersMessage.Players)
                  {
                     Context.Players.FirstOrDefault(p => p.ID == updatedPlayer.ID)?.Sync(updatedPlayer);
                  }
               }
               break;
            case Message.Opcode.SERVER_SHUTDOWN:
               if (aMessage is ServerShutdownMessage serverShutdownMessage)
               {
                  if (string.IsNullOrEmpty(serverShutdownMessage.Reason) == false)
                  {
                     // If a reason for the server's shutdown was provided.

                     // Display a message including the reason before leaving the screen.
                     AlertThenExit($"The server shut down: {serverShutdownMessage.Reason}");
                  }
                  else
                  {
                     // If no reason for the shutdown was provided.

                     // In practice, this will only happen when smoothly exiting a single-player
                     // game and displaying a message to the user could only be confusing.
                     // Display nothing and immediately exit the screen.
                     OnBackClick(this, EventArgs.Empty);
                  }
               }
               break;
            case Message.Opcode.SERVER_STATE:
               if (aMessage is ServerStateMessage serverStateMessage)
               {
                  if (serverStateMessage.State == StateEnum.POST_GAME)
                  {
                     // If the server is announcing its transition to the post-game state.

                     // Follow the server's lead by loading the post-game screen.
                     mWindow.LoadScreen(mServiceProvider?.GetService<PostgameScreen>());
                  }
                  else
                  {
                     // If the server is transitioning to any other state.

                     // It doesn't work like that. The server should only ever transition to the
                     // post-game state from the in-game state. If it's going somewhere else, we'll
                     // consider it modified and likely hostile.
                     AlertThenExit("The server acted irregularly. Disconnecting.");
                  }
               }
               break;
            case Message.Opcode.TURN:
               if (aMessage is TurnMessage turnMessage)
               {
                  Stopwatch stopwatch = new Stopwatch();
                  stopwatch.Start();

                  mTurnCount += 1;
                  // Update the active player ID and (try to) look at that player.
                  mActivePlayerID = turnMessage.ActivePlayerID;
                  if (mPlayerViewsMap.TryGetValue(mActivePlayerID, out PlayerView playerView) == true)
                  {
                     mCamera.LookAt(playerView);
                  }

                  Player player = Context.Players.FirstOrDefault(p => p.ID == mActivePlayerID);
                  if (player != null)
                  {
                     uint turnCountAtLaunch = mTurnCount;
                     Task.Run(async () =>
                     {
                        while ((mTurnCount == turnCountAtLaunch) &&
                               (player.IsCharging == false) &&
                               (player.IsFiring == false) &&
                               (stopwatch.ElapsedMilliseconds < 20000))
                        {
                           await Task.Delay(50);

                           if (mMoveCountdownText != null)
                           {
                              // Format the text such that it counts down from the turn timeout constant
                              // (e.g. 20) and displays in the format 20.0s, 19.5s, 09.5s, 07.0s, etc.
                              mMoveCountdownText.Value = string.Format("{0:00.0}s",
                                 Constants.TurnTimeoutInSeconds - (stopwatch.ElapsedMilliseconds / 1000.0));
                           }
                        }

                        if (mMoveCountdownText != null)
                        {
                           mMoveCountdownText.Value = "00.0s";
                        }
                     });

                     if (mSkipButton != null)
                     {
                        // Enable or disable the skip turn button based on the current turn.
                        // If it's now the local player's turn, it will be enabled.
                        mSkipButton.IsEnabled = mActivePlayerID == Context.LocalPlayerID;
                     }
                  }
               }
               break;
            case Message.Opcode.WIND:
               if (aMessage is WindMessage windMessage)
               {
                  mWorld.Wind = windMessage.Wind;
                  if (mWindVane != null)
                  {
                     mWindVane.Wind = windMessage.Wind;
                  }
               }
               break;
         }
      }

      #endregion Message handling
   }
}
