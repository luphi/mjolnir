﻿using Mjolnir.Common;
using Mjolnir.Common.Messages;
using Mjolnir.Common.Models;
using Mjolnir.Common.Server;
using System.Collections.Generic;
using System.Linq;

namespace Mjolnir.Client.Common
{
   public static class Context
   {
      public static uint LocalPlayerID = uint.MaxValue;

      public static bool IsAdmin = false;

      public static Stage Stage = null;

      public static readonly List<Player> Players = new List<Player>();

      public static ServerSettingsMessage ServerSettings = null;

      public static TeamEnum? WinningTeam = null;

      public static ServerBootstrap ServerBootstrap = null;


      public static Player LocalPlayer
      {
         get
         {
            if (mLocalPlayer == null)
            {
               mLocalPlayer = Players.FirstOrDefault(p => p.ID == LocalPlayerID);
            }

            return mLocalPlayer;
         }
      }
      private static Player mLocalPlayer = null;

      public static bool IsVeteran => LocalPlayer?.IsVeteran == true;

      public static bool IsSingleplayer => ServerBootstrap != null;

      public static void Reset()
      {
         if (IsSingleplayer == true)
         {
            ServerBootstrap.Stop();
         }

         mLocalPlayer = null;
         LocalPlayerID = uint.MaxValue;
         IsAdmin = false;
         Stage = null;
         Players.Clear();
         ServerSettings = null;
         WinningTeam = null;
         ServerBootstrap?.Stop();
         ServerBootstrap = null;
      }
   }
}
