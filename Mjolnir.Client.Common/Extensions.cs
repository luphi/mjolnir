﻿using Mjolnir.Common.Models;
using Microsoft.Xna.Framework;
using System;

namespace Mjolnir.Client.Common
{
   public static class Extensions
   {
      private static readonly Color[] sNameColors =
      {
            Color.LightBlue,
            Color.LightCoral,
            Color.LightCyan,
            Color.LightGoldenrodYellow,
            Color.LightGray,
            Color.LightGreen,
            Color.LightPink,
            Color.LightSalmon,
            Color.LightSeaGreen,
            Color.LightSkyBlue,
            Color.LightSlateGray,
            Color.LightSteelBlue,
            Color.LightYellow
        };

      /// <summary>
      /// Create a Monogame/XNA Color object from an RGB hex string.
      /// This assumes the red-green-blue order and ignores alpha.
      /// </summary>
      /// <param name="aHex">Hex string representing a color.</param>
      /// <returns>A Monogame Color object represented by the hex string with no
      /// transparency, or Color.White if the string could not be parsed.</returns>
      public static Color ToColor(this string aHex)
      {
         try
         {
            // Remove some commonly-prefixed characters/strings from the string.
            // The goal is to have just the numbers whereas, for example, it's common HTML
            // practice to add a # at the start of the string (e.g. #DDEE00).
            string toParse = aHex.Replace("#", string.Empty)
                                 .Replace("0x", string.Empty)
                                 .Replace("0X", string.Empty);

            // If the string is too long (maybe it includes an alpha value), truncate it.
            if (toParse.Length > 6)
            {
               toParse = toParse.Substring(0, 6);
            }

            // If the string is less than six characters, add a "0" for any missing
            // character. For example, 5566 become 556600.
            toParse = toParse.PadRight(6, '0');

            // Parse the three components using base 16.
            return new Color
            {
               R = Convert.ToByte(toParse.Substring(0, 2), 16),
               G = Convert.ToByte(toParse.Substring(2, 2), 16),
               B = Convert.ToByte(toParse.Substring(4, 2), 16),
               A = 255
            };
         }
         catch
         {
            return Color.White;
         }
      }

      /// <summary>
      /// Derive a Color from the string's hash code. This is deterministic meaning
      /// a specific string will always return the same Color.
      /// The possible colors come from a pre-determined list of light Colors.
      /// </summary>
      /// <param name="aString">Any string.</param>
      /// <returns>A color resulting from the string's hash code.</returns>
      public static Color Colorize(this string aString)
      {
         return sNameColors[Math.Abs(aString.GetHashCode()) % sNameColors.Length];
      }

      /// <summary>
      /// Produce a washed out or duller version of the given color.
      /// The alpha value remains the same.
      /// </summary>
      /// <param name="aColor">Any Color object.</param>
      /// <returns>A duller Color.</returns>
      public static Color Dull(this Color aColor)
      {
         return new Color
         {
            R = (byte)(aColor.R / 3),
            G = (byte)(aColor.G / 3),
            B = (byte)(aColor.B / 3),
            A = aColor.A
         };
      }

      /// <summary>
      /// Produce a brightened version of the given color.
      /// The alpha value remains the same.
      /// </summary>
      /// <param name="aColor">Any Color object.</param>
      /// <returns>A brighter Color.</returns>
      public static Color Brighten(this Color aColor)
      {
         byte r = (byte)(aColor.R + ((255 - aColor.R) * 0.33f));
         byte g = (byte)(aColor.G + ((255 - aColor.G) * 0.33f));
         byte b = (byte)(aColor.B + ((255 - aColor.B) * 0.33f));
         return new Color { R = r, G = g, B = b, A = aColor.A };
      }

      public static Vector ToModel(this Vector2 aVector)
      {
         return new Vector(aVector.X, aVector.Y);
      }

      public static Vector2 ToXna(this Vector aVector)
      {
         return new Vector2((float)aVector.X, (float)aVector.Y);
      }
   }
}
