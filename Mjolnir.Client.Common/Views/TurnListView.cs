﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.UI;
using Mjolnir.Common.Collections;
using Mjolnir.Common.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Mjolnir.Client.Common.Views
{
   public class TurnListView : IVisual, IDisposable
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly ConcurrentList<Text> mTexts = new ConcurrentList<Text>();
      private readonly Quad mBackground;
      private uint mLocalPlayerDelay = 0; // The most recent delay for the local player.

      public double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      private double mX = 0.0;

      public double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      private double mY = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mForceWidth = value;
            mForceHeight = null;
            mWidth = value;
            mBackground.Width = value;
            Repopulate(); // Sets mHeight.
         }
      }
      private double mWidth = 0.0;
      private double? mForceWidth = null;

      public double Height
      {
         get => mHeight;
         set
         {
            mForceWidth = null;
            mForceHeight = value;
            mHeight = value;
            mBackground.Height = value;
            Repopulate(); // Sets mWidth;
         }
      }
      private double mHeight = 0.0;
      private double? mForceHeight = null;

      public double AspectRatio => 1.0;

      public uint Padding
      {
         get => mPadding;
         set
         {
            mPadding = value;
            Repopulate();
         }
      }
      private uint mPadding = 5;

      public Rectangle Bounds => new Rectangle((int)X, (int)Y, (int)Width, (int)Height);

      public TurnList Model
      {
         get => mModel;
         set => Load(value);
      }
      private TurnList mModel = null;

      public TurnListView(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mBackground = aServiceProvider.GetService<Quad>();

         mBackground.Color = new Color
         {
            R = 0,
            G = 0,
            B = 0,
            A = 100
         };
         mBackground.X = mX;
         mBackground.Y = mY;
      }

      public void Dispose()
      {
         if (mModel != null)
         {
            mModel.PropertyChanged -= OnPropertyChanged;
            mModel = null;
         }
      }

      public void Draw()
      {
         mBackground.Draw();

         foreach (Text text in mTexts)
         {
            text.Draw();
         }
      }

      public override string ToString()
      {
         return $"{{ {nameof(Model)} = {Model?.ToString() ?? "null"}, {nameof(Bounds)} = {Bounds} }}";
      }

      private void Load(TurnList aTurnList)
      {
         if (aTurnList == null)
         {
            return;
         }

         if (mModel != null)
         {
            mModel.PropertyChanged -= OnPropertyChanged;
         }

         mModel = aTurnList;
         mModel.PropertyChanged += OnPropertyChanged;
         Repopulate();
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         mBackground.X += aX;
         mBackground.Y += aY;

         foreach (Text text in mTexts)
         {
            text.X += aX;
            text.Y += aY;
         }
      }

      /// <summary>
      /// Recreate the view's graphical texts from the current turn list and update width
      /// or height accordingly.
      /// </summary>
      private void Repopulate()
      {
         // If no turn list model is set or no size can be determined.
         if ((mModel == null) || ((mForceWidth == null) && (mForceHeight == null)))
         {
            return;
         }

         // Remove the old texts. New ones will be created (if there is at least one player remaining).
         mTexts.Clear();


         List<Player> queue = mModel.Queue;
         if (queue.Count == 0)
         {
            // Set dimensions using just the padding.
            if (mForceWidth != null)
            {
               mHeight = 2 * mPadding;
            }
            else // if (mForceHeight != null)
            {
               mWidth = 2 * mPadding;
            }

            // Exit here. There's nothing else to do without a turn queue.
            return;
         }

         // For each player in the queue, create a Text displaying his/her name and delay
         // relative to the local player. At the same time, keep track of the widest Text.
         Text widestText = null;
         foreach (Player player in queue)
         {
            Text text = mServiceProvider.GetService<Text>();
            if (player.ID == Context.LocalPlayerID)
            {
               // Display the difference from the most recent delay. In other words, show
               // the player how much delay his/her last turn added.
               uint difference = player.Delay - mLocalPlayerDelay;
               text.Value = $"{player.Name}: {difference}";
               mLocalPlayerDelay = player.Delay;
            }
            else
            {
               // Display the delay value relative to the local player's delay. The value
               // will be prepened with a - or + (e.g. -300 or +400).
               int difference = (int)player.Delay - (int)(Context.LocalPlayer?.Delay ?? 0);
               text.Value = $"{player.Name}: {(difference >= 0 ? $"+{difference}" : difference.ToString())}";
            }

            // If this is the first Text or this one is wider than the widest so far.
            if ((widestText == null) || (text.Width > widestText.Width))
            {
               widestText = text;
            }

            mTexts.Add(text);
         }

         if (mForceWidth != null)
         {
            // If this view's dimensions are being determined by a specific width.

            widestText.Width = mWidth - (2 * mPadding);
            for (int i = 0; i < mTexts.Count; i++)
            {
               Text text = mTexts[i];
               text.X = mX + mPadding;
               text.Y = mY + mPadding + (i * widestText.Height);
               if (ReferenceEquals(text, widestText) == false)
               {
                  text.Height = widestText.Height;
               }
            }
            mHeight = (widestText.Height * mTexts.Count) + (2 * mPadding);
            mBackground.Height = mHeight;
         }
         else // if (mForceHeight != null)
         {
            // If this view's dimensions are being determined by a specific ehgith.

            double textHeight = (mHeight - (2 * mPadding)) / mTexts.Count;
            for (int i = 0; i < mTexts.Count; i++)
            {
               Text text = mTexts[i];
               text.X = mX + mPadding;
               text.Y = mY + mPadding + (i * textHeight);
               text.Height = textHeight;
            }
            mWidth = widestText.Width + (2 * mPadding);
            mBackground.Width = mWidth;
         }
      }

      /// <summary>
      /// Invoked when any property of the turn list model is changed.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
      {
         // Any property changed within the turn list (player removal, player addition, or change
         // to a single player's delay) will result in some aspect of this view changing. Therefore,
         // we'll repopulate the texts in all cases.
         Repopulate();
      }
   }
}
