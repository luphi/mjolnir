﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.UI;
using Mjolnir.Common;
using Mjolnir.Common.Collections;
using Mjolnir.Common.Models;
using NLog;
using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace Mjolnir.Client.Common.Views
{
   public class SatelliteView : IVisual, IDisposable
   {
      private const string THOR_CONTENT = "UI/thor";
      private const string SATELLITE_CONTENT = "UI/satellite";
      private const string BEAM_CONTENT = "UI/beam";

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly MjolnirGame mGame;
      private readonly SpriteBatch mSpriteBatch;
      private readonly ContentManager mContentManager;
      private readonly Text mLevelText;
      private readonly ConcurrentList<BeamData> mActiveBeams = new ConcurrentList<BeamData>();
      private Texture2D mBeamTexture = null;
      private Texture2D mSatelliteTexture = null;
      private float mRotation = 0.0f;

      private uint mDrawCall = 0;

      public double X { get; set; } = 0.0;

      public double Y { get; set; } = 0.0;

      public double Width { get; set; } = 0.0;

      public double Height { get; set; } = 0.0;

      public double AspectRatio => 1.0;

      public Rectangle Bounds => new Rectangle((int)X, (int)Y, (int)Width, (int)Height);

      public Body Model
      {
         get => mModel;
         set => Load(value);
      }
      private Body mModel = null;

      public SatelliteView(IServiceProvider aServiceProvider)
      {
         mGame = aServiceProvider.GetService<MjolnirGame>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mContentManager = aServiceProvider.GetService<ContentManager>();
         mLevelText = aServiceProvider.GetService<Text>();

         mGame.InvokeOnUIThread(() =>
         {
            try
            {
               mBeamTexture = mContentManager.Load<Texture2D>(BEAM_CONTENT);
            }
            catch (Exception e)
            {
               mLog.Warn(e, $"Exception thrown while loading thor beam texture \"{BEAM_CONTENT}\"");
               mBeamTexture = null;
            }
         });
      }

      public void Dispose()
      {
         if (mModel != null)
         {
            mModel.PropertyChanged -= OnPropertyChanged;
            mModel = null;
         }
      }

      public void Draw()
      {
         mDrawCall += 1;

         if ((mModel == null) || (mSatelliteTexture == null))
         {
            return;
         }

         Vector2 position, origin;
         if (mBeamTexture != null)
         {
            foreach (BeamData beam in mActiveBeams)
            {
               double beamWidth = mModel.Radius / 2.0;
               try
               {
                  position = new Vector2((float)(beam.Origin.X - (beamWidth / 2.0)),
                                         (float)beam.Origin.Y);
               }
               catch (Exception e)
               {
                  mLog.Error(e, $"mActiveBeams.Count = {mActiveBeams.Count}, draw call {mDrawCall}");
                  continue;
               }
               origin = new Vector2(mBeamTexture.Width / 2.0f, 0.0f);
               Vector beamVector = beam.Target - beam.Origin;
               Vector2 vectorScale = new Vector2((float)(beamWidth / mBeamTexture.Width),
                                                 (float)(beamVector.Length / mBeamTexture.Height));
               mSpriteBatch.Draw(mBeamTexture,
                                 position + (origin * vectorScale.X),
                                 null,
                                 Color.White * (float)beam.Countdown,
                                 (float)(beamVector.AngleInRadians - Vector.DownInRadians),
                                 origin,
                                 vectorScale,
                                 SpriteEffects.None,
                                 1.0f);
            }
         }

         // Prepare position and origin vectors. The position is the (virtual) screen
         // coordinate corresponding to the top-left corner of surface being drawn here.
         position = new Vector2((float)X, (float)Y);
         // The origin is the relative point around which the sprite will be rotated.
         // Although not documented anywhere, the origin does not use screen coordinates nor
         // normalized device coordinates. It should use the texture's coordinates.
         origin = new Vector2(mSatelliteTexture.Width / 2f, mSatelliteTexture.Height / 2f);
         // Finally, calculate the scale relative to the original bitmap.
         float scalarScale = (float)(Width / mSatelliteTexture.Width);
         mSpriteBatch.Draw(// The bitmap source from which we'll draw a small portion.
                           mSatelliteTexture,
                           // The position to draw at.
                           position + (origin * scalarScale),
                           // Region in the bitmap to be drawn. (null draws the full texture.)
                           null,
                           // Color mask.
                           Color.White,
                           // Clockwise rotation, in radians, to be applied.
                           mRotation,
                           // The point, relative to the drawn position, around which rotation happens.
                           origin,
                           // Scale factor. Determines the drawn dimensions.
                           scalarScale,
                           // Sprite effects (none, horizontal flip, or vertical flip).
                           SpriteEffects.None,
                           // Layer depth.
                           1.0f);

         if (mModel is Thor)
         {
            mLevelText.Draw();
         }
      }

      public void Fire(Vector aTarget)
      {
         if ((aTarget != null) && (mModel != null))
         {
            // From the target, calculate the pitch of the satellite.
            // The pitch is simply the opposite (negation) of the vector pointing to the target.
            Vector pitchVector = (aTarget - mModel.Center) * -1.0;
            double pitch = pitchVector.AngleInRadians;

            // Calculate the clockwise rotation, in radians, to be applied. The satellite's image
            // depicts it as pointing down meaning that its default pitch is "up."
            mRotation = (float)(pitch - Vector.UpInRadians);

            LaunchBeamTask(new BeamData(mModel.Center, aTarget));
         }
      }

      public void Fire(double aDirection)
      {
         if (mModel != null)
         {
            // From the firing direction, calculate the pitch of the satellite.
            // The pitch is simply the opposite direction of the firing so offset by 180 degrees (pi radians).
            double pitch = aDirection - Math.PI;
            // Keep the value in the [0.0, 2*pi) range. Probably not necessary, but it's easy.
            if (pitch < 0.0)
            {
               pitch += 2.0 * Math.PI;
            }

            // Calculate the clockwise rotation, in radians, to be applied. The satellite's image
            // depicts it as pointing down meaning that its default pitch is "up."
            mRotation = (float)(pitch - Vector.UpInRadians);

            LaunchBeamTask(new BeamData(mModel.Center,
                                        mModel.Center + (Vector.FromRadians(aDirection) * 5000.0)));
            // The length, 5000, is arbitrary. It's just made really long so it appears inifinite.
         }
      }

      private void Load(Body aSatellite)
      {
         // If another model was previously loaded, unhook the property changed event.
         if (mModel != null)
         {
            mModel.PropertyChanged -= OnPropertyChanged;
         }

         mModel = aSatellite;

         // Set the level text first so it has a set height. Resize() will use its height.
         // (Only Thors have a level so this will be an empty string for more generic satellite.)
         mLevelText.Value = (mModel as Thor)?.Level.ToString() ?? string.Empty;
         Resize();
         Reposition();

         if (mModel != null)
         {
            mModel.PropertyChanged += OnPropertyChanged;

            string content = mModel is Thor ? THOR_CONTENT : SATELLITE_CONTENT;
            mGame.InvokeOnUIThread(() =>
            {
               try
               {
                  mSatelliteTexture = mContentManager.Load<Texture2D>(content);
               }
               catch (Exception e)
               {
                  mLog.Warn(e, $"Exception thrown while loading satellite/Thor texture \"{content}\"");
                  mSatelliteTexture = null;
               }
            });
         }
         else
         {
            mSatelliteTexture = null;
         }
      }

      private void Reposition()
      {
         X = mModel?.X ?? 0.0;
         Y = mModel?.Y ?? 0.0;

         mLevelText.X = X + (Width / 2.0) - (mLevelText.Width / 2.0);
         mLevelText.Y = Y + (Height / 2.0) - (mLevelText.Height / 2.0);
      }

      private void Resize()
      {
         Width = 2.0 * (mModel?.Radius ?? 0.0);
         Height = 2.0 * (mModel?.Radius ?? 0.0);

         mLevelText.Width = Width / 3.0; // The Width setter also sets height while keeping the aspect ratio.
      }

      private void OnPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
      {
         //if (!(mModel is Thor))
         //    mLog.Debug($"SatelliteView OnPropertyChanged() -> {aArgs.PropertyName}");
         switch (aArgs.PropertyName)
         {
            case nameof(Body.X):
            case nameof(Body.Y):
               Reposition();
               break;
            case nameof(Body.Radius):
               Resize();
               break;
         }

         if (aSender is Thor thor)
         {
            switch (aArgs.PropertyName)
            {
               case nameof(Thor.Level):
                  mLevelText.Value = thor.Level.ToString();
                  break;
            }
         }
      }

      private void LaunchBeamTask(BeamData aData)
      {
         if (aData == null)
         {
            return;
         }

         int delay = 50;
         double dt = delay / 1000.0;

         mActiveBeams.Add(aData);

         Task.Run(async () =>
         {
            while (aData.Countdown > 0.0)
            {
               await Task.Delay(delay);
               aData.Alpha -= (byte)(dt * 255.0);
               aData.Countdown -= dt;
            }

            mActiveBeams.Remove(aData);
         });
      }

      private class BeamData
      {
         public Vector Origin;
         public Vector Target;
         public double Countdown = Constants.ThorCooldownDurationInMilliseconds / 1000.0;
         public byte Alpha = 255;

         public BeamData(Vector aOrigin, Vector aTarget)
         {
            Origin = aOrigin;
            Target = aTarget;
         }
      }
   }
}
