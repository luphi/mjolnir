﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.UI;
using Mjolnir.Common;
using Mjolnir.Common.Collections;
using Mjolnir.Common.Models;
using NLog;
using System;
using System.ComponentModel;

namespace Mjolnir.Client.Common.Views
{
   public class ProjectileView : IVisual, IDisposable
   {
      private enum Behavior
      {
         NONE,
         // Use the X coordinate as input to sine in order to calculate a rotation.
         X_AS_ROTATION,
         // The projectile is rotated to match the direction of the velocity vector. In other
         // words, the projectile will fly like an arrow.
         FOLLOW_VELOCITY
      }

      /// <summary>
      /// The width and/or height, in pixels, of a single frame in a projectile spritesheet.
      /// </summary>
      private const int FRAME_DIAMETER_IN_PIXELS = 64;

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly MjolnirGame mGame;
      private readonly ContentManager mContentManager;
      private readonly ContentWrapper mContentWrapper;
      private readonly SpriteBatch mSpriteBatch;
      private readonly ConcurrentList<Body> mBodies = new ConcurrentList<Body>();
      private Texture2D mTexture = null;
      private Image mCollisionCircle = null;
      private Quad mPositionQuad = null;
      private Behavior mBehavior = Behavior.NONE;
      private Body mFocus = null;

      public double X { get; set; } = 0.0;

      public double Y { get; set; } = 0.0;

      public double Width { get; set; } = 0.0;

      public double Height { get; set; } = 0.0;

      public double AspectRatio => 1.0;

      public Rectangle Bounds => new Rectangle((int)X, (int)Y, (int)Width, (int)Height);

      public Projectile Model
      {
         get => mModel;
         set => LoadProjectile(value);
      }
      private Projectile mModel = null;

      public double Rotation { get; set; } = 0.0;

      public bool IsDebugEnabled { get; set; } = false;

      public ProjectileView(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mGame = aServiceProvider.GetService<MjolnirGame>();
         mContentManager = aServiceProvider.GetService<ContentManager>();
         mContentWrapper = aServiceProvider.GetService<ContentWrapper>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();

         mGame.InvokeOnUIThread(() =>
         {
            try
            {
               mCollisionCircle = mServiceProvider.GetService<Image>();
               mCollisionCircle.Content = "UI/circle"; // Calls on ContentManager's Load().
               mPositionQuad = mServiceProvider.GetService<Quad>();
               mPositionQuad.Color = Color.Aqua; // Can be done off the UI thread, but can't be null.
            }
            catch (Exception e)
            {
               mLog.Error(e, $"Exception thrown while loading debug graphics for a {nameof(ProjectileView)}");
               mCollisionCircle = null;
               mPositionQuad = null;
            }
         });
      }

      public void Dispose()
      {
         if (mModel != null)
         {
            mModel.PropertyChanged -= OnFocusPropertyChanged;
            mModel = null;
         }
      }

      public void Draw()
      {
         // If the model is unloaded, texture is unloaded, the projectile has not yet launched, or the
         // projectile is done (i.e. launched and detonated).
         if ((mModel == null) || (mTexture == null) && (mModel.IsLaunched == false) || (mModel.IsDone == true))
         {
            return;
         }

         // Prepare an origin for all bodies. The origin is the relative point around which the
         // sprite will be rotated. Although not documented anywhere, the origin does not use screen
         // coordinates nor normalized device coordinates. It should use the texture's coordinates.
         Vector2 origin = new Vector2(FRAME_DIAMETER_IN_PIXELS / 2.0f, FRAME_DIAMETER_IN_PIXELS / 2.0f);

         // Calculate a scale relative to the original bitmap for all bodies.
         float scale = (float)(Width / FRAME_DIAMETER_IN_PIXELS);

         foreach (Body body in mBodies)
         {
            // Prepare position for just this body. The position is the (virtual) screen
            // coordinate corresponding to the top-left corner of surface being drawn here.
            Vector2 position = new Vector2((float)body.X, (float)body.Y);
            mSpriteBatch.Draw(
                // The bitmap source from which we'll draw a small portion.
                mTexture,
                // The position to draw at.
                position + (origin * scale),
                // Region in the bitmap to be drawn. (null draws the full texture.)
                null,
                // Color mask.
                Color.White,
                // Rotation, in radians, to be applied.
                (float)Rotation,
                // The point, relative to the drawn position, around which rotation happens.
                origin,
                // Scale factor. Determines the drawn dimensions.
                scale,
                // Sprite effects (none, horizontal flip, or vertical flip).
                SpriteEffects.None,
                // Layer depth.
                1.0f);

            // If the debug graphics, which show the collision circle, (X, Y) position of the
            // projectile/body, and ?, are enabled and loaded.
            if ((IsDebugEnabled == true) && (mCollisionCircle != null) && (mPositionQuad != null))
            {
               // Move the debug graphics to the appropriate positions for this body.
               mCollisionCircle.X = body.X;
               mCollisionCircle.Y = body.Y;
               mPositionQuad.X = X - (mPositionQuad.Width / 2.0);
               mPositionQuad.Y = Y - (mPositionQuad.Height / 2.0);

               mCollisionCircle.Draw();
               mPositionQuad.Draw();
            }
         }
      }

      public override string ToString()
      {
         return $"{{ {nameof(Model)} = {Model?.ToString() ?? "null"}, {nameof(Bounds)} = {Bounds}, " +
                $"{nameof(Rotation)} = {Rotation}, {nameof(IsDebugEnabled)} = {IsDebugEnabled} }}";
      }

      private void LoadProjectile(Projectile aProjectile)
      {
         // If another model was previously loaded, unhook the property changed event.
         if (mModel != null)
         {
            mModel.PropertyChanged -= OnProjectilePropertyChanged;
         }

         mModel = aProjectile;

         mBodies.Clear(); // Must be cleared for both following cases.

         // If the model is effectively being unloaded.
         if (aProjectile == null)
         {
            // Reset all model-derived variables to their default values.
            mTexture = null;
            LoadFocus(null);
         }
         else
         {
            switch ((mModel.Player.Shot.MobileID, mModel.Player.Shot.ShotID))
            {
               case (MobileEnum.AKUDA, ShotEnum.SHOT_1):
               case (MobileEnum.LARVA, ShotEnum.SHOT_1):
                  mBehavior = Behavior.X_AS_ROTATION;
                  break;
               case (MobileEnum.AKUDA, ShotEnum.SHOT_2):
               case (MobileEnum.AKUDA, ShotEnum.SPECIAL_SHOT):
                  mBehavior = Behavior.FOLLOW_VELOCITY;
                  break;
               default:
                  mBehavior = Behavior.NONE;
                  break;
            }

            LoadFocus(aProjectile.Focus);

            mBodies.AddRange(mModel.CorporealBodies);

            mModel.PropertyChanged += OnProjectilePropertyChanged;

            mGame.InvokeOnUIThread(() =>
            {
               try
               {
                  mTexture = mContentManager.Load<Texture2D>(mContentWrapper.ProjectileTexturePath(aProjectile));
               }
               catch (Exception e)
               {
                  mLog.Error(e, $"Exception thrown while loading texture(s) of projectile of " +
                                $"{nameof(Shot)} {aProjectile.Player.Shot}");
                  mTexture = null;
               }

               Resize();
            });
         }
      }

      private void LoadFocus(Body aFocus)
      {
         if (mFocus != null)
         {
            mFocus.PropertyChanged -= OnFocusPropertyChanged;
         }

         mFocus = aFocus;

         if (aFocus == null)
         {
            // If the focus is effectively being unloaded.

            // Note: the IVisual properties will remain unchanged in case the camera is
            // locked on this view. That way, it won't jump to (0, 0) seemingly at random.
         }
         else
         {
            // If a new focus is being loaded.

            Reposition();
            Resize();

            mFocus.PropertyChanged += OnFocusPropertyChanged;
         }
      }

      private void Reposition()
      {
         if (mFocus == null)
         {
            return;
         }

         // Have this view match the focus body so that the camera can follow it, when appropriate.
         X = mFocus.X;
         Y = mFocus.Y;
      }

      private void Resize()
      {
         if (mFocus == null)
         {
            return;
         }

         Width = 2.0 * mFocus.Radius;
         Height = 2.0 * mFocus.Radius;
         Rotation = X / (2.0 * Math.PI * mFocus.Radius); // TODO not for every projectile

         if ((mCollisionCircle != null) && (mPositionQuad != null))
         {
            mCollisionCircle.Width = Width;
            mCollisionCircle.Height = Width;
            mPositionQuad.Width = Width / 5.0;
            mPositionQuad.Height = Width / 5.0;
         }
      }

      private void OnProjectilePropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
      {
         switch (aArgs.PropertyName)
         {
            case nameof(Projectile.CorporealBodies):
               if (mModel != null)
               {
                  mBodies.Clear();
                  mBodies.AddRange(mModel.CorporealBodies);
               }
               break;
            case nameof(Projectile.Focus):
               LoadFocus(mModel?.Focus);
               break;
         }
      }

      private void OnFocusPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
      {
         switch (aArgs.PropertyName)
         {
            case nameof(Body.X):
            case nameof(Body.Y):
               Reposition();

               if (mBehavior == Behavior.X_AS_ROTATION)
               {
                  // Apply a rotation in direct correlation to the projectile's circumference.
                  // This will spin the projectile equal to distance it would have rolled
                  // along the ground, although this also applies in the air too.
                  Rotation = X / (2.0 * Math.PI * mFocus.Radius);
               }
               break;
            case nameof(Body.Velocity):
               if (mBehavior == Behavior.FOLLOW_VELOCITY)
               {

               }
               break;
         }
      }
   }
}
