﻿using Mjolnir.Client.Common.Graphics;
using Mjolnir.Common.Disk;
using Mjolnir.Common.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using NLog;
using System;
using System.ComponentModel;

namespace Mjolnir.Client.Common.Views
{
   /// <summary>
   /// A graphical representation of a stage (AKA the terrain and bounds on which the game takes place).
   /// 
   /// Although this class is derived from the Stage model, it's more like a wrapper than a child.
   /// Base Stages should be passed to this class through its Load() method, after which this class
   /// will act as a view for that specific stage.
   /// </summary>
   public class StageView : IVisual, IDisposable
   {
      /// <summary>
      /// Thickness, in pixels, of the solely visual border drawn around holes dug in the terrain.
      /// </summary>
      //private const int BORDER_THICKNESS = 2;

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly MjolnirGame mGame;
      private readonly GraphicsDevice mGraphicsDevice;
      private readonly ContentManager mContentManager;
      private readonly IContentWrapper mContentWrapper;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly SpriteBatch mSpriteBatch;
      private Texture2D mBackgroundTexture = null;
      private Texture2D mForegroundTexture = null;
      private Rectangle mBackgroundBounds = Rectangle.Empty;
      private Rectangle mForegroundBounds = Rectangle.Empty;

      public Stage Model
      {
         get => mModel;
         set => Load(value);
      }
      private Stage mModel = null;

      public double X
      {
         get => 0.0;
         set { }
      }

      public double Y
      {
         get => 0.0;
         set { }
      }

      public double Width
      {
         get => mViewport.VirtualWidth;
         set { }
      }

      public double Height
      {
         get => mViewport.VirtualHeight;
         set { }
      }

      public double AspectRatio
      {
         get
         {
            if (mModel != null)
            {
               return (float)mModel.Width / mModel.Height;
            }
            else if (Height != 0.0)
            {
               return Width / Height;
            }

            return 1.0;
         }
      }

      public Rectangle Bounds => ForegroundBounds;

      public Rectangle BackgroundBounds => mBackgroundBounds;

      public Rectangle ForegroundBounds => mForegroundBounds;

      public Rectangle CameraBounds { get; private set; } = Rectangle.Empty;

      public StageView(IServiceProvider aServiceProvider)
      {
         mGame = aServiceProvider.GetService<MjolnirGame>();
         mGraphicsDevice = aServiceProvider.GetService<GraphicsDevice>();
         mContentManager = aServiceProvider.GetService<ContentManager>();
         mContentWrapper = aServiceProvider.GetService<IContentWrapper>();
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
      }

      public void Dispose()
      {
         if (mModel != null)
         {
            mModel.PropertyChanged -= OnPropertyChanged;
            mModel = null;
         }
      }

      public void Draw()
      {
         DrawForeground();
      }

      public void DrawBackground()
      {
         if (mBackgroundTexture != null)
         {
            mSpriteBatch.Draw(mBackgroundTexture, BackgroundBounds, Color.White);
         }
      }

      public void DrawForeground()
      {
         if (mForegroundTexture != null)
         {
            mSpriteBatch.Draw(mForegroundTexture, ForegroundBounds, Color.White);
         }
      }

      public override string ToString()
      {
         return $"{{ {nameof(Model)} = {Model?.ToString() ?? "null"}, " +
                $"{nameof(X)} = {X}, {nameof(Y)} = {Y}, {nameof(Width)} = {Width}, " +
                $"{nameof(Height)} = {Height} }}";
      }

      private void Load(Stage aStage)
      {
         // If another model was previously loaded, unhook the property changed event.
         if (mModel != null)
         {
            mModel.PropertyChanged -= OnPropertyChanged;
         }

         mModel = aStage;

         if (aStage == null)
         {
            // If the model is effectively being unloaded.

            // Reset all model-derived members to their default values.
            Unload();
         }
         else
         {
            mModel.PropertyChanged += OnPropertyChanged;

            if (mModel.IsLoaded == false)
            {
               Unload();
               return;
            }

            mGame.InvokeOnUIThread(() =>
            {
               try
               {
                  LoadForegroundAndApplyMask();
                  mForegroundTexture = CloneTexture(mContentManager.Load<Texture2D>(
                          mContentWrapper.StageForegroundPath(aStage)));
                  mBackgroundTexture = mContentManager.Load<Texture2D>(mContentWrapper.StageBackgroundPath(aStage));
                  // The stage model is position at (0, 0) for convenience so the foreground, the portion
                  // of the stage applicable to gameplay, is also position at (0, 0).
                  mForegroundBounds = new Rectangle(0, 0, mForegroundTexture.Width, mForegroundTexture.Height);

                  // Scale the background such that it's right edge lines up with the right edge of the
                  // foreground, factoring in the parallax factor (a ratio).
                  // Given:
                  // parallaxRatio = (background.width - viewport.width) / (foreground.width - viewport.width)
                  // Solving for background.width:
                  // background.width = ((foreground.width - viewport.width) * parallaxRatio) + viewport.width
                  mBackgroundBounds.Width = (int)(((mForegroundBounds.Width - mViewport.VirtualWidth) *
                                            Camera.ParallaxRatio) + mViewport.VirtualWidth);
                  // Maintain the aspect ratio of the background.
                  mBackgroundBounds.Height = (int)(mBackgroundBounds.Width * mBackgroundTexture.Height /
                                             (float)mBackgroundTexture.Width);
                  // Vertically align the background to the bottom edge of the foreground.
                  int parallaxedForegroundHeight = (int)(((mForegroundBounds.Height - mViewport.VirtualHeight) *
                                                   Camera.ParallaxRatio) + mViewport.VirtualHeight);
                  mBackgroundBounds.Y = mForegroundBounds.Y + parallaxedForegroundHeight - mBackgroundBounds.Height;
                  mBackgroundBounds.X = 0; // Should be zero already but just in case.

                  // While we're at it, calculate the effective background height factoring in the
                  // parallax effect and apply it, and the width, to the CameraBounds.
                  int backgroundHeight = (int)(((mBackgroundBounds.Height - mViewport.VirtualHeight) /
                                         Camera.ParallaxRatio) + mViewport.VirtualHeight);

                  CameraBounds = new Rectangle(0,
                                               mForegroundBounds.Y + mForegroundBounds.Height - backgroundHeight,
                                               mForegroundBounds.Width,
                                               backgroundHeight);
               }
               catch (Exception e)
               {
                  mLog.Error(e, $"Exception thrown while loading background and foreground images " +
                                $"of stage {aStage.Name}");
                  Unload();
               }
            });
         }
      }

      private void Unload()
      {
         mBackgroundTexture = null;
         mForegroundTexture = null;
         mBackgroundBounds = Rectangle.Empty;
         mForegroundBounds = Rectangle.Empty;
         CameraBounds = Rectangle.Empty;
      }

      private Texture2D CloneTexture(Texture2D aTexture)
      {
         if (aTexture == null)
         {
            return null;
         }

         Texture2D clone = new Texture2D(mGraphicsDevice, aTexture.Width, aTexture.Height);
         Color[] data = new Color[aTexture.Width * aTexture.Height];
         aTexture.GetData(data);
         clone.SetData(data);

         return clone;
      }

      private void LoadForegroundAndApplyMask()
      {
         if (mModel == null)
         {
            return;
         }

         try
         {
            // MonoGame will maintain its own cache of the texture. Meanwhile, this view
            // will clone that master version so that stage destruction (digging) can be
            // implemented without losing the original data.
            mForegroundTexture = CloneTexture(mContentManager.Load<Texture2D>(
                mContentWrapper.StageForegroundPath(mModel)));
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while reloading the foreground image of stage {Model.Name}");
            mForegroundTexture = null;
            return;
         }

         // Get the pixel data from the (foreground) texture in preparation for modiying it.
         Color[] data = new Color[mForegroundTexture.Width * mForegroundTexture.Height];
         mForegroundTexture.GetData(data);

         for (int x = mModel.X; x < mModel.Width; x++)
         {
            for (int y = mModel.Y; y < mModel.Height; y++)
            {
               // If the mask within the model indicates this pixel is empty space.
               if (mModel.IsSolid(x, y) == false)
               {
                  // Clear this pixel by simply assigning its color to transparent.
                  data[(y * mForegroundTexture.Width) + x] = Color.Transparent;
               }
            }
         }

         // Update the foreground texture with some pixels now missing.
         mForegroundTexture.SetData(data);
      }

      private void OnPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
      {
         switch (aArgs.PropertyName)
         {
            case nameof(Stage.Mask):
               mGame.InvokeOnUIThread(() =>
               {
                  // TODO comment
                  LoadForegroundAndApplyMask();
               });
               break;
         }
      }
   }
}
