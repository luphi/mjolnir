﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.UI;
using Mjolnir.Common;
using Mjolnir.Common.Collections;
using Mjolnir.Common.Models;
using NLog;
using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace Mjolnir.Client.Common.Views
{
   public class PlayerView : IVisual, IDisposable
   {
      private const int COMPASS_DIAMETER = 96;

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly MjolnirGame mGame;
      private readonly ContentManager mContentManager;
      private readonly ContentWrapper mContentWrapper;
      private readonly SpriteBatch mSpriteBatch;
      private readonly ProgressBar mShieldHPBar;
      private readonly ProgressBar mHPBar;
      private readonly Text mName;
      private readonly ConcurrentList<Text> mDamageTexts = new ConcurrentList<Text>();
      private readonly double mTextHeight;
      private Texture2D mMobileTexture = null;
      private Texture2D mCompassTexture = null;
      private Texture2D mCompassArrowTexture = null;
      private Image mCollisionCircle;
      private Quad mPositionQuad;
      private Quad mFootingQuad;
      private Rectangle mCompassRect = Rectangle.Empty;
      private Vector mTrajectoryVetcor = Vector.Zero;
      private int mPreviousShieldHP = 0;
      private int mPreviousHP = 0;
      private bool mHasShield = false;

      public double X { get; set; } = 0.0;

      public double Y { get; set; } = 0.0;

      public double Width { get; set; } = 0.0;

      public double Height { get; set; } = 0.0;

      public double AspectRatio => 1.0;

      public Rectangle Bounds => new Rectangle((int)X, (int)Y, (int)Width, (int)Height);

      public Player Model
      {
         get => mModel;
         set => Load(value);
      }
      private Player mModel = null;

      public SatelliteView SatelliteView { get; }

      public bool IsDebugEnabled { get; set; } = false;

      public PlayerView(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mGame = aServiceProvider.GetService<MjolnirGame>();
         mContentManager = aServiceProvider.GetService<ContentManager>();
         mContentWrapper = aServiceProvider.GetService<ContentWrapper>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mShieldHPBar = aServiceProvider.GetService<ProgressBar>();
         mHPBar = aServiceProvider.GetService<ProgressBar>();
         mName = aServiceProvider.GetService<Text>();
         mTextHeight = aServiceProvider.GetService<ResolutionIndependentViewport>().VirtualHeight / 20.0;
         SatelliteView = aServiceProvider.GetService<SatelliteView>();

         mShieldHPBar.BarColor = Color.Blue;
         mHPBar.BarColor = Color.Green;

         mGame.InvokeOnUIThread(() =>
         {
            try
            {
               mCompassArrowTexture = mContentManager.Load<Texture2D>("UI/compass_arrow");
            }
            catch (Exception e)
            {
               mLog.Error(e, $"Exception thrown while loading texture for the compass arrow");
               mCompassArrowTexture = null;
            }

            try
            {
               mCollisionCircle = aServiceProvider.GetService<Image>();
               mCollisionCircle.Content = "UI/circle"; // Calls on ContentManager's Load().
               mPositionQuad = aServiceProvider.GetService<Quad>();
               mPositionQuad.Color = Color.Aqua; // Can be done off the UI thread, but can't be null.
               mFootingQuad = aServiceProvider.GetService<Quad>();
               mFootingQuad.Color = Color.Chartreuse;
            }
            catch (Exception e)
            {
               mLog.Error(e, $"Exception thrown while loading debug graphics for a {nameof(PlayerView)}");
               mCollisionCircle = null;
               mPositionQuad = null;
               mFootingQuad = null;
            }
         });
      }

      public void Dispose()
      {
         if (mModel != null)
         {
            mModel.PropertyChanged -= OnPropertyChanged;
            mModel = null;
         }
      }

      public void Draw()
      {
         if (mModel == null)
         {
            return;
         }

         // Draw the fire angle compass behind the player.
         if (mCompassTexture != null)
         {
            Vector2 position = new Vector2(mCompassRect.X, mCompassRect.Y);
            Vector2 origin = new Vector2(mCompassTexture.Width / 2f, mCompassTexture.Height / 2f);
            bool isPlayerFacedAway = (mModel?.Direction ?? Direction.LEFT) == Direction.RIGHT;
            float scale = (float)mCompassRect.Width / mCompassTexture.Width;
            mSpriteBatch.Draw(mCompassTexture,
                              position + (origin * scale),
                              null,
                              Color.White,
                              (float)(mModel.Pitch - Vector.UpInRadians),
                              origin,
                              scale,
                              isPlayerFacedAway ? SpriteEffects.FlipHorizontally : SpriteEffects.None,
                              1.0f);
         }

         if (mHasShield == true)
         {
            mShieldHPBar.Draw();
         }

         mHPBar.Draw();
         mName.Draw();

         if (mMobileTexture != null)
         {
            // Prepare position and origin vectors. The position is the (virtual) screen
            // coordinate corresponding to the top-left corner of surface being drawn here.
            Vector2 position = new Vector2((float)X, (float)Y);
            // The origin is the relative point around which the sprite will be rotated.
            // Although not documented anywhere, the origin does not use screen coordinates nor
            // normalized device coordinates. It should use the texture's coordinates.
            Vector2 origin = new Vector2(mMobileTexture.Width / 2f, mMobileTexture.Height / 2f);
            // Determine if the texture should be horizontally mirrored (facing right) or
            // not (facing left).
            bool isPlayerFacedAway = mModel.Direction == Direction.RIGHT;
            // A couple of mobiles face away from the direction they shoot. Determine if this
            // player's mobile is either one and apply another possible horizontal mirroring.
            bool isMobileFacedAway = (mModel.MobileID == MobileEnum.AKUDA) || (mModel.MobileID == MobileEnum.ARACH);
            // Finally, calculate the scale relative to the original bitmap.
            float scale = (float)(Width / mMobileTexture.Width);
            mSpriteBatch.Draw(
                // The bitmap source from which we'll draw a small portion.
                mMobileTexture,
                // The position to draw at.
                position + (origin * scale),
                // Region in the bitmap to be drawn. (null draws the full texture.)
                null,
                // Color mask.
                Color.White,
                // Clockwise rotation, in radians, to be applied.
                (float)(mModel.Pitch - Vector.UpInRadians),
                // The point, relative to the drawn position, around which rotation happens.
                origin,
                // Scale factor. Determines the drawn dimensions.
                scale,
                // Sprite effects (none, horizontal flip, or vertical flip).
                isPlayerFacedAway ^ isMobileFacedAway ? SpriteEffects.FlipHorizontally : SpriteEffects.None,
                // Layer depth.
                1.0f);
         }

         foreach (Text text in mDamageTexts)
         {
            text.Draw();
         }

         if (mCompassArrowTexture != null)
         {
            Vector2 position = new Vector2(mCompassRect.X, mCompassRect.Y);
            Vector2 origin = new Vector2(mCompassArrowTexture.Width / 2f, mCompassArrowTexture.Height / 2f);
            float scale = (float)mCompassRect.Width / mCompassArrowTexture.Width;
            mSpriteBatch.Draw(mCompassArrowTexture,
                              position + (origin * scale),
                              null,
                              Color.White,
                              // The texture depicts an arrow pointing directly right from the image's center,
                              // hence using "right" as the reference angle.
                              (float)(mTrajectoryVetcor.AngleInRadians - Vector.RightInRadians),
                              origin,
                              scale,
                              SpriteEffects.None,
                              1.0f);
         }

         if (mModel.Mobile?.HasSatellite == true)
         {
            SatelliteView.Draw();
         }

         // If the debug graphics, which show the collision circle, (X, Y) position of the player/body,
         // and (X, Y) position of the player's/body's footing, are enabled.
         if (IsDebugEnabled == true)
         {
            mCollisionCircle.Draw();
            mPositionQuad.Draw();
            mFootingQuad.Draw();
         }
      }

      public override string ToString()
      {
         return $"{{ {nameof(Model)} = {Model?.ToString() ?? "null"}, {nameof(Bounds)} = {Bounds}, " +
                $"{nameof(IsDebugEnabled)} = {IsDebugEnabled} }}";
      }

      private void Load(Player aPlayer)
      {
         // If another model was previously loaded, unhook the property changed event.
         if (mModel != null)
         {
            mModel.PropertyChanged -= OnPropertyChanged;
         }

         mModel = aPlayer;

         mDamageTexts.Clear();

         // If the model is effectively being unloaded.
         if (aPlayer == null)
         {
            // Reset all model-derived members to their default values.
            mMobileTexture = null;
            mCompassTexture = null;
            mCompassRect = Rectangle.Empty;
            mName.Value = string.Empty;
            SatelliteView.Model = null;
         }
         else
         {
            mName.Value = aPlayer.Name;
            mGame.InvokeOnUIThread(() =>
            {
               LoadMobile();
               Resize();
               Reposition();
            });

            mModel.PropertyChanged += OnPropertyChanged;
         }
      }

      private void LoadMobile()
      {
         Mobile mobile = mModel?.Mobile;
         if (mobile == null)
         {
            return;
         }

         mHasShield = mModel.HasShield;
         if (mHasShield == true)
         {
            mPreviousShieldHP = mModel.ShieldHP;
            mShieldHPBar.Percentage = (double)mModel.ShieldHP / mobile.ShieldHP;
         }

         mPreviousHP = mModel.HP;
         mHPBar.Percentage = (double)mModel.HP / mobile.HP;

         SatelliteView.Model = mobile.HasSatellite ? mModel.Satellite : null;

         try
         {
            mMobileTexture = mContentManager.Load<Texture2D>(mContentWrapper.MobileTexturePath(mModel.Mobile));
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while loading mobile texture for mobile {mobile?.Name}");
            mMobileTexture = null;
         }

         try
         {
            mCompassTexture = mContentManager.Load<Texture2D>(mContentWrapper.CompassTexturePath(mobile));
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while loading compass texture for mobile {mobile?.Name}");
            mCompassTexture = null;
         }
      }

      private void Reposition()
      {
         if (mModel == null)
         {
            return;
         }

         // Use the model's getter to remember the trajectory vector. With this, we don't need to recalculate
         // the vector on each draw.
         mTrajectoryVetcor = mModel.Trajectory;

         // Match the model's X and Y positions. Both the model's and these graphical coordinates
         // use the top-left corner of the quad, surface, etc. as the origin.
         X = mModel.X;
         Y = mModel.Y;

         // Move the debug graphics to their new positions, enabled or not.
         mCollisionCircle.X = X;
         mCollisionCircle.Y = Y;
         mPositionQuad.X = X - (mPositionQuad.Width / 2.0);
         mPositionQuad.Y = Y - (mPositionQuad.Height / 2.0);
         Vector footing = mModel.Footing;
         mFootingQuad.X = footing.X - (mFootingQuad.Width / 2.0);
         mFootingQuad.Y = footing.Y - (mFootingQuad.Height / 2.0);

         // Step 1: create a vector pointing up and right at 45 degrees (in cartesian coordinates).
         // This represents the offset vector if the player is looking right and standing straight up.
         // If either is untrue, the vector will change in the next step(s).
         Vector compassOffsetVector = Vector.FromRadians(Math.PI / 4.0);
         // Step 2: if the player is facing left, mirror the vector across the X axis so it also
         // faces left at 135 degrees (3*pi/4).
         if (mModel.Direction == Direction.LEFT)
         {
            compassOffsetVector.X = -compassOffsetVector.X;
         }
         // Step 3: rotate the vector to account for the angle at which the player is standing.
         // That angle is the difference from what would be considered standing up straight.
         compassOffsetVector = Vector.Rotate(compassOffsetVector, Vector.UpInRadians - mModel.Pitch);
         // Step 4: give the vector a length equal to the distance from the player's center to any corner.
         compassOffsetVector *= Math.Sqrt(Math.Pow(Width, 2.0) + Math.Pow(Height, 2.0)) / 2.0;
         // Step 5: flip the Y value. The vector has been using cartesian coorindates up to this point.
         // However, the graphical coordinates are vertically flipped with -Y being up.
         compassOffsetVector.Y = -compassOffsetVector.Y;
         // Apply the translation from the player's center to form the compass' quad.
         mCompassRect = new Rectangle((int)(X + (Width / 2.0) + compassOffsetVector.X - (COMPASS_DIAMETER / 2.0)),
                                      (int)(Y + (Height / 2.0) + compassOffsetVector.Y - (COMPASS_DIAMETER / 2.0)),
                                      mCompassRect.Width,
                                      mCompassRect.Height);

         // Move the HP bar below the mobile, with some padding between them.
         // If the player's mobile has a shield, this may be repositioned in the next step.
         mHPBar.X = X;
         mHPBar.Y = Y + Height + (mTextHeight / 5.0);

         // If the player's mobile is one with a shield.
         if (mHasShield == true)
         {
            // Move the shield HP bar below the mobile, with some padding between them.
            mShieldHPBar.X = mHPBar.X;
            mShieldHPBar.Y = mHPBar.Y;
            // Move the HP bar below the shield HP bar.
            mHPBar.Y = mShieldHPBar.Y + mShieldHPBar.Height + (mTextHeight / 5.0);
         }

         // Move the name text below the HP bar.
         mName.Height = mTextHeight;
         mName.X = X;
         mName.Y = mHPBar.Y + mHPBar.Height;
      }

      private void Resize()
      {
         if (mModel == null)
         {
            return;
         }

         Width = mModel.Mobile?.Diameter ?? 0.0;
         Height = mModel.Mobile?.Diameter ?? 0.0;

         mCompassRect = new Rectangle(mCompassRect.X, mCompassRect.Y, COMPASS_DIAMETER, COMPASS_DIAMETER);

         mHPBar.Width = Width;
         mHPBar.Height = mTextHeight / 3.0;

         mShieldHPBar.Width = mHPBar.Width;
         mShieldHPBar.Height = mHPBar.Height;

         mName.Height = mTextHeight;

         // Resize the debug graphics. The collision circle will match the circle used for collisions
         // while the quads will be just large enough to be visible.
         mCollisionCircle.Width = 2.0 * mModel.Radius;
         mCollisionCircle.Height = 2.0 * mModel.Radius;
         mPositionQuad.Width = Width / 10.0;
         mPositionQuad.Height = Width / 10.0;
         mFootingQuad.Width = mPositionQuad.Width;
         mFootingQuad.Height = mPositionQuad.Height;
      }

      private void DisplayHPChange(int aChange)
      {
         // Create and display text to show the amount the HP changed.
         Text valueText = mServiceProvider.GetService<Text>();
         if (aChange > 0)
         {
            valueText.Value = $"+{aChange}";
            valueText.Color = Color.Blue;
         }
         else
         {
            valueText.Value = $"-{aChange}";
            valueText.Color = Color.Red;
         }

         // Initially center the text over the player.
         valueText.X = X + (Width / 2.0) - (valueText.Width / 2.0);
         valueText.Y = Y + (Height / 2.0) - (valueText.Height / 2.0);

         // Add it to the list so Draw() knows to draw it.
         mDamageTexts.Add(valueText);

         // Spawn a task to do two things: 1) slowly float the text up, and
         // 2) remove it from the list once it's above this view.
         Task.Run(async () =>
         {
            // Determine a Y offset for the text. An offset is used in order to
            // use the view's current Y as the origin in case the player moves.
            double yOffset = valueText.Y - Y;

            // While the text is not fully above the view.
            while (valueText.Y + valueText.Height > Y)
            {
               // Slowly translate the text upward at an arbitrary but slow rate.
               await Task.Delay(50);
               yOffset -= Height / 40.0;
               valueText.Y = Y + yOffset;
            }

            // Remove the text. We're done with it.
            mDamageTexts.Remove(valueText);
         });
      }

      private void OnPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
      {
         switch (aArgs.PropertyName)
         {
            case nameof(Player.X):
            case nameof(Player.Y):
            case nameof(Player.Direction):
            case nameof(Player.Pitch):
            case nameof(Player.AimInRadians):
               mGame.InvokeOnUIThread(() =>
               {
                  Reposition();
               });
               break;
            case nameof(Player.Radius):
               mGame.InvokeOnUIThread(() =>
               {
                  Resize();
                  Reposition();
               });
               break;
            case nameof(Player.MobileID):
               mGame.InvokeOnUIThread(() =>
               {
                  LoadMobile();
                  Resize();
                  Reposition();
               });
               break;
            case nameof(Player.ShieldHP):
               // Update the shield HP bar.
               if (mModel.Mobile != null)
               {
                  mShieldHPBar.Percentage = (double)mModel.ShieldHP / mModel.Mobile.ShieldHP;
               }

               // If the player's shield regenerated.
               if (mModel.ShieldHP > mPreviousShieldHP)
               {
                  DisplayHPChange(mModel.ShieldHP - mPreviousShieldHP);
               }

               mPreviousShieldHP = mModel.ShieldHP;
               break;
            case nameof(Player.HP):
               // Update the HP bar.
               if (mModel.Mobile != null)
               {
                  mHPBar.Percentage = (float)mModel.HP / mModel.Mobile.HP;
               }

               // If the player took damage (presumably from being shot).
               if (mModel.HP < mPreviousHP)
               {
                  DisplayHPChange(mPreviousHP - mModel.HP);
               }

               mPreviousHP = mModel.HP;
               break;
            case nameof(Player.Name):
               mName.Value = mModel.Name;
               break;
         }
      }
   }
}
