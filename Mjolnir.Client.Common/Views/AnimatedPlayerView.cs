﻿using BunGound.Client.Common.Disk;
using BunGound.Client.Common.Graphics;
using BunGound.Client.Common.UI;
using BunGound.Common;
using BunGound.Common.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using NLog;
using System;
using System.ComponentModel;

namespace BunGound.Client.Common.Views
{
    /// <summary>
    /// This is the original idea for the player viewmodel, during one point in development.
    /// In contrast to the current one, it uses spritesheets for animations.
    /// This file is being kept in source just in case but its deletion should be considered.
    /// </summary>
    public class AnimatedPlayerView : IVisual, IDisposable
    {
        public enum AnimationEnum { IDLE, WALK, CHARGE, FIRE }

        private const float FRAME_DURATION = 0.25f; // Duration of a single frame in seconds.
        private const int FRAMES_PER_ANIMATION = 4; // Number of frames in any/all animations.
        private const int FRAME_DIAMETER = 216 / 4; // TODO

        private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
        private readonly ContentManager mContentManager;
        private readonly ContentWrapper mContentWrapper;
        private readonly SpriteBatch mSpriteBatch;
        private readonly Image mDebugCircle = null;
        private Texture2D mMobileTexture = null;
        private int mMobileFrameIndex = 0;
        private float mMobileFrameTime = 0f;

        public float X { get; set; } = 0f;

        public float Y { get; set; } = 0f;

        public float Width { get; set; } = 0f;

        public float Height { get; set; } = 0f;

        public float AspectRatio => 1f;

        public Rectangle Bounds => new Rectangle((int)X, (int)Y, (int)Width, (int)Height);

        public Player Model
        {
            get => mModel;
            set => Load(value);
        }
        private Player mModel = null;

        public bool IsMirrored { get; set; } = false;

        public float Rotation { get; set; } = 0f;

        public AnimationEnum Animation
        {
            get => mAnimation;
            set
            {
                if (mAnimation != value)
                {
                    mAnimation = value;
                    mMobileFrameIndex = 0;
                    mMobileFrameTime = 0f;
                }
            }
        }
        private AnimationEnum mAnimation = AnimationEnum.IDLE;

        public bool IsDebugEnabled { get; set; } = false;

        public AnimatedPlayerView(IServiceProvider aServiceProvider)
        {
            mContentManager = aServiceProvider.GetService<ContentManager>();
            mContentWrapper = aServiceProvider.GetService<ContentWrapper>();
            mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();

            mDebugCircle = aServiceProvider.GetService<Image>();
            mDebugCircle.Content = "UI/circle";
        }

        public void Dispose()
        {
            if (mModel != null)
                mModel.PropertyChanged -= OnPropertyChanged;
        }

        public void Draw()
        {
            Draw(null);
        }

        public void Draw(GameTime aGameTime)
        {
            if (mMobileTexture != null)
            {
                if (aGameTime != null)
                {
                    // Continue counting the time spent on the current frame and move onto the next
                    // frame if enough time has passed.
                    mMobileFrameTime += (float)aGameTime.ElapsedGameTime.TotalSeconds;
                    if (mMobileFrameTime >= FRAME_DURATION)
                    {
                        mMobileFrameIndex = (mMobileFrameIndex + 1) % FRAMES_PER_ANIMATION;
                        mMobileFrameTime -= FRAME_DURATION;
                    }
                }

                // Calculate the source rectangle. This is the rectangular portion of the texture
                // that will be drawn. This rectangle's unit is texels, or texture pixels.
                // Mobile textures are laid out in a 4x4 grid where each row represents an animation.
                // The top-most row is the idle animation, the row below it is the walk animation,
                // then the charge animation, and then fire animation.
                Rectangle source = new Rectangle(mMobileFrameIndex * FRAME_DIAMETER, // X
                                                 (int)mAnimation * FRAME_DIAMETER,   // Y
                                                 FRAME_DIAMETER,                     // Width
                                                 FRAME_DIAMETER);                    // Height

                // Prepare position and origin vectors. The position is the (virtual) screen
                // coordinate corresponding to the top-left corner of surface being drawn here.
                Vector2 position = new Vector2(X, Y);
                // The origin is the relative point around which the sprite will be rotated.
                // Although not documented anywhere, the origin does not use screen coordinates nor
                // normalized device coordinates. It should use the texture's coordinates.
                Vector2 origin = new Vector2(FRAME_DIAMETER / 2f, FRAME_DIAMETER / 2f);
                // Finally, calculate the scale relative to the original bitmap.
                float scale = Width / FRAME_DIAMETER;
                mSpriteBatch.Draw(
                    // The bitmap source from which we'll draw a small portion.
                    mMobileTexture,
                    // The position to draw at.
                    position + (origin * scale),
                    // Region in the bitmap to be drawn.
                    source,
                    // Color mask.
                    Color.White,
                    // Rotation, in radians, to be applied.
                    Rotation,
                    // The point, relative to the drawn position, around which rotation happens.
                    origin,
                    // Scale factor. Determines the drawn dimensions.
                    scale,
                    // Sprite effects (none, horizontal flip, or vertical flip).
                    IsMirrored ? SpriteEffects.FlipHorizontally : SpriteEffects.None,
                    // Layer depth.
                    1f);
            }

            // If the debug circle (used for collision detections) is enabled, draw it on top of the player.
            if (IsDebugEnabled)
                mDebugCircle.Draw();
        }

        public override string ToString()
        {
            return $"{{ {nameof(Model)} = {Model?.ToString() ?? "null"}, {nameof(Bounds)} = {Bounds}, " +
                $"{nameof(IsMirrored)} = {IsMirrored}, {nameof(Rotation)} = {Rotation} }}";
        }

        private void Load(Player aPlayer)
        {
            if (aPlayer == null)
                return;

            // If another model was previously loaded, unhook the property changed event.
            if (mModel != null)
                mModel.PropertyChanged -= OnPropertyChanged;

            mModel = aPlayer;
            X = aPlayer.X;
            Y = aPlayer.Y;
            Width = aPlayer.Mobile?.Diameter ?? 0f;
            Height = aPlayer.Mobile?.Diameter ?? 0f;
            mDebugCircle.X = X;
            mDebugCircle.Y = Y;
            mDebugCircle.Width = Width;
            mDebugCircle.Height = Height;
            IsMirrored = mModel.Direction == Direction.RIGHT;
            mModel.PropertyChanged += OnPropertyChanged;

            try
            {
                mMobileTexture = mContentManager.Load<Texture2D>(mContentWrapper.MobileTexturePath(aPlayer.Mobile));
            }
            catch (Exception e)
            {
                mLog.Error(e, $"Exception thrown while loading texture of mobile {aPlayer.Mobile?.Name}");
                mMobileTexture = null;
            }
        }

        private void OnPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
        {
            switch (aArgs.PropertyName)
            {
                case nameof(Player.X):
                    X = mModel.X;
                    mDebugCircle.X = X;
                    break;
                case nameof(Player.Y):
                    Y = mModel.Y;
                    mDebugCircle.Y = Y;
                    break;
                case nameof(Player.IsWalking):
                    if (mModel.IsWalking)
                        Animation = AnimationEnum.WALK;
                    else
                        Animation = AnimationEnum.IDLE;
                    break;
                case nameof(Player.Direction):
                    IsMirrored = mModel.Direction == Direction.RIGHT;
                    break;
                case nameof(Player.Mobile):
                    Width = mModel.Mobile?.Diameter ?? 0f;
                    Height = mModel.Mobile?.Diameter ?? 0f;
                    mDebugCircle.Width = Width;
                    mDebugCircle.Height = Height;
                    break;
                case nameof(Player.Rotation):
                    // The rotation of this view and of the model are related but with effectively
                    // one difference: the model's rotation is absolute whereas this view's can be
                    // thought of as relative. The view's rotation is the rotation applied to the
                    // sprite, around a center point, where a value of zero (radians) means no
                    // rotation is applied. However, an equivalent model would have a rotation of
                    // -Pi/2 (radians), keeping in mind negative Y is up. As a result, the view's
                    // rotation is effectively offset from the model's by Pi/2.
                    Rotation = mModel.Rotation + (float)(Math.PI / 2.0);
                    break;
            }
        }
    }
}
