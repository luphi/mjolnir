﻿using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using NLog;
using System;
using System.Collections.Generic;
using Mjolnir.Common.Collections;

namespace Mjolnir.Client.Common.UI
{
   /// <summary>
   /// In comparison to the standard Table, this one is simpler and contains only two columns. It also
   /// lacks a scrollbar, configurable weights, and configurable row counts.
   /// Headers and content are clipped to their respective edges (i.e. the left header is left-aligned
   /// and the right header is right-aligned).
   /// </summary>
   public class SplitTable : IVisual
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly ConcurrentList<Text> mTexts = new ConcurrentList<Text>();

      public double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      private double mX = 0.0;

      public double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      private double mY = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            Resize();
         }
      }
      private double mWidth = 0.0;

      public double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            Resize();
         }
      }
      private double mHeight = 0.0;

      public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

      public Rectangle Bounds => new Rectangle((int)mX, (int)mY, (int)mWidth, (int)mHeight);

      public uint Padding
      {
         get => mPadding;
         set
         {
            mPadding = value;
            Resize();
         }
      }
      private uint mPadding = 5; // Pixels.

      public string LeftHeader
      {
         get => mLeftHeader;
         set
         {
            if (value != null)
            {
               mLeftHeader = value;
               Resize();
            }
         }
      }
      private string mLeftHeader = string.Empty;

      public string RightHeader
      {
         get => mRightHeader;
         set
         {
            if (value != null)
            {
               mRightHeader = value;
               Resize();
            }
         }
      }
      private string mRightHeader = string.Empty;

      public List<List<string>> Content
      {
         get => mContent;
         set
         {
            if (value != null)
            {
               lock (mContent)
               {
                  mContent.Clear();

                  foreach (List<string> row in value)
                  {
                     if (row.Count == 0)
                     {
                        continue;
                     }

                     if (row.Count == 1)
                     {
                        mContent.Add(new List<string>
                        {
                           row[0],
                           string.Empty
                        });
                     }
                     else
                     {
                        mContent.Add(new List<string>
                        {
                           row[0],
                           row[1]
                        });
                     }
                  }
               }

               Resize();
            }
         }
      }
      private readonly List<List<string>> mContent = new List<List<string>>();

      public SplitTable(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
      }

      public void Draw()
      {
         foreach (Text test in mTexts)
         {
            test.Draw();
         }
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         foreach (Text text in mTexts)
         {
            text.X += aX;
            text.Y += aY;
         }
      }

      private void Resize()
      {
         lock (mContent)
         {
            double rowHeight = (mHeight - (2 * mPadding)) / (mContent.Count + 1);

            mTexts.Clear();

            // Create the left header's Text and align it to the top left.
            Text leftHeader = mServiceProvider.GetService<Text>();
            leftHeader.Value = mLeftHeader;
            leftHeader.Height = rowHeight;
            leftHeader.X = mX + mPadding;
            leftHeader.Y = mY + mPadding;
            mTexts.Add(leftHeader);

            // Create thr right header's Text and align it to the top right.
            Text rightHeader = mServiceProvider.GetService<Text>();
            rightHeader.Value = mRightHeader;
            rightHeader.Height = rowHeight;
            rightHeader.X = mX + mWidth - mPadding - rightHeader.Width;
            rightHeader.Y = mY + mPadding;

            // If the left header's text overlaps the right's.
            if ((leftHeader.X + leftHeader.Width + mPadding) > rightHeader.X)
            {
               leftHeader.Truncate((uint)(rightHeader.X - mPadding - leftHeader.X));
            }
            mTexts.Add(rightHeader);

            // For each row of content. (Where each row contains exactly two elements.)
            for (int i = 0; i < mContent.Count; i++)
            {
               List<string> row = mContent[i];

               // If the first element is not empty.
               if (string.IsNullOrEmpty(row[0]) == false)
               {
                  // Create a Text for the first element and snap it to the left edge.
                  Text text = mServiceProvider.GetService<Text>();
                  text.Value = row[0];
                  text.Height = rowHeight;
                  text.X = mX + mPadding;
                  text.Y = mY + ((i + 1) * rowHeight) + mPadding;
                  mTexts.Add(text);
               }

               // If the second element is not empty.
               if (string.IsNullOrEmpty(row[1]) == false)
               {
                  // Create a Text for the second element and snap it to the right edge.
                  Text text = mServiceProvider.GetService<Text>();
                  text.Value = row[1];
                  text.Height = rowHeight;
                  text.X = mX + mWidth - text.Width - mPadding;
                  text.Y = mY + ((i + 1) * rowHeight) + mPadding;
                  mTexts.Add(text);
               }
            }
         }
      }
   }
}
