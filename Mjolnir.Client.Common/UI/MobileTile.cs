﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Common.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using System;

namespace Mjolnir.Client.Common.UI
{
   /// <summary>
   /// A button featuring a mobile for use by the lobby screen.
   /// The mobile represented by this button/tile is centered within the bounds.
   /// A border is shown when hovering over this tile to indicate it is interactable
   /// and a border is permanently shown when the tile is selected.
   /// </summary>
   public class MobileTile : ButtonBase
   {
      private readonly ContentWrapper mContentWrapper;
      private readonly Configuration mConfiguration;
      private readonly Image mImage;
      private Color mOriginalColor; // Remembers the border's color prior to a hover event.
      private bool mIsHovered = false;

      public override double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      private double mX = 0.0;

      public override double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      private double mY = 0.0;

      public override double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            Resize();
         }
      }
      private double mWidth = 0.0;

      public override double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            Resize();
         }
      }
      private double mHeight = 0.0;

      public Color Color
      {
         get => mColor;
         set
         {
            mColor = value;
            if (mIsHovered == true)
            {
               mBorder.Color = value.Brighten();
            }
            else
            {
               mBorder.Color = value;
            }
         }
      }
      private Color mColor;

      public bool IsSelected
      {
         get => mIsSelected;
         set
         {
            mIsSelected = value;
            HasBorder = value;
         }
      }
      private bool mIsSelected;

      public Mobile Mobile
      {
         get => mMobile;
         set
         {
            mMobile = value;
            if (value != null)
            {
               mImage.Content = mContentWrapper.MobileTexturePath(value);
               Resize();
            }
         }
      }
      private Mobile mMobile = null;

      public MobileTile(IServiceProvider aServiceProvider) : base(aServiceProvider)
      {
         mContentWrapper = aServiceProvider.GetService<ContentWrapper>();
         mConfiguration = aServiceProvider.GetService<Configuration>();
         mImage = aServiceProvider.GetService<Image>();

         HoverEvent += OnHover;
         UnhoverEvent += OnUnhover;

         Color = mConfiguration.SecondaryColor.ToColor(); // Make use of the setter to apply the default color.
      }

      public override void Draw()
      {
         mImage.Draw();

         base.Draw();
      }

      public override string ToString()
      {
         return $"{{ {nameof(Mobile)} = {Mobile?.Name}, {nameof(IsSelected)} = {IsSelected}, " +
                $"{nameof(Bounds)} = {Bounds} }}";
      }

      private void OnHover(object aSender, EventArgs aArgs)
      {
         mIsHovered = true;
         mOriginalColor = mColor;

         Color = mConfiguration.SecondaryColor.ToColor();
         HasBorder = true;
      }

      private void OnUnhover(object aSender, EventArgs aArgs)
      {
         mIsHovered = false;

         Color = mOriginalColor;
         HasBorder = mIsSelected;
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         mBorder.X += aX;
         mBorder.Y += aY;

         mImage.X += aX;
         mImage.Y += aY;
      }

      private void Resize()
      {
         mBorder.X = mX;
         mBorder.Y = mY;
         mBorder.Width = mWidth;
         mBorder.Height = mHeight;

         if (Mobile != null)
         {
            Rectangle paddedBounds = Bounds;
            paddedBounds.X += (int)Padding;
            paddedBounds.Y += (int)Padding;
            paddedBounds.Width -= (int)(Padding * 2);
            paddedBounds.Height -= (int)(Padding * 2);
            mImage.CenterInBounds(paddedBounds);
         }
      }
   }
}
