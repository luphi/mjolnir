﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mjolnir.Client.Common.UI
{
   public class ChatLog : IVisual
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly Configuration mConfiguration;
      private readonly List<SingleMessage> mMessages = new List<SingleMessage>();
      private readonly Quad mBackground;

      public double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      private double mX = 0.0;

      public double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      private double mY = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            ResizeAndCull();
         }
      }
      private double mWidth = 0.0;

      public double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            ResizeAndCull();
         }
      }
      private double mHeight = 0.0;

      public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

      public uint Padding
      {
         get => mPadding;
         set
         {
            mPadding = value;
            ResizeAndCull();
         }
      }
      private uint mPadding = 5;

      public Rectangle Bounds => new Rectangle((int)mX, (int)mY, (int)mWidth, (int)mHeight);

      /// <summary>
      /// Maximum number of lines to appear in the bounds.
      /// This is not the maximum number of messages.
      /// </summary>
      public uint MaxLines
      {
         get => mMaxLines;
         set
         {
            if (value > 0)
            {
               mMaxLines = value;
               ResizeAndCull();
            }
         }
      }
      private uint mMaxLines = 10;

      /// <summary>
      /// Time, in milliseconds, after which a message added to this log will be removed.
      /// If a newer message pushes it out, it may be removed sooner.
      /// </summary>
      public uint Expiration { get; set; } = 15000;

      public ChatLog(IServiceProvider aServiceProvider, Configuration aConfiguration)
      {
         mServiceProvider = aServiceProvider;
         mConfiguration = aConfiguration;
         mBackground = aServiceProvider.GetService<Quad>();

         mBackground.Color = new Color
         {
            R = 0,
            G = 0,
            B = 0,
            A = 100
         };
      }

      public void Draw()
      {
         mBackground.Draw();

         lock (mMessages)
         {
            foreach (SingleMessage message in mMessages)
            {
               foreach (Text text in message.Texts)
               {
                  text.Draw();
               }
            }
         }
      }

      /// <summary>
      /// Add a message to this log. It will be drawn and placed along the bottom edge until
      /// a newer message is added. Messages added here will also expire according to the
      /// duration set with the Expiration property.
      /// </summary>
      /// <param name="aName">Player's name, to appear on the left.</param>
      /// <param name="aMessage">The message from the player, to appear on the right.</param>
      /// <param name="aIsPrivate">Flag indicating if the message is private (to the team).</param>
      public void Add(string aName, string aMessage, bool aIsPrivate = false)
      {
         if ((string.IsNullOrEmpty(aName) == true) || (string.IsNullOrEmpty(aMessage) == true))
         {
            return;
         }

         SingleMessage message = new SingleMessage(aName, aMessage, aIsPrivate);
         lock (mMessages)
         {
            mMessages.Add(message);
         }

         ResizeAndCull();

         Task.Run(async () =>
         {
            await Task.Delay((int)Expiration);
            lock (mMessages)
            {
               mMessages.Remove(message);
            }
            ResizeAndCull();
         });
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         mBackground.X += aX;
         mBackground.Y += aY;

         lock (mMessages)
         {
            foreach (SingleMessage chat in mMessages)
            {
               foreach (Text text in chat.Texts)
               {
                  text.X += aX;
                  text.Y += aY;
               }
            }
         }
      }

      private void ResizeAndCull()
      {
         // If it's too soon to resize anything.
         if ((mWidth == 0.0) || (mHeight == 0.0))
         {
            return;
         }

         Rectangle bounds = Bounds;

         // Calculate the boundaries for the two distinct sections.
         // Players' names to the left and messages to the right. Messages use a large majority of the width.
         int padding = (int)(bounds.Height * 0.05);
         Rectangle nameBounds = new Rectangle(bounds.X + padding,
                                              bounds.Y + padding,
                                              (bounds.Width / 3) - (padding * 2),
                                              bounds.Height - (padding * 2));
         Rectangle messageBounds = new Rectangle(bounds.X + nameBounds.Width + padding,
                                                 bounds.Y + padding,
                                                 bounds.Width - nameBounds.Width - (padding * 2),
                                                 bounds.Height - (padding * 2));

         uint lineSum = 0;
         double lowestY = messageBounds.Y + messageBounds.Height;
         uint textHeight = (uint)(bounds.Height / (double)mMaxLines);
         List<SingleMessage> messagesToRemove = new List<SingleMessage>();
         lock (mMessages)
         {
            List<SingleMessage> reversed = new List<SingleMessage>(mMessages);
            reversed.Reverse();
            foreach (SingleMessage message in reversed)
            {
               // If a previous iteration found a message to remove.
               if (messagesToRemove.Count > 0)
               {
                  // Any further messages would also be outside the bounds. Remove it too.
                  messagesToRemove.Add(message);
                  continue;
               }

               message.Texts.Clear();

               // Step 1: create the line(s), as strings, to represent the message.
               // Create a Text object with the appropriate height that will be used to
               // calculate the width of the would-be resulting lines.
               Text text = mServiceProvider.GetService<Text>();
               text.Height = textHeight;
               string[] split = message.Message.Split(' '); // Array of single words
               List<string> lines = new List<string>(); // Width-limited lines from the above
               string concat = string.Empty; // String reused to determine lines

               // For each individual word in the message.
               foreach (string word in split)
               {
                  // Concatenate the word onto the line.
                  string wouldBeConcat = $"{(string.IsNullOrEmpty(concat) ? string.Empty : $"{concat} ")}{word}";

                  if (text.WouldBeWidth(wouldBeConcat) >= messageBounds.Width)
                  {
                     // If this would exceed the width of the bounds.

                     // Remember the line without the most recent word.
                     lines.Add(concat);
                     // Assign the leftover word to what will become the next line.
                     concat = word;
                  }
                  else
                  {
                     // If this line still fits within the bounds.

                     // Apppend it and continue to the next word.
                     concat = wouldBeConcat;
                  }
               }

               // Make a line of any remaining words.
               if (string.IsNullOrEmpty(concat) == false)
               {
                  lines.Add(concat);
               }

               // Create visual text lines out of the width-limited (string) lines.
               // Reverse the list in order to create the bottom-most lines first.
               lines.Reverse();
               for (int i = 0; i < lines.Count; i++)
               {
                  if (i > 0)
                  {
                     text = mServiceProvider.GetService<Text>();
                  }

                  text.Value = lines[i];
                  text.Height = textHeight;
                  text.X = messageBounds.X;
                  text.Y = messageBounds.Y + messageBounds.Height - (textHeight * (lineSum + 1));

                  // Apply a color to the text based on whether it's a public (to all) message
                  // or a private (just to the team) message.
                  if (message.IsPrivate)
                  {
                     text.Color = mConfiguration.SecondaryColor.ToColor();
                  }
                  else
                  {
                     text.Color = mConfiguration.PrimaryColor.ToColor();
                  }

                  if (text.Y < messageBounds.Y)
                  {
                     messagesToRemove.Add(message);
                  }
                  else
                  {
                     message.Texts.Add(text);
                     lineSum += 1;
                  }
               }

               // If a previous iteration found a message to remove.
               if (messagesToRemove.Count > 0)
               {
                  // No more messages will fit. We're done. Future messages will be culled.
                  continue;
               }

               // Create, position, and add the name.
               text = mServiceProvider.GetService<Text>();
               text.Height = textHeight;
               text.Value = message.Name;
               text.Color = message.Name.Colorize();
               if (text.Width > nameBounds.Width)
               {
                  text.Truncate((uint)nameBounds.Width);
               }
               text.X = nameBounds.X;
               // Place the name at the Y coordinate of the highest message line.
               text.Y = message.Texts[message.Texts.Count - 1].Y;
               if (text.Y < lowestY)
               {
                  lowestY = text.Y;
               }
               message.Texts.Add(text);
            }

            // Remove the messages that no longer fit in the bounds from the main list.
            foreach (SingleMessage message in messagesToRemove)
            {
               mMessages.Remove(message);
            }
         }

         // Position and size the background such that it will fall behind all text but
         // no more than necessary.
         mBackground.X = bounds.X;
         mBackground.Y = lowestY;
         mBackground.Width = bounds.Width;
         mBackground.Height = messageBounds.Y + messageBounds.Height - lowestY;
      }

      /// <summary>
      /// Private helper class for associating player names, messages, and Texts.
      /// </summary>
      private class SingleMessage
      {
         public readonly string Name;
         public readonly string Message;
         public readonly bool IsPrivate;
         public readonly List<Text> Texts = new List<Text>();

         public SingleMessage(string aName, string aMessage, bool aIsPrivate)
         {
            Name = aName;
            Message = aMessage;
            IsPrivate = aIsPrivate;
         }
      }
   }
}
