﻿using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NLog;
using System;
using Mjolnir.Client.Common.Input;

namespace Mjolnir.Client.Common.UI
{
   public class ButtonBase : IControl, IVisual
   {
      protected readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      protected readonly SpriteBatch mSpriteBatch;
      protected readonly Border mBorder;
      private bool mIsMouseInBounds = false;

      public virtual double X { get; set; } = 0.0;

      public virtual double Y { get; set; } = 0.0;

      public virtual double Width { get; set; } = 0.0;

      public virtual double Height { get; set; } = 0.0;

      public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

      public virtual Rectangle Bounds => new Rectangle((int)X, (int)Y, (int)Width, (int)Height);

      public virtual uint Padding { get; set; } = 3; // Pixels.

      public bool HasBorder { get; set; } = false;

      public event EventHandler HoverEvent;
      public event EventHandler UnhoverEvent;
      public event EventHandler ClickEvent;

      public ButtonBase(IServiceProvider aServiceProvider)
      {
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mBorder = aServiceProvider.GetService<Border>();
      }

      public virtual void Respond(InputEvent aInput)
      {
         if (aInput.Op == InputEvent.Opcode.HOVER)
         {
            bool wasMouseInBounds = mIsMouseInBounds;
            mIsMouseInBounds = Bounds.Contains(aInput.Position.ToXna()) == true;

            if (wasMouseInBounds != mIsMouseInBounds)
            {
               if (mIsMouseInBounds == true)
               {
                  try
                  {
                     HoverEvent?.Invoke(this, EventArgs.Empty);
                  }
                  catch (Exception e)
                  {
                     mLog.Error(e, "Exception thrown while propagating a hover event");
                  }
               }
               else
               {
                  try
                  {
                     UnhoverEvent?.Invoke(this, EventArgs.Empty);
                  }
                  catch (Exception e)
                  {
                     mLog.Error(e, "Exception thrown while propagating an unhover event");
                  }
               }
            }
         }
         else if ((aInput.Op == InputEvent.Opcode.MOUSE_DOWN) && Bounds.Contains(aInput.Position.ToXna()))
         {
            try
            {
               ClickEvent?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception e)
            {
               mLog.Error(e, "Exception thrown while propagating a click event");
            }
         }
      }

      public virtual void Draw()
      {
         if (HasBorder == true)
         {
            mBorder.Draw();
         }
      }
   }
}
