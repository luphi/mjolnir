﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Common.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using NLog;
using System;
using System.Collections.Generic;
using Mjolnir.Client.Common.Input;
using System.Collections.Concurrent;
using Mjolnir.Common.Collections;

namespace Mjolnir.Client.Common.UI
{
   public class ItemTray : IControl, IVisual
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly ContentWrapper mContentWrapper;
      private readonly Border mBorder;
      private readonly Quad[] mSlotHighlights = new Quad[6];
      private readonly Quad[] mSlotHints = new Quad[6];
      private readonly ConcurrentDictionary<ItemSlot, ImageButton> mButtons =
                   new ConcurrentDictionary<ItemSlot, ImageButton>();
      private readonly ConcurrentList<int> mHighlightedSlots =
                   new ConcurrentList<int>(); // No slots are highlighted at first.
      private readonly ConcurrentList<int> mUnoccupiedSlots =
                   new ConcurrentList<int> { 0, 1, 2, 3, 4, 5 }; // All slots unoccupied at first.

      public class TrayData
      {
         public Item Item;
         public int FirstIndex;
      }

      public double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      private double mX = 0.0;

      public double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      private double mY = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            mHeight = value / AspectRatio;
            Resize();
         }
      }
      private double mWidth = 0.0;

      public double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            mWidth = value * AspectRatio;
            Resize();
         }
      }
      private double mHeight = 0.0;

      public double AspectRatio => 6.0; // The tray contains room for six slots, so a 6:1 ratio.

      public Rectangle Bounds => new Rectangle((int)mX, (int)mY, (int)mWidth, (int)mHeight);

      public event EventHandler<TrayData> ClickEvent;

      public ItemTray(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mContentWrapper = aServiceProvider.GetService<ContentWrapper>();
         mBorder = aServiceProvider.GetService<Border>();

         Configuration configuration = aServiceProvider.GetService<Configuration>();
         Color primaryColor = configuration.PrimaryColor.ToColor();
         Color secondaryColor = configuration.SecondaryColor.ToColor();
         for (int i = 0; i < 6; i++)
         {
            Quad quad = aServiceProvider.GetService<Quad>();
            quad.Color = secondaryColor;
            mSlotHighlights[i] = quad;
            quad = aServiceProvider.GetService<Quad>();
            quad.Color = primaryColor;
            mSlotHints[i] = quad;
         }

         mBorder.Color = primaryColor;
      }

      public void Respond(InputEvent aInput)
      {
         foreach (ImageButton button in mButtons.Values)
         {
            button.Respond(aInput);
         }
      }

      public void Draw()
      {
         foreach (int index in mHighlightedSlots)
         {
            mSlotHighlights[index].Draw();
         }

         // Draw each button (whose images/icons are the items).
         foreach (ImageButton button in mButtons.Values)
         {
            button.Draw();
         }

         // For any slot that is not holding an item, draw the "hint" to indicate it's empty.
         foreach (int index in mUnoccupiedSlots)
         {
            mSlotHints[index].Draw();
         }

         mBorder.Draw();
      }

      /// <summary>
      /// Display the given array of Items while maintaining their slot positions and sizes.
      /// The array of Items must be exactly six in length (i.e. Item[6]).
      /// This is intended to be used as a way to display a Player's items.
      /// </summary>
      /// <param name="aItems"></param>
      public void Sync(Item[] aItems)
      {
         if (aItems?.Length != 6)
         {
            return;
         }

         mServiceProvider.GetService<MjolnirGame>().InvokeOnUIThread(() =>
         {
            // Instantiation of these buttons must be done on the UI thread. In order to
            // guarantee this, a flag is set to trigger this sync on the next draw due to
            // draws (hopefully) being limited to the UI thread.

            // Clear any existing buttons and create new ones based on the item list.
            mButtons.Clear();

            // Reset the list of unoccupied slots (used to determine which slot hints should
            // be drawn). Indexes will be subtracted from this list in the following loop.
            lock (mUnoccupiedSlots)
            {
               mUnoccupiedSlots.Clear();
               mUnoccupiedSlots.AddRange(new int[] { 0, 1, 2, 3, 4, 5 });
            }

            // Cycle through the item slots.
            for (int i = 0; i < 6; i++)
            {
               Item item = aItems[i];
               // If this slot has an item.
               if (item != null)
               {
                  // Continue determining which slots are unoccupied by removing the indexes
                  // of this item from the list.
                  lock (mUnoccupiedSlots)
                  {
                     mUnoccupiedSlots.Remove(i);
                     if (item.Slots == 2)
                     {
                        mUnoccupiedSlots.Remove(i + 1);
                     }
                  }

                  // Instantiate the button and provide it with an image based on the item.
                  ImageButton button = mServiceProvider.GetService<ImageButton>();
                  button.Padding = 0;
                  button.Content = mContentWrapper.ItemTexturePath(item);
                  // Add a click event that tells observers which item slots were clicked.
                  TrayData data = new TrayData { Item = item, FirstIndex = i };
                  button.ClickEvent += delegate (object aSender, EventArgs aArgs)
                  {
                     try
                     {
                        ClickEvent?.Invoke(this, data);
                     }
                     catch (Exception e)
                     {
                        mLog.Error(e, "Excpetion thrown while propagating a click event");
                     }
                  };

                  // Map the item's first index and number of slots to the button.
                  mButtons[new ItemSlot { FirstIndex = i, Slots = item.Slots }] = button;

                  i += (int)item.Slots - 1; // Increments i an extra index if the item uses two slots.
               }
            }

            // Position and size the buttons.
            Resize();
         });
      }

      /// <summary>
      /// Visually draw attention to the given indexes of the tray when one or two indexes are
      /// passed as a parameter. If any other value is passed (e.g. null), any existing highlight(s)
      /// will be removed.
      /// </summary>
      /// <param name="aIndexes">One or two indexes to be highlighted. No more, no less.</param>
      public void HighlightIndexes(int[] aIndexes)
      {
         mHighlightedSlots.Clear();

         if ((aIndexes?.Length >= 1) && (aIndexes?.Length <= 2))
         {
            mHighlightedSlots.AddRange(aIndexes);
         }
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         mBorder.X += aX;
         mBorder.Y += aY;

         foreach (Quad highlight in mSlotHighlights)
         {
            highlight.X += aX;
            highlight.Y += aY;
         }

         foreach (Quad hint in mSlotHints)
         {
            hint.X += aX;
            hint.Y += aY;
         }

         foreach (ImageButton button in mButtons.Values)
         {
            button.X += aX;
            button.Y += aY;
         }
      }

      private void Resize()
      {
         mBorder.X = mX;
         mBorder.Y = mY;
         mBorder.Width = mWidth;
         mBorder.Height = mHeight;

         for (int i = 0; i < mSlotHighlights.Length; i++)
         {
            Quad highlight = mSlotHighlights[i];
            highlight.Width = mWidth / AspectRatio;
            highlight.Height = mHeight;
            highlight.X = mX + (i * highlight.Width);
            highlight.Y = mY;
         }

         double slotWidth = (mWidth - (2 * mBorder.Thickness)) / AspectRatio;
         double slotHeight = mHeight - (2 * mBorder.Thickness);

         for (int i = 0; i < mSlotHints.Length; i++)
         {
            Quad hint = mSlotHints[i];
            hint.Width = mBorder.Thickness;
            hint.Height = mBorder.Thickness;
            hint.X = mX + mBorder.Thickness + (i * slotWidth) + (slotWidth / 2.0) - (hint.Width / 2.0);
            hint.Y = mY + mBorder.Thickness + (slotHeight / 2.0) - (hint.Height / 2.0);
         }

         foreach (KeyValuePair<ItemSlot, ImageButton> pair in mButtons)
         {
            ImageButton button = pair.Value;
            button.X = mX + mBorder.Thickness + (pair.Key.FirstIndex * slotWidth);
            button.Y = mY + mBorder.Thickness;
            button.Width = slotWidth * pair.Key.Slots;
            button.Height = slotHeight;
         }
      }

      private class ItemSlot
      {
         public int FirstIndex;
         public uint Slots;
      }
   }
}
