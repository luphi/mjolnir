﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Mjolnir.Client.Common.Input;
using System;

namespace Mjolnir.Client.Common.UI
{
   /// <summary>
   /// A fullscreen, popup dialog that displays a message, provides a text input, and displays
   /// "CANCEL" and "OK" buttons.
   /// </summary>
   public class InputModalWindow : ConfirmationModalWindow
   {
      private readonly TextInput mInput;

      /// <summary>
      /// The current text the user has entered.
      /// </summary>
      public string Value => mInput.Value;

      public InputModalWindow(IServiceProvider aServiceProvider) : base(aServiceProvider)
      {
         mInput = aServiceProvider.GetService<TextInput>();

         mInput.EnterEvent += delegate (object aSender, EventArgs aArgs)
         {
            OnAcknowledgeEvent();
         };

         mInput.EscapeEvent += delegate (object aSender, EventArgs aArgs)
         {
            OnCancelEvent();
         };

         mInput.IsFocused = true;
      }

      public override void Respond(InputEvent aInput)
      {
         base.Respond(aInput);

         mInput.Respond(aInput);
      }

      public override void Draw()
      {
         base.Draw();

         mInput.Draw();
      }

      protected override Rectangle Resize()
      {
         //double textHeight = Bounds.Height * 0.05;
         Rectangle paddedBounds = base.Resize();

         mInput.Width = paddedBounds.Width;
         mInput.X = paddedBounds.X;
         lock (mButtons)
         {
            // Place the input right above the "CANCEL" and "OK" buttons.
            mInput.Y = paddedBounds.Y + paddedBounds.Height - mButtons[0].Height - mInput.Height;
         }

         return Rectangle.Empty; // This virtual method has a return value so this class is able to get the usable,
                                 // padded bounds set by the child class. There is no child of this class so its
                                 // return value is never used. So just return anything.
      }
   }
}
