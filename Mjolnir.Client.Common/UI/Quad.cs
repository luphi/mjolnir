﻿using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Mjolnir.Client.Common.UI
{
   /// <summary>
   /// A visible, solid-color rectangle.
   /// </summary>
   public class Quad : IVisual
   {
      private readonly SpriteBatch mSpriteBatch;
      private Texture2D mTexture = null;
      private Color mColor = Color.White;

      public Color Color
      {
         get => mColor;
         set
         {
            mColor = value;
            mTexture?.SetData(new[]
            {
               value
            });
         }
      }
      public double X { get; set; } = 0.0;

      public double Y { get; set; } = 0.0;

      public double Width { get; set; } = 0.0;

      public double Height { get; set; } = 0.0;

      public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

      public Rectangle Bounds => new Rectangle((int)X, (int)Y, (int)Width, (int)Height);

      public Quad(IServiceProvider aServiceProvider)
      {
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();

         MjolnirGame game = aServiceProvider.GetService<MjolnirGame>();
         game.InvokeOnUIThread(() =>
         {
            mTexture = new Texture2D(aServiceProvider.GetService<GraphicsDevice>(),
                                     1, // Width
                                     1, // Height
                                     false,
                                     SurfaceFormat.Color);
         });
      }

      public void Draw()
      {
         if (mTexture != null)
         {
            mSpriteBatch.Draw(mTexture, Bounds, Color.White);
         }
      }

      public override string ToString()
      {
         return $"{{ {nameof(Bounds)} = {Bounds}, {nameof(Color)} = {mColor} }}";
      }
   }
}
