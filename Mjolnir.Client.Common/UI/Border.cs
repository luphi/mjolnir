﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using NLog;
using System;

namespace Mjolnir.Client.Common.UI
{
   public class Border : IVisual
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly Quad mTopEdge;
      private readonly Quad mRightEdge;
      private readonly Quad mBottomEdge;
      private readonly Quad mLeftEdge;

      public double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      protected double mX = 0.0;

      public double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      protected double mY = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            Resize();
         }
      }
      protected double mWidth = 0.0;

      public double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            Resize();
         }
      }
      protected double mHeight = 0.0;

      public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

      public Rectangle Bounds => new Rectangle((int)mX, (int)mY, (int)mWidth, (int)mHeight);

      public uint Thickness
      {
         get => mThickness;
         set
         {
            mThickness = value;
            Resize();
         }
      }
      private uint mThickness = 2; // Pixels.

      public Color Color
      {
         get => mColor;
         set
         {
            mColor = value;
            mTopEdge.Color = value;
            mRightEdge.Color = value;
            mBottomEdge.Color = value;
            mLeftEdge.Color = value;
         }
      }
      private Color mColor;

      public Border(IServiceProvider aServiceProvider)
      {
         mTopEdge = aServiceProvider.GetService<Quad>();
         mRightEdge = aServiceProvider.GetService<Quad>();
         mBottomEdge = aServiceProvider.GetService<Quad>();
         mLeftEdge = aServiceProvider.GetService<Quad>();

         Color = aServiceProvider.GetService<Configuration>().PrimaryColor.ToColor();
      }

      public void Draw()
      {
         mTopEdge.Draw();
         mRightEdge.Draw();
         mBottomEdge.Draw();
         mLeftEdge.Draw();
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         mTopEdge.X += aX;
         mTopEdge.Y += aY;

         mRightEdge.X += aX;
         mRightEdge.Y += aY;

         mBottomEdge.X += aX;
         mBottomEdge.Y += aY;

         mLeftEdge.X += aX;
         mLeftEdge.Y += aY;
      }

      private void Resize()
      {
         // If it's too soon to resize anything.
         if ((mWidth == 0.0) || (mHeight == 0.0))
         {
            return;
         }

         mTopEdge.X = mX;
         mTopEdge.Y = mY;
         mTopEdge.Width = mWidth;
         mTopEdge.Height = mThickness;

         mRightEdge.X = mX + mWidth - mThickness;
         mRightEdge.Y = mY;
         mRightEdge.Width = mThickness;
         mRightEdge.Height = mHeight;

         mBottomEdge.X = mX;
         mBottomEdge.Y = mY + mHeight - mThickness;
         mBottomEdge.Width = mWidth;
         mBottomEdge.Height = mThickness;

         mLeftEdge.X = mX;
         mLeftEdge.Y = mY;
         mLeftEdge.Width = mThickness;
         mLeftEdge.Height = mHeight;
      }
   }
}
