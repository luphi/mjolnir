﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using NLog;
using System;

namespace Mjolnir.Client.Common.UI
{
   public class PowerBar : IVisual
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly Border mBorder;
      private readonly Quad mLastPowerMarker;
      private readonly Quad[] mQuartileMarkers = new Quad[3];
      private readonly Quad mBar;

      public double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      protected double mX = 0.0;

      public double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      protected double mY = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            Resize();
         }
      }
      protected double mWidth = 0.0;

      public double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            Resize();
         }
      }
      protected double mHeight = 0.0;

      public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

      public Rectangle Bounds => new Rectangle((int)mX, (int)mY, (int)mWidth, (int)mHeight);

      public Color BarColor
      {
         get => mBar.Color;
         set => mBar.Color = value;
      }

      public Color BorderColor
      {
         get => mBorder.Color;
         set => mBorder.Color = value;
      }

      public bool HasBorder
      {
         get => mHasBorder;
         set
         {
            mHasBorder = value;
            Resize();
         }
      }
      private bool mHasBorder = true;

      /// <summary>
      /// Percentage to be displayed by the power bar, ranging from 0.0 to 1.0.
      /// Any other value will be clamped to this range.
      /// </summary>
      public double Percentage
      {
         get => mPercentage;
         set
         {
            mPercentage = Math.Clamp(value, 0.0, 1.0);

            if (mWidth > 0.0)
            {
               if (mHasBorder)
               {
                  mBar.Width = mPercentage * (mWidth - (2 * mBorder.Thickness));
               }
               else
               {
                  mBar.Width = mPercentage * mWidth;
               }
            }
         }
      }
      private double mPercentage = 1.0;

      public PowerBar(IServiceProvider aServiceProvider)
      {
         mBorder = aServiceProvider.GetService<Border>();
         mBorder.Thickness = 1;
         mLastPowerMarker = aServiceProvider.GetService<Quad>();
         for (int i = 0; i < mQuartileMarkers.Length; i++)
         {
            mQuartileMarkers[i] = aServiceProvider.GetService<Quad>();
         }
         mBar = aServiceProvider.GetService<Quad>();

         Configuration configuration = aServiceProvider.GetService<Configuration>();
         mBorder.Color = configuration.PrimaryColor.ToColor();
         mBar.Color = configuration.SecondaryColor.ToColor();
         mLastPowerMarker.Color = configuration.PrimaryColor.ToColor();
         foreach (Quad marker in mQuartileMarkers)
         {
            marker.Color = configuration.SecondaryColor.ToColor();
         }
      }

      public void Draw()
      {
         foreach (Quad marker in mQuartileMarkers)
         {
            marker.Draw();
         }
         mBar.Draw();
         mLastPowerMarker.Draw();
         mBorder.Draw();
      }

      public void RememberPower()
      {
         mLastPowerMarker.X = mBar.X + mBar.Width - (mLastPowerMarker.Width / 2.0);
         double minX = mHasBorder ? mX + mBorder.Thickness : mX;
         double maxX = (mHasBorder ? mX + mWidth - mBorder.Thickness : mX + mWidth) - mLastPowerMarker.Width;
         mLastPowerMarker.X = Math.Clamp(mLastPowerMarker.X, minX, maxX);
      }

      public override string ToString()
      {
         return $"{{ {nameof(Percentage)} = {Percentage}, {nameof(HasBorder)} = {HasBorder}, " +
                $"{nameof(Bounds)} = {Bounds} }}";
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         mBorder.X += aX;
         mBorder.Y += aY;

         foreach (Quad marker in mQuartileMarkers)
         {
            marker.X += aX;
            marker.Y += aY;
         }

         mLastPowerMarker.X += aX;
         mLastPowerMarker.Y += aY;

         mBar.X += aX;
         mBar.Y += aY;
      }

      private void Resize()
      {
         // If it's too soon to resize anything.
         if ((mWidth == 0.0) || (mHeight == 0.0))
         {
            return;
         }

         double x = mX;
         double width = mWidth;
         if (HasBorder == true)
         {
            mBorder.X = mX;
            mBorder.Y = mY;
            mBorder.Width = mWidth;
            mBorder.Height = mHeight;

            x = mX + mBorder.Thickness;
            width = mWidth - (2.0 * mBorder.Thickness);
         }

         mLastPowerMarker.Y = mY;
         mLastPowerMarker.Width = width / 100.0;
         if (mLastPowerMarker.Width < 5.0)
         {
            mLastPowerMarker.Width = 5.0;
         }
         mLastPowerMarker.Height = mHeight;

         for (int i = 0; i < mQuartileMarkers.Length; i++)
         {
            Quad marker = mQuartileMarkers[i];
            marker.X = x + (width / 4.0 * (i + 1));
            marker.Y = mY;
            marker.Width = mLastPowerMarker.Width;
            marker.Height = mHeight;
         }

         mBar.X = x;
         mBar.Y = mY;
         mBar.Height = mHeight;

         // Use the percentage setter to set the bar's width and the last power marker's X position.
         Percentage = mPercentage;
      }
   }
}
