﻿using Mjolnir.Client.Common.Disk;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using System;
using Mjolnir.Client.Common.Input;

namespace Mjolnir.Client.Common.UI
{
   /// <summary>
   /// A button made up of text and an optional border.
   /// This button uses one color for both the text and (optional) border.
   /// </summary>
   public class TextButton : ButtonBase
   {
      private readonly Configuration mConfiguration;
      private readonly Text mText;
      private Color mOriginalColor; // Remembers the color prior to a hover event.

      public override double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      private double mX = 0.0;

      public override double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      private double mY = 0.0;

      public override double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            Resize(false);
         }
      }
      private double mWidth = 0.0;

      public override double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            Resize(true);
         }
      }
      private double mHeight = 0.0;

      public override uint Padding
      {
         get => mHorizontalPadding;
         set
         {
            mHorizontalPadding = value;
            mVerticalPadding = value - 10;
            if (mVerticalPadding < 0)
            {
               mVerticalPadding = 0;
            }
         }
      }
      private uint mHorizontalPadding = 10;
      private uint mVerticalPadding = 0;

      public string Text
      {
         get => mText.Value;
         set
         {
            mText.Value = value;
            Resize(true);
         }
      }

      public Color Color
      {
         get => mColor;
         set
         {
            mColor = value;
            if (mIsEnabled == true)
            {
               mBorder.Color = value;
               mText.Color = value;
            }
            else
            {
               Color dulledColor = value.Dull();
               mBorder.Color = dulledColor;
               mText.Color = dulledColor;
            }
         }
      }
      private Color mColor;

      public bool IsEnabled
      {
         get => mIsEnabled;
         set
         {
            mIsEnabled = value;
            Color = mColor;
         }
      }
      private bool mIsEnabled = true;

      public TextButton(IServiceProvider aServiceProvider) : base(aServiceProvider)
      {
         mConfiguration = aServiceProvider.GetService<Configuration>();
         mText = aServiceProvider.GetService<Text>();

         HoverEvent += OnHover;
         UnhoverEvent += OnUnhover;

         Color = mConfiguration.PrimaryColor.ToColor(); // Make use of the setter to apply the default color.
         Height = UI.Text.DefaultHeight;
      }

      public override void Respond(InputEvent aInput)
      {
         if (mIsEnabled == true)
         {
            base.Respond(aInput);
         }
      }

      public override void Draw()
      {
         mText.Draw();

         base.Draw();
      }

      public override string ToString()
      {
         return $"{{ Text = \"{Text}\", Bounds = {Bounds} }}";
      }

      private void OnHover(object aSender, EventArgs aArgs)
      {
         mOriginalColor = mColor;
         if (aSender is TextButton button)
         {
            button.Color = mConfiguration.SecondaryColor.ToColor();
         }
      }

      private void OnUnhover(object aSender, EventArgs aArgs)
      {
         if (aSender is TextButton button)
         {
            button.Color = mOriginalColor;
         }
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         mBorder.X += aX;
         mBorder.Y += aY;

         mText.X += aX;
         mText.Y += aY;
      }

      private void Resize(bool aShouldPriotizeHeight)
      {
         // If it's too soon to resize anything.
         if ((mWidth == 0.0) && (mHeight == 0.0))
         {
            return;
         }

         if (aShouldPriotizeHeight == true)
         {
            // If the dimensions are being set based on a given height value.

            mText.Height = mHeight - (2 * mVerticalPadding);
            double minWidth = mText.Width + (2 * mHorizontalPadding);
            if (minWidth > mWidth)
            {
               mWidth = minWidth;
            }
         }
         else if (mText.Width > (mWidth - (2 * mHorizontalPadding)))
         {
            // If dimensions are being set based on a given width value AND the text's width
            // is greater than it's allowed to be.

            mText.Width = mWidth - (2 * mHorizontalPadding);
            mHeight = mText.Height + (2 * mVerticalPadding);
         }
         mText.X = mX + (mWidth / 2.0) - (mText.Width / 2.0);
         mText.Y = mY + mVerticalPadding;

         mBorder.X = mX;
         mBorder.Y = mY;
         mBorder.Width = mWidth;
         mBorder.Height = mHeight;
      }
   }
}
