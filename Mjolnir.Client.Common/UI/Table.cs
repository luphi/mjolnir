﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Client.Common.Input;
using Mjolnir.Common.Collections;
using Mjolnir.Common.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mjolnir.Client.Common.UI
{
   public class Table : IControl, IVisual
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly Scrollbar mScrollbar;
      private readonly Quad mHighlight;
      private readonly ConcurrentList<List<string>> mRowsStrings = new ConcurrentList<List<string>>(); // Contents of the rows as strings.
      private readonly ConcurrentList<List<Text>> mRowsVisuals = new ConcurrentList<List<Text>>();// Contents of the rows as Texts.
      private readonly ConcurrentList<int> mVisibleRows = new ConcurrentList<int>(); // Indexes in the above list to be drawn.
      private readonly ConcurrentList<Text> mHeaderVisuals = new ConcurrentList<Text>();
      private uint mRowHeight = 0;
      private int mHighlightedRow = -1;
      private int mOrderedByColumn = -1;
      private bool mIsSwiping = false;

      public double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      private double mX = 0.0;

      public double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      private double mY = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            Resize();
         }
      }
      private double mWidth = 0.0;

      public double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            Resize();
         }
      }
      private double mHeight = 0.0;

      public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

      public Rectangle Bounds => new Rectangle((int)mX, (int)mY, (int)mWidth, (int)mHeight);

      private Rectangle HeaderBounds
      {
         get
         {
            // If no header(s) is/are defined.
            if (mHeaders.Count == 0)
            {
               // Return header bounds with a height of zero, effectively meaning there is no header.
               return new Rectangle((int)(X + mPadding),
                                    (int)(Y + mPadding),
                                    (int)(mWidth - (3 * mPadding) - mScrollbar.Width),
                                    0);
            }

            // If there is/are header(s), return a 
            return new Rectangle((int)(X + mPadding),
                                 (int)(Y + mPadding),
                                 (int)(mWidth - (3 * mPadding) - mScrollbar.Width),
                                 (int)Text.DefaultHeight);

         }
      }

      private Rectangle ContentBounds
      {
         get
         {
            Rectangle headerBounds = HeaderBounds;
            return new Rectangle((int)(X + mPadding),
                                 (int)(Y + mPadding + headerBounds.Height),
                                 (int)(mWidth - (3 * mPadding) - mScrollbar.Width),
                                 (int)(mHeight - (2 * mPadding) - headerBounds.Height));
         }
      }

      public uint Padding
      {
         get => mPadding;
         set
         {
            mPadding = value;
            Resize();
         }
      }
      private uint mPadding = 5; // Pixels.


      public List<string> Headers
      {
         get => mHeaders;
         set
         {
            mHeaders = value;

            // If no weights have been given, default to uniform column widths.
            if (mWeights.Count == 0)
            {
               for (int i = 0; i < value.Count; i++)
               {
                  mWeights.Add(1);
               }
            }

            mHeaderVisuals.Clear();
            foreach (string header in value)
            {
               Text text = mServiceProvider.GetService<Text>();
               text.Value = header;
               mHeaderVisuals.Add(text);
            }

            Resize();
         }
      }
      private List<string> mHeaders = new List<string>();

      /// <summary>
      /// A list of column weights. The number of weights given determines the number of
      /// columns. The weights determine the distriution of the width given to each column.
      /// For example, if given the weights { 3, 1, 1 }, the first column will be given 3/5
      /// of the total width and the other two will be given 1/5 each.
      /// </summary>
      public List<int> Weights
      {
         get => mWeights;
         set
         {
            mWeights = value;
            Resize();
         }
      }
      private List<int> mWeights = new List<int>();

      /// <summary>
      /// Maximum number of rows visible at any one time. Determines height of each row and,
      /// therefore, text size.
      /// </summary>
      public uint VisibleRows
      {
         get => mNumVisibleRows;
         set
         {
            mNumVisibleRows = value;
            Resize();
         }
      }
      private uint mNumVisibleRows = 25;

      // TODO "scrollbar hidden when no overflow" flag

      public event EventHandler<string> SelectEvent;

      public Table(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mScrollbar = aServiceProvider.GetService<Scrollbar>();
         mHighlight = aServiceProvider.GetService<Quad>();
         mHighlight.Color = aServiceProvider.GetService<Configuration>().SecondaryColor.ToColor().Dull();
         mScrollbar.ScrollEvent += OnScroll;
      }

      public void AddRows(List<List<string>> aContents)
      {
         foreach (List<string> row in aContents)
         {
            List<Text> drawableRow = new List<Text>();
            int column = 0;
            for (int i = 1; i < row.Count; i++)
            {
               // Create the Text and add it to the row.
               Text text = mServiceProvider.GetService<Text>();
               text.Value = row[i];
               drawableRow.Add(text);
               column += 1;
            }

            // Add the new content and row to their respective lists.
            mRowsStrings.Add(row);
            mRowsVisuals.Add(drawableRow);
         }

         // Resize (everything) to place the new Texts in the appropriate places with appropriate sizes.
         Resize();
         Cull();
      }

      public void Clear()
      {
         mRowsStrings.Clear();
         mRowsVisuals.Clear();
         mVisibleRows.Clear();

         mScrollbar.ScrollableHeight = ContentBounds.Height;
         mHighlightedRow = -1;
         mOrderedByColumn = -1;
      }

      public void Draw()
      {
         mScrollbar.Draw();

         // If there is a header, draw it.
         if (mHeaders.Count > 0)
         {
            foreach (Text header in mHeaderVisuals)
            {
               header.Draw();
            }
         }

         // Draw the rows.
         foreach (int index in mVisibleRows)
         {
            // If this row is being highlighted (i.e. it was selected).
            if (mHighlightedRow == index)
            {
               // Draw the highlight behind the row's text.
               mHighlight.Y = mRowsVisuals[index][0].Y;
               mHighlight.Draw();
            }

            // Draw the row's text(s).
            foreach (Text text in mRowsVisuals[index])
            {
               text.Draw();
            }
         }
      }

      public void Respond(InputEvent aInput)
      {
         mScrollbar.Respond(aInput);

         if (aInput.Op == InputEvent.Opcode.MOUSE_DOWN)
         {
            if (MjolnirGame.IsDesktop == false)
            {
               // If not running on a desktop with a mouse, and therefore running on a mobile device.

               // Spawn a task that will wait very briefly and check to see if the mouse down event
               // resulted in a swipe. We do this to avoid digging holes unnecessarily as swiping
               // on a touch device is always preceded by a mouse down (finger press) event.
               Task.Run(async () =>
               {
                  await Task.Delay(100);
                  // If not swiping.
                  if (mIsSwiping == false)
                  {
                     // Perform the click after an unnoticeably small delay. 
                     OnClick(aInput.Position);
                  }
               });
            }
            else
            {
               // If running on a desktop with no expectation of any subsequent swipe events.

               // Immediately perform the click.
               OnClick(aInput.Position);
            }
         }
         else if (aInput.Op == InputEvent.Opcode.SWIPE)
         {
            mIsSwiping = true;
         }
         else if (aInput.Op == InputEvent.Opcode.MOUSE_UP)
         {
            mIsSwiping = false;
         }
      }

      private void OnClick(Vector aPosition)
      {
         Rectangle headerBounds = HeaderBounds;
         Rectangle listBounds = ContentBounds;

         if ((mHeaders.Count > 0) && (headerBounds.Contains(aPosition.ToXna()) == true))
         {
            // If the click occurred within the/a header.

            // Sort the rows by the values under the clicked header.
            // Determine which header was clicked on.
            int index = 0;

            // Since header texts are left-justified, we'll be able to know which header
            // was clicked by comparing the cursor's X against the header's X.
            for (int i = 0; i < mHeaderVisuals.Count; i++)
            {
               if (aPosition.X < mHeaderVisuals[i].X)
               {
                  break;
               }

               index = i;
            }

            // Make a copy of the rows for sorting in the next step.
            // And, while we're at it, remember the key of the highlighted row
            // if a row is highlighted.
            List<List<string>> newRows;
            string highlightedKey = null;
            if ((mHighlightedRow >= 0) && (mRowsStrings[mHighlightedRow].Count > 0))
            {
               // Remember the highlighted key in order to find its new index
               // after replacing the rows with the sort ones.
               highlightedKey = mRowsStrings[mHighlightedRow][0];
            }
            newRows = new List<List<string>>(mRowsStrings);

            // Quick bounds check.
            if (index + 1 < newRows.Count)
            {
               // Sort by the values of the selected index.
               // Note that it's "index + 1" because the content rows set aside their first
               // index for a key (which is not displayed).

               if (mOrderedByColumn == index)
               {
                  // If the list is already ordered (ascending) by this column.

                  newRows = newRows.OrderByDescending(r => r[index + 1]).ToList();
                  index = -1; // Reassign index so mOrderedRow gets (re)set to -1 later.
               }
               else
               {
                  newRows = newRows.OrderBy(r => r[index + 1]).ToList();
               }

               Clear();
               AddRows(newRows);
               // Note: FindIndex() returns -1 if the item isn't found. Conveniently, -1
               // is the value used by this class to indicate "not set."
               mHighlightedRow = newRows.FindIndex(r => r[0] == highlightedKey);
               mOrderedByColumn = index;
            }
         }
         else if (listBounds.Contains(aPosition.ToXna()) == true)
         {
            // If the click occurred somewhere within the list, likely over a row.

            for (int i = 0; i < mVisibleRows.Count; i++)
            {
               int index = mVisibleRows[i];
               Rectangle rowBounds = new Rectangle(listBounds.X,
                                                   (int)mRowsVisuals[index][0].Y,
                                                   listBounds.Width,
                                                   (int)mRowHeight);

               if (rowBounds.Contains(aPosition.ToXna()) == true)
               {
                  mHighlightedRow = index;
                  try
                  {
                     SelectEvent?.Invoke(this, mRowsStrings[index].Count > 0 ? mRowsStrings[index][0] : string.Empty);
                  }
                  catch (Exception e)
                  {
                     mLog.Error(e, "Excpetion thrown while propagating a select event");
                  }
                  break;
               }
            }
         }
      }

      private void OnScroll(object aSender, Scrollbar.ScrollDetails aDetails)
      {
         if (aDetails.IsTopClamped == true)
         {
            Rectangle listBounds = ContentBounds;
            for (int i = 0; i < mRowsVisuals.Count; i++)
            {
               foreach (Text content in mRowsVisuals[i])
               {
                  content.Y = listBounds.Y + (content.Height * i);
               }
            }
            Cull();
         }
         else if (aDetails.IsBottomClamped == true)
         {
            Rectangle listBounds = ContentBounds;
            for (int i = mRowsVisuals.Count - 1; i >= 0; i--)
            {
               foreach (Text content in mRowsVisuals[i])
               {
                  content.Y = listBounds.Y + listBounds.Height - (content.Height * (mRowsVisuals.Count - i));
               }
            }
            Cull();
         }
         else
         {
            TranslateContent(0.0, -aDetails.Scroll);
         }
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         mScrollbar.X += aX;
         mScrollbar.Y += aY;
         TranslateContent(aX, aY);
      }

      private void TranslateContent(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         foreach (List<Text> row in mRowsVisuals)
         {
            foreach (Text text in row)
            {
               text.X += aX;
               text.Y += aY;
            }
         }
         Cull();
      }

      private void Resize()
      {
         // If it's too soon to resize anything.
         if ((mWidth == 0.0) || (mHeight == 0.0))
         {
            return;
         }

         mRowHeight = ((uint)mHeight - (2 * mPadding)) / (mNumVisibleRows + 1);

         Rectangle headerBounds = HeaderBounds;
         Rectangle listBounds = ContentBounds;
         mScrollbar.Height = mHeight - (2 * mPadding);
         mScrollbar.OverflowableBounds = listBounds;
         double totalScroll = (float)listBounds.Height / mNumVisibleRows * mRowsVisuals.Count;
         if (totalScroll <= mScrollbar.OverflowableBounds.Height)
         {
            mScrollbar.ScrollableHeight = mScrollbar.OverflowableBounds.Height;
         }
         else
         {
            mScrollbar.ScrollableHeight = totalScroll;
         }
         mScrollbar.X = mX + mWidth - mScrollbar.Width - Padding;
         mScrollbar.Y = mY + Padding;

         // Resize the row highlight. This will be displayed only if a row is selected.
         // Its Y coordinate will be set at draw time.
         mHighlight.X = listBounds.X;
         mHighlight.Width = listBounds.Width;
         mHighlight.Height = mRowHeight;

         // Calculate column widths.
         List<int> columnWidthsInPixels = new List<int>();
         int weightSum = mWeights.Sum();
         foreach (int weight in mWeights)
         {
            columnWidthsInPixels.Add((int)((double)weight / weightSum * listBounds.Width));
         }

         // Apply column widths.
         int columnIndex = 0;
         int widthSum = 0;

         foreach (Text header in mHeaderVisuals)
         {
            header.Height = headerBounds.Height;
            int columnWidth = columnWidthsInPixels[columnIndex];
            if (header.Width > columnWidth)
            {
               header.Truncate((uint)columnWidth);
            }
            header.X = headerBounds.X + widthSum;
            header.Y = headerBounds.Y;
            columnIndex += 1;
            widthSum += columnWidth;
         }

         int rowIndex = 0;
         foreach (List<Text> row in mRowsVisuals)
         {
            columnIndex = 0;
            widthSum = 0;
            foreach (Text text in row)
            {
               text.Height = mRowHeight;
               int columnWidth = columnWidthsInPixels[columnIndex];
               if (text.Width > columnWidth)
               {
                  text.Truncate((uint)columnWidth);
               }
               text.X = listBounds.X + widthSum;
               text.Y = listBounds.Y + (rowIndex * mRowHeight);
               columnIndex += 1;
               widthSum += columnWidth;
            }
            rowIndex += 1;
         }
      }

      private void Cull()
      {
         Rectangle listBounds = ContentBounds;

         mVisibleRows.Clear();
         for (int i = 0; i < mRowsVisuals.Count; i++)
         {
            if ((mRowsVisuals[i].Count > 0) && (listBounds.Contains(mRowsVisuals[i][0].Bounds) == true))
            {
               mVisibleRows.Add(i);
            }
         }
      }
   }
}
