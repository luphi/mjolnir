﻿using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using NLog;
using System;

namespace Mjolnir.Client.Common.UI
{
   /// <summary>
   /// A visible image, loaded from the content manager.
   /// </summary>
   public class Image : IVisual
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly SpriteBatch mSpriteBatch;
      private readonly ContentManager mContentManager;
      private Texture2D mTexture = null;

      public double X { get; set; } = 0.0;

      public double Y { get; set; } = 0.0;

      public double Width { get; set; } = 0.0;

      public double Height { get; set; } = 0.0;

      /// <summary>
      /// The aspect ratio (width / height) of the loaded image. If no image has been
      /// loaded yet, this is just one.
      /// </summary>
      public double AspectRatio
      {
         get
         {
            if (mTexture != null)
            {
               return (double)mTexture.Width / mTexture.Height;
            }
            else if (Height != 0.0)
            {
               return Width / Height;
            }
            else
            {
               return 1.0;
            }
         }
      }

      public Rectangle Bounds => new Rectangle((int)X, (int)Y, (int)Width, (int)Height);

      public uint NativeWidth => (uint)(mTexture?.Width ?? 0);

      public uint NativeHeight => (uint)(mTexture?.Height ?? 0);

      /// <summary>
      /// The name of the image to be loaded through the content manager.
      /// As this wraps MonoGame's ContentManager, this file name does not include the
      /// file extension but does include the path (if not in the root directory).
      /// </summary>
      public string Content
      {
         get => mContent;
         set
         {
            // If the image is being set to nothing (i.e. do not display anything).
            if (string.IsNullOrEmpty(value) == true)
            {
               mContent = null;
               mTexture = null;
            }
            // If the image is being set to something that should exist.
            else
            {
               try
               {
                  mContent = value;
                  mTexture = mContentManager.Load<Texture2D>(mContent);
               }
               catch (Exception e)
               {
                  mLog.Warn(e, $"Exception thrown while loading image \"{value}\"");
                  mTexture = null;
                  mContent = null;
               }
            }
         }
      }
      private string mContent = null;

      public bool IsMirrored { get; set; } = false;

      public Image(IServiceProvider aServiceProvider)
      {
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mContentManager = aServiceProvider.GetService<ContentManager>();
      }

      public void Draw()
      {
         if (mTexture != null)
         {
            // Prepare position and origin vectors. The position is the (virtual) screen
            // coordinate corresponding to the top-left corner of surface being drawn here.
            Vector2 position = new Vector2((float)X, (float)Y);
            // Calculate the scale relative to the original bitmap.
            float scale = (float)(Width / mTexture.Width);
            mSpriteBatch.Draw(
                // The bitmap source from which we'll draw a small portion.
                mTexture,
                // The position to draw at.
                position,
                // Region in the bitmap to be drawn. (null draws the full texture.)
                null,
                // Color mask.
                Color.White,
                // Rotation, in radians, to be applied.
                0f,
                // The point, relative to the drawn position, around which rotation happens.
                Vector2.Zero,
                // Scale factor. Determines the drawn dimensions.
                scale,
                // Sprite effects (none, horizontal flip, or vertical flip).
                IsMirrored ? SpriteEffects.FlipHorizontally : SpriteEffects.None,
                // Layer depth.
                1.0f);
         }
      }

      public override string ToString()
      {
         return $"{{ {nameof(Content)} = {(Content == null ? "null" : $"\"{Content}\"")}, " +
                $"{nameof(NativeWidth)} = {NativeWidth}, {nameof(NativeHeight)} = {NativeHeight}, " +
                $"{nameof(IsMirrored)} = {IsMirrored}, {nameof(Bounds)} = {Bounds} }}";
      }
   }
}
