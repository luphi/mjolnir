﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Mjolnir.Common.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using NLog;
using System;
using Mjolnir.Client.Common.Input;

namespace Mjolnir.Client.Common.UI
{
   public class Scrollbar : IControl, IVisual
   {
      public class ScrollDetails
      {
         public double Scroll = 0.0;
         public bool IsTopClamped = false;
         public bool IsBottomClamped = false;
      }

      private const uint BORDER_THICKNESS = 1; // Pixels.
      private const uint TRACK_PADDING = 1; // Pixels.

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly Quad mBorder;
      private readonly Quad mTrack;
      private readonly Quad mThumb;

      //private bool mIsScrolling = false;
      //private Vector mPreviousCursorPosition = Vector.Zero;
      //private int mPreviousScrollWheelValue = 0;
      //private bool mIsMousePressed = false;

      private Vector mMousePosition = Vector.Zero; // The last-known position of the mouse/cursor.
      private bool mIsMouseHeld = false; // Simply whether or not the left mouse button is currently pressed.
      private bool mIsDragging = false; // True when the user clicked on the thumb in order to drag it.

      private Rectangle ThumbBounds => new Rectangle((int)(mX + BORDER_THICKNESS + TRACK_PADDING),
                                                     (int)(mY + BORDER_THICKNESS + TRACK_PADDING),
                                                     (int)(mWidth - (2 * BORDER_THICKNESS) - (2 * TRACK_PADDING)),
                                                     (int)(mHeight - (2 * BORDER_THICKNESS) - (2 * TRACK_PADDING)));

      public double X
      {
         get => mX;
         set
         {
            mX = value;
            Reposition();
         }
      }
      private double mX = 0.0;

      public double Y
      {
         get => mY;
         set
         {
            mY = value;
            Reposition();
         }
      }
      private double mY = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            Resize();
         }
      }
      private double mWidth = 0.0;

      public double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            Resize();
         }
      }
      private double mHeight = 0.0;

      public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

      public Rectangle Bounds => new Rectangle((int)mX, (int)mY, (int)mWidth, (int)mHeight);

      /// <summary>
      /// Bounds of the visible area that this scrollbar applies to. This area should be
      /// "overflow-able" in the sense that not all content within it is gauranteed to fit
      /// and some may overflow and be hidden from view.
      /// This property must be set by the scrollable UI component this scrollbar is used by.
      /// </summary>
      public Rectangle OverflowableBounds
      {
         get => mOverflowableBounds;
         set
         {
            mOverflowableBounds = value;
            Resize();
         }
      }
      private Rectangle mOverflowableBounds = Rectangle.Empty;

      /// <summary>
      /// The total height of the content, seen and hidden, to which scrolling applies.
      /// In other words, this is the height of the OverflowableBounds plus the hidden,
      /// overflowed content.
      /// This property must be set by the scrollable UI component this scrollbar is used by.
      /// </summary>
      public double ScrollableHeight
      {
         get => mScrollableHeight;
         set
         {
            mScrollableHeight = value;
            Resize();
         }
      }
      private double mScrollableHeight = 0.0;

      public event EventHandler<ScrollDetails> ScrollEvent;

      public Scrollbar(IServiceProvider aServiceProvider)
      {
         mBorder = aServiceProvider.GetService<Quad>();
         mTrack = aServiceProvider.GetService<Quad>();
         mThumb = aServiceProvider.GetService<Quad>();

         ResolutionIndependentViewport viewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mWidth = viewport.VirtualWidth * 0.02;

         mBorder.Color = aServiceProvider.GetService<Configuration>().PrimaryColor.ToColor();
         mTrack.Color = Color.Black;
         mThumb.Color = mBorder.Color;
      }

      public void Draw()
      {
         mBorder.Draw();
         mTrack.Draw();
         mThumb.Draw();
      }

      public void Respond(InputEvent aInput)
      {
         Vector previousMousePosition = mMousePosition;
         double scrollDistance = 0.0;

         // If the mouse was moved.
         if (aInput.Op == InputEvent.Opcode.HOVER)
         {
            // Track the mouse's position. This information is used for more than one possible action.
            mMousePosition = aInput.Position;
         }

         // If the left mouse button was pressed or released.
         if ((aInput.Op == InputEvent.Opcode.MOUSE_DOWN) || (aInput.Op == InputEvent.Opcode.MOUSE_UP))
         {
            // Track the mouse's position, determine if the left mouse button is currently pressed,
            // and use both variables to determine if the user is now dragging the thumb.
            mMousePosition = aInput.Position;
            mIsMouseHeld = aInput.Op == InputEvent.Opcode.MOUSE_DOWN;
            mIsDragging = mIsMouseHeld && ThumbBounds.Contains(mMousePosition.ToXna());
         }

         // If the mouse is currently hovering over the overflowable (content) bounds or this scrollbar's bounds.
         if ((OverflowableBounds.Contains(mMousePosition.ToXna()) == true) ||
             (Bounds.Contains(mMousePosition.ToXna()) == true))
         {
            if (aInput.Op == InputEvent.Opcode.SCROLL)
            {
               // If the scroll wheel was turned

               // Some context on the scroll amount being calculated:
               // Monogame's ScrollWheelValue (of MouseState) holds a summed value of scroll wheel movement
               // since the Game started. The value doesn't seem to be anything meaningful but it appears one
               // detent, with default system settings, corresponds to the value 120.
               // The change in value between input polling is not constant but will be a multiple of that
               // single-detent value (i.e. three detents since the last poll will result in the value changing
               // by 360). Each detent will trigger a scroll equal to OverflowableBounds.Height / 10, or 10% of
               // it. 10% was chosen arbitrarily.
               scrollDistance = aInput.Delta.Y * (OverflowableBounds.Height / (120.0 * 10.0));
            }
            else if (aInput.Op == InputEvent.Opcode.SWIPE)
            {
               // If the user performed a swipe on a touchscreen

               // Scroll by the distance the user swiped in the direction he/she swiped.
               scrollDistance = -aInput.Delta.Y;
               mIsDragging = true;
            }
         }

         // If the user is dragging the thumb.
         if ((aInput.Op == InputEvent.Opcode.HOVER) && mIsDragging)
         {
            // Set the scroll distance equal to the Y distance the mouse has moved.
            scrollDistance = (mMousePosition - previousMousePosition).Y;
         }

         // If a scroll event should be triggered and the thumb moved.
         if (scrollDistance != 0.0)
         {
            // Calculate the percentage the thumb should be moved or has been dragged.
            double minY = mY + BORDER_THICKNESS + TRACK_PADDING;
            double maxY = minY + (mHeight - (2 * BORDER_THICKNESS) - (2 * TRACK_PADDING) - mThumb.Height);
            double originalY = mThumb.Y;
            ScrollDetails details = new ScrollDetails();
            mThumb.Y += scrollDistance;
            if (mThumb.Y < minY)
            {
               // If the user scrolled to the very top.

               mThumb.Y = minY;
               details.IsTopClamped = true;
            }
            else if (mThumb.Y > maxY)
            {
               // If the user scrolled to the very bottom.

               mThumb.Y = maxY;
               details.IsBottomClamped = true;
            }

            // With clamping done, calculate the final scroll amount in (virtual) pixels.
            details.Scroll = (mScrollableHeight - mOverflowableBounds.Height) *
                             (mThumb.Y - originalY) / (maxY - minY);
            if (details.Scroll != 0.0)
            {
               try
               {
                  ScrollEvent?.Invoke(this, details);
               }
               catch (Exception e)
               {
                  mLog.Error(e, "Exception thrown while propagating a scroll event");
               }
            }
         }
      }

      private void Reposition()
      {
         mBorder.X = mX;
         mBorder.Y = mY;

         mTrack.X = mX + BORDER_THICKNESS;
         mTrack.Y = mY + BORDER_THICKNESS;

         mThumb.X = mX + BORDER_THICKNESS + TRACK_PADDING;
         mThumb.Y = mY + BORDER_THICKNESS + TRACK_PADDING;
      }

      private void Resize()
      {
         mBorder.Width = mWidth;
         mBorder.Height = mHeight;

         mTrack.Width = mWidth - (2 * BORDER_THICKNESS);
         mTrack.Height = mHeight - (2 * BORDER_THICKNESS);

         mThumb.Width = mWidth - (2 * BORDER_THICKNESS) - (2 * TRACK_PADDING);
         double fullThumbHeight = mHeight - (2 * BORDER_THICKNESS) - (2 * TRACK_PADDING);
         mThumb.Height = fullThumbHeight * OverflowableBounds.Height / ScrollableHeight;
      }
   }
}
