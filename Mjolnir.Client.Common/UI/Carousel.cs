﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using NLog;
using System;
using System.Collections.Generic;
using Mjolnir.Client.Common.Input;

namespace Mjolnir.Client.Common.UI
{
   public class Carousel : IControl, IVisual
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly Configuration mConfiguration;
      private readonly TextButton mLeftArrow;
      private readonly TextButton mRightArrow;
      private readonly List<Text> mEntryTexts = new List<Text>();
      private int mIndex = 0;

      public double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      protected double mX = 0.0;

      public double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      protected double mY = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            Resize();
         }
      }
      protected double mWidth = 0.0;

      public double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            Resize();
         }
      }
      protected double mHeight;

      public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

      public Rectangle Bounds => new Rectangle((int)mX, (int)mY, (int)mWidth, (int)mHeight);

      public List<string> Entries
      {
         get => mEntries;
         set
         {
            lock (mEntries)
            {
               mEntries = value;
            }

            lock (mEntryTexts)
            {
               mEntryTexts.Clear();
               foreach (string entry in value)
               {
                  Text text = mServiceProvider.GetService<Text>();
                  text.Value = entry;
                  if (mIsEnabled)
                  {
                     text.Color = mColor;
                  }
                  else
                  {
                     text.Color = mColor.Dull();
                  }

                  mEntryTexts.Add(text);
               }
            }
            
            mIndex = 0;
            Resize();
         }
      }
      private List<string> mEntries = new List<string>();

      public string Selected
      {
         get => mSelected;
         set
         {
            lock (mEntries)
            {
               int index = mEntries.IndexOf(value);
               if (index >= 0)
               {
                  mIndex = index;
                  mSelected = mEntries[index];
               }
            }
         }
      }
      private string mSelected = null;

      public Color Color
      {
         get => mColor;
         set
         {
            mColor = value;
            Color textColor;

            if (mIsEnabled)
            {
               mLeftArrow.Color = mConfiguration.PrimaryColor.ToColor();
               mRightArrow.Color = mLeftArrow.Color;

               textColor = value;
            }
            else
            {
               Color washedArrowColor = mConfiguration.PrimaryColor.ToColor().Dull();
               mLeftArrow.Color = washedArrowColor;
               mRightArrow.Color = washedArrowColor;
               textColor = value.Dull();
            }

            lock (mEntryTexts)
            {
               foreach (Text entry in mEntryTexts)
               {
                  entry.Color = textColor;
               }
            }
         }
      }
      private Color mColor;

      public bool IsEnabled
      {
         get => mIsEnabled;
         set
         {
            mIsEnabled = value;
            Color = mColor;
         }
      }
      private bool mIsEnabled = true;

      public event EventHandler<string> SwivelEvent;

      public Carousel(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mConfiguration = aServiceProvider.GetService<Configuration>();
         mLeftArrow = aServiceProvider.GetService<TextButton>();
         mRightArrow = aServiceProvider.GetService<TextButton>();

         mLeftArrow.Text = "<";
         mLeftArrow.ClickEvent += OnLeftArrowClick;

         mRightArrow.Text = ">";
         mRightArrow.ClickEvent += OnRightArrowClick;

         Color = mConfiguration.PrimaryColor.ToColor();
         Height = Text.DefaultHeight;
      }

      public void Respond(InputEvent aInput)
      {
         if (mIsEnabled == true)
         {
            mLeftArrow.Respond(aInput);
            mRightArrow.Respond(aInput);
         }
      }

      public void Draw()
      {
         mLeftArrow.Draw();
         mRightArrow.Draw();

         lock (mEntryTexts)
         {
            if ((mIndex >= 0) && (mIndex < mEntryTexts.Count))
            {
               mEntryTexts[mIndex].Draw();
            }
         }
      }

      private void OnLeftArrowClick(object aSender, EventArgs aArgs)
      {
         string entry = string.Empty;

         lock (mEntries)
         {
            mIndex -= 1;
            if (mIndex < 0)
            {
               mIndex = mEntries.Count - 1;
            }

            if (mEntries.Count > 0)
            {
               entry = mEntries[mIndex];
            }
         }

         if (string.IsNullOrEmpty(entry) == false)
         {
            try
            {
               SwivelEvent?.Invoke(this, entry);
            }
            catch (Exception e)
            {
               mLog.Error(e, "Exception thrown while propagating a swivel event");
            }
         }
      }

      private void OnRightArrowClick(object aSender, EventArgs aArgs)
      {
         string entry = string.Empty;

         lock (mEntries)
         {
            mIndex += 1;
            if (mIndex > mEntries.Count - 1)
            {
               mIndex = 0;
            }

            if (mEntries.Count > 0)
            {
               entry = mEntries[mIndex];
            }
         }

         if (string.IsNullOrEmpty(entry) == false)
         {
            try
            {
               SwivelEvent?.Invoke(this, entry);
            }
            catch (Exception e)
            {
               mLog.Error(e, "Exception thrown while propagating a swivel event");
            }
         }
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         mLeftArrow.X += aX;
         mLeftArrow.Y += aY;

         mRightArrow.X += aX;
         mRightArrow.Y += aY;

         lock (mEntryTexts)
         {
            foreach (Text entry in mEntryTexts)
            {
               entry.X += aX;
               entry.Y += aY;
            }
         }
      }

      private void Resize()
      {
         // If it's too soon to resize anything.
         if ((mWidth == 0.0) || (mHeight == 0.0))
         {
            return;
         }

         mLeftArrow.Height = mHeight;
         mLeftArrow.X = mX;
         mLeftArrow.Y = mY;

         mRightArrow.Height = mHeight;
         mRightArrow.X = mX + mWidth - mRightArrow.Width;
         mRightArrow.Y = mY;

         double centerX = mX + (mWidth / 2.0);
         double centerY = mY + (mHeight / 2.0);
         double widthBetweenArrows = mRightArrow.X - mLeftArrow.X - mLeftArrow.Width;
         lock (mEntryTexts)
         {
            foreach (Text entry in mEntryTexts)
            {
               entry.Height = mHeight;
               if (entry.Width > widthBetweenArrows)
               {
                  entry.Width = widthBetweenArrows;
               }
               entry.X = centerX - (entry.Width / 2.0);
               entry.Y = centerY - (entry.Height / 2.0);
            }
         }
      }
   }
}
