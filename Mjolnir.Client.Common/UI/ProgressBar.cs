﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using NLog;
using System;

namespace Mjolnir.Client.Common.UI
{
   public class ProgressBar : IVisual
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly Border mBorder;
      private readonly Quad mBar;

      public double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      protected double mX = 0.0;

      public double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      protected double mY = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            Resize();
         }
      }
      protected double mWidth = 0.0;

      public double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            Resize();
         }
      }
      protected double mHeight = 0.0;

      public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

      public Rectangle Bounds => new Rectangle((int)mX, (int)mY, (int)mWidth, (int)mHeight);

      public Color BarColor
      {
         get => mBar.Color;
         set
         {
            mBar.Color = value;
         }
      }

      public Color BorderColor
      {
         get => mBorder.Color;
         set
         {
            mBorder.Color = value;
         }
      }

      /// <summary>
      /// Percentage to be displayed by the progress bar, ranging from 0.0 to 1.0.
      /// Any other value will be clamped to this range.
      /// </summary>
      public double Percentage
      {
         get => mPercentage;
         set
         {
            mPercentage = Math.Clamp(value, 0.0, 1.0);

            if (mWidth > 0.0)
            {
               mBar.Width = mPercentage * (mWidth - (2 * mBorder.Thickness));
            }
         }
      }
      private double mPercentage = 1.0;

      public ProgressBar(IServiceProvider aServiceProvider)
      {
         mBorder = aServiceProvider.GetService<Border>();
         mBar = aServiceProvider.GetService<Quad>();

         Configuration configuration = aServiceProvider.GetService<Configuration>();
         mBorder.Color = configuration.PrimaryColor.ToColor();
         mBar.Color = configuration.SecondaryColor.ToColor();
      }

      public void Draw()
      {
         mBar.Draw();
         mBorder.Draw();
      }

      public override string ToString()
      {
         return $"{{ {nameof(Percentage)} = {Percentage}, {nameof(Bounds)} = {Bounds} }}";
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         mBorder.X += aX;
         mBorder.Y += aY;

         mBar.X += aX;
         mBar.Y += aY;
      }

      private void Resize()
      {
         // If it's too soon to resize anything.
         if ((mWidth == 0.0) || (mHeight == 0.0))
         {
            return;
         }

         mBorder.X = mX;
         mBorder.Y = mY;
         mBorder.Width = mWidth;
         mBorder.Height = mHeight;

         mBar.X = mX + mBorder.Thickness;
         mBar.Y = mY;
         Percentage = mPercentage; // Sets the bar's width based on the current percentage.
         mBar.Height = mHeight;
      }
   }
}
