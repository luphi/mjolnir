﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using NLog;
using System;
using System.Threading;
using System.Threading.Tasks;
using Mjolnir.Client.Common.Input;
using Microsoft.Xna.Framework.Input;

namespace Mjolnir.Client.Common.UI
{
   /// <summary>
   /// Text or keyboard input widget. Allows a user to input and edit a string.
   /// 
   /// This is an incomplete class. Important parts of it are left to be implemented
   /// by child classes that must be platform-specific.
   /// </summary>
   public class TextInput : IControl, IVisual
   {
      protected readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly Configuration mConfiguration;
      private readonly Quad mBackground;
      protected readonly Quad mCursor;
      protected readonly Text mText;
      private readonly Border mBorder;
      protected int mCursorIndex = 0;
      protected bool mIsMouseInBounds = false;
      protected bool mShouldDrawCursor = false;
      private CancellationTokenSource mTokenSource = null;
      private Color mOriginalColor; // Remembers the color prior to a hover event.

      public double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      protected double mX = 0.0;

      public double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      protected double mY = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            Resize();
         }
      }
      protected double mWidth = 0.0;

      public double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            Resize();
         }
      }
      protected double mHeight;

      public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

      public Rectangle Bounds => new Rectangle((int)mX, (int)mY, (int)mWidth, (int)mHeight);

      public uint Padding
      {
         get => mPadding;
         set
         {
            mPadding = value;
            Resize();
         }
      }
      protected uint mPadding = 3; // Pixels.

      /// <summary>
      /// The string that has been input by the user.
      /// </summary>
      public string Value
      {
         get => mText.Value;
         set
         {
            mText.Value = value;
            mCursorIndex = 0;
            PositionCursor();
         }
      }

      /// <summary>
      /// Color to be used for the text and cursor. If a background is enabled, the
      /// background color will be the inverse color (e.g. white text -> black background).
      /// </summary>
      public Color Color
      {
         get => mColor;
         set
         {
            mColor = value;
            mText.Color = value;
            mCursor.Color = value;
            mBorder.Color = value;
            Color inverted = new Color
            {
               R = (byte)(255 - value.R),
               G = (byte)(255 - value.G),
               B = (byte)(255 - value.B),
               A = value.A
            };
            mBackground.Color = inverted;
         }
      }
      private Color mColor;

      /// <summary>
      /// Flag to set in order to indicate if the user is focusing on this object. If
      /// focused, text input is effectively enabled along with the flashing cursor.
      /// </summary>
      public virtual bool IsFocused
      {
         get => mIsFocused;
         set
         {
            if (mIsFocused == value)
            {
               return;
            }

            mIsFocused = value;
            if (value == true)
            {
               StartCursorTask();
            }
            else
            {
               StopCursorTask();
            }
         }
      }
      protected bool mIsFocused = false;

      /// <summary>
      /// Flag to enable or disable the background. If true, the background will be drawn.
      /// </summary>
      public bool HasBackground { get; set; } = false;

      /// <summary>
      /// Flag to enable or disable the border. If true, the border will be drawn.
      /// </summary>
      public bool HasBorder { get; set; } = true;

      public event EventHandler HoverEvent;

      public event EventHandler UnhoverEvent;

      /// <summary>
      /// Event invoked when the escape key or equivalent is entered.
      /// </summary>
      public event EventHandler EscapeEvent;

      /// <summary>
      /// Event invoked when the enter/return key or equivalent is entered.
      /// </summary>
      public event EventHandler EnterEvent;

      public TextInput(IServiceProvider aServiceProvider)
      {
         mConfiguration = aServiceProvider.GetService<Configuration>();
         mBackground = aServiceProvider.GetService<Quad>();
         mCursor = aServiceProvider.GetService<Quad>();
         mText = aServiceProvider.GetService<Text>();
         mBorder = aServiceProvider.GetService<Border>();

         Color = mConfiguration.PrimaryColor.ToColor(); // Make use of the setter to apply the default color.
         Height = Text.DefaultHeight + (2 * mPadding);

         HoverEvent += OnHover;
         UnhoverEvent += OnUnhover;
      }

      public virtual void Respond(InputEvent aInput)
      {
         switch (aInput.Op)
         {
            // If this input should remain unmodified but lose its focus.
            case InputEvent.Opcode.BACK:
               IsFocused = false;
               OnBackEvent();
               break;
            // If a keyboard key was pressed (or, on mobile, the equivalent on-screen keyboard key).
            case InputEvent.Opcode.KEY_DOWN:
               // If this input is currently focused (i.e. was selected so that text could be entered).
               if (mIsFocused == true)
               {
                  if (aInput.Key == Keys.Enter)
                  {
                     // If the enter/return key was pressed.
                     OnEnterEvent();
                  }
                  else if (aInput.Key == Keys.Back)
                  {
                     // If the backspace key was pressed.

                     // If there is at least one character to remove and the cursor isn't at the
                     // very first index.
                     if ((string.IsNullOrEmpty(mText.Value) == false) && (mCursorIndex > 0))
                     {
                        // Remove one character before (to the left of) the cursor.
                        mText.Value = mText.Value.Remove(mCursorIndex - 1, 1);
                        mCursorIndex -= 1;
                        PositionCursor();
                     }
                  }
               }
               break;
            case InputEvent.Opcode.CHAR:
               if (mIsFocused == true)
               {
                  // If there's enough room to append the inputted character.
                  if (mText.WouldBeWidth(mText.Value + aInput.Character) <= (mWidth - (2.0 * Padding)))
                  {
                     // Add the character at the cursor's current index.
                     mText.Value = mText.Value.Insert(mCursorIndex, aInput.Character.ToString());
                     mCursorIndex += 1;
                     PositionCursor();
                  }
               }
               break;
            // If the mouse/cursor moved.
            case InputEvent.Opcode.HOVER:
               bool wasMouseInBounds = mIsMouseInBounds;
               mIsMouseInBounds = Bounds.Contains(aInput.Position.ToXna());
               if (wasMouseInBounds != mIsMouseInBounds)
               {
                  if (mIsMouseInBounds == true)
                  {
                     // If the mouse entered the bounds of this input.

                     try
                     {
                        HoverEvent?.Invoke(this, EventArgs.Empty);
                     }
                     catch (Exception e)
                     {
                        mLog.Error(e, "Exception thrown while propagating a hover event");
                     }
                  }
                  else
                  {
                     // If the mouse left the bounds of this input.

                     try
                     {
                        UnhoverEvent?.Invoke(this, EventArgs.Empty);
                     }
                     catch (Exception e)
                     {
                        mLog.Error(e, "Exception thrown while propagating an unhover event");
                     }
                  }
               }
               break;
            case InputEvent.Opcode.MOUSE_DOWN:
               // Apply the focus setter to do various things. The exact behavior will depend on the platform
               // (e.g. on Android, IsFocused = false will close the on-screen keyboard).
               IsFocused = Bounds.Contains(aInput.Position.ToXna());

               // If the user no longer wants to add text.
               if (mIsFocused == false)
               {
                  OnBackEvent();
               }
               break;
         }
      }

      public void Draw()
      {
         if (HasBackground == true)
         {
            mBackground.Draw();
         }

         if (HasBorder == true)
         {
            mBorder.Draw();
         }

         mText.Draw();

         if (mShouldDrawCursor == true)
         {
            mCursor.Draw();
         }
      }

      public override string ToString()
      {
         return $"{{ Value = \"{Value}\", Bounds = {Bounds} }}";
      }

      protected void OnBackEvent()
      {
         try
         {
            EscapeEvent?.Invoke(this, EventArgs.Empty);
         }
         catch (Exception e)
         {
            mLog.Error(e, "Excpetion thrown while propagating an escape event");
         }
      }

      protected void OnEnterEvent()
      {
         try
         {
            EnterEvent?.Invoke(this, EventArgs.Empty);
         }
         catch (Exception e)
         {
            mLog.Error(e, "Excpetion thrown while propagating an enter event");
         }
      }

      protected void StartCursorTask()
      {
         // If the cancellation token source is allocated, the task is already running.
         if (mTokenSource != null)
         {
            return;
         }

         mTokenSource = new CancellationTokenSource();
         CancellationToken token = mTokenSource.Token;
         Task.Run(async () =>
         {
            while (token.IsCancellationRequested == false)
            {
               mShouldDrawCursor = !mShouldDrawCursor;
               await Task.Delay(500);
            }
         });
      }

      protected void StopCursorTask()
      {
         mShouldDrawCursor = false;
         mTokenSource?.Cancel();
         mTokenSource = null;
      }

      protected void PositionCursor()
      {
         mCursor.X = mText.GetXAtIndex(mCursorIndex);
         mShouldDrawCursor = true;
      }

      private void OnHover(object aSender, EventArgs aArgs)
      {
         mOriginalColor = mColor;
         if (aSender is TextInput input)
         {
            input.Color = mConfiguration.SecondaryColor.ToColor();
         }
      }

      private void OnUnhover(object aSender, EventArgs aArgs)
      {
         if (aSender is TextInput input)
         {
            input.Color = mOriginalColor;
         }
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         mBackground.X += aX;
         mBackground.Y += aY;

         mBorder.X += aX;
         mBorder.Y += aY;

         mCursor.X += aX;
         mCursor.Y += aY;

         mText.X += aX;
         mText.Y += aY;
      }

      private void Resize()
      {
         // If it's too soon to resize anything.
         if ((mWidth == 0.0) || (mHeight == 0.0))
         {
            return;
         }

         mBackground.X = mX;
         mBackground.Y = mY;
         mBackground.Width = mWidth;
         mBackground.Height = mHeight;

         mBorder.X = mBackground.X;
         mBorder.Y = mBackground.Y;
         mBorder.Width = mBackground.Width;
         mBorder.Height = mBackground.Height;

         mText.X = mX + Padding;
         mText.Y = mY + Padding;
         mText.Height = mHeight - (2.0 * Padding);

         PositionCursor();
         mShouldDrawCursor = false;
         mCursor.Y = mText.Y;
         mCursor.Width = 2.0; // Pixels.
         mCursor.Height = mHeight - (2.0 * Padding);
      }
   }
}
