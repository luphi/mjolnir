﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using NLog;
using System;

namespace Mjolnir.Client.Common.UI
{
   public class Text : IVisual
   {
      private static SpriteFont sFont = null;

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly MjolnirGame mGame;
      private readonly SpriteBatch mSpriteBatch;
      private string mValue = string.Empty;
      private double mScale = 1.0;

      public double X { get; set; } = 0.0;

      public double Y { get; set; } = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mForceWidth = value;
            mForceHeight = null;
            mWidth = value;

            if (string.IsNullOrEmpty(mValue) == true)
            {
               mScale = 0.0;
               mHeight = 0.0;
            }
            else if (sFont != null)
            {
               Vector2 size = sFont.MeasureString(mValue);
               mScale = mWidth / size.X;
               mHeight = mScale * size.Y;
            }
         }
      }
      private double mWidth = 0.0;
      private double? mForceWidth = null;

      public double Height
      {
         get => mHeight;
         set
         {
            mForceHeight = value;
            mForceWidth = null;
            mHeight = value;

            if (string.IsNullOrEmpty(mValue) == true)
            {
               mScale = 0.0;
               mWidth = 0.0;
            }
            else if (sFont != null)
            {
               Vector2 size = sFont.MeasureString(mValue);
               mScale = mHeight / size.Y;
               mWidth = mScale * size.X;
            }
         }
      }
      private double mHeight = 0.0;
      private double? mForceHeight = null;

      public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

      /// <summary>
      /// The default height, in pixels, of a new Text instance. This can be thought of as
      /// the default font size.
      /// Determined during startup on a per-platform basis.
      /// </summary>
      public static double DefaultHeight { get; set; } = 0.0;

      public Rectangle Bounds => new Rectangle((int)X, (int)Y, (int)mWidth, (int)mHeight);

      public string Value
      {
         get => mValue;
         set
         {
            mValue = value ?? string.Empty; // If null, use an empty string instead.
            if (string.IsNullOrEmpty(mValue) == true)
            {
               mWidth = 0.0;
            }
            else if (sFont != null)
            {
               Vector2 size = sFont.MeasureString(mValue);

               if (mForceWidth != null)
               {
                  // If the text is to be sized with a specific width.

                  mScale = (float)(mForceWidth / size.X);
                  mHeight = mScale * size.Y;
                  mWidth = (float)mForceWidth;
               }
               else if (mForceHeight != null)
               {
                  // If the text is to be sized with a speciifc height.

                  mScale = (float)(mForceHeight / size.Y);
                  mWidth = mScale * size.X;
                  mHeight = (float)mForceHeight;
               }
               else
               {
                  // If neither dimension was set (almost certainly never).

                  mWidth = size.X * mScale;
                  mHeight = size.Y * mScale;
               }
            }
         }
      }

      public Color Color { get; set; } = Color.White;

      public Text(IServiceProvider aServiceProvider)
      {
         mGame = aServiceProvider.GetService<MjolnirGame>();
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();

         if (sFont == null)
         {
            mGame.InvokeOnUIThread(() =>
            {
               try
               {
                  sFont = aServiceProvider.GetService<ContentManager>().Load<SpriteFont>("Font");
               }
               catch (Exception e)
               {
                  mLog.Error(e, "Exception thrown while loading the font");
                  sFont = null;
               }
            });
         }

         Color = aServiceProvider.GetService<Configuration>().PrimaryColor.ToColor();
         Height = DefaultHeight;
      }

      public void Draw()
      {
         if (sFont != null)
         {
            mSpriteBatch.DrawString(sFont,                           // Font instance.
                                    mValue,                          // String to display.
                                    new Vector2((float)X, (float)Y), // Top-left position to draw at.
                                    Color,                           // Color mask.
                                    0f,                              // Rotation (none).
                                    Vector2.Zero,                    // Center of rotation (none).
                                    (float)mScale,                   // Scale factor, determines height.
                                    SpriteEffects.None,              // Modificators (none).
                                    1f);                             // Layer depth.
         }
      }

      /// <summary>
      /// Maintain the current height but truncate the text/value to fit the given width.
      /// The text will be suffixed with an ellipsis ("...") to indicate it was shortened.
      /// </summary>
      /// <param name="aWidth">New width in pixels.</param>
      public void Truncate(uint aWidth)
      {
         if (sFont == null)
         {
            return;
         }

         while ((uint)(sFont.MeasureString($"{mValue}...").X * mScale) > aWidth)
         {
            if (string.IsNullOrEmpty(mValue) == true)
            {
               break;
            }

            mValue = mValue.Remove(mValue.Length - 1, 1);
         }

         mValue = $"{mValue}...";
      }

      /// <summary>
      /// Find the (virtual) X position of the character at the given index.
      /// For example, consider the string "asdf." GetXAtIndex(2) would return the X
      /// position of "d" (its left edge).
      /// </summary>
      /// <param name="aIndex">Index within the string represented by this graphical test.</param>
      /// <returns>X position of the character at the given index.</returns>
      public double GetXAtIndex(int aIndex)
      {
         if ((aIndex < 0) || (string.IsNullOrEmpty(mValue) == true) || (sFont == null))
         {
            return X;
         }

         if (aIndex >= mValue.Length)
         {
            return X + mWidth;
         }

         return X + (mScale * sFont.MeasureString(mValue.Substring(0, aIndex)).X);
      }

      /// <summary>
      /// Calculate the width of this text if its value (string) is the given one.
      /// </summary>
      /// <param name="aValue">A string to measure.</param>
      /// <returns>Would-be width in pixels.</returns>
      public double WouldBeWidth(string aValue)
      {
         if (sFont != null)
         {
            Vector2 size = sFont.MeasureString(aValue);

            // If the scale is known.
            // (If the string is not empty.)
            if (mScale != 0.0)
            {
               return size.X * mScale;
            }

            // Failing the above, calculate the would-be width from the height whether
            // the height is set or not.
            return mHeight / size.Y * size.X;
         }

         return 0.0;
      }

      public override string ToString()
      {
         return $"{{ Value = \"{Value}\", Bounds = {Bounds} }}";
      }
   }
}
