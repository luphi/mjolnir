﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using System;

namespace Mjolnir.Client.Common.UI
{
   /// <summary>
   /// A button featuring an image centered within the bounds.
   /// An optional border can be enabled that matches this button's bounds.
   /// </summary>
   public class ImageButton : ButtonBase
   {
      protected readonly Configuration mConfiguration;
      protected readonly Image mImage;

      public override double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      private double mX = 0.0;

      public override double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      private double mY = 0.0;

      public override double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            Resize();
         }
      }
      private double mWidth = 0.0;

      public override double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            Resize();
         }
      }
      private double mHeight = 0.0;

      public override uint Padding { get; set; } = 5; // Pixels.

      public virtual Color Color
      {
         get => mColor;
         set
         {
            mColor = value;
            mBorder.Color = value;
         }
      }
      protected Color mColor;

      public string Content
      {
         get => mImage.Content;
         set
         {
            mImage.Content = value;
            Resize();
         }
      }

      public ImageButton(IServiceProvider aServiceProvider) : base(aServiceProvider)
      {
         mConfiguration = aServiceProvider.GetService<Configuration>();
         mImage = aServiceProvider.GetService<Image>();

         Color = mConfiguration.SecondaryColor.ToColor(); // Make use of the setter to apply the default color.
      }

      public override void Draw()
      {
         base.Draw();

         mImage.Draw();
      }

      public override string ToString()
      {
         return $"{{ {nameof(Content)} = {Content}, {nameof(Bounds)} = {Bounds} }}";
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         mBorder.X += aX;
         mBorder.Y += aY;

         mImage.X += aX;
         mImage.Y += aY;
      }

      private void Resize()
      {
         mBorder.X = mX;
         mBorder.Y = mY;
         mBorder.Width = mWidth;
         mBorder.Height = mHeight;

         Rectangle paddedBounds = Bounds;
         if (Padding != 0.0)
         {
            paddedBounds.X += (int)Padding;
            paddedBounds.Y += (int)Padding;
            paddedBounds.Width -= (int)(Padding * 2);
            paddedBounds.Height -= (int)(Padding * 2);
         }
         mImage.CenterInBounds(paddedBounds);
      }
   }
}
