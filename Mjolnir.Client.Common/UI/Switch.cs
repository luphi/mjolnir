﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using NLog;
using System;
using Mjolnir.Client.Common.Input;

namespace Mjolnir.Client.Common.UI
{
   public class Switch : IControl, IVisual
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly Border mBorder;
      private readonly Quad mTrack;
      private readonly Quad mThumb;
      private readonly Color mPrimaryColor;

      public double X
      {
         get => mX;
         set
         {
            double diff = value - mX;
            mX = value;
            Translate(diff, 0.0);
         }
      }
      protected double mX = 0.0;

      public double Y
      {
         get => mY;
         set
         {
            double diff = value - mY;
            mY = value;
            Translate(0.0, diff);
         }
      }
      protected double mY = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            mHeight = value / 2.0;
            Resize();
         }
      }
      protected double mWidth = 0.0;

      public double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            mWidth = value * 2.0;
            Resize();
         }
      }
      protected double mHeight = 0.0;

      public double AspectRatio => Height == 0.0 ? 1.0 : Width / Height;

      public Rectangle Bounds => new Rectangle((int)mX, (int)mY, (int)mWidth, (int)mHeight);

      public Color Color
      {
         get => mColor;
         set
         {
            if (mColor != value)
            {
               mColor = value;
               Recolor();
            }
         }
      }
      private Color mColor;

      public bool IsChecked
      {
         get => mIsChecked;
         set
         {
            if (mIsChecked != value)
            {
               mIsChecked = value;
               Resize();
               Recolor();
            }
         }
      }
      private bool mIsChecked = false;

      public bool IsEnabled
      {
         get => mIsEnabled;
         set
         {
            if (mIsEnabled != value)
            {
               mIsEnabled = value;
               Recolor();
            }
         }
      }
      private bool mIsEnabled = true;

      public event EventHandler<bool> CheckEvent;

      public Switch(IServiceProvider aServiceProvider)
      {
         mBorder = aServiceProvider.GetService<Border>();
         mTrack = aServiceProvider.GetService<Quad>();
         mThumb = aServiceProvider.GetService<Quad>();

         Configuration configuration = aServiceProvider.GetService<Configuration>();
         mPrimaryColor = configuration.PrimaryColor.ToColor();
         mBorder.Color = mPrimaryColor;
         mTrack.Color = Color.Black;
         mThumb.Color = mPrimaryColor;

         Color = configuration.SecondaryColor.ToColor();
      }

      public void Respond(InputEvent aInput)
      {
         if ((aInput.Op == InputEvent.Opcode.MOUSE_DOWN) &&
             (IsEnabled == true) &&
             (Bounds.Contains(aInput.Position.ToXna()) == true))
         {
            IsChecked = !IsChecked;

            try
            {
               CheckEvent?.Invoke(this, mIsChecked);
            }
            catch (Exception e)
            {
               mLog.Error(e, "Exception thrown while propagating a check event");
            }
         }
      }

      public void Draw()
      {
         mTrack.Draw();
         mBorder.Draw();
         mThumb.Draw();
      }

      public override string ToString()
      {
         return $"{{ {nameof(IsChecked)} = {IsChecked}, {nameof(IsEnabled)} = {IsEnabled} }}";
      }

      private void Translate(double aX, double aY)
      {
         if ((aX == 0.0) && (aY == 0.0))
         {
            return;
         }

         mBorder.X += aX;
         mBorder.Y += aY;

         mTrack.X += aX;
         mTrack.Y += aY;

         mThumb.X += aX;
         mThumb.Y += aY;
      }

      private void Resize()
      {
         // If it's too soon to resize anything.
         if ((mWidth == 0.0) || (mHeight == 0.0))
         {
            return;
         }

         mBorder.X = mX;
         mBorder.Y = mY;
         mBorder.Width = mWidth;
         mBorder.Height = mHeight;

         mTrack.X = mX;
         mTrack.Y = mY;
         mTrack.Width = mWidth;
         mTrack.Height = mHeight;

         double internalPadding = mHeight * 0.005;
         mThumb.Height = mHeight - (2 * (mBorder.Thickness + internalPadding));
         mThumb.Width = mThumb.Height;
         if (IsChecked == true)
         {
            mX = mX + mBorder.Thickness + internalPadding;
         }
         else
         {
            mX = mX + mWidth - mBorder.Thickness - internalPadding - mThumb.Width;
         }
         mY = mY + mBorder.Thickness + internalPadding;
      }

      private void Recolor()
      {
         Color borderColor = mPrimaryColor;
         Color trackColor = Color.Black;
         Color thumbColor = mPrimaryColor;

         if ((mIsEnabled == true) && (mIsChecked == true))
         {
            trackColor = mColor;
         }
         else if (mIsEnabled == false)
         {
            borderColor = mPrimaryColor.Dull();
            thumbColor = mPrimaryColor.Dull();
            if (IsChecked == true)
            {
               trackColor = mColor.Dull();
            }
         }
         mBorder.Color = borderColor;
         mTrack.Color = trackColor;
         mThumb.Color = thumbColor;
      }
   }
}
