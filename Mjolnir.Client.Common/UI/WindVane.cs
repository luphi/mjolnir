﻿using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using NLog;
using System;
using Mjolnir.Common.Models;

namespace Mjolnir.Client.Common.UI
{
   public class WindVane : IVisual
   {
      private const string WIND_VANE_BACKGROUND_CONTENT = "UI/wind_vane_background";
      private const string WIND_VANE_ARROW_CONTENT = "UI/wind_vane_arrow";

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly SpriteBatch mSpriteBatch;
      private readonly Texture2D mBackgroundTexture;
      private readonly Texture2D mArrowTexture;
      private readonly Text mSpeedText;
      private float mRotation = 0f;

      public double X
      {
         get => mX;
         set
         {
            mX = value;
            Resize();
         }
      }
      private double mX = 0.0;

      public double Y
      {
         get => mY;
         set
         {
            mY = value;
            Resize();
         }
      }
      private double mY = 0.0;

      public double Width
      {
         get => mWidth;
         set
         {
            mWidth = value;
            mHeight = value;
            Resize();
         }
      }
      private double mWidth = 0.0;

      public double Height
      {
         get => mHeight;
         set
         {
            mHeight = value;
            mWidth = value;
            Resize();
         }
      }
      private double mHeight = 0.0;

      public double AspectRatio => 1.0; // Wind vanes will always be 1:1.

      public Rectangle Bounds => new Rectangle((int)mX, (int)mY, (int)mWidth, (int)mHeight);

      /// <summary>
      /// The wind vector as known to the World.
      /// </summary>
      public Vector Wind
      {
         get => mWind;
         set
         {
            mWind = value;

            if (value == null)
            {
               mSpeedText.Value = "0.0";
               mRotation = 0f;
            }
            else
            {
               // Format the wind vector's length, the wind speed, as the nearest integer.
               mSpeedText.Value = ((int)Math.Round(value.Length)).ToString();

               // Create an equivalent wind vector that is mirrored vertically. This is done
               // because this game views -Y as up but a user would be very confused to see,
               // for example, downward wind pointing upward.
               Vector verticallyMirroredWind = new Vector(value);
               verticallyMirroredWind.Y = -verticallyMirroredWind.Y;
               mRotation = (float)verticallyMirroredWind.AngleInRadians;
            }

            Resize();
         }
      }
      private Vector mWind = null;

      public WindVane(IServiceProvider aServiceProvider)
      {
         mSpriteBatch = aServiceProvider.GetService<SpriteBatch>();
         mSpeedText = aServiceProvider.GetService<Text>();
         mSpeedText.Value = "0";

         ContentManager contentManager = aServiceProvider.GetService<ContentManager>();
         try
         {
            mBackgroundTexture = contentManager.Load<Texture2D>(WIND_VANE_BACKGROUND_CONTENT);
         }
         catch (Exception e)
         {
            mLog.Warn(e, $"Exception thrown while loading wind vane background \"{WIND_VANE_BACKGROUND_CONTENT}\"");
            mBackgroundTexture = null;
         }

         try
         {
            mArrowTexture = contentManager.Load<Texture2D>(WIND_VANE_ARROW_CONTENT);
         }
         catch (Exception e)
         {
            mLog.Warn(e, $"Exception thrown while loading wind vane arrow \"{WIND_VANE_ARROW_CONTENT}\"");
            mArrowTexture = null;
         }
      }

      public void Draw()
      {
         if (mBackgroundTexture != null)
         {
            mSpriteBatch.Draw(mBackgroundTexture, Bounds, Color.White);
         }

         if (mArrowTexture != null)
         {
            // Prepare position and origin vectors. The position is the (virtual) screen
            // coordinate corresponding to the top-left corner of surface being drawn here.
            Vector2 position = new Vector2((float)mX, (float)mY);
            // The origin is the relative point around which the sprite will be rotated.
            // Although not documented anywhere, the origin does not use screen coordinates nor
            // normalized device coordinates. It should use the texture's coordinates.
            Vector2 origin = mArrowTexture.Bounds.Center.ToVector2();
            // Finally, calculate the scale relative to the original bitmap.
            float scale = (float)(mWidth / mArrowTexture.Width);
            mSpriteBatch.Draw(
                // The bitmap source from which we'll draw a small portion.
                mArrowTexture,
                // The position to draw at.
                position + (origin * scale),
                // Region in the bitmap to be drawn.
                null,
                // Color mask.
                Color.White,
                // Rotation, in radians, to be applied.
                mRotation,
                // The point, relative to the drawn position, around which rotation happens.
                origin,
                // Scale factor. Determines the drawn dimensions.
                scale,
                // Sprite effects (none, horizontal flip, or vertical flip).
                SpriteEffects.None,
                // Layer depth.
                1f);
         }
         mSpeedText.Draw();
      }

      private void Resize()
      {
         // Center the speed in the bounds using half of the bounds' height.
         mSpeedText.Height = mHeight / 2.0; // The Height setter also sets width while keeping the aspect ratio.
         mSpeedText.X = mX + (mWidth / 2.0) - (mSpeedText.Width / 2.0);
         mSpeedText.Y = mY + (mHeight / 2.0) - (mSpeedText.Height / 2.0);
      }
   }
}
