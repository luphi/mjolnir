﻿using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using NLog;
using System;
using System.Collections.Generic;
using Mjolnir.Client.Common.Input;

namespace Mjolnir.Client.Common.UI
{
   /// <summary>
   /// A fullscreen, popup dialog that displays a messageand displays "CANCEL" and "OK" buttons.
   /// </summary>

   public class ConfirmationModalWindow : IControl, IVisual
   {
      protected readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      protected readonly IServiceProvider mServiceProvider;
      protected readonly ResolutionIndependentViewport mViewport;
      protected readonly Quad mVeil;
      protected readonly Quad mWindow;
      protected readonly Border mBorder;
      protected readonly List<TextButton> mButtons = new List<TextButton>();
      protected readonly List<Text> mTexts = new List<Text>();

      public double X
      {
         get => 0.0;
         set { }
      }

      public double Y
      {
         get => 0.0;
         set { }
      }

      public double Width
      {
         get => mViewport.VirtualWidth;
         set { }
      }

      public double Height
      {
         get => mViewport.VirtualHeight;
         set { }
      }

      public double AspectRatio => (double)mViewport.VirtualWidth / mViewport.VirtualHeight;

      public Rectangle Bounds => new Rectangle(0, 0, mViewport.VirtualWidth, mViewport.VirtualHeight);

      /// <summary>
      /// The message to be displayed to the user. Line wrapping will be applied if the
      /// message exceeds the dialog's width for one line.
      /// </summary>
      public string Message
      {
         get => mMessage;
         set
         {
            if (string.IsNullOrEmpty(value) == false)
            {
               mMessage = value;
               Resize();
            }
         }
      }
      private string mMessage = string.Empty;

      /// <summary>
      /// Invoked when the message is acknowledged.
      /// </summary>
      public event EventHandler AcknowledgeEvent;
      /// <summary>
      /// Invoked when the user cancels the input, either by clicking the "CANCE" button or
      /// pressing escape while entering text.
      /// </summary>
      public event EventHandler CancelEvent;

      public ConfirmationModalWindow(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mVeil = aServiceProvider.GetService<Quad>();
         mWindow = aServiceProvider.GetService<Quad>();
         mBorder = aServiceProvider.GetService<Border>();
         mButtons.Add(aServiceProvider.GetService<TextButton>()); // "OK" button.
         mButtons.Add(aServiceProvider.GetService<TextButton>()); // "CANCEL" button.

         mVeil.Color = new Color
         {
            R = 0,
            G = 0,
            B = 0,
            A = 155
         };
         mWindow.Color = Color.Black;
         mBorder.Color = aServiceProvider.GetService<Configuration>().SecondaryColor.ToColor();

         mButtons[0].Text = "OK";
         mButtons[0].ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            try
            {
               AcknowledgeEvent?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception e)
            {
               mLog.Error(e, "Exception thrown while propagating an acknowledge event");
            }
         };
         mButtons[1].Text = "CANCEL";
         mButtons[1].ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            try
            {
               CancelEvent?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception e)
            {
               mLog.Error(e, "Exception thrown while propagating a cancel event");
            }
         };
      }

      public virtual void Respond(InputEvent aInput)
      {
         if (aInput.Op == InputEvent.Opcode.BACK)
         {
            OnCancelEvent();
         }
         else
         {
            foreach (TextButton button in mButtons)
            {
               button.Respond(aInput);
            }
         }
      }

      public virtual void Draw()
      {
         mVeil.Draw();
         mWindow.Draw();
         mBorder.Draw();

         lock (mTexts)
         {
            foreach (Text text in mTexts)
            {
               text.Draw();
            }
         }

         foreach (TextButton button in mButtons)
         {
            button.Draw();
         }
      }

      protected void OnAcknowledgeEvent()
      {
         try
         {
            AcknowledgeEvent?.Invoke(this, EventArgs.Empty);
         }
         catch (Exception e)
         {
            mLog.Error(e, "Exception thrown while propagating an acknowledge event");
         }
      }

      protected void OnCancelEvent()
      {
         try
         {
            CancelEvent?.Invoke(this, EventArgs.Empty);
         }
         catch (Exception e)
         {
            mLog.Error(e, "Exception thrown while propagating a cancel event");
         }
      }

      protected virtual Rectangle Resize()
      {
         Rectangle viewportBounds = Bounds;
         Rectangle windowBounds = new Rectangle(viewportBounds.X + (viewportBounds.Width / 4),
                                                viewportBounds.Y + (viewportBounds.Height / 4),
                                                viewportBounds.Width / 2,
                                                viewportBounds.Height / 2);
         double padding = windowBounds.Height * 0.05;
         Rectangle paddedBounds = new Rectangle((int)(windowBounds.X + padding),
                                                (int)(windowBounds.Y + padding),
                                                (int)(windowBounds.Width - (2.0 * padding)),
                                                (int)(windowBounds.Height - (2.0 * padding)));

         mVeil.X = viewportBounds.X;
         mVeil.Y = viewportBounds.Y;
         mVeil.Width = viewportBounds.Width;
         mVeil.Height = viewportBounds.Height;

         mWindow.X = windowBounds.X;
         mWindow.Y = windowBounds.Y;
         mWindow.Width = windowBounds.Width;
         mWindow.Height = windowBounds.Height;

         mBorder.X = windowBounds.X;
         mBorder.Y = windowBounds.Y;
         mBorder.Width = windowBounds.Width;
         mBorder.Height = windowBounds.Height;

         lock (mTexts)
         {
            mTexts.Clear();
            // Step 1: create the line(s), as strings, to represent the message.
            // Create a Text object with the appropriate height that will be used to
            // calculate the width of the would-be resulting lines.
            Text text = mServiceProvider.GetService<Text>();
            string[] split = Message.Split(' '); // Array of single words
            List<string> lines = new List<string>(); // Width-limited lines from the above
            string concat = string.Empty; // String reused to determine lines

            // For each individual word in the message.
            foreach (string word in split)
            {
               // Concatenate the word onto the line.
               string wouldBeConcat = $"{(string.IsNullOrEmpty(concat) ? string.Empty : $"{concat} ")}{word}";

               if (text.WouldBeWidth(wouldBeConcat) >= paddedBounds.Width)
               {
                  // If this would exceed the width of the bounds.

                  // Remember the line without the most recent word.
                  lines.Add(concat);
                  // Assign the leftover word to what will become the next line.
                  concat = word;
               }
               else
               {
                  // If this line still fits within the bounds.

                  // Apppend it and continue to the next word.
                  concat = wouldBeConcat;
               }
            }

            // Make a line of any remaining words.
            if (string.IsNullOrEmpty(concat) == false)
            {
               lines.Add(concat);
            }

            lock (mTexts)
            {
               for (int i = 0; i < lines.Count; i++)
               {
                  if (i > 0)
                  {
                     text = mServiceProvider.GetService<Text>();
                  }
                  text.X = paddedBounds.X;
                  text.Y = paddedBounds.Y + (text.Height * i);
                  text.Value = lines[i];
                  mTexts.Add(text);
               }
            }
         }

         foreach (TextButton button in mButtons)
         {
            button.Height = Text.DefaultHeight * 2.0;
         }

         // "OK" button.
         mButtons[0].X = paddedBounds.X + paddedBounds.Width - mButtons[0].Width;
         mButtons[0].Y = paddedBounds.Y + paddedBounds.Height - mButtons[0].Height;

         // "CANCEL" button.
         mButtons[1].X = paddedBounds.X;
         mButtons[1].Y = mButtons[0].Y;

         return paddedBounds; // Return the padded bounds so that a child class may override this method while
                              // also having access to the padded bounds it creates. (See InputOverlay.)
      }
   }
}
