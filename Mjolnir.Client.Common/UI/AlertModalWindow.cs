﻿using Mjolnir.Client.Common.Graphics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using NLog;
using System;
using System.Collections.Generic;
using Mjolnir.Client.Common.Input;

namespace Mjolnir.Client.Common.UI
{
   /// <summary>
   /// A fullscreen, popup alert dialog that displays a message and an "OK" button.
   /// </summary>
   public class AlertModalWindow : IControl, IVisual
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IServiceProvider mServiceProvider;
      private readonly ResolutionIndependentViewport mViewport;
      private readonly Quad mVeil;
      private readonly Quad mWindow;
      private readonly Border mBorder;
      private readonly TextButton mOkButton;
      private readonly List<Text> mTexts = new List<Text>();

      /// <summary>
      /// The message to be displayed to the user. Line wrapping will be applied if the
      /// message exceeds the dialog's width for one line.
      /// </summary>
      public string Message
      {
         get => mMessage;
         set
         {
            if (!string.IsNullOrEmpty(value))
            {
               mMessage = value;
               Resize();
            }
         }
      }
      private string mMessage = string.Empty;

      public double X
      {
         get => 0.0;
         set { }
      }

      public double Y
      {
         get => 0.0;
         set { }
      }

      public double Width
      {
         get => mViewport.VirtualWidth;
         set { }
      }

      public double Height
      {
         get => mViewport.VirtualHeight;
         set { }
      }

      public double AspectRatio => (double)mViewport.VirtualWidth / mViewport.VirtualHeight;

      public Rectangle Bounds => new Rectangle(0, 0, mViewport.VirtualWidth, mViewport.VirtualHeight);

      /// <summary>
      /// Invoked when the message is acknowledged. This will always be invoked.
      /// </summary>
      public event EventHandler AcknowledgeEvent;

      public AlertModalWindow(IServiceProvider aServiceProvider)
      {
         mServiceProvider = aServiceProvider;
         mViewport = aServiceProvider.GetService<ResolutionIndependentViewport>();
         mVeil = aServiceProvider.GetService<Quad>();
         mWindow = aServiceProvider.GetService<Quad>();
         mBorder = aServiceProvider.GetService<Border>();
         mOkButton = aServiceProvider.GetService<TextButton>();

         mVeil.Color = new Color
         {
            R = 0,
            G = 0,
            B = 0,
            A = 155
         };
         mWindow.Color = Color.Black;
         mBorder.Color = Color.LightSkyBlue;

         mOkButton.Text = "OK";
         mOkButton.ClickEvent += delegate (object aSender, EventArgs aArgs)
         {
            try
            {
               AcknowledgeEvent?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception e)
            {
               mLog.Error(e, "Exception thrown while propagating an acknowledge event");
            }
         };
      }

      public void Respond(InputEvent aInput)
      {
         mOkButton.Respond(aInput);
      }

      public void Draw()
      {
         mVeil.Draw();
         mWindow.Draw();
         mBorder.Draw();
         lock (mTexts)
         {
            foreach (Text text in mTexts)
            {
               text.Draw();
            }
         }
         mOkButton.Draw();
      }

      private void Resize()
      {
         Rectangle viewportBounds = Bounds;
         Rectangle windowBounds = new Rectangle((int)(viewportBounds.X + (viewportBounds.Width * 0.25)),
                                                (int)(viewportBounds.Y + (viewportBounds.Height * 0.25)),
                                                viewportBounds.Width / 2,
                                                viewportBounds.Height / 2);
         double padding = windowBounds.Height * 0.05;
         //double textHeight = windowBounds.Height * 0.1;
         Rectangle paddedBounds = new Rectangle((int)(windowBounds.X + padding),
                                                (int)(windowBounds.Y + padding),
                                                (int)(windowBounds.Width - (2.0 * padding)),
                                                (int)(windowBounds.Height - (2.0 * padding)));

         mVeil.X = viewportBounds.X;
         mVeil.Y = viewportBounds.Y;
         mVeil.Width = viewportBounds.Width;
         mVeil.Height = viewportBounds.Height;

         mWindow.X = windowBounds.X;
         mWindow.Y = windowBounds.Y;
         mWindow.Width = windowBounds.Width;
         mWindow.Height = windowBounds.Height;

         mBorder.X = windowBounds.X;
         mBorder.Y = windowBounds.Y;
         mBorder.Width = windowBounds.Width;
         mBorder.Height = windowBounds.Height;

         lock (mTexts)
         {
            mTexts.Clear();
            // Step 1: create the line(s), as strings, to represent the message.
            // Create a Text object with the appropriate height that will be used to
            // calculate the width of the would-be resulting lines.
            Text text = mServiceProvider.GetService<Text>();
            string[] split = mMessage.Split(' '); // Array of single words
            List<string> lines = new List<string>(); // Width-limited lines from the above
            string concat = string.Empty; // String reused to determine lines

            // For each individual word in the message.
            foreach (string word in split)
            {
               // Concatenate the word onto the line.
               string wouldBeConcat = $"{(string.IsNullOrEmpty(concat) ?string.Empty : $"{concat} ")}{word}";

               if (text.WouldBeWidth(wouldBeConcat) >= paddedBounds.Width)
               {
                  // If this would exceed the width of the bounds.

                  // Remember the line without the most recent word.
                  lines.Add(concat);
                  // Assign the leftover word to what will become the next line.
                  concat = word;
               }
               else
               {
                  // If this line still fits within the bounds.

                  // Apppend it and continue to the next word.
                  concat = wouldBeConcat;
               }
            }

            // Make a line of any remaining words.
            if (!string.IsNullOrEmpty(concat))
            {
               lines.Add(concat);
            }

            for (int i = 0; i < lines.Count; i++)
            {
               if (i > 0)
               {
                  text = mServiceProvider.GetService<Text>();
               }

               text.X = paddedBounds.X;
               text.Y = paddedBounds.Y + (text.Height * i);
               text.Value = lines[i];
               mTexts.Add(text);
            }
         }

         mOkButton.Height = Text.DefaultHeight * 2.0;
         mOkButton.X = paddedBounds.X + paddedBounds.Width - mOkButton.Width;
         mOkButton.Y = paddedBounds.Y + paddedBounds.Height - mOkButton.Height;
      }
   }
}
