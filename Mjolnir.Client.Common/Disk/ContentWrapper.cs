﻿using Mjolnir.Common.Disk;
using Mjolnir.Common.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using NLog;
using System;
using System.Linq;
using System.Threading;
using Mjolnir.Common;

namespace Mjolnir.Client.Common.Disk
{
   /// <summary>
   /// This object is responsible locating and, in the case of stages, loading assets
   /// on/from disk. All asset locations on disk are discovered through this object.
   /// </summary>
   public class ContentWrapper : IContentWrapper
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly MjolnirGame mGame;
      private readonly ContentManager mContentManager;
      private readonly int[] mCompassAngles = new int[] { 5, 10, 15, 20, 25, 30, 35, 40, 45,
                                                          50, 55, 60, 65, 70, 75, 80, 85, 90 };

      public ContentWrapper(IServiceProvider aServiceProvider)
      {
         mGame = aServiceProvider.GetService<MjolnirGame>();
         mContentManager = aServiceProvider.GetService<ContentManager>();
      }


      #region IContentWrapper

      public bool[,] LoadStage(Stage aStage)
      {
         if (aStage == null)
         {
            return null;
         }

         string foregroundPath = StageForegroundPath(aStage);
         bool[,] mask = null;

         // Content loading must be done on the UI thread. However, that's complicated in this method as
         // we can't fire and forget the below delegate. The data its loading is requirued for this method's
         // return value. For that reason, a reset event will be used to block this method until the UI
         // thread delegate completes.
         ManualResetEvent resetEvent = new ManualResetEvent(false);

         // Invoke the foreground texture loading on the UI thread. This method, LoadStage(), may have
         // been called from the UI thread already. In which case, this will execute immediately.
         mGame.InvokeOnUIThread(() =>
         {
            Texture2D texture = null;
            try
            {
               texture = mContentManager.Load<Texture2D>(foregroundPath);
            }
            catch (Exception e)
            {
               mLog.Error(e, $"Could not find foreground image for {aStage.Name} at {foregroundPath}");
               texture = null;
            }

            if (texture != null)
            {
               Color[] data = new Color[texture.Width * texture.Height];
               mask = new bool[texture.Width, texture.Height];
               texture.GetData(data);
               for (int y = 0; y < texture.Height; y++)
               {
                  for (int x = 0; x < texture.Width; x++)
                  {
                     mask[x, y] = data[(y * texture.Width) + x].A != Color.Transparent.A;
                  }
               }
            }

            // Unblock the thread that created this delegate.
            resetEvent.Set();
         });

         // Wait for the delegate to finish.
         resetEvent.WaitOne();

         return mask;
      }

      public string StageForegroundPath(Stage aStage)
      {
         return aStage == null ? "Stages/null" : $"Stages/{aStage.Name}/{aStage.Foreground}";
      }

      public string StageBackgroundPath(Stage aStage)
      {
         return aStage == null ? "Stages/null" : $"Stages/{aStage.Name}/{aStage.Background}";
      }

      #endregion IContentWrapper


      public string MobileTexturePath(Mobile aMobile)
      {
         return $"Mobiles/{aMobile?.Name}";
      }

      public string ProjectileTexturePath(Projectile aProjectile)
      {
         // TODO
         return (aProjectile.Player.Shot.MobileID, aProjectile.Player.Shot.ShotID) switch
         {
            (MobileEnum.AKUDA, ShotEnum.SHOT_1) => "Projectiles/Akuda/shot_1",
            (MobileEnum.AKUDA, ShotEnum.SHOT_2) => "Projectiles/Akuda/shot_2",
            (MobileEnum.AKUDA, ShotEnum.SPECIAL_SHOT) => "Projectiles/Akuda/shot_ss",
            _ => $"Projectiles/ball",
         };
      }

      public string ItemTexturePath(Item aItem)
      {
         return aItem == null ? "Items/null" : $"Items/{aItem.Name}";
      }

      public string CompassTexturePath(Mobile aMobile)
      {
         if (aMobile == null)
         {
            return "UI/null";
         }

         uint degrees = (uint)(aMobile.TrueAngleRangeInRadians * 180.0 / Math.PI);

         // There are compass images prepared for true angle ranges in multiples of five
         // (5, 10, 15, ..., 90 degrees). We want to find the angle from that list nearest
         // to the given angle.
         int nearest = mCompassAngles.OrderBy(a => Math.Abs(degrees - a)).First();

         // The compasses are named with a predictable pattern: compass_5, compass_10, etc.
         return $"UI/compass_{nearest}";
      }
   }
}
