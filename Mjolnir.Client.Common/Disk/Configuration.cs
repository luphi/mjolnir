﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Mjolnir.Client.Common.Disk
{
   /// <summary>
   /// Holds various settings for the application and some user-related information.
   /// This class is [Serializable] as it's intended to be read from disk at startup.
   /// The values defined here are effectively this game's default settings.
   /// </summary>
   [Serializable]
   public class Configuration
   {
      private const string CONFIG_FILENAME = "client_config.xml";

      private static readonly ILogger sLog = LogManager.GetCurrentClassLogger();

      /// <summary>
      /// IP address and admin password pair. This [Serializable] class is used in place
      /// of a map/dictionary due to the lack of a serializable map in .NET (out
      /// of the box).
      /// </summary>
      [Serializable]
      public class HostnamePasswordPair
      {
         /// <summary>
         /// Hostname or IP address of the server associated with the paired admin password.
         /// </summary>
         public string Hostname { get; set; } = "127.0.0.1";

         /// <summary>
         /// Password used to grant admin privileges. This password must match that
         /// of associated server, set in its equivalent configuration.
         /// </summary>
         public string AdminPassword { get; set; } = "correct horse battery staple";
      }

      /// <summary>
      /// Display name for the player/user to identify the player in-game or in-lobby.
      /// This has no functional effects.
      /// </summary>
      public string PlayerName { get; set; } = "Nanashi";

      /// <summary>
      /// Windowed mode flag. When true, the (desktop) application will display in a window.
      /// When false, the (desktop) application will display fullscreen on the current
      /// display. For mobile applications, this will be hardcoded to true.
      /// </summary>
      public bool IsWindowed { get; set; } = true;

      /// <summary>
      /// Developer tools flag. When true, will enable the semi-hidden developer screen
      /// and features therein.
      /// </summary>
      public bool IsDeveloper { get; set; } = false;

      /// <summary>
      /// Display resolution width when run in windowed mode. This effectively only applies
      /// to the desktop application.
      /// </summary>
      public int ViewportWidth { get; set; } = 1600;

      /// <summary>
      /// Display resolution height when run in windowed mode. This effectively only applies
      /// to the desktop application.
      /// </summary>
      public int ViewportHeight { get; set; } = 900;

      /// <summary>
      /// Internal, reference viewport width. All visuals are scaled relative to the
      /// virtual resolution. This is a preferred value in that it or the preferred height
      /// may be used, but not both, depending on the actual viewport's aspect ratio.
      /// </summary>
      public int PreferredVirtualWidth { get; set; } = 854;

      /// <summary>
      /// Internal, reference viewport height. All visuals are scaled relative to the
      /// virtual resolution. This is a preferred value in that it or the preferred width
      /// may be used, but not both, depending on the actual viewport's aspect ratio.
      /// </summary>
      public int PreferredVirtualHeight { get; set; } = 480;

      /// <summary>
      /// A color to be used most by the various user interface components.
      /// Applies to standard text, borders, etc.
      /// </summary>
      public string PrimaryColor { get; set; } = "FFFFFF";

      /// <summary>
      /// A color to be used as a highlight by the user interface components.
      /// Applies to interactive components on hover events, shows focus, etc.
      /// </summary>
      public string SecondaryColor { get; set; } = "87CEFA";

      /// <summary>
      /// Hostname or IP address of the, or a, registry. When entering multiplayer, this
      /// registry will be contacted to retrieve a list of currently-hosted (game) servers.
      /// </summary>
      public string RegistryHostname { get; set; } = "127.0.0.1";

      /// <summary>
      /// Port of the, or a, registry. It's recommended that the default value be used
      /// at all times.
      /// </summary>
      public int RegistryPort { get; set; } = 51510;

      /// <summary>
      /// List of server addresse and admin password pairs to be used to request admin
      /// privileges. If connecting to the server hosted at the given address, the
      /// associated admin password is sent to the server. If this password matches the
      /// server's configured admin password, admin privileges are granted to this client.
      /// </summary>
      public List<HostnamePasswordPair> AddressPasswordPairs { get; } = new List<HostnamePasswordPair>
      {
         new HostnamePasswordPair
         {
            Hostname = "127.0.0.1",
            AdminPassword = "marcus"
         },
         new HostnamePasswordPair
         {
            Hostname = "127.0.0.1",
            AdminPassword = "correct horse battery staple"
         }
      };

      public static Configuration Load()
      {
         if (!File.Exists(CONFIG_FILENAME))
         {
            return null;
         }

         try
         {
            XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
            StreamReader reader = File.OpenText(CONFIG_FILENAME);
            Configuration serializable = (Configuration)serializer.Deserialize(reader);
            reader.Close();
            return serializable;
         }
         catch (Exception e)
         {
            sLog.Error(e, $"Exception thrown while loading client configuration file " +
                          $"{Path.Combine(Directory.GetCurrentDirectory(), CONFIG_FILENAME)}");
            return null;
         }
      }

      public static void Store(Configuration aConfiguration)
      {
         if (aConfiguration == null)
         {
            return;
         }

         try
         {
            XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
            StreamWriter writer = new StreamWriter(CONFIG_FILENAME, false);
            serializer.Serialize(writer, aConfiguration);
            writer.Close();
         }
         catch (Exception e)
         {
            sLog.Error(e, $"Exception thronw while storing a client configurtion file to " +
                          $"{Path.Combine(Directory.GetCurrentDirectory(), CONFIG_FILENAME)}");
         }
      }
   }
}
