﻿using Microsoft.Xna.Framework;

namespace Mjolnir.Client.Common.Graphics
{
    public interface IVisual
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double AspectRatio { get; }
        public Rectangle Bounds { get; }

        void Draw();
    }

    public static class VisualExtensions
    {
        public static void CenterInBounds(this IVisual aVisual, Rectangle aBounds)
        {
            if ((aVisual == null) || (aBounds == Rectangle.Empty))
                return;

            if ((aBounds.Width / (double)aBounds.Height) > aVisual.AspectRatio)
            {
                double width = aBounds.Height * aVisual.AspectRatio;
                aVisual.Height = aBounds.Height;
                aVisual.Width = width;
                aVisual.X = aBounds.X + (aBounds.Width / 2.0) - (aVisual.Width / 2.0);
                aVisual.Y = aBounds.Y;
            }
            else
            {
                double height = aBounds.Width / aVisual.AspectRatio;
                aVisual.Width = aBounds.Width;
                aVisual.Height = height;
                aVisual.X = aBounds.X;
                aVisual.Y = aBounds.Y + (aBounds.Height / 2.0) - (aVisual.Height / 2.0);
            }
        }
    }
}
