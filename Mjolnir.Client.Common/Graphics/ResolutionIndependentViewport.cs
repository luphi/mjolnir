﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NLog;
using System;

namespace Mjolnir.Client.Common.Graphics
{
    public class ResolutionIndependentViewport : IDisposable
    {
        private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
        private readonly GameWindow mWindow;
        private readonly GraphicsDevice mGraphicsDevice;
        private readonly int mVirtualWidth;
        private readonly int mVirtualHeight;

        public Matrix ScaleMatrix => Matrix.CreateScale((float)mGraphicsDevice.Viewport.Width / VirtualWidth,
                                                        (float)mGraphicsDevice.Viewport.Height / VirtualHeight, 1f);
        public int VirtualWidth => mVirtualWidth;
        public int VirtualHeight => mVirtualHeight;
        public Point VirtualCenter => new Point(mVirtualWidth / 2, mVirtualHeight / 2);
        public Rectangle VirtualBounds => new Rectangle(0, 0, VirtualWidth, VirtualHeight);
        public int ViewportWidth => mGraphicsDevice.Viewport.Width;
        public int ViewportHeight => mGraphicsDevice.Viewport.Height;
        public Point ViewportCenter => new Point(mGraphicsDevice.Viewport.Width / 2,
                                                 mGraphicsDevice.Viewport.Height / 2);
        public Rectangle ViewportBounds => mWindow.ClientBounds;

        public ResolutionIndependentViewport(GameWindow aWindow,
                                             GraphicsDevice aGraphicsDevice,
                                             int aVirtualWidth,
                                             int aVirtualHeight)
        {
            mWindow = aWindow;
            mGraphicsDevice = aGraphicsDevice;
            Rectangle viewportBounds = aWindow.ClientBounds;
            float viewportAspectRatio = (float)viewportBounds.Width / viewportBounds.Height;
            // If the viewport is wider than the virtual resolution aspect ratio
            if (viewportAspectRatio >= ((float)mVirtualWidth / mVirtualHeight))
            {
                // Use the preferred virtual height and determine the appropriate width
                mVirtualHeight = aVirtualHeight;
                mVirtualWidth = (int)(aVirtualHeight * viewportAspectRatio);
            }
            else
            {
                // Use the preferred virtual width and determine the appropriate height
                mVirtualWidth = aVirtualWidth;
                mVirtualHeight = (int)(aVirtualWidth / viewportAspectRatio);
            }
            mWindow.ClientSizeChanged += OnWindowDimensionsChanged;
            OnWindowDimensionsChanged(this, EventArgs.Empty);
        }

        public void Dispose()
        {
            mWindow.ClientSizeChanged -= OnWindowDimensionsChanged;
        }

        private void OnWindowDimensionsChanged(object aSender, EventArgs aEventArgs)
        {
            Rectangle clientBounds = mWindow.ClientBounds;
            float scaleX = (float)clientBounds.Width / mVirtualWidth;
            float scaleY = (float)clientBounds.Height / mVirtualHeight;
            float scale = MathHelper.Max(scaleX, scaleY);
            int width = (int)Math.Round(scale * mVirtualWidth);
            int height = (int)Math.Round(scale * mVirtualHeight);
            int x = (clientBounds.Width / 2) - (width / 2);
            int y = (clientBounds.Height / 2) - (height / 2);
            mGraphicsDevice.Viewport = new Viewport(x, y, width, height);
        }
    }
}
