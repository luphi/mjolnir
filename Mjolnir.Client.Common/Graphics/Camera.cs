﻿using Mjolnir.Common.Models;
using Microsoft.Xna.Framework;
using NLog;
using System;

namespace Mjolnir.Client.Common.Graphics
{
    /// <summary>
    /// Provides view matrices for positioning graphical entities and helper methods for
    /// converting between viewport and virtual coordinates.
    /// </summary>
    public class Camera
    {
        private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
        private readonly ResolutionIndependentViewport mViewport;
        private float mZoom = 1f;

        /// <summary>
        /// The parallax effect factor/ratio. For example, if this value is 0.5, parallaxed
        /// batch draws will translate at half (0.5) the rate of primary view matrix.
        /// </summary>
        public static float ParallaxRatio { get; } = 0.25f;

        /// <summary>
        /// View matrix to be applied to static UI elements. This matrix is independent of
        /// the camera's position and only applies the scale.
        /// </summary>
        public Matrix InterfaceViewMatrix => Matrix.Identity * mViewport.ScaleMatrix;

        /// <summary>
        /// View matrix to be applied to dynamic world bodies (players, projectiles, stage) that
        /// are affected by the camera's position.
        /// </summary>
        public Matrix WorldViewMatrix => GetViewMatrix(1f) * mViewport.ScaleMatrix;

        /// <summary>
        /// View matrix to be applied to batch draws intended to have a fake perspective
        /// (parallax) effect.
        /// </summary>
        public Matrix ParallaxViewMatrix => GetViewMatrix(ParallaxRatio) * mViewport.ScaleMatrix;

        /// <summary>
        /// The center/origin of the viewport, in virtual coordinates.
        /// </summary>
        private Vector Center { get; }

        /// <summary>
        /// The camera's current position, in virtual coordinates. This is defined as the
        /// top, left coordinate of the viewport.
        /// </summary>
        public Vector Position { get; private set; }

        /// <summary>
        /// The camera's current center position, or the coordinates the camera is looking
        /// at, in virtual coordintes.
        /// </summary>
        public Vector Target => Position + Center;

        /// <summary>
        /// Limits placed on the camera. The camera will prevent itself from viewing anything
        /// outside these bounds. For example, given a bounds X of 0 and width of 1600, the
        /// right edge of the (virtual) viewport will never surpass 1600.
        /// </summary>
        public Rectangle Bounds { get; set; }

        public Camera(ResolutionIndependentViewport aViewport)
        {
            mViewport = aViewport;
            Center = new Vector(mViewport.VirtualWidth / 2.0, mViewport.VirtualHeight / 2.0);
            Position = Vector.Zero;
            Bounds = Rectangle.Empty;
        }

        /// <summary>
        /// Return the camera to its original state at position (0, 0) without bounds.
        /// </summary>
        public void Reset()
        {
            Position = Vector.Zero;
            Bounds = Rectangle.Empty;
        }

        /// <summary>
        /// Move the camera's target by the given offset.
        /// </summary>
        /// <param name="aOffset">Offset in (X, Y) coordinates to move the camera's target.</param>
        public void Translate(Vector aOffset)
        {
            Position += aOffset;
            ClampToBounds();
        }

        /// <summary>
        /// Instruct the camera to focus/center on the given target coordinate.
        /// </summary>
        /// <param name="aTarget">(X, Y) coordinate to look at.</param>
        public void LookAt(Vector aTarget)
        {
            if (aTarget == null)
                return;

            Position = aTarget - Center;
            ClampToBounds();
        }

        /// <summary>
        /// Instruct the camera to focus/center on the center of the given bounds.
        /// </summary>
        /// <param name="aTarget">Bounds to look at.</param>
        public void LookAt(Rectangle aTarget)
        {
            LookAt(new Vector(aTarget.X + (aTarget.Width / 2), aTarget.Y + (aTarget.Height / 2)));
        }

        public void LookAt(IVisual aTarget)
        {
            LookAt(aTarget?.Bounds ?? Rectangle.Empty);
        }

        public Vector ViewportToVirtual(Vector aPosition)
        {
            // The code in use assumes that the viewport begins at (0, 0). If that assumption isn't safe,
            // complete this commented code and return it instead.
            return Vector2.Transform(aPosition.ToXna(), Matrix.Invert(InterfaceViewMatrix)).ToModel();
            //return Vector2.Transform(aPosition - new Vector2(viewport.X, viewport.Y), Matrix.Invert(ViewMatrix));
        }

        public Vector VirtualToViewport(Vector aPosition)
        {
            // The code in use assumes that the viewport begins at (0, 0). If that assumption isn't safe,
            // complete this commented code and return it instead.
            return Vector2.Transform(aPosition.ToXna(), InterfaceViewMatrix).ToModel();
            //return Vector2.Transform(aPosition + new Vector2(viewport.X, viewport.Y), ViewMatrix);
        }

        public Vector VirtualToWorld(Vector aPosition)
        {
            return aPosition + Position;
        }

        private Matrix GetViewMatrix(float aParallaxFactor)
        {
            return Matrix.CreateTranslation(new Vector3((-Position * aParallaxFactor).ToXna(), 0f)) *
                   Matrix.CreateTranslation(new Vector3((-Center).ToXna(), 0f)) *
                   Matrix.CreateScale(mZoom, mZoom, 1) *
                   Matrix.CreateTranslation(new Vector3(Center.ToXna(), 0f));
        }

        private void ClampToBounds()
        {
            if (Bounds == Rectangle.Empty)
                return;

            Position = new Vector(Math.Clamp(Position.X, Bounds.X, Bounds.X + Bounds.Width - mViewport.VirtualWidth),
                                  Math.Clamp(Position.Y, Bounds.Y, Bounds.Y + Bounds.Height - mViewport.VirtualHeight));

        }
    }
}
