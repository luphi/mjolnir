﻿using Android.Views;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Mjolnir.Client.Common;
using Mjolnir.Client.Common.Disk;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using InputEvent = Mjolnir.Client.Common.Input.InputEvent;

namespace Mjolnir.Client.Android
{
    public class AndroidGame : MjolnirGame
    {
        private readonly ConcurrentDictionary<int, TouchOrigin> mTouches = new ConcurrentDictionary<int, TouchOrigin>();
        private readonly ConcurrentQueue<View.KeyEventArgs> mKeyEvents = new ConcurrentQueue<View.KeyEventArgs>();
        //private bool mIsSwiping = false;

        public AndroidGame(MjolnirActivity aActivity, Configuration aConfiguration)
            : base(/* aIsDesktop = */ false, /* aConfiguration = */ aConfiguration)
        {
            // Register some Android-specific classes starting with the options screen.
            mServiceCollection.AddTransient<Screens.OptionsScreen>();
            // The common TextInput UI control is only partially complete. The remainder, which
            // is platform-specific, is implemented within this project's child TextInput.
            mServiceCollection.AddTransient<Common.UI.TextInput, UI.TextInput>();
            // The TextInput, when focused, will make use of the (on-screen) Soft Keyboard. Showing
            // and hiding the keyboard is done through the Activity.
            mServiceCollection.AddSingleton(aActivity);
            // Prepare a list of Screens to be used as the main menu entries, and subsequent
            // screens when selected. The main menu screen will resolve this when it adds entries.
            mServiceCollection.AddSingleton(new Common.Screens.MainMenuScreen.Options
            {
                Entries = new List<Type>
                {
                    typeof(Common.Screens.SingleplayerScreen),
                    typeof(Common.Screens.MultiplayerScreen),
                    typeof(Screens.OptionsScreen)
                }
            });
        }

        protected override void Initialize()
        {
            IsMouseVisible = false;

            View view = Services.GetService<View>();
            view.KeyPress += OnKeyPress; // Also receives back button events.

            base.Initialize();
        }

        /// <summary>
        /// This method checks for 
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Update(GameTime gameTime)
        {
            // Get the states of all ongoing touches on the screen (basically, that's one per finger).
            TouchCollection touches = TouchPanel.GetState();
            foreach (TouchLocation touch in touches)
            {
                // If the user removed their finger or touch was invalidated for some reason.
                if ((touch.State == TouchLocationState.Released) || (touch.State == TouchLocationState.Invalid))
                {
                    // Remove the touch's origin from the map effectively forgetting it ever happened.
                    // Note: in testing, all touches were seen with an associated release state.
                    mTouches.TryRemove(touch.Id, out _); // Remove it and ignore the out parameter.
                }
                // If the user placed a finger on the screen just now.
                else if (touch.State == TouchLocationState.Pressed)
                {
                    // Remember the ID along with where the touch was first made.
                    mTouches[touch.Id] = new TouchOrigin { Position = touch.Position };
                }

                switch (touch.State)
                {
                    case TouchLocationState.Pressed:
                        // TODO comment
                        //Task.Run(async () =>
                        //{
                        //    await Task.Delay(100);
                        //    // If not swiping.
                        //    if (!mIsSwiping)
                        //    {
                                // A touch press is equivalent to a mouse (left) button click so queue such an event.
                                mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.MOUSE_DOWN)
                                {
                                    Position = touch.Position.ToModel()
                                });
                        //    }
                        //});
                        break;
                    case TouchLocationState.Released:
                        // A touch release is equivalent to a mouse (left) button release so queue such an event.
                        // It is possible that the user moved his/her finger in between the precending press
                        // event but that's not handed with the Moved state.
                        mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.MOUSE_UP)
                        {
                            Position = touch.Position.ToModel()
                        });
                        // TODO comment
                        //mIsSwiping = false;
                        break;
                    case TouchLocationState.Moved:
                        // If the map of touch origins contains a touch with this ID (it does), get the origin.
                        if (mTouches.TryGetValue(touch.Id, out TouchOrigin origin))
                        {
                            // If the distance between the current position of the user's finger and the
                            // original position, when first pressed, is greater than or equal to three pixels.
                            if (Math.Sqrt(Math.Pow(touch.Position.X - origin.Position.X, 2.0) +
                                          Math.Pow(touch.Position.Y - origin.Position.Y, 2.0)) >= 3.0)
                            {
                                // That's enough distance to consider this a swipe.
                                //mIsSwiping = true;

                                // If the mapped origin is not already flagged as a swipe.
                                if (!origin.IsSwipe)
                                {
                                    // Flag it and report a swipe with a delta calculated from this most
                                    // recent touch and the *original* touch (when Pressed).
                                    origin.IsSwipe = true;
                                    mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.SWIPE)
                                    {
                                        Delta = (touch.Position - origin.Position).ToModel()
                                    });
                                }
                                // If Monogame can give us a copy of the most recent TouchLocation prior
                                // to this one (it can), get that TouchLocation.
                                else if (touch.TryGetPreviousLocation(out TouchLocation previousTouch))
                                {
                                    // Report a swipe with a delta calculated from this most recent touch
                                    // and the previous one.
                                    mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.SWIPE)
                                    {
                                        Delta = (touch.Position - previousTouch.Position).ToModel()
                                    });
                                }
                            }
                        }
                        break;
                }
            }

            // For each on-screen keyboard event.
            while (mKeyEvents.TryDequeue(out View.KeyEventArgs args))
            {
                // This key code corresponds to a back event. That is, the user hit the back button
                // (the triangle button at the bottom of the screen) or performed the equivalent gesture.
                if (args.KeyCode == Keycode.Back)
                    mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.BACK));
                // This key code represents the on-screen keyboards enter, or return, button.
                else if (args.KeyCode == Keycode.Enter)
                {
                    // Because we don't really recognize down and up states, we'll push both a down and
                    // up event to avoid any confusion. Otherwise, it may appear as if the enter key is
                    // permanently held down.
                    mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.KEY_DOWN)
                    {
                        Key = Keys.Enter
                    });
                    mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.KEY_UP)
                    {
                        Key = Keys.Enter
                    });
                }
                // This key code represents the on-screen keyboards backspace key. Despite its name
                // coming from "delete," this key is associated with removing a character to the left
                // of the cursor (i.e. backspace), not to the right (i.e. delete).
                else if (args.KeyCode == Keycode.Del)
                {
                    // For the same reason as the above, we'll push both a down and up event here.
                    mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.KEY_DOWN)
                    {
                        Key = Keys.Back
                    });
                    mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.KEY_UP)
                    {
                        Key = Keys.Back
                    });
                }
                // If the input contains some sort of recognize letter.
                else if (args.Event.UnicodeChar != 0)
                {
                    // Push the event as a single-character input.
                    mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.CHAR)
                    {
                        Character = Convert.ToChar(args.Event.UnicodeChar)
                    });
                }
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// Called by Android when the on-screen keyboard has been used to enter some sort of text-related
        /// input such as a letter, backspace, or even a system-level back event.
        /// </summary>
        /// <param name="aSender"></param>
        /// <param name="aArgs"></param>
        private void OnKeyPress(object aSender, View.KeyEventArgs aArgs)
        {
            // If the key pressed isn't unidentifiable and the key was pressed just now.
            if ((aArgs.KeyCode != Keycode.Unknown) && (aArgs.Event.Action == KeyEventActions.Down))
            {
                // Push the event to the queue that will be checked momentarily by Update().
                mKeyEvents.Enqueue(aArgs);
            }
        }

        private class TouchOrigin
        {
            public Vector2 Position;
            public bool IsSwipe = false;
        }
    }
}