using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Mjolnir.Client.Common;
using Mjolnir.Client.Common.Disk;
using Microsoft.Xna.Framework;
using NLog;
using NLog.Config;
using System.IO;
using System.Reflection;
using System.Xml;
using Mjolnir.Common;
using Android.Views.InputMethods;

namespace Mjolnir.Client.Android
{
   [Activity(
       Label = Constants.Name,
       MainLauncher = true,
       Icon = "@drawable/icon",
       //Icon = "../Content/Exported/Icon/Icon.png",
       AlwaysRetainTaskState = true,
       LaunchMode = LaunchMode.SingleInstance,
       ScreenOrientation = ScreenOrientation.Landscape,
       ConfigurationChanges = ConfigChanges.Keyboard |
                              ConfigChanges.KeyboardHidden |
                              ConfigChanges.ScreenSize
   )]
   public class MjolnirActivity : AndroidGameActivity
   {
      private View mView = null;
      private InputMethodManager mInputMethodManager = null;

      public void ShowKeyboard()
      {
         mInputMethodManager?.ShowSoftInput(mView, ShowFlags.Forced);
         mInputMethodManager?.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.ImplicitOnly);
      }

      public void HideKeyboard()
      {
         mInputMethodManager?.HideSoftInputFromWindow(mView.WindowToken, HideSoftInputFlags.None);
      }

      protected override void OnCreate(Bundle aBundle)
      {
         base.OnCreate(aBundle);

         // There's an NLog configuration file included in this project as an embedded resource
         // (using the Embedded Resource build cation). In order for NLog's LogManager to find it,
         // it needs to be given a handle to the embedded file. Embedded resources are retrieved
         // using GetManifestResourceStream().
         Assembly assembly = GetType().Assembly;
         Stream stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name}.NLog.config");
         LogManager.Configuration = new XmlLoggingConfiguration(XmlReader.Create(stream), null);
         ILogger log = LogManager.GetCurrentClassLogger();

         Configuration configuration = new Configuration
         {
            ViewportWidth = Resources.DisplayMetrics.WidthPixels,
            ViewportHeight = Resources.DisplayMetrics.HeightPixels,
            IsWindowed = true,
            IsDeveloper = true,
            RegistryHostname = "192.168.0.107"
         };

         if (configuration.IsWindowed)
         {
            log.Info("Using windowed mode");
         }
         else
         {
            log.Info("Using full screen mode");
         }
         log.Info($"Using viewport resolution {configuration.ViewportWidth}x{configuration.ViewportHeight}");
         log.Info($"Using preferred virtual (rendered) width {configuration.PreferredVirtualWidth}");
         log.Info($"Using preferred virtual (rendered) height {configuration.PreferredVirtualHeight}");
         foreach (Configuration.HostnamePasswordPair pair in configuration.AddressPasswordPairs)
         {
            log.Info($"Remembering admin password \"{pair.AdminPassword}\" for server \"{pair.Hostname}\"");
         }
         if (configuration.IsDeveloper)
         {
            log.Info("Including developer tools");
         }

         MjolnirGame game = new AndroidGame(this, configuration);
         mView = game.Services.GetService<View>();
         mInputMethodManager = Application.GetSystemService(InputMethodService) as InputMethodManager;
         SetContentView(mView);
         game.Run();
      }
   }
}
