﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Mjolnir.Client.Android.UI
{
   public class TextInput : Common.UI.TextInput
   {
      private readonly MjolnirActivity mActivity;

      public override bool IsFocused
      {
         get => base.IsFocused;
         set
         {
            if (mIsFocused == value)
            {
               return;
            }

            if (value == true)
            {
               mActivity.ShowKeyboard();
            }
            else
            {
               mActivity.HideKeyboard();
            }

            base.IsFocused = value;
         }
      }

      public TextInput(IServiceProvider aServiceProvider) : base(aServiceProvider)
      {
         mActivity = aServiceProvider.GetService<MjolnirActivity>();
      }
   }
}