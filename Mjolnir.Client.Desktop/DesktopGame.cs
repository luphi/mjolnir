﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Mjolnir.Client.Common;
using Mjolnir.Client.Common.Disk;
using Mjolnir.Client.Common.Input;
using Mjolnir.Common.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Mjolnir.Client.Desktop
{
   public class DesktopGame : MjolnirGame
   {
      private readonly ConcurrentQueue<TextInputEventArgs> mTextInputs = new ConcurrentQueue<TextInputEventArgs>();
      private Rectangle mBounds = Rectangle.Empty;

      private Point mMousePosition = Point.Zero;
      private bool mIsMousePressed = false;
      private int mScrollValue = 0;

      private Keys[] mKeysWeCareAbout = new Keys[0];
      private IEnumerable<Keys> mPressedKeys = new Keys[0];
      private int mPressedKeyCount = 0;

      public DesktopGame(Configuration aConfiguration)
          : base(/* aIsDesktop = */ true, /* aConfiguration = */ aConfiguration)
      {
         // Register some desktop-specific classes starting with the options screen.
         mServiceCollection.AddTransient<Screens.OptionsScreen>();

         // The common TextInput UI control is only partially complete. The remainder, which
         // is platform-specific, is implemented within this project's child TextInput.
         mServiceCollection.AddTransient<Common.UI.TextInput, UI.TextInput>();

         // Prepare a list of Screens to be used as the main menu entries, and subsequent
         // screens when selected. The main menu screen will resolve this when it adds entries.
         mServiceCollection.AddSingleton(new Common.Screens.MainMenuScreen.Options
         {
            Entries = new List<Type>
            {
               typeof(Common.Screens.SingleplayerScreen),
               typeof(Common.Screens.MultiplayerScreen),
               typeof(Screens.OptionsScreen),
               typeof(Common.Screens.ExitScreen)
            }
         });
      }

      protected override void Initialize()
      {
         IsMouseVisible = true;
         Window.TextInput += OnTextInput;

         // Make a list of keys that might be used for input response throughout this game. This list
         // is likely excessive
         mKeysWeCareAbout = new Keys[]
         {
            // Letter keys.
            Keys.A, Keys.B, Keys.C, Keys.D, Keys.E, Keys.F, Keys.G, Keys.H, Keys.I, Keys.J, Keys.K,
            Keys.L, Keys.M, Keys.N, Keys.O, Keys.P, Keys.Q, Keys.R, Keys.S, Keys.T, Keys.U, Keys.V,
            Keys.W, Keys.X, Keys.Y, Keys.Z,
            // Arrow keys.
            Keys.Up, Keys.Down, Keys.Left, Keys.Right,
            // Number keys (the main ones above the letters, not the numpad ones).
            Keys.D0, Keys.D1, Keys.D2, Keys.D3, Keys.D4, Keys.D5, Keys.D6, Keys.D7, Keys.D8, Keys.D9,
            // Numpad number keys.
            Keys.NumPad0, Keys.NumPad1, Keys.NumPad2, Keys.NumPad3, Keys.NumPad4, Keys.NumPad5,
            Keys.NumPad6, Keys.NumPad7, Keys.NumPad8, Keys.NumPad9, 
            // Other common keys.
            Keys.Space, Keys.Enter, Keys.Back, Keys.Delete,
         };

         base.Initialize();
      }

      protected override void LoadContent()
      {
         mBounds = new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);

         base.LoadContent();
      }

      protected override void Update(GameTime aGameTime)
      {
         // Poll the mouse for its current state and check for changes we care about.
         MouseState mouseState = Mouse.GetState();
         Point previousMousePosition = mMousePosition;
         mMousePosition = new Point(mouseState.X, mouseState.Y);

         // If the mouse has been moved AND its been moved somewhere within the window.
         if ((mMousePosition != previousMousePosition) && (mBounds.Contains(mMousePosition) == true))
         {
            // Push a hover event to repsent the mouse has been moved but not clicked.
            mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.HOVER)
            {
               Position = new Vector(mMousePosition.X, mMousePosition.Y)
            });
         }

         bool wasMousePressed = mIsMousePressed;
         mIsMousePressed = mouseState.LeftButton == ButtonState.Pressed;
         if ((mIsMousePressed == true) && (wasMousePressed == false))
         {
            // If the mouse's left button was pressed just now.

            // Push a click (down) event.
            mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.MOUSE_DOWN)
            {
               Position = new Vector(mMousePosition.X, mMousePosition.Y)
            });
         }
         else if ((mIsMousePressed == false) && (wasMousePressed == true))
         {
            // If the mouse's left button was released just now.

            // Push an unclick (up) event.
            mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.MOUSE_UP)
            {
               Position = new Vector(mMousePosition.X, mMousePosition.Y)
            });
         }

         int previousScrollValue = mScrollValue;
         mScrollValue = mouseState.ScrollWheelValue;
         // If the mouse's scroll wheel is positioned at a different detent (AKA if the
         // user spun the scroll wheel).
         if (previousScrollValue != mScrollValue)
         {
            // Push a scroll event and pass the scroll value with it. The scroll value,
            // which uses unspecific units, indicates how much the scroll wheel was
            // spun since the last poll.
            mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.SCROLL)
            {
               Delta = new Vector(0.0, -(mScrollValue - previousScrollValue))
            });
         }

         int previousPressedKeyCount = mPressedKeyCount;
         mPressedKeyCount = Keyboard.GetState().GetPressedKeyCount();
         // If the number of (keyboard) keys being pressed in this iteration differs from the
         // number in the last iteration.
         if (previousPressedKeyCount != mPressedKeyCount)
         {
            // Remember the keys that were pressed (held down) the last iteration.
            IEnumerable<Keys> previouslyPressedKeys = mPressedKeys;

            // Get the keys that are pressed this iteration.
            mPressedKeys = Keyboard.GetState().GetPressedKeys().Intersect(mKeysWeCareAbout);

            // For each key that is pressed now but was not pressed in the last iteration.
            foreach (Keys key in mPressedKeys.Except(previouslyPressedKeys))
            {
               mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.KEY_DOWN)
               {
                  Key = key
               });
            }

            // For each key that is not pressed now but was pressed in the last iteration.
            foreach (Keys key in previouslyPressedKeys.Except(mPressedKeys))
            {
               mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.KEY_UP)
               {
                  Key = key
               });
            }
         }

         // For each text-related key resulting from a key press or held key.
         while (mTextInputs.TryDequeue(out TextInputEventArgs args) == true)
         {
            if (args.Key == Keys.Escape)
            {
               // If the escape key was pressed, or remains held.

               // We arbitrarily treat the escape key similar to a mobile device's back event.
               mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.BACK));
            }
            else if ((char.IsWhiteSpace(args.Character) == true) ||
                     (char.IsLetterOrDigit(args.Character) == true) ||
                     (char.IsPunctuation(args.Character) == true) ||
                     (char.IsSymbol(args.Character) == true))
            {
               // If the character is white space (e.g. ' '), a letter (e.g. 'a'), a digit (e.g. '1'),
               // punctuation (e.g. '.'), or a symbol (e.g. '|').

               // Push a character event where the chacacter will either be some sort of letter
               // or a single space.
               mInputEventsQueue.Enqueue(new InputEvent(InputEvent.Opcode.CHAR)
               {
                  Character = args.Character
               });
            }
         }

         base.Update(aGameTime);
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnTextInput(object aSender, TextInputEventArgs aArgs)
      {
         mTextInputs.Enqueue(aArgs);
      }
   }
}
