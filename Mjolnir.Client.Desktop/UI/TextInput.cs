﻿using Microsoft.Xna.Framework.Input;
using Mjolnir.Client.Common.Input;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Mjolnir.Client.Desktop.UI
{
   /// <summary>
   /// Desktop (keyboard-based input) implementation of the common TextInput.
   /// </summary>
   public class TextInput : Common.UI.TextInput
   {
      private CancellationTokenSource mTokenSource = null;
      private bool mIsLeftHeld = false;
      private bool mIsRightHeld = false;

      public TextInput(IServiceProvider aServiceProvider) : base(aServiceProvider)
      {
      }

      public override void Respond(InputEvent aInput)
      {
         if (aInput.Op == InputEvent.Opcode.KEY_DOWN)
         {
            if (aInput.Key == Keys.Left)
            {
               mIsLeftHeld = true;
            }

            if (aInput.Key == Keys.Right)
            {
               mIsRightHeld = true;
            }

            // If the left arrow was just pressed.
            if (aInput.Key == Keys.Left)
            {
               // Cancel any ongoing task just in case.
               mTokenSource?.Cancel();
               mTokenSource = null;

               // If the cursor isn't already as far left as it can go.
               if (mCursorIndex > 0)
               {
                  // Immediately move the cursor to the left by one character.
                  mCursorIndex -= 1;
               }

               // Graphically move the cursor to match the index.
               PositionCursor();
            }

            // If the right arrow was just pressed.
            if (aInput.Key == Keys.Right)
            {
               // If the cursor isn't already as far right as it can go.
               if (mCursorIndex < mText.Value.Length)
               {
                  // Immediately move the cursor to the right by one character.
                  mCursorIndex += 1;
               }

               // Graphically move the cursor to match the index.
               PositionCursor();
            }

            if ((aInput.Key == Keys.Left) || (aInput.Key == Keys.Right))
            {
               // Spawn a task to continually move the cursor while either key is held.
               mTokenSource = new CancellationTokenSource();
               CancellationToken token = mTokenSource.Token;
               Task.Run(async () =>
               {
                  // Apply an initial delay before trying moving the cursor again.
                  await Task.Delay(500);

                  // While either left or right arrow key is held.
                  while (token.IsCancellationRequested == false)
                  {
                     if (mIsLeftHeld == true)
                     {
                        if (mCursorIndex > 0)
                        {
                           mCursorIndex -= 1;
                        }

                        PositionCursor();
                     }
                     if (mIsRightHeld == true)
                     {
                        if (mCursorIndex < mText.Value.Length)
                        {
                           mCursorIndex += 1;
                        }

                        PositionCursor();
                     }

                     // Apply a delay before moving the cursor again (if either key is held).
                     await Task.Delay(100);
                  }
               });
            }
         }
         else if (aInput.Op == InputEvent.Opcode.KEY_UP)
         {
            if (aInput.Key == Keys.Left)
            {
               mIsLeftHeld = false;
            }

            if (aInput.Key == Keys.Right)
            {
               mIsRightHeld = false;
            }

            if ((mIsLeftHeld == false) && (mIsRightHeld == false))
            {
               mTokenSource?.Cancel();
               mTokenSource = null;
            }
         }

         base.Respond(aInput);
      }
   }
}
