﻿using Mjolnir.Client.Common;
using Mjolnir.Client.Common.Disk;
using NLog;
using System;

namespace Mjolnir.Client.Desktop
{
   public static class Program
   {
      private static readonly ILogger sLog = LogManager.GetCurrentClassLogger();

      [STAThread]
      static void Main()
      {
         // Read, or create, a configuration to be used by the game.
         Configuration configuration = Configuration.Load();

         if (configuration == null)
         {
            sLog.Warn("No configuration file was found. A default one will be used and stored.");
            configuration = new Configuration();
            Configuration.Store(configuration);
         }

         if (configuration.IsWindowed)
            sLog.Info("Using windowed mode");
         else
            sLog.Info("Using full screen mode");
         sLog.Info($"Using viewport resolution {configuration.ViewportWidth}x{configuration.ViewportHeight}");
         sLog.Info($"Using preferred virtual (rendered) width {configuration.PreferredVirtualWidth}");
         sLog.Info($"Using preferred virtual (rendered) height {configuration.PreferredVirtualHeight}");
         foreach (Configuration.HostnamePasswordPair pair in configuration.AddressPasswordPairs)
            sLog.Info($"Remembering admin password \"{pair.AdminPassword}\" for server \"{pair.Hostname}\"");
         sLog.Info($"Will attempt to use registry at {configuration.RegistryHostname}:{configuration.RegistryPort}");
         if (configuration.IsDeveloper)
            sLog.Info("Including developer tools");

         using MjolnirGame game = new DesktopGame(configuration);
         game.Run();
      }
   }
}
