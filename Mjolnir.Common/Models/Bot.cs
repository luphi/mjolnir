﻿using Mjolnir.Common.Messages;
using Mjolnir.Common.Physics;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace Mjolnir.Common.Models
{
   [DataContract]
   public class Bot : Player, IDisposable
   {
      private const double sMinimumBotTurnDurationInSeconds = 3.0;

      private const string NAME_PREFIX = "[Bot] ";

      public override bool IsBot => true;

      public Bot(uint aID) : base(aID, GetName(), false)
      {
         IsReady = true;
         MobileID = Mobile.RandomID;
      }

      public Bot(Bot aOther) : base(aOther)
      {
      }

      public override Body Clone()
      {
         return new Bot(this);
      }

      public void Dispose()
      {
         // Return this bot's name to the list of available ones.
         ReturnName();
      }

      public void OnStateTransition(StateEnum aNextState)
      {
         switch (aNextState)
         {
            case StateEnum.LOBBY:

               break;
            case StateEnum.PRE_GAME:

               break;
            case StateEnum.IN_GAME:

               break;
            case StateEnum.POST_GAME:

               break;
         }
      }

      public async Task<List<Message>> OnTurnAsync(CancellationToken aToken,
                                                   World aWorld,
                                                   IEnumerable<Player> aTargets)
      {
         // If there are no targets (presumably because all opponents are waiting to respawn).
         if (aTargets.Count() == 0)
         {
            // Might as well skip the turn and not take up any time. That said, we'll make it
            // look like the bot is thinking for a second. Hopefully players will see the bot
            // "thinking" as an indication that it's taking its turn, to avoid confusion.

            return await SkipTurnAsync(aToken);
         }

         Stopwatch stopwatch = Stopwatch.StartNew();
         Random random = new Random();
         List<Task> tasks = new List<Task>();
         ConcurrentDictionary<FireArgs, Events> possibleTurns = new ConcurrentDictionary<FireArgs, Events>();

         foreach (Player target in aTargets)
         {
            foreach (Direction direction in Enum.GetValues(typeof(Direction)))
            {
               for (double aimInRadians = MinAimInRadians;
                    aimInRadians <= MaxAimInRadians;
                    aimInRadians += (MaxAimInRadians - MinAimInRadians) / 10.0)
               {
                  for (double powerAsPercent = 0.1; powerAsPercent <= 1.0; powerAsPercent += 0.1)
                  {
                     FireArgs args = new FireArgs
                     {
                        Direction = direction,
                        AimInRadians = aimInRadians,
                        PowerAsPercent = powerAsPercent,
                     };

                     tasks.Add(Task.Run(async () =>
                     {
                        World worldClone = aWorld.Clone();
                        Bot botClone = worldClone.Players.FirstOrDefault(p => p.ID == ID) as Bot;
                        botClone.Direction = args.Direction;
                        botClone.AimInRadians = args.AimInRadians;

                        if (worldClone.Fire(botClone, args.PowerAsPercent, out _) == true)
                        {
                           Events events = await worldClone.StartAsync();
                           if (events != null)
                           {
                              possibleTurns[args] = events;
                           }
                        }
                     }));
                  }
               }
            }
         }

         // Wait for all tasks to finish. These tasks run in parallel and simulate various
         // possible turns as fast as the machine will allow.
         mLog.Warn($"awaiting {tasks.Count} tasks");
         await Task.WhenAll(tasks.ToArray());
         mLog.Warn($"waited {stopwatch.Elapsed.TotalSeconds} seconds with " +
                   $"{Constants.TurnTimeoutInSeconds - stopwatch.Elapsed.TotalSeconds} seconds remaining");

         Dictionary<FireArgs, int> damageDeltas = new Dictionary<FireArgs, int>();
         foreach (KeyValuePair<FireArgs, Events> pair in possibleTurns)
         {
            int damageToTargets = 0;
            int damageToAllies = 0;
            foreach (Damage damage in pair.Value.Damages)
            {
               if (aTargets.Select(p => p.ID).Contains(damage.PlayerID) == true)
               {
                  damageToTargets += damage.Amount;
               }
               else
               {
                  damageToAllies += damage.Amount;
               }
            }
            damageDeltas[pair.Key] = damageToTargets - damageToAllies;
         }

         IEnumerable<int> tops = damageDeltas.Select(p => p.Value)
                                             .OrderByDescending(d => d)
                                             .Take(25);
         mLog.Warn($"top 25 damage deltas = {string.Join(", ", tops)}");

         // TODO: this whole thing, basically
         // TODO: choose the best run by greatest damage to targets and least to allies
         // TODO: configure the clone using the fire parameters of the beste run
         // TODO: return a PlayerMessage of the clone and FireMessage with remaining parameters
         if (tops.Count() == 0)
         {
            return await SkipTurnAsync(aToken);
         }

         KeyValuePair<FireArgs, Events> top = possibleTurns.FirstOrDefault();
         mLog.Warn($"chose arguments {top.Key} with events {top.Value}");
         Bot clone = Clone() as Bot;
         clone.Direction = top.Key.Direction;
         clone.AimInRadians = top.Key.AimInRadians;

         stopwatch.Stop();

         // If the minium "thinking" time hasn't already passed while processing potential moves.
         if (stopwatch.Elapsed.TotalSeconds < sMinimumBotTurnDurationInSeconds)
         {
            // Figure out the remaining time and wait it out.
            double remainingDelayInSeconds = sMinimumBotTurnDurationInSeconds - stopwatch.Elapsed.TotalSeconds;
            await Task.Delay((int)(remainingDelayInSeconds * 1000.0), aToken);
         }

         return new List<Message>
         {
            new PlayerMessage(clone),
            new FireMessage(ID, top.Key.PowerAsPercent)
         };
      }

      /// <summary>
      /// Get a list of parameters for Fire()
      /// </summary>
      /// <param name="aPlayerTarget"></param>
      /// <returns></returns>
      public bool TryGetFireArgsForPoint(Vector aTarget,
                                         Vector aAcceleration,
                                         double aApexInPixels,
                                         out Direction aDirection,
                                         out double aAimInRadians,
                                         out double aPowerAsPercent)
      {
         // Default outputs in case of errors or failure.
         aDirection = Direction.LEFT;
         aAimInRadians = 0.0;
         aPowerAsPercent = 0.0;

         Shot shot = Shot;
         if (shot == null)
         {
            return false;
         }

         // Create and slightly configure a clone of this bot. We'll need it for its getters and to
         // hold some values.
         Bot clone = Clone() as Bot;
         clone.Direction = aTarget.X > X ? Direction.RIGHT : Direction.LEFT;

         // Get the origin point: the point a project would be placed at when launched. Note that
         // this getter uses the aim value which will almost certainly change a little soon.
         Vector origin = clone.ProjectilePosition;
         // Move it down and right to account for bodies being positioned by their top-left corners.
         origin += new Vector(shot.BodyRadius, shot.BodyRadius);

         // Calculate the squared value of the maximum speed. This is an easy optimization that avoids
         // relatively costly square root calculations.
         double maxSpeedSquared = Math.Pow(Constants.FullPowerAsSpeedInPixelsPerSecond, 2.0);

         // Calculate the initial Y velocity as v_0y = sqrt(2 * g * apex_height).
         // This is derived from the common motion equations:
         // v_y^2 = v_0y^2 - (2 * g * (y - y0))   and   y = (v_0Y * t) - (0.5 * g * t^2).
         // Substituting values for the apex, meaning v_y = 0, allows us to solve the former
         // equation for 'y' which is the Y coordinate at the apex.
         // Substituing the solution for 'y' into the latter equation and solving for v_0y
         // gives the equation used below.
         double v_0y = Math.Sqrt(2.0 * aAcceleration.Y * aApexInPixels);

         // We'll use the quadratic equation to solve for total flight time. Here, we create
         // some variables for the different components of the quadratic equation.
         double a = Constants.Gravity / 2.0;
         double b = -v_0y;
         double c = aTarget.Y - origin.Y;
         double discriminant = (b * b) - (4.0 * a * c); // The part under the square root in the quadratic
                                                        // equation is called the "discriminant," FYI.

         // Apply the components to get the two possible solutions for the time, then choose one.
         double sqrtDiscriminant = Math.Sqrt(discriminant);
         double t_1 = (-b + sqrtDiscriminant) / (2.0 * a);
         double t_2 = (-b - sqrtDiscriminant) / (2.0 * a);
         double t = t_1 > t_2 ? t_1 : t_2; // Choose the one with the greater flight time.

         // The X velocity is simpler, especially now that the time has been calculated. Using the
         // motion equation:
         // d_x = d_x0 + (v_x0 * t) + (0.5 * a_x * t^2)
         // with d_x0 being zero, d_x being target.X - origin.X, and solving for v_x0, we get
         // v_x0 = ((target.x - origin.x) / t) - (0.5 * a_x * t).
         double v_0x = ((aTarget.X - origin.X) / t) - (0.5 * aAcceleration.X * t);

         // Create a velocity vector from the components and use it to calculate the necessary
         // trajectory angle. This angle more or may not be possible.
         // Note: The math above uses traditional equations of motion. Very importantly, that means
         // -Y is down in said equations while we use -Y as up. So, just flip the Y component.
         Vector velocity = new Vector(v_0x, -v_0y);
         double wouldBeTrajectoryInRadians = velocity.AngleInRadians;

         // If the calculated trajectory, as the bot's aim, is within the shot's true angle
         // and the calculated speed does not exceed the maximum, full-power speed.
         if ((clone.TrySetTrajectory(wouldBeTrajectoryInRadians) == true) &&
             (velocity.LengthSquared <= maxSpeedSquared))
         {
            Vector predictedPosition = new Vector(origin.X + (velocity.X * t) + (0.5 * aAcceleration.X * t * t),
                                      origin.Y + (velocity.Y * t) + (0.5 * aAcceleration.Y * t * t));
            mLog.Debug($"{nameof(TryGetFireArgsForPoint)}() predicted final position = {predictedPosition}");
            Vector predictedVelocity = new Vector(velocity.X + (aAcceleration.X * t),
                                                  velocity.Y + (aAcceleration.Y * t));
            mLog.Debug($"{nameof(TryGetFireArgsForPoint)}() predicted final velocity = {predictedVelocity}");

            aDirection = clone.Direction;
            aAimInRadians = clone.AimInRadians;
            aPowerAsPercent = velocity.Length / Constants.FullPowerAsSpeedInPixelsPerSecond;
            return true;
         }

         return false;
      }


      #region Private helper methods and class(es)

      private async Task<List<Message>> SkipTurnAsync(CancellationToken aToken)
      {
         try
         {
            // Simply wait.
            await Task.Delay((int)(sMinimumBotTurnDurationInSeconds * 1000.0), aToken);
         }
         catch (TaskCanceledException)
         {
            // Tasks may be cancelled, unlikely though that may be. It's not an error.
            mLog.Debug($"Bot \"{Name}\" had its turn cancelled while waiting to skip");
         }
         catch (Exception e)
         {
            // Exceptions other than TaskCancelledException should be logged.
            mLog.Error(e, $"Bot \"{Name}\" ran into an unexplaind error while waiting to skip");
         }

         // Return a "skip this turn" message.
         return new List<Message>
         {
            new Message(Message.Opcode.SKIP)
         };
      }

      /// <summary>
      /// Attempts to set this bot's aim, in radians, from the given trajectory, in radians, iff
      /// the trajectory is within the current true angle range (considering direction and pitch).
      /// Note that the aim is independent of other factors and ranges, at most, from 0 to 2*pi.
      /// Also note that the trajectory is in typical trig. coordinates. Refer to any traditional
      /// unit circle for reference.
      /// </summary>
      /// <param name="aTrajectoryInRadians"></param>
      /// <returns></returns>
      private bool TrySetTrajectory(double aTrajectoryInRadians)
      {
         // This method essentially reverses the steps of Player's Trajectory getter.
         // Step 1: Create a vector from the trajectory. We'lll be transforming this as necessary
         // in order to fit it into the [0, 2*pi] range.
         Vector trajectory = Vector.FromRadians(aTrajectoryInRadians);

         // Step 2: Flip the Y axis. In this way, the vector works with the graphical/physical coordinates
         // in which -Y is up.
         trajectory.Y = -trajectory.Y;

         // Step 3: Rotate the vector to account for the angle at which the player is standing, Pitch.
         trajectory = Vector.Rotate(trajectory, -(Vector.UpInRadians - Pitch));

         // Step 4: If the player is facing left, mirror the vector across the Y axis so it's rightward.
         if (Direction == Direction.LEFT)
         {
            trajectory.X = -trajectory.X;
         }

         double wouldBeAimInRadians = trajectory.AngleInRadians;
         double distanceFromCenterInRadians = wouldBeAimInRadians.DistanceInRadians(Player.CenterAimInRadians);

         // If the trajectory is *not* within the current true angle range after factoring in the bot's
         // direction and pitch.
         if (distanceFromCenterInRadians > (TrueAngleRangeInRadians / 2.0))
         {
            return false;
         }

         // If we didn't return above, the trajectory is good and can be applied.
         AimInRadians = wouldBeAimInRadians;
         return true;
      }


      #region Name selection

      private static string GetName()
      {
         // Select a name from the available ones randomly.
         string name = sBotNames[new Random().Next(sBotNames.Count)];

         // Remove it from the list of available names.
         sBotNames.Remove(name);

         // Prepend the name with a simple indication that this is a bot.
         return $"{NAME_PREFIX}{name}";
      }

      private void ReturnName()
      {
         sBotNames.Add(Name.Substring(NAME_PREFIX.Length)); // Substring() removes the "[Bot] " name prefix.
      }

      private static readonly List<string> sBotNames = new List<string>
      {
         "Albert", "Allen", "Bert", "Bob", "Cecil", "Clarence", "Elliot", "Elmer", "Ernie", "Eugene",
         "Fergus", "Ferris", "Frank", "Frasier", "Fred", "George", "Graham", "Harvey", "Irwin",
         "Lester", "Marvin", "Neil", "Niles", "Oliver", "Opie", "Toby", "Ulric", "Ulysses", "Uri",
         "Waldo", "Wally", "Wesley", "Yanni", "Yogi", "Yuri"
      };

      #endregion Name selection


      private class FireArgs
      {
         public Direction Direction;
         public double AimInRadians;
         public double PowerAsPercent;

         public override string ToString()
         {
            return $"{{ {nameof(Direction)} = {Direction}, {nameof(AimInRadians)} = {AimInRadians}, " +
                   $"{nameof(PowerAsPercent)} = {PowerAsPercent} }}";
         }
      }

      #endregion Private helper methods and class(es)
   }
}
