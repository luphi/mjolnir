﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Mjolnir.Common.Models
{
   /// <summary>
   /// Utility for determining the order of turns in-game.
   /// This behaves similar to a priority queue with two notable differences:
   /// 1) This is not a queue data type. It's given a list of players and returns
   ///    their turn order but does not have insertion/removal methods.
   /// 2) Entries with equal priority maintain their relative order in contrast
   ///    to the usual priority queue behavior of randomizing order of equal
   ///    entries.
   /// </summary>
   public class TurnList : PropertyChangedBase
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private List<Player> mPlayers = new List<Player>();

      public List<Player> Queue => new List<Player>(mPlayers);

      public Player Head => mPlayers.FirstOrDefault();

      public void Add(Player aPlayer)
      {
         aPlayer.PropertyChanged += OnPropertyChanged;

         lock (mPlayers)
         {
            mPlayers.Add(aPlayer);
         }

         Sort();
      }

      public void Remove(Player aPlayer)
      {
         lock (mPlayers)
         {
            if (mPlayers.Remove(aPlayer) == true)
            {
               aPlayer.PropertyChanged -= OnPropertyChanged;

               Sort();
            }
         }
      }

      public void Clear()
      {
         lock (mPlayers)
         {
            foreach (Player player in mPlayers)
            {
               player.PropertyChanged -= OnPropertyChanged;
            }

            mPlayers.Clear();
         }
         OnPropertyChanged(nameof(TurnList));
      }

      /// <summary>
      /// Randomize the order of the players.
      /// This is intended to be used in cases where the players have zeroized delays.
      /// </summary>
      /// <returns>The new order of the players as an array of IDs.</returns>
      public uint[] Shuffle()
      {
         uint[] order;

         // This is an implementation of the Fisher-Yates shuffle algorithm.
         Random random = new Random();
         lock (mPlayers)
         {
            // If no players have a delay greater than zero (i.e. all of them are zero).
            if (mPlayers.Any(p => p.Delay != 0) == false)
            {
               for (int i = mPlayers.Count - 1; i > 0; --i) // The decrement must happen before the iteration.
               {
                  int j = random.Next(i + 1);
                  Player player = mPlayers[i];
                  player.Delay = 0;
                  mPlayers[i] = mPlayers[j];
                  mPlayers[j] = player;
               }
            }

            order = mPlayers.Select(p => p.ID).ToArray();
         }
         OnPropertyChanged(nameof(TurnList));

         return order;
      }

      /// <summary>
      /// Match the order of the given list of IDs.
      /// </summary>
      /// <param name="aOrder"></param>
      public void Sync(uint[] aOrder)
      {
         lock (mPlayers)
         {
            // If the two enumerables have the same number of items.
            if (aOrder.Length == mPlayers.Count)
            {
               // This is an O(n^2) operation. Is there a better way? Probably not but consider trying.
               mPlayers = mPlayers.OrderBy(p => Array.IndexOf(aOrder, p.ID)).ToList();
            }
         }

         OnPropertyChanged(nameof(TurnList));
      }

      public override string ToString()
      {
         List<string> playersAndDelays = new List<string>();

         lock (mPlayers)
         {
            foreach (Player player in mPlayers)
            {
               playersAndDelays.Add($"{player.Name}: {player.Delay}");
            }
         }

         return $"{{ {string.Join(", ", playersAndDelays)} }}";
      }

      private void Sort()
      {
         lock (mPlayers)
         {
            // Note: OrderBy() is a stable sort. It maintains the order of items with
            // equal values.
            // If player A is in line before player B and the two have the same delay,
            // player A will remain in front of player B after sorting.
            mPlayers = mPlayers.OrderBy(p => p.Delay).ToList();
         }

         OnPropertyChanged(nameof(TurnList));
      }

      private void OnPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
      {
         if (aArgs.PropertyName == nameof(Player.Delay))
         {
            Sort();
         }
      }
   }
}
