﻿using NLog;
using System.Runtime.Serialization;

namespace Mjolnir.Common.Models
{
   /// <summary>
   /// Represents a physical body on which forces (e.g. gravity) may act.
   /// </summary>
   [DataContract]
   [KnownType(typeof(Player))]
   public class Body : PropertyChangedBase
   {
      private static uint sBodyCounter = 0;

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();

      /// <summary>
      /// Unique ID to represent the body. In contrast to the similar serialized property, ID,
      /// this ID is only to be used locally for Body comparisons.
      /// </summary>
      public uint _id { get; } = sBodyCounter++;


      #region Serialized properties

      [DataMember]
      public double X
      {
         get => mX;
         set => SetProperty(ref mX, value);
      }
      protected double mX = 0.0;

      [DataMember]
      public double Y
      {
         get => mY;
         set => SetProperty(ref mY, value);
      }
      protected double mY = 0.0;

      [DataMember]
      public double Radius
      {
         get => mRadius;
         set => SetProperty(ref mRadius, value);
      }
      protected double mRadius = 0.0;

      /// <summary>
      /// The body's current rotation, in radians, due to the slope of the stage. In other words,
      /// this is the angle at which the body is leaning or standing. On a flat portion of a stage,
      /// this value will be pi/2 or directly up in a cartesian system.
      /// </summary>
      [DataMember]
      public double Pitch
      {
         get => mPitch;
         set => SetProperty(ref mPitch, value);
      }
      protected double mPitch = Vector.UpInRadians;

      [DataMember]
      public Direction Direction
      {
         get => mDirection;
         set => SetProperty(ref mDirection, value);
      }
      protected Direction mDirection = Direction.LEFT;

      [DataMember]
      public Vector Velocity
      {
         get => mVelocity;
         set => SetProperty(ref mVelocity, value);
      }
      protected Vector mVelocity = Vector.Zero;

      [DataMember]
      public Vector Acceleration
      {
         get => mAcceleration;
         set => SetProperty(ref mAcceleration, value);
      }
      protected Vector mAcceleration = Vector.Zero;

      [DataMember]
      public bool IsSettled
      {
         get => mIsSettled;
         set => SetProperty(ref mIsSettled, value);
      }
      protected bool mIsSettled = true;

      [DataMember]
      public bool IsWalking
      {
         get => mIsWalking;
         set => SetProperty(ref mIsWalking, value);
      }
      protected bool mIsWalking = false;

      [DataMember]
      public bool IsSubmerged
      {
         get => mIsSubmerged;
         set => SetProperty(ref mIsSubmerged, value);
      }
      private bool mIsSubmerged = false;

      // TODO may not need to be serialized
      [DataMember]
      public DynamicBehaviorEnum Behavior
      {
         get => mBehavior;
         set => SetProperty(ref mBehavior, value);
      }
      private DynamicBehaviorEnum mBehavior = DynamicBehaviorEnum.STANDARD;

      #endregion


      #region Helper getters and setters

      public Vector Position
      {
         get => new Vector(X, Y);
         set
         {
            X = value.X;
            Y = value.Y;
         }
      }

      public Vector Center => new Vector(X + Radius, Y + Radius);

      public Vector Footing => new Vector(mX + mRadius, mY + (2.0 * mRadius));

      #endregion


      public Body() : this(0.0)
      {
      }

      public Body(double aRadius) : this(0.0, 0.0, aRadius)
      {
      }

      public Body(double aX, double aY, double aRadius)
      {
         X = aX;
         Y = aY;
         Radius = aRadius;
      }

      public Body(Body aOther)
      {
         if (aOther == null)
         {
            return;
         }

         X = aOther.X;
         Y = aOther.Y;
         Radius = aOther.Radius;
         Pitch = aOther.Pitch;
         Direction = aOther.Direction;
         Velocity = new Vector(aOther.Velocity);
         Acceleration = new Vector(aOther.Acceleration);
         IsSettled = aOther.IsSettled;
         IsWalking = aOther.IsWalking;
         IsSubmerged = aOther.IsSubmerged;
         Behavior = aOther.Behavior;
      }

      public virtual Body Clone()
      {
         return new Body(this);
      }

      public bool Sync(Body aUpdate, SyncFlags aFlags = SyncFlags.ALL)
      {
         if (aUpdate == null)
         {
            return false;
         }

         int initialHash = GetHashCode();

         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.X) == true))
         {
            X = aUpdate.X;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.Y) == true))
         {
            Y = aUpdate.Y;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.RADIUS) == true))
         {
            Radius = aUpdate.Radius;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.PITCH) == true))
         {
            Pitch = aUpdate.Pitch;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.DIRECTION) == true))
         {
            Direction = aUpdate.Direction;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.VELOCITY) == true))
         {
            Velocity.Sync(aUpdate.Velocity);
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.ACCELERATION) == true))
         {
            Acceleration.Sync(aUpdate.Acceleration);
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.IS_SETTLED) == true))
         {
            IsSettled = aUpdate.IsSettled;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.IS_WALKING) == true))
         {
            IsWalking = aUpdate.IsWalking;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.IS_SUBMERGED) == true))
         {
            IsSubmerged = aUpdate.IsSubmerged;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.BEHAVIOR) == true))
         {
            Behavior = aUpdate.Behavior;
         }

         return initialHash != GetHashCode();
      }

      public override string ToString()
      {
         return $"{{ {nameof(Position)} = {Position}, {nameof(Radius)} = {Radius}, " +
                $"{nameof(Pitch)} = {Pitch}, {nameof(Direction)} = {Direction}, " +
                $"{nameof(Velocity)} = {Velocity}, {nameof(Acceleration)} = {Acceleration}, " +
                $"{nameof(IsSettled)} = {IsSettled}, {nameof(IsWalking)} = {IsWalking}, " +
                $"{nameof(IsSubmerged)} = {IsSubmerged}, {nameof(Behavior)} = {Behavior} }}";
      }

      public override bool Equals(object aObject)
      {
         return (aObject is Body other) && (other._id == _id);
      }

      public override int GetHashCode()
      {
         unchecked // Allows integer overflow.
         {
            int hash = 439;
            hash *= 967 ^ X.GetHashCode();
            hash *= 83 ^ Y.GetHashCode();
            hash *= 157 ^ Radius.GetHashCode();
            hash *= 307 ^ Pitch.GetHashCode();
            hash *= 431 ^ Direction.GetHashCode();
            hash *= 953 ^ (Velocity?.GetHashCode() ?? 1);
            hash *= 167 ^ (Acceleration?.GetHashCode() ?? 1);
            hash *= IsSettled ? 421 : 919;
            hash *= IsWalking ? 751 : 73;
            hash *= IsSubmerged ? 293 : 761;
            hash *= 383 ^ Behavior.GetHashCode();
            return hash;
         }
      }
   }
}
