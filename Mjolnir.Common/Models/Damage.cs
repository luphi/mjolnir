﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Models
{
   [DataContract]
   public class Damage
   {
      #region Serialized properties

      [DataMember]
      public uint PlayerID { get; set; }

      [DataMember]
      public int Amount { get; set; }

      #endregion Serialized properties


      public override string ToString()
      {
         return $"{{ {nameof(PlayerID)} = {PlayerID}, {nameof(Amount)} = {Amount} }}";
      }

      public override int GetHashCode()
      {
         unchecked // Allows integer overflow.
         {
            int hash = 79;
            hash *= 491 ^ PlayerID.GetHashCode();
            hash *= 719 ^ Amount.GetHashCode();
            return hash;
         }
      }
   }
}
