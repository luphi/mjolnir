﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Mjolnir.Common.Models
{
   public class Mobile
   {
      public MobileEnum ID { get; }

      public string Name { get; }

      public Shot Shot1 { get; }

      public Shot Shot2 { get; }

      public Shot SpecialShot { get; }

      public int HP { get; }

      public int ShieldHP { get; }

      public uint BaseDelay { get; }

      public uint TurnDelay { get; }

      /// <summary>
      /// The mobile's width and height.
      /// This applies to both the world and view.
      /// </summary>
      public uint Diameter { get; }

      /// <summary>
      /// The range, in radians, for which firing from the mobile will be at full power. A
      /// penalty will be applied outside this range that lowers the power/damage of a shot.
      /// All angles allow for a 90-degree range for shots. This true angle range will be
      /// within but no larger than this standard range.
      /// For all mobiles, this value is a multiple of five.
      /// </summary>
      public double TrueAngleRangeInRadians { get; }

      public bool HasSatellite { get; }

      /// <summary>
      /// Helper getter that returns a list of all Mobile models. This list contains
      /// instances of the models.
      /// </summary>
      public static List<Mobile> List
      {
         get
         {
            // Get the full list of usable mobile IDs.
            List<MobileEnum> idList = IDs;

            // Generate a list of instantiations of all types.
            List<Mobile> mobileList = new List<Mobile>();
            foreach (MobileEnum type in idList)
            {
               mobileList.Add(new Mobile(type));
            }

            // Finally, return the list.
            return mobileList;
         }
      }

      /// <summary>
      /// Helper getter that returns a list of all usable Mobile IDs (enum values). This excludes
      /// the NONE value.
      /// </summary>
      public static List<MobileEnum> IDs
      {
         get
         {
            // Get a list all mobile types from the enumeration defining them.
            List<MobileEnum> list = Enum.GetValues(typeof(MobileEnum)).Cast<MobileEnum>().ToList();

            // Remove the NONE value.
            list.RemoveAt(0);

            return list;
         }
      }

      public static MobileEnum RandomID
      {
         get
         {
            List<MobileEnum> list = IDs;
            return list[new Random().Next(list.Count)];
         }
      }

      /// <summary>
      /// Helper getter that returns an instance of a random Mobile.
      /// </summary>
      public static Mobile Random
      {
         get
         {
            List<MobileEnum> options = IDs;
            return new Mobile(options[new Random().Next(options.Count)]);
         }
      }

      public Mobile(MobileEnum aID)
      {
         ID = aID;
         Shot1 = new Shot(aID, ShotEnum.SHOT_1);
         Shot2 = new Shot(aID, ShotEnum.SHOT_2);
         SpecialShot = new Shot(aID, ShotEnum.SPECIAL_SHOT);


         #region Mobile-specific values

         switch (aID)
         {
            case MobileEnum.NONE:
            default:
               Name = "Random";
               break;
            case MobileEnum.AKUDA:
               Name = "Akuda";
               HP = 1000;
               ShieldHP = 0;
               BaseDelay = 510;
               TurnDelay = 10;
               Diameter = 48;
               TrueAngleRangeInRadians = 55.0 * Math.PI / 180.0;
               HasSatellite = false;
               break;
            case MobileEnum.ARACH:
               Name = "Arach";
               HP = 1100;
               ShieldHP = 0;
               BaseDelay = 520;
               TurnDelay = 10;
               Diameter = 44;
               TrueAngleRangeInRadians = 55.0 * Math.PI / 180.0;
               HasSatellite = false;
               break;
            case MobileEnum.B_SEAT:
               Name = "B.Seat";
               HP = 760;
               ShieldHP = 220;
               BaseDelay = 490;
               TurnDelay = 10;
               Diameter = 42;
               TrueAngleRangeInRadians = 60.0 * Math.PI / 180.0;
               HasSatellite = true;
               break;
            case MobileEnum.DJ:
               Name = "DJ";
               HP = 750;
               ShieldHP = 250;
               BaseDelay = 490;
               TurnDelay = 10;
               Diameter = 42;
               TrueAngleRangeInRadians = 60.0 * Math.PI / 180.0;
               HasSatellite = false;
               break;
            case MobileEnum.DROID_SLING:
               Name = "Droid Sling";
               HP = 1000;
               ShieldHP = 0;
               BaseDelay = 500;
               TurnDelay = 10;
               Diameter = 46;
               TrueAngleRangeInRadians = 45.0 * Math.PI / 180.0;
               HasSatellite = false;
               break;
            case MobileEnum.FROST:
               Name = "Frost";
               HP = 1200;
               ShieldHP = 0;
               BaseDelay = 490;
               TurnDelay = 10;
               Diameter = 54;
               TrueAngleRangeInRadians = 60.0 * Math.PI / 180.0;
               HasSatellite = false;
               break;
            case MobileEnum.KAME:
               Name = "Kame";
               HP = 950;
               ShieldHP = 0;
               BaseDelay = 490;
               TurnDelay = 12;
               Diameter = 50;
               TrueAngleRangeInRadians = 30.0 * Math.PI / 180.0;
               HasSatellite = false;
               break;
            case MobileEnum.LARVA:
               Name = "Larva";
               HP = 1000;
               ShieldHP = 0;
               BaseDelay = 490;
               TurnDelay = 10;
               Diameter = 48;
               TrueAngleRangeInRadians = 45.0 * Math.PI / 180.0;
               HasSatellite = false;
               break;
            case MobileEnum.SASQUATCH:
               Name = "Sasquatch";
               HP = 1100;
               ShieldHP = 0;
               BaseDelay = 520;
               TurnDelay = 10;
               Diameter = 48;
               TrueAngleRangeInRadians = 25.0 * Math.PI / 180.0;
               HasSatellite = false;
               break;
            case MobileEnum.SHIELD:
               Name = "Shield";
               HP = 1000;
               ShieldHP = 0;
               BaseDelay = 525;
               TurnDelay = 10;
               Diameter = 40;
               TrueAngleRangeInRadians = 55.0 * Math.PI / 180.0;
               HasSatellite = false;
               break;
            case MobileEnum.THUNDER:
               Name = "Thunder";
               HP = 760;
               ShieldHP = 220;
               BaseDelay = 500;
               TurnDelay = 10;
               Diameter = 42;
               TrueAngleRangeInRadians = 30.0 * Math.PI / 180.0;
               HasSatellite = false;
               break;
            case MobileEnum.TRIKE:
               Name = "Trike";
               HP = 1100;
               ShieldHP = 0;
               BaseDelay = 490;
               TurnDelay = 10;
               Diameter = 54;
               TrueAngleRangeInRadians = 50.0 * Math.PI / 180.0;
               HasSatellite = false;
               break;
            case MobileEnum.WIZARD:
               Name = "Wizard";
               HP = 760;
               ShieldHP = 220;
               BaseDelay = 500;
               TurnDelay = 10;
               Diameter = 44;
               TrueAngleRangeInRadians = 40.0 * Math.PI / 180.0;
               HasSatellite = false;
               break;
            case MobileEnum.ZOOMER:
               Name = "Zoomer";
               HP = 1000;
               ShieldHP = 0;
               BaseDelay = 480;
               TurnDelay = 10;
               Diameter = 40;
               TrueAngleRangeInRadians = 80.0 * Math.PI / 180.0;
               HasSatellite = false;
               break;
         }

         #endregion
      }
   }
}
