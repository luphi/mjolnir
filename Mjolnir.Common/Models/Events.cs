﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Models
{
   /// <summary>
   /// Collection of events that occurred during a run of the world (where a run is the period
   /// between a Start() call and the world settling or a call to Stop()).
   /// </summary>
   [DataContract]
   public class Events
   {
      #region Serialized properties

      [DataMember]
      public Landing[] Landings { get; set; }

      [DataMember]
      public Body[] Halts { get; set; }

      [DataMember]
      public Player[] Deaths { get; set; }

      [DataMember]
      public Shooting[] Shootings { get; set; }

      [DataMember]
      public Damage[] Damages { get; set; }

      #endregion Serialized properties


      #region Unserialized performance properties

      public ulong Steps { get; }

      public double ElapsedSeconds { get; }

      #endregion Unserialized performance properties


      public Events(Landing[] aLandings,
                    Body[] aHalts,
                    Player[] aDeaths,
                    Shooting[] aShootings,
                    Damage[] aDamages,
                    ulong aSteps,
                    double aElapsedSeconds)
      {
         Landings = aLandings;
         Halts = aHalts;
         Deaths = aDeaths;
         Shootings = aShootings;
         Damages = aDamages;
         Steps = aSteps;
         ElapsedSeconds = aElapsedSeconds;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Landings)}.Length = {Landings.Length}, {nameof(Halts)}.Length = {Halts.Length}, " +
                $"{nameof(Deaths)}.Length = {Deaths.Length}, {nameof(Shootings)}.Length = {Shootings.Length} " +
                $"{nameof(Damages)}.Length = {Damages.Length}, {nameof(Steps)} = {Steps}, " +
                $"{nameof(ElapsedSeconds)} = {ElapsedSeconds:F2} }}";
      }

      public override int GetHashCode()
      {
         unchecked // Allows integer overflow.
         {
            int hash = 607;
            foreach (Landing landing in Landings)
            {
               hash *= 601 ^ landing.GetHashCode();
            }
            foreach (Body halt in Halts)
            {
               hash *= 149 ^ halt.GetHashCode();
            }
            foreach (Player death in Deaths)
            {
               hash *= 331 ^ death.GetHashCode();
            }
            foreach (Shooting shooting in Shootings)
            {
               hash *= 89 ^ shooting.GetHashCode();
            }
            foreach (Damage damage in Damages)
            {
               hash *= 431 ^ damage.GetHashCode();
            }
            // Steps and ElapsedSeconds are excluded as they will vary from system to system
            // and are only meant for development anyway.
            return hash;
         }
      }
   }
}
