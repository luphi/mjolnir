﻿using System;
using System.Runtime.Serialization;

namespace Mjolnir.Common.Models
{
   [DataContract]
   public class Vector
   {
      public static Vector Zero => new Vector(0.0, 0.0);


      #region Serialized properties

      [DataMember]
      public double X { get; set; }

      [DataMember]
      public double Y { get; set; }

      #endregion


      #region Constants

      public static double RightInRadians => 0.0;

      public static double UpInRadians => 3.0 * Math.PI / 2.0;

      public static double LeftInRadians => Math.PI;

      public static double DownInRadians => Math.PI / 2.0;

      #endregion


      /// <summary>
      /// The length or magnitude of the vector.
      /// </summary>
      public double Length
      {
         get => Math.Sqrt(Math.Pow(X, 2.0) + Math.Pow(Y, 2.0));
         set
         {
            double factor = 1.0 / Length * value;
            X *= factor;
            Y *= factor;
         }
      }

      /// <summary>
      /// The squared value of the length or magnitude of the vector.
      /// This is intended for cases where only a comparison is needed. By leaving the value
      /// squared, the relatively slow square root calculation is avoided.
      /// </summary>
      public double LengthSquared => Math.Pow(X, 2.0) + Math.Pow(Y, 2.0);

      /// <summary>
      /// The direction of the vector, in radians.
      /// </summary>
      public double AngleInRadians => Math.Atan2(Y, X).WrapRadians();

      public Vector(double aX, double aY)
      {
         X = aX;
         Y = aY;
      }

      public Vector(double aValue)
      {
         X = aValue;
         Y = aValue;
      }

      public Vector(Vector aOther)
      {
         if (aOther == null)
         {
            return;
         }

         X = aOther.X;
         Y = aOther.Y;
      }

      public static Vector FromRadians(double aRadians)
      {
         return new Vector(Math.Cos(aRadians), Math.Sin(aRadians));
      }
      
      public bool Sync(Vector aUpdate)
      {
         if (aUpdate == null)
         {
            return false;
         }

         int initialHash = GetHashCode();

         X = aUpdate.X;
         Y = aUpdate.Y;

         return initialHash != GetHashCode();
      }

      public static Vector operator -(Vector aVector)
      {
         return new Vector(-aVector.X, -aVector.Y);
      }

      public static Vector operator +(Vector aVector1, Vector aVector2)
      {
         return new Vector(aVector1.X + aVector2.X, aVector1.Y + aVector2.Y);
      }

      public static Vector operator -(Vector aVector1, Vector aVector2)
      {
         return new Vector(aVector1.X - aVector2.X, aVector1.Y - aVector2.Y);
      }

      public static Vector operator *(Vector aVector1, Vector aVector2)
      {
         return new Vector(aVector1.X * aVector2.X, aVector1.Y * aVector2.Y);
      }

      public static Vector operator *(Vector aVector, double aMultiplier)
      {
         return new Vector(aVector.X * aMultiplier, aVector.Y * aMultiplier);
      }

      public static Vector operator *(double aMultiplier, Vector aVector)
      {
         return new Vector(aVector.X * aMultiplier, aVector.Y * aMultiplier);
      }

      public static Vector operator /(Vector aVector1, Vector aVector2)
      {
         return new Vector(aVector1.X / aVector2.X, aVector1.Y / aVector2.Y);
      }

      public static Vector operator /(Vector aVector, double aDivisor)
      {
         return new Vector(aVector.X / aDivisor, aVector.Y / aDivisor);
      }

      public static bool operator ==(Vector aVector1, Vector aVector2)
      {
         return (aVector1?.X == aVector2?.X) && (aVector1?.Y == aVector2?.Y);
      }

      public static bool operator !=(Vector aVector1, Vector aVector2)
      {
         return (aVector1?.X != aVector2?.X) || (aVector1?.Y != aVector2?.Y);
      }

      public static double Distance(Vector aVector1, Vector aVector2)
      {
         return Math.Sqrt(Math.Pow(aVector1.X - aVector2.X, 2.0) + Math.Pow(aVector1.Y - aVector2.Y, 2.0));
      }

      public static double Dot(Vector aVector1, Vector aVector2)
      {
         return (aVector1.X * aVector2.X) + (aVector1.Y * aVector2.Y);
      }

      public static Vector Project(Vector aFrom, Vector aOnto)
      {
         // The projection of vector A onto vector B is calculated with (A dot B_u) * B_u
         // where B_u is a unit vector with the same direction of vector B (noramlized B).
         Vector axis = new Vector(aOnto);
         axis.Normalized();
         return Dot(aFrom, axis) * axis;
      }

      public static Vector Reflect(Vector aIncident, Vector aNormal)
      {
         // The formula for reflecting a vector is r = i - 2 * (i dot n) * n where:
         // r = reflected vector
         // i = incident vector
         // n = normalized normal vector
         Vector normal = aNormal.Normalized();
         return aIncident - (2.0 * Dot(aIncident, normal) * normal);
      }

      public static Vector Rotate(Vector aVector, double aAngleInRadians)
      {
         return new Vector((aVector.X * Math.Cos(aAngleInRadians)) - (aVector.Y * Math.Sin(aAngleInRadians)),
                           (aVector.X * Math.Sin(aAngleInRadians)) + (aVector.Y * Math.Cos(aAngleInRadians)));
      }

      public Vector Normalized()
      {
         double factor = 1.0 / Length;
         return new Vector(X * factor, Y * factor);
      }

      public override string ToString()
      {
         return $"{{ {nameof(X)} = {X:F2}, " +
                $"{nameof(Y)} = {Y:F2} }}";
      }

      public override bool Equals(object aObject)
      {
         if (aObject is Vector vector)
         {
            return Equals(vector);
         }

         return false;
      }

      public bool Equals(Vector aVector)
      {
         return (X == aVector.X) && (Y == aVector.Y);
      }

      public override int GetHashCode()
      {
         unchecked // Allows integer overflow.
         {
            return (X.GetHashCode() * 397) ^ Y.GetHashCode();
         }
      }
   }
}
