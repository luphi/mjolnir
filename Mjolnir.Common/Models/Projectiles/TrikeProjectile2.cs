﻿using System;
using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class TrikeProjectile2 : Projectile
   {
      private Body mOrbiter1 = null;
      private Body mOrbiter2 = null;
      private double mOffsetLengthFromFocus = 0.0;
      private double mTheta = 0.0;

      public TrikeProjectile2(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new TrikeProjectile2(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         Focus = new Body
         {
            Radius = Player.Shot.BodyRadius,
            Position = aPosition,
            Velocity = aVelocity,
            IsSettled = false
         };
         // The Focus will proceed with a normal, unaltered trajectory and the orbiting bodies
         // will be slightly offset from it but follow the same general trajectory.
         mOrbiter1 = new Body(Focus);
         mOrbiter2 = new Body(Focus);

         ProgressedBodies.Add(Focus);
         CorporealBodies.Add(Focus);
         CorporealBodies.Add(mOrbiter1);
         CorporealBodies.Add(mOrbiter2);
      }

      public override void Step(double aDT, Vector aWind)
      {
         if ((Focus == null) || (mOrbiter1 == null) || (mOrbiter2 == null))
         {
            return;
         }

         if (mOffsetLengthFromFocus < Constants.TrikeMaxOrbitRadiusInPixels)
         {
            mOffsetLengthFromFocus += Constants.TrikeMaxOrbitRadiusInPixels * aDT;

            if (mOffsetLengthFromFocus > Constants.TrikeMaxOrbitRadiusInPixels)
            {
               mOffsetLengthFromFocus = Constants.TrikeMaxOrbitRadiusInPixels;
            }
         }

         mTheta += Math.PI * aDT; // Results in one rotation every two seconds.
                                  // Create a unit vector at angle phi that will be used to translate the
                                  // two orbiting bodies.
         Vector offset = Vector.FromRadians(mTheta);

         // Set the offset vector's length.
         offset *= mOffsetLengthFromFocus;

         // Offset the first orbiting body.
         mOrbiter1.Position = Focus.Position + offset;
         mOrbiter1.Velocity = Focus.Velocity;

         // Point the vector in the opposite direction.
         offset *= -1.0;

         // Offset the second orbiting body opposite the first.
         mOrbiter2.Position = Focus.Position + offset;
         mOrbiter2.Velocity = Focus.Velocity;
      }

      public override void Remove(Body aBody)
      {
         if (ReferenceEquals(aBody, Focus) == true)
         {
            // If the body is the Focus.

            // Remove the Focus from the list of corporeal bodies but leave it in the list of
            // progressed bodies. With this, the Focus will continue but will not interact with
            // the stage or players. However, the orbiters will still follow it.
            if (CorporealBodies.Remove(aBody))
            {
               // Invoke the event on the list property to indicate the list has changed.
               OnPropertyChanged(nameof(CorporealBodies));
            }
         }
         else
         {
            // If the body is one of the orbiters.

            // Completely remove the body.
            base.Remove(aBody);
         }
      }

      private TrikeProjectile2(TrikeProjectile2 aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mOrbiter1 = aOther.mOrbiter1 != null ? aWorldState.BodiesMap[aOther.mOrbiter1._id] : null;
         mOrbiter2 = aOther.mOrbiter2 != null ? aWorldState.BodiesMap[aOther.mOrbiter2._id] : null;
         mOffsetLengthFromFocus = aOther.mOffsetLengthFromFocus;
         mTheta = aOther.mTheta;
      }
   }
}
