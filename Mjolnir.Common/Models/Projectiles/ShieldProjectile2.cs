﻿using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class ShieldProjectile2 : Projectile
   {
      private bool mIsFirstStage = false; // True if the initial body is still active, false otherwise.

      public ShieldProjectile2(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new ShieldProjectile2(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         base.Launch(aWorld, aPosition, aVelocity);

         mIsFirstStage = true;
      }

      public override void Remove(Body aBody)
      {
         // This projectile's unique quirk its pseudo-launch of a second body after the loss of the first.
         // This second body continues from the position of the first with the first's velocity. Another way
         // to look at it: this projectile has a single primary body with two lives.

         if ((mIsFirstStage == true) && (ReferenceEquals(Focus, aBody) == true))
         {
            // If the body being removed is the first one.

            mIsFirstStage = false;
            // Nothing more needs to be done. The primary body and list of bodies will be unchanged until
            // the second time this method is called.
         }
         else
         {
            // If the second body is active and this projectile is behaviorally identical to the base.

            // Just hand things off to the base method.
            base.Remove(aBody);
         }
      }

      private ShieldProjectile2(ShieldProjectile2 aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mIsFirstStage = aOther.mIsFirstStage;
      }
   }
}
