﻿using System;
using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class DroidSlingProjectile1 : Projectile
   {
      private readonly double mFinalOffsetLength;
      private Body mSine1 = null;
      private Body mSine2 = null;
      private double mOffsetLengthFromFocus = 0.0;
      private double mTheta = 0.0;

      public DroidSlingProjectile1(Player aPlayer) : base(aPlayer)
      {
         mFinalOffsetLength = 2.0 * aPlayer.Shot.BodyRadius;
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new DroidSlingProjectile1(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         Focus = new Body
         {
            Radius = Player.Shot.BodyRadius,
            Position = aPosition,
            Velocity = aVelocity,
            IsSettled = false
         };

         // The Focus will proceed with a normal, unaltered trajectory and the sine wave bodies
         // will be slightly offset from it but follow the same general trajectory.
         mSine1 = new Body(Focus);
         mSine2 = new Body(Focus);

         ProgressedBodies.Add(Focus);
         CorporealBodies.Add(Focus);
         CorporealBodies.Add(mSine1);
         CorporealBodies.Add(mSine2);
         // Note: the primary is not added to the list of corporeal bodies.
      }

      public override void Step(double aDT, Vector aWind)
      {
         if ((Focus == null) || (mSine1 == null) || (mSine2 == null))
         {
            return;
         }

         if (mOffsetLengthFromFocus < mFinalOffsetLength)
         {
            mOffsetLengthFromFocus += mFinalOffsetLength * aDT;

            if (mOffsetLengthFromFocus > mFinalOffsetLength)
            {
               mOffsetLengthFromFocus = mFinalOffsetLength;
            }
         }

         mTheta += Math.PI * aDT; // Results in period of two seconds.

         // Here's a useful feature of 2D vectors: 90° rotations can be done by flipping the copoments.
         // In coordinate systems where positive Y is up, this means (x, y) -> (-y, x) is a 90°,
         // counterclockwise rotation. Because this game treats positive Y as down, this results in a
         // clockwise rotation.
         Vector offset = new Vector(-Focus.Velocity.Y, Focus.Velocity.X)
         {
            Length = mOffsetLengthFromFocus * Math.Sin(mTheta)
         };

         // Offset the first sinusoidal body.
         mSine1.Position = Focus.Position + offset;
         mSine1.Velocity = Focus.Velocity;

         // Repeat the process for the other body with an opposite direction and distance from the
         // ghost focus.
         offset *= -1.0;
         // Offset the second sinusoidal body opposite the first.
         mSine2.Position = Focus.Position + offset;
         mSine2.Velocity = Focus.Velocity;
      }

      public override void Remove(Body aBody)
      {
         if (ReferenceEquals(aBody, Focus) == true)
         {
            // If the body is the Focus.

            // Remove the Focus from the list of corporeal bodies but leave it in the list of
            // progressed bodies. With this, the Focus will continue but will not interact with
            // the stage or players. However, the sine wave bodies will still follow it.
            if (CorporealBodies.Remove(aBody))
            {
               // Invoke the event on the list property to indicate the list has changed.
               OnPropertyChanged(nameof(CorporealBodies));
            }
         }
         else
         {
            // If the body is one of the sines.

            // Completely remove the body.
            base.Remove(aBody);
         }
      }

      private DroidSlingProjectile1(DroidSlingProjectile1 aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mFinalOffsetLength = aOther.mFinalOffsetLength;
         mSine1 = aOther.mSine1 != null ? aWorldState.BodiesMap[aOther.mSine1._id] : null;
         mSine2 = aOther.mSine2 != null ? aWorldState.BodiesMap[aOther.mSine2._id] : null;
         mOffsetLengthFromFocus = aOther.mOffsetLengthFromFocus;
         mTheta = aOther.mTheta;
      }
   }
}
