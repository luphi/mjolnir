﻿using System;
using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class WizardProjectile2 : Projectile
   {
      private Body mLaser1 = null;
      private Body mLaser2 = null;
      private double mOffsetLengthFromFocus = 0.0;
      private double mTheta = 0.0;

      public WizardProjectile2(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new WizardProjectile2(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         Focus = new Body
         {
            Radius = Player.Shot.BodyRadius,
            Position = aPosition,
            Velocity = aVelocity,
            IsSettled = false
         };
         // This child Projectile uses the focal body as an unseen, non-corporeal body.
         // This ghost focus will proceed with a normal, unaltered trajectory and the actual bodies
         // will be slightly offset from it but follow the same general trajectory.
         mLaser1 = new Body(Focus);
         mLaser2 = new Body(Focus);

         ProgressedBodies.Add(Focus);
         CorporealBodies.Add(mLaser1);
         CorporealBodies.Add(mLaser2);
         // Note: the Focus is not added to the list of corporeal bodies.
      }

      public override void Step(double aDT, Vector aWind)
      {
         if ((Focus == null) || (mLaser1 == null) || (mLaser2 == null))
         {
            return;
         }

         if (mOffsetLengthFromFocus < Constants.WizardMaxLaserOffsetInPixels)
         {
            mOffsetLengthFromFocus += Constants.WizardMaxLaserOffsetInPixels * aDT;

            if (mOffsetLengthFromFocus > Constants.WizardMaxLaserOffsetInPixels)
            {
               mOffsetLengthFromFocus = Constants.WizardMaxLaserOffsetInPixels;
            }
         }

         mTheta += Math.PI * aDT; // Results in period of two seconds.

         // Here's a useful feature of 2D vectors: 90° rotations can be done by flipping the copoments.
         // In coordinate systems where positive Y is up, this means (x, y) -> (-y, x) is a 90°,
         // counterclockwise rotation. Because this game treats positive Y as down, this results in a
         // clockwise rotation.
         Vector offset = new Vector(-Focus.Velocity.Y, Focus.Velocity.X)
         {
            Length = mOffsetLengthFromFocus * Math.Sin(mTheta)
         };

         // Offset the first sinusoidal body.
         mLaser1.Position = Focus.Position + offset;
         mLaser1.Velocity = Focus.Velocity;

         // Repeat the process for the other body with an opposite direction and distance from the
         // ghost focus.
         offset *= -1.0;
         
         // Offset the second sinusoidal body opposite the first.
         mLaser2.Position = Focus.Position + offset;
         mLaser2.Velocity = Focus.Velocity;
      }

      private WizardProjectile2(WizardProjectile2 aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mLaser1 = aOther.mLaser1 != null ? aWorldState.BodiesMap[aOther.mLaser1._id] : null;
         mLaser2 = aOther.mLaser2 != null ? aWorldState.BodiesMap[aOther.mLaser2._id] : null;
         mOffsetLengthFromFocus = aOther.mOffsetLengthFromFocus;
         mTheta = aOther.mTheta;
      }
   }
}
