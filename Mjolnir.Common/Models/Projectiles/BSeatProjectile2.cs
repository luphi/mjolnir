﻿using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class BSeatProjectile2 : Projectile
   {
      private const uint mNumberOfShots = 3;

      private readonly double mFiringPauseInSeconds = 1.0 / Constants.BSeatFiringFrequencyInHertz;
      private bool mIsFiring = false;
      private bool mIsDone = false;
      private uint mShotsFired = 0;
      private Vector mTarget = null;
      private double mElapsedSeconds = 0.0;

      public override bool IsSettled => mIsDone;

      public override bool IsDone => mIsDone;

      public BSeatProjectile2(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new BSeatProjectile2(this, aWorldState);
      }

      public override void Step(double aDT, Vector aWind)
      {
         // If the body collided with something.
         if (mIsFiring == true)
         {
            // Firing is underway and a target is set. Lasers will fire periodically
            // from the satellite above the (B.Seat) player that fired this projectile.
            mElapsedSeconds += aDT;
            // If enough time for the next laser has passed.
            if (mElapsedSeconds >= mFiringPauseInSeconds)
            {
               // Do one more laser fire. If this is the last laser to fire, this method
               // will clean up.
               DoFire();

               // Continue tracking the time that has passed.
               mElapsedSeconds -= mFiringPauseInSeconds;
            }
         }
      }

      public override void OnStageCollision(Body aBody, Vector aPoint)
      {
         OnAnyCollision(aBody, aPoint);
      }

      public override void OnPlayerCollision(Body aBody, Vector aPoint, Player aPlayer)
      {
         OnAnyCollision(aBody, aPoint);
      }

      private void OnAnyCollision(Body aBody, Vector aPoint)
      {
         // The body's job was to illuminate the target. With its job done, it can go.
         Remove(aBody);

         // Set a couple variables that will be used by Step() going forward and...
         mIsFiring = true;
         mTarget = aPoint;
         // ...immediately fire a single laser.
         DoFire();
      }

      private void DoFire()
      {
         // Add a "detonation" at the target the body hit. The World will treat this
         // detonation a bit differently and cast a beam towards it rather than perform
         // a shooting directly at the target.
         Detonations.Add(mTarget);
         mShotsFired += 1;

         // If we've fired all of the lasers.
         if (mShotsFired >= mNumberOfShots)
         {
            // Unflag the "is firing" variable to be safe and flag this projectile as
            // "is done" so the world can settle.
            mIsFiring = false;
            mIsDone = true;
         }
      }

      private BSeatProjectile2(BSeatProjectile2 aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mFiringPauseInSeconds = aOther.mFiringPauseInSeconds;
         mIsFiring = aOther.mIsFiring;
         mIsDone = aOther.mIsDone;
         mShotsFired = aOther.mShotsFired;
         mTarget = aOther.mTarget != null ? new Vector(aOther.mTarget) : null;
         mElapsedSeconds = aOther.mElapsedSeconds;
      }
   }
}
