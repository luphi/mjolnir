﻿using System;
using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class KameProjectile2 : Projectile
   {
      private Body mStream1 = null;
      private Body mStream2 = null;
      private uint mPhase = 0;
      private double mT = 0.0;
      private double mSine = 0.0;
      private double mTheta = 0.0;

      public KameProjectile2(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new KameProjectile2(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         Focus = new Body
         {
            Radius = Player.Shot.BodyRadius,
            Position = aPosition,
            Velocity = aVelocity,
            IsSettled = false
         };

         // This child Projectile uses the focal body as an unseen, non-corporeal body.
         // This ghost focus will proceed with a normal, unaltered trajectory and the actual bodies
         // will be slightly offset from it but follow the same general trajectory.
         mStream1 = new Body(Focus);
         mStream2 = new Body(Focus);

         ProgressedBodies.Add(Focus);
         CorporealBodies.Add(mStream1);
         CorporealBodies.Add(mStream2);
         // Note: the Focus is not added to the list of corporeal bodies.
      }

      public override void Step(double aDT, Vector aWind)
      {
         if ((Focus == null) || (mStream1 == null) || (mStream2 == null))
         {
            return;
         }

         mT += aDT;

         switch (mPhase)
         {
            case 0:
               mTheta = 0.5 * Math.PI * mT * mT; // The period "speeds up" as time increases.
               mSine = Math.Sin(mTheta);

               // If theta has reached or passed 2*pi.
               if (mTheta >= 2.0 * Math.PI)
               {
                  mPhase += 1; // Next phase.
               }
               break;
            case 1:
               mTheta = Math.PI * mT; // Results in period of two seconds.
               mSine = Math.Sin(mTheta);

               // If the ghost focus began falling this step.
               if (Focus.Velocity.Y >= 0.0)
               {
                  mPhase += 1; // Next phase.
               }
               break;
            case 2:
            default:
               // The two streams will converge on the ghost focus with the sine value
               // approaching zero. Once zero, it will remain zero.
               double previousSine = mSine;
               // If the sine is not currently zero, continuing approaching it.
               if (mSine != 0.0)
               {
                  mSine -= 0.5 * Math.Sign(mSine) * aDT;
                  // If the sine value passed zero.
                  // (Consider sine = 0.1 and previous sine = 0.2, then the product is positive. This
                  // holds for -0.1 and -0.2. If sine = -0.1 and previous sine = -0.1, then the product
                  // is negative. With this, we can know when zero has been passed.)
                  if (mSine * previousSine < 0.0)
                  {
                     mSine = 0.0;
                  }
               }
               break;
         }


         // Here's a useful feature of 2D vectors: 90° rotations can be done by flipping the copoments.
         // In coordinate systems where positive Y is up, this means (x, y) -> (-y, x) is a 90°,
         // counterclockwise rotation. Because this game treats positive Y as down, this results in a
         // clockwise rotation.
         Vector offset = new Vector(-Focus.Velocity.Y, Focus.Velocity.X)
         {
            Length = Constants.KameStreamMaxOffsetInPixels * mSine
         };
         // Offset the first sinusoidal body.
         mStream1.Position = Focus.Position + offset;
         mStream1.Velocity = Focus.Velocity;

         // Repeat the process for the other body with an opposite direction and distance from the
         // ghost focus.
         offset *= -1.0;
         // Offset the second sinusoidal body opposite the first.
         mStream2.Position = Focus.Position + offset;
         mStream2.Velocity = Focus.Velocity;
      }

      private KameProjectile2(KameProjectile2 aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mStream1 = aOther.mStream1 != null ? aWorldState.BodiesMap[aOther.mStream1._id] : null;
         mStream2 = aOther.mStream2 != null ? aWorldState.BodiesMap[aOther.mStream2._id] : null;
         mPhase = aOther.mPhase;
         mT = aOther.mT;
         mSine = aOther.mSine;
         mTheta = aOther.mTheta;
      }
   }
}
