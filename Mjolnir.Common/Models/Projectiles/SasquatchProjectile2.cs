﻿using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class SasquatchProjectile2 : Projectile
   {
      private Body mAdjacent1 = null;
      private Body mAdjacent2 = null;

      public SasquatchProjectile2(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new SasquatchProjectile2(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         base.Launch(aWorld, aPosition, aVelocity);

         // This child Projectile contains three bodies where the primary proceeds at an
         // unaltered trajectory and the remaining two fly adjacent to the primary. The
         // primary body is between these other two.
         double theta = aVelocity.AngleInRadians;
         mAdjacent1 = new Body(Focus)
         {
            Velocity = Vector.FromRadians(theta + 0.03) * aVelocity.Length
         };
         mAdjacent2 = new Body(Focus)
         {
            Velocity = Vector.FromRadians(theta - 0.03) * aVelocity.Length
         };

         // The Focus was already added to the progressed bodies list by the base method.
         ProgressedBodies.Add(mAdjacent1);
         ProgressedBodies.Add(mAdjacent2);

         // The Focus was already added to the corporeal bodies list by the base method.
         CorporealBodies.Add(mAdjacent1);
         CorporealBodies.Add(mAdjacent2);
      }

      public override void Step(double aDT, Vector aWind)
      {
         if ((Focus == null) || (mAdjacent1 == null) || (mAdjacent2 == null))
         {
            return;
         }

         Vector difference = mAdjacent1.Position - Focus.Position;
         if (difference.Length > Constants.SasquatchMaxBombOffsetInPixels)
         {
            difference.Length = Constants.SasquatchMaxBombOffsetInPixels;
            mAdjacent1.Position = Focus.Position + difference;
         }

         difference = mAdjacent2.Position - Focus.Position;
         if (difference.Length > Constants.SasquatchMaxBombOffsetInPixels)
         {
            difference.Length = Constants.SasquatchMaxBombOffsetInPixels;
            mAdjacent2.Position = Focus.Position + difference;
         }
      }

      private SasquatchProjectile2(SasquatchProjectile2 aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mAdjacent1 = aOther.mAdjacent1 != null ? aWorldState.BodiesMap[aOther.mAdjacent1._id] : null;
         mAdjacent2 = aOther.mAdjacent2 != null ? aWorldState.BodiesMap[aOther.mAdjacent2._id] : null;
      }
   }
}
