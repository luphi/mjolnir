﻿using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   /// <summary>
   /// Due to having nearly identical projectile behavior, this Projectile is shared by
   /// DJ's and Trike's special shots.
   /// </summary>
   public class BurnProjectile : Projectile
   {
      private readonly int mBurnTimeInMilliseconds;
      private bool mIsBurning = false;
      private double mBurnCountdown = 0.0;
      private double mTriggerCountdown = 0.0;

      public BurnProjectile(Player aPlayer, int aBurnTimeInMilliseconds) : base(aPlayer)
      {
         mBurnTimeInMilliseconds = aBurnTimeInMilliseconds;
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new BurnProjectile(this, aWorldState);
      }

      public override void Step(double aDT, Vector aWind)
      {
         if (mIsBurning == true)
         {
            // Continue the "waiting to hurt someone" countdown.
            mTriggerCountdown -= aDT;

            // If enough time has passed and another mini detonation should happen.
            if (mTriggerCountdown <= 0.0)
            {
               // Do the detonation.
               Detonations.Add(Focus.Center);
               mTriggerCountdown += 1.0 / Constants.BurnFrequencyInHertz;
            }

            // Continue the "bouncing around the stage" countdown.
            mBurnCountdown -= aDT;

            // If the ball has expired after bouncing around the stage.
            if (mBurnCountdown <= 0.0)
            {
               Remove(Focus);
            }
         }
      }

      public override void OnStageCollision(Body aBody, Vector aPoint)
      {
         OnAnyCollision();
      }

      public override void OnPlayerCollision(Body aBody, Vector aPoint, Player aPlayer)
      {
         OnAnyCollision();
      }

      private void OnAnyCollision()
      {
         // If this is the first contact with the stage or a player.
         if (mIsBurning == false)
         {
            // Set a flag that causes Step() to harm nearby players.
            mIsBurning = true;

            // Keep the body where it is. It will be removed when it's done burning.
            Focus.IsSettled = true;

            // Do one detonation immediately. This also has the effect of preventing the
            // World from settling with the Focus now settled.
            Detonations.Add(Focus.Center);

            // Set some time-tracking variables that are used to 1) expire the projectile, and
            // 2) trigger continuous mini detonations while the projectile is still active.
            mBurnCountdown = mBurnTimeInMilliseconds / 1000.0;
            mTriggerCountdown = 1.0 / Constants.BurnFrequencyInHertz;
         }
      }

      private BurnProjectile(BurnProjectile aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mBurnTimeInMilliseconds = aOther.mBurnTimeInMilliseconds;
         mIsBurning = aOther.mIsBurning;
         mBurnCountdown = aOther.mBurnCountdown;
         mTriggerCountdown = aOther.mTriggerCountdown;
      }
   }
}
