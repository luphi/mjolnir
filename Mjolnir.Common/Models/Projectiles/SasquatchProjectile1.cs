﻿using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class SasquatchProjectile1 : Projectile
   {
      private static bool sIsClockwise = false;

      private bool mIsClockwise = false;
      private Body mActual = null;
      private double mOffsetLengthFromFocus = 0.0;

      public SasquatchProjectile1(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new SasquatchProjectile1(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         Focus = new Body
         {
            Radius = Player.Shot.BodyRadius,
            Position = aPosition,
            Velocity = aVelocity,
            IsSettled = false
         };
         mActual = new Body(Focus);
         // This child Projectile uses the focal body as an unseen, non-corporeal body.
         // This ghost focus will proceed with a normal, unaltered trajectory while a second,
         // corporeal body will be slightly offset from it but follow the same general trajectory.
         ProgressedBodies.Add(Focus);
         CorporealBodies.Add(mActual);
         // Note: the Focus is not added to the list of corporeal bodies.

         // Use the static boolean to determine if the actual body's offset will be to the left or to
         // the right of the ghost focus.
         mIsClockwise = sIsClockwise;
         sIsClockwise = !sIsClockwise;
      }

      public override void Step(double aDT, Vector aWind)
      {
         if ((Focus == null) || (mActual == null))
         {
            return;
         }

         if (mOffsetLengthFromFocus < Constants.SasquatchMaxMissileOffsetInPixels)
         {
            mOffsetLengthFromFocus += Constants.SasquatchMaxMissileOffsetInPixels * aDT;

            if (mOffsetLengthFromFocus > Constants.SasquatchMaxMissileOffsetInPixels)
            {
               mOffsetLengthFromFocus = Constants.SasquatchMaxMissileOffsetInPixels;
            }
         }

         Vector velocity = Focus.Velocity;
         // Here's a useful feature of 2D vectors: 90° rotations can be done by flipping the copoments.
         // In coordinate systems where positive Y is up, this means (x, y) -> (-y, x) is a 90°,
         // counterclockwise rotation. Because this game treats positive Y as down, this results in a
         // clockwise rotation.
         Vector offset = new Vector(-velocity.Y, velocity.X)
         {
            Length = mOffsetLengthFromFocus
         };

         // If this projectile is offset in the other direction.
         if (mIsClockwise == true)
         {
            // Point the vector in the opposite direction.
            offset *= -1.0;
         }

         // Set the corporeal body's properties.
         mActual.Position = Focus.Position + offset;
         mActual.Velocity = Focus.Velocity;
      }

      private SasquatchProjectile1(SasquatchProjectile1 aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mIsClockwise = aOther.mIsClockwise;
         mActual = aOther.mActual != null ? aWorldState.BodiesMap[aOther.mActual._id] : null;
         mOffsetLengthFromFocus = aOther.mOffsetLengthFromFocus;
      }
   }
}
