﻿using System;
using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class ZoomerProjectile12 : Projectile
   {
      // There are three modes to this projectile based on the angle at which its fired.
      // 1) For high angles, 75+ degrees, the body is lobbed with lightweight dynamic behavior applied
      //    making it essentially a slower version of most projectiles. Basically, it's a mortar.
      // 2) For low angles, 15- degrees, the body flies like an airfoil creating lift as it travels
      //    effectively canceling out gravity. However, this only applies for a predetermined distance
      //    before the body then behaves normally (the dynamic behavior is changed).
      // 3) For mid angles, in the (15, 75) range, the body flies like an airfoil just as mode 2 but
      //    does a sharp hook movement after a predetermined distance. After this, it will retain its
      //    airfoil behavior at a downward trajectory.
      // Wind is still aplied for all three modes but gravity applies only to mode 1 and sufficiently
      // long mode 2 shots.

      private enum Mode
      {
         BULLET,   // Low angles.
         HOOK,     // Mid angles.
         MORTAR    // High angles.
      }

      private Mode mMode = Mode.MORTAR; // Just an initial value. Will be reassigned at launch.
      private Vector mPositionAtFire = null;
      private double? mDistanceUntilDescent = null;
      private bool mIsDescending = false;

      public ZoomerProjectile12(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new ZoomerProjectile12(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         base.Launch(aWorld, aPosition, aVelocity);

         // Note: the vector is flipped in order to make it match the typical trig orientation.
         // It's easier to read in comparison to the flipped-Y coordinates used by computers.
         double angle = new Vector(aVelocity.X, -aVelocity.Y).AngleInRadians;

         if ((angle >= 5.0 * Math.PI / 12.0) && (angle <= 7.0 * Math.PI / 12.0))
         {
            // If the trajectory is between 75 degrees (5*pi/12) and 115 degrees (7*pi/12).

            mMode = Mode.MORTAR;
         }
         else if ((angle <= Math.PI / 12.0) || (angle >= 11.0 * Math.PI / 12.0))
         {
            // If the trajectory is 15 degrees (pi/12) or below, or 165 degrees (11*pi/12) or above.

            mMode = Mode.BULLET;
         }
         else
         {
            // If the trajectory is an inbetween, middle angle.

            mMode = Mode.HOOK;
         }

         switch (mMode)
         {
            case Mode.MORTAR:
               Focus.Behavior = DynamicBehaviorEnum.FEATHER;
               break;
            case Mode.BULLET:
            case Mode.HOOK:
               Focus.Behavior = DynamicBehaviorEnum.AIRFOIL;
               mPositionAtFire = aPosition;
               mDistanceUntilDescent = Constants.ZoomerDistanceCoefficient * aVelocity.Length /
                                       Constants.Gravity;
               break;
         }
      }

      public override void Step(double aDT, Vector aWind)
      {
         if ((Focus == null) || (mPositionAtFire == null) || (mDistanceUntilDescent == null))
         {
            return;
         }

         if ((mIsDescending == false) && ((Focus.Position - mPositionAtFire).Length >= mDistanceUntilDescent))
         {
            mIsDescending = true;

            if (mMode == Mode.BULLET)
            {
               // Treat the body as if it is losing or has lost its lift. As a result, it no
               // longer behaves like an airfoil and is now a generic but lightweight object.
               Focus.Behavior = DynamicBehaviorEnum.FEATHER;
            }
            else if (mMode == Mode.HOOK)
            {
               // The goal now is to immediately change the body's direction with its current
               // velocity (trajectory) and the wind being the two determining factors.
               // Sufficiently strong wind would be the primary determining factor.

               // As a general rule, a larger (positive) dot product means the vectors are closer in
               // direction, a smaller (negative) dot product means they oppose, and values near zero
               // are perpendicular.

               Vector unitVelocity = Focus.Velocity.Normalized();
               Vector unitWind = aWind.Normalized();
               double dotProduct = Vector.Dot(unitVelocity, unitWind);
               // The dot product ranges from + or - the product of the vectors' lengths [-1.0, 1.0]
               // and this normalizes the value to the 0.0 - 1.0 range. This is done to keep the input
               // into the distribution function (next step) in the expected range.
               dotProduct = Math.Abs(dotProduct);

               // This is a logistic function with a midpoint at 0.5 and an arbitrarily-chosen
               // growth rate. (The function looks like an 'S' when plotted.)
               // The input is expected to be 0 to 1 and output will be 0 to 1 as well. As a result,
               // the velocity will be weighted less the more perpendicular the vectors are.
               double velocityWeight = 1.0 /
                                      (1.0 + Math.Exp(-Constants.ZoomerGrowthRate * (dotProduct - 0.5)));
               
               // The wind gets weighted with the remainder so that it always has an effect, but VERY
               // little for vectors in the same or opposite general direction(s).
               double windWeight = 1.0 - velocityWeight;

               // Create a vector to represent the new velocity if wind were not a factor.
               // This vector is simply directed down at 45 degrees in the same X direction.
               Vector rotatedVelocity = new Vector(Math.Sign(Focus.Velocity.X), 1.0);

               // Create a similar vector to represent the new wind if velocity were not a factor.
               Vector rotatedWind;
               if ((aWind.X == 0.0) && (aWind.Y < 0.0))
               {
                  // If the wind is directly up.

                  // Use a purely horizontal vector to represent the wind. This vector is pointing
                  // in the same direction as the velocity.
                  rotatedWind = new Vector(Math.Sign(Focus.Velocity.X), 0.0);
               }
               else if (aWind.X == 0.0)
               {
                  // If the wind is direclty down.

                  // Use a purely vertical vector to represent the wind. This vector is pointing
                  // straight up.
                  rotatedWind = new Vector(0.0, -1.0);
               }
               else
               {
                  // Rember if the wind is in a westward direction. We'll need to know this later.
                  bool isWestward = aWind.X < 0.0;
                  // Create an eastward vector to represent the wind and assume +Y = up for the rest
                  // of this block. The result will be horizontally mirrored again if it was originally
                  // westward and will be vertically mirrored in al lcases. The math is simpler this way.
                  Vector wind = new Vector(Math.Abs(aWind.X), -aWind.Y);
                  // Apply a 90-degree counterclockwise rotation.
                  // Note: (x, y) -> (-y, x) is counterclockwise if +Y = up.
                  wind = new Vector(-wind.Y, wind.X);
                  // The vector currently rotated such that that the its direction is in the
                  // northern hemisphere meaning its direction is between 0 and pi. We want to
                  // rotate the vector such that theta_1 / pi = theta_2 / (pi / 2). You can think of
                  // this as compressing the vector into one quadrant (the northeast one, for now).
                  wind = Vector.Rotate(wind, -wind.AngleInRadians / 2.0);
                  // Apply a 90-degree clockwise rotation.
                  // Note: (x, y) -> (y, -x) is clockwise if +Y = up.
                  wind = new Vector(wind.Y, -wind.X);

                  // The vector is now in the southeastern quadrant.
                  // If the wind was originally westward.
                  if (isWestward)
                  {
                     // Horizontally mirror the vector so its pointing westward again.
                     wind.X = -wind.X;
                  }
                  
                  // Return to the vector to +Y = down coordinates.
                  rotatedWind = new Vector(wind.X, -wind.Y);
               }

               // Calculate the resulting velocity vector using the wind and previous velocity
               // vectors with the weights determined above.
               Vector result = velocityWeight * rotatedVelocity + windWeight * rotatedWind;

               // Maintain the original speed of the body.
               result.Length = Focus.Velocity.Length;

               Focus.Velocity = result;
            }
         }
      }

      protected ZoomerProjectile12(ZoomerProjectile12 aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mMode = aOther.mMode;
         mPositionAtFire = new Vector(aOther.mPositionAtFire);
         mDistanceUntilDescent = aOther.mDistanceUntilDescent;
         mIsDescending = aOther.mIsDescending;
      }
   }
}
