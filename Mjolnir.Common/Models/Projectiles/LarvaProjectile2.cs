﻿using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class LarvaProjectile2 : Projectile
   {
      private Vector mTetherVector = null;
      private double mTetheredDistance = 0.0;

      public LarvaProjectile2(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new LarvaProjectile2(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         // Create a ghost focus that will manage the (tethered) position of the four
         // actual balls.
         Focus = new Body
         {
            Radius = Player.Shot.BodyRadius,
            Position = aPosition,
            Velocity = aVelocity,
            IsSettled = false
         };

         ProgressedBodies.Add(Focus);
         // Note: the Focus is not added to the list of corporeal bodies.

         // Create four balls that will initially be tethered to the focus.
         for (int i = 0; i < 4; i++)
         {
            // Each ball will be of the private Ball type, itself a child of Body.
            Ball ball = new Ball
            {
               Radius = Player.Shot.BodyRadius,
               Position = aPosition,
               Velocity = aVelocity,
               IsSettled = false
            };

            CorporealBodies.Add(ball);
            // Note: the ball is not added to the list of progressed bodies yet.
         }

         // Calculate a vector to be used to tether the four balls together during their
         // initial fly through the air. This vector will be more or less vertical but
         // will have some rotation applied due to the firing angle.
         mTetherVector = new Vector(0.0, 1.0);

         // Create a reference vector. The more the trajectory differs from this reference,
         // the more rotation is applied to the tether vector. This reference is essentially
         // a 45 degree firing angle meaning that firing at 45 degrees would result in a
         // perfectly vertical tether vector.
         Vector referenceVector = new Vector(1.0, -1.0);

         // If the player is facing left, the reference vector should too.
         if (Player.Direction == Direction.LEFT)
         {
            referenceVector.X = -referenceVector.X;
         }

         // Rotate the tether vector, which is currently perfectly vertical, by an amount
         // relative to the reference vector.
         mTetherVector = Vector.Rotate(mTetherVector,
                                       (aVelocity.AngleInRadians - referenceVector.AngleInRadians) / 2.0);
      }

      public override void Step(double aDT, Vector aWind)
      {
         if ((Focus == null) || (mTetherVector == null))
         {
            return;
         }

         if (mTetheredDistance < Constants.LarvaBallMaxTetherDistanceInPixels)
         {
            mTetheredDistance += Constants.LarvaBallMaxTetherDistanceInPixels * aDT;

            if (mTetheredDistance > Constants.LarvaBallMaxTetherDistanceInPixels)
            {
               mTetheredDistance = Constants.LarvaBallMaxTetherDistanceInPixels;
            }
         }

         // For each body that changes over time.
         // (Initially, this will just be the Focus but will include at least one ball
         // once any ball collides with the stage.)
         foreach (Body body in ProgressedBodies)
         {
            // If the body is a Ball and it has begun its countdown due to colliding
            // with the stage.
            if ((body is Ball ball) && (ball.Countdown.HasValue == true))
            {
               // Continue the countdown.
               ball.Countdown -= aDT;

               // If the ball has expired after bouncing around the stage.
               if (ball.Countdown <= 0.0)
               {
                  // Detonate it where it is and remove it.
                  Detonations.Add(ball.Center);
                  Remove(ball);
               }
            }
         }

         // If the Focus is still unsettled, effectively meaning that the four actual balls
         // are still tethered to it.
         if (Focus.IsSettled == false)
         {
            // Iterate through the four balls using i to determine spacing.
            int i = 0;
            foreach (Ball ball in CorporealBodies)
            {
               // Create a vector to be used as the offset from the Focus for this ball.
               // This vector's direction is derived from the tether vector determiend at
               // launch and its distance is derived from the position in the corporeal bodies.
               Vector offsetVector = new Vector(mTetherVector)
               {
                  Length = mTetheredDistance * i
               };

               // Set the ball's position by adding the offset vector to the Focus' position.
               ball.Position = Focus.Position + offsetVector;

               // Set the ball's velocity to match the Focus.
               ball.Velocity = Focus.Velocity;

               i += 1;
            }
         }
      }

      public override void OnStageCollision(Body aBody, Vector aPoint)
      {
         // If the Focus is still unsettled, effectively meaning that this is the first
         // time any ball has collided with the stage.
         if (Focus.IsSettled == false)
         {
            // The Focus is settled once any of the ball collides with the stage.
            Focus.IsSettled = true;

            // Add all of the balls (the corporeal bodies list) to the list
            // of progressed bodies so physics are applied to them going forward.
            ProgressedBodies.AddRange(CorporealBodies);
         }

         // If the body is a Ball and it hasn't begun counting down (i.e. this is the
         // first time it collided with the stage).
         if ((aBody is Ball ball) && (ball.Countdown.HasValue == false))
         {
            // Begin the countdown for this ball by setting its Countdown property.
            ball.Countdown = Constants.LarvaBounceDurationInMilliseconds / 1000.0;
         }
      }

      private LarvaProjectile2(LarvaProjectile2 aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mTetherVector = new Vector(aOther.mTetherVector);
         mTetheredDistance = aOther.mTetheredDistance;
      }

      /// <summary>
      /// Private class to represent one of the four balls that are part of this Projectile.
      /// This ball is essentially just a Body with a specific dynamic behavior and an
      /// additional variable to track how long the ball has been bouncing around the stage.
      /// </summary>
      private class Ball : Body
      {
         public double? Countdown = null;

         public Ball()
         {
            Behavior = DynamicBehaviorEnum.BALL;
         }
      }
   }
}
