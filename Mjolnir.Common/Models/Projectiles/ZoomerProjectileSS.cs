﻿using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class ZoomerProjectileSS : ZoomerProjectile12
   {
      private readonly double mPowerUpTimeInSeconds = Constants.ZoomerPowerUpTimeInMilliseconds / 1000.0;
      private bool mIsPoweredUp = false;
      private double mElapsedSeconds = 0.0;

      public ZoomerProjectileSS(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new ZoomerProjectileSS(this, aWorldState);
      }

      public override void Step(double aDT, Vector aWind)
      {
         if (Focus == null)
         {
            return;
         }

         base.Step(aDT, aWind);

         mElapsedSeconds += aDT;

         // If still in the first, lower-power stage but it's time for the second, full-power stage.
         if ((mIsPoweredUp == false) && (mElapsedSeconds >= mPowerUpTimeInSeconds))
         {
            // Add a couple more, overlapping bodies in order to perform a second and third
            // detonation on a collision. Although these detonations could be performed directly,
            // this method has the benefit of raising an event that a projectile view can receive
            // and represent (some visual effect to show the projectile powered up).
            for (int i = 0; i < 2; i++)
            {
               Body shadowBody = new Body(Focus);
               ProgressedBodies.Add(shadowBody);
               CorporealBodies.Add(shadowBody);
            }

            OnPropertyChanged(nameof(ProgressedBodies));
            OnPropertyChanged(nameof(CorporealBodies));

            mIsPoweredUp = true;
         }
      }

      public override void OnStageCollision(Body aBody, Vector aPoint)
      {
         OnAnyCollision(aPoint);
      }

      public override void OnPlayerCollision(Body aBody, Vector aPoint, Player aPlayer)
      {
         OnAnyCollision(aPoint);
      }

      private void OnAnyCollision(Vector aPoint)
      {
         // There are either one or three bodies in this projectile depending on the time.
         // Immediately do a detonation for it/them by borrowing a base method.
         foreach (Body body in CorporealBodies)
         {
            base.OnStageCollision(body, aPoint);
         }
      }

      private ZoomerProjectileSS(ZoomerProjectileSS aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mPowerUpTimeInSeconds = aOther.mPowerUpTimeInSeconds;
         mIsPoweredUp = aOther.mIsPoweredUp;
         mElapsedSeconds = aOther.mElapsedSeconds;
      }
   }
}
