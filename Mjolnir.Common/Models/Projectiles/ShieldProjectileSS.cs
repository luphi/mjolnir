﻿using Mjolnir.Common.Physics;
using System.Linq;

namespace Mjolnir.Common.Models.Projectiles
{
   public class ShieldProjectileSS : Projectile
   {
      private readonly double mPowerUpTimeInSeconds = Constants.ShieldPowerUpTimeInMilliseconds / 1000.0;
      private bool mIsPoweredUp = false;
      private double mElapsedSeconds = 0.0;

      public ShieldProjectileSS(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new ShieldProjectileSS(this, aWorldState);
      }

      public override void Step(double aDT, Vector aWind)
      {
         mElapsedSeconds += aDT;

         // If still in the first, half-power stage but it's time for the second, full-power stage.
         if ((mIsPoweredUp == false) && (mElapsedSeconds >= mPowerUpTimeInSeconds))
         {
            // Add a second, overlapping body in order to perform a second detonation on a collision.
            // Although a second detonation could be performed directly, this method has the benefit
            // of raising an event that a projectile view can receive and represent (some visual
            // effect to show the projectile powered up).
            Body shadowBody = new Body(Focus);
            ProgressedBodies.Add(shadowBody);
            CorporealBodies.Add(shadowBody);

            OnPropertyChanged(nameof(ProgressedBodies));
            OnPropertyChanged(nameof(CorporealBodies));
            mIsPoweredUp = true;
         }
      }

      public override void OnStageCollision(Body aBody, Vector aPoint)
      {
         OnAnyCollision(aPoint);
      }

      public override void OnPlayerCollision(Body aBody, Vector aPoint, Player aPlayer)
      {
         OnAnyCollision(aPoint);
      }

      private void OnAnyCollision(Vector aPoint)
      {
         // There are either one or two bodies in this projectile depending on the time.
         // Immediately do a detonation for it/them by borrowing a base method.
         foreach (Body body in CorporealBodies.ToList())
         {
            base.OnStageCollision(body, aPoint);
         }
      }

      private ShieldProjectileSS(ShieldProjectileSS aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mPowerUpTimeInSeconds = aOther.mPowerUpTimeInSeconds;
         mIsPoweredUp = aOther.mIsPoweredUp;
         mElapsedSeconds = aOther.mElapsedSeconds;
      }
   }
}
