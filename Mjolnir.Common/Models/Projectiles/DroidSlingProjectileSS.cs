﻿using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class DroidSlingProjectileSS : Projectile
   {
      private World mWorld = null;
      private Vector mRecentVelocity = Vector.Zero;
      private bool mWasSettled = false;
      private bool mHasLanded = false;
      private double mElapsedSeconds = 0.0;

      public DroidSlingProjectileSS(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new DroidSlingProjectileSS(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         base.Launch(aWorld, aPosition, aVelocity);

         mWorld = aWorld;
      }

      public override void Step(double aDT, Vector aWind)
      {
         if ((Focus == null) || (mWorld == null))
         {
            return;
         }

         // If we haven't already determined that the mine landed, is currently settled on the stage,
         // and was not settled last step.
         if ((mHasLanded == false) && (Focus.IsSettled == true) && (mWasSettled == false))
         {
            // This is when the mine's unique behavior begins.
            // It will detonate if it does not collide with a player before the timeout so we'll begin
            // counting the time now.
            mHasLanded = true;
            mElapsedSeconds = 0.0;

            // The mine will walk in the same direction it was just flying.
            Focus.Direction = mRecentVelocity.X <= 0.0 ? Direction.LEFT : Direction.RIGHT;
         }

         // If the mine landed on the stage this or a previous step.
         if (mHasLanded == true)
         {
            if (mElapsedSeconds >= Constants.DroidSlingMineTimeoutInSeconds)
            {
               // If the mine has been walking around long enough.

               // Its time is up. Boom.
               Detonations.Add(Focus.Center);
            }
            else
            {
               // If the mine is still walking along the stage.

               // Continue counting the elapsed time.
               mElapsedSeconds += aDT;

               // If the mine isn't walking.
               if (Focus.IsWalking == false)
               {
                  if (mWorld.Walk(Focus) == true)
                  {
                     // If, when attempting to walk, the World claims the mine CAN walk in
                     // its current direction

                     mWorld.Start();
                  }
                  else
                  {
                     // If the mine is unable to walk in its current direction.

                     // About-face and try again.
                     Focus.Direction = Focus.Direction == Direction.LEFT ? Direction.RIGHT : Direction.LEFT;

                     if (mWorld.Walk(Focus) == false)
                     {
                        // If, when attempting to walk, the World still claims the mine cannot walk in the
                        // other direction either.

                        // This is a very unlikely case but the mine is stuck and, rather than just
                        // flip back and forth dozens of times per second, go boom.
                        Detonations.Add(Focus.Center);
                     }
                     else
                     {
                        // If the mine was able to walk in the opposite direction after all.

                        mWorld.Start();
                     }
                  }
               }
            }
         }

         mRecentVelocity = Focus.Velocity;
         mWasSettled = Focus.IsSettled;
      }

      private DroidSlingProjectileSS(DroidSlingProjectileSS aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mWorld = aWorldState.World;
         mRecentVelocity = new Vector(aOther.mRecentVelocity);
         mWasSettled = aOther.mWasSettled;
         mHasLanded = aOther.mHasLanded;
         mElapsedSeconds = aOther.mElapsedSeconds;
      }
   }
}
