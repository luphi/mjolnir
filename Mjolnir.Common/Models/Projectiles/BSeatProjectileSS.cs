﻿using Mjolnir.Common.Physics;
using System;

namespace Mjolnir.Common.Models.Projectiles
{
   public class BSeatProjectileSS : Projectile
   {
      private const uint mNumberOfShots = 5;

      private readonly double mFiringPauseInSeconds = 1.0 / Constants.BSeatFiringFrequencyInHertz;
      private readonly double mDeltaThetaInRadians = Constants.BSeatSatelliteMovementDThetaInDegrees *
                                                     Math.PI / 180.0;
      private readonly Body mOriginalSatellite;
      private readonly Body mSatellite;
      private bool mIsFiring = false;
      private bool mIsDone = false;
      private uint mShotsFired = 0;
      private Vector mTarget = null;
      private double mElapsedSeconds = 0.0;

      public override bool IsSettled => mIsDone;

      public override bool IsDone => mIsDone;

      public BSeatProjectileSS(Player aPlayer) : base(aPlayer)
      {
         mOriginalSatellite = new Body(aPlayer.Satellite);
         mSatellite = aPlayer.Satellite;
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new BSeatProjectileSS(this, aWorldState);
      }

      public override void Step(double aDT, Vector aWind)
      {
         if (mIsFiring == true)
         {
            // If the body collided with something.

            // Firing is underway and a target is set. Lasers will fire periodically
            // from the satellite above the (B.Seat) player that fired this projectile.
            mElapsedSeconds += aDT;

            // If enough time for the next laser has passed.
            if (mElapsedSeconds >= mFiringPauseInSeconds)
            {
               // Do one more laser fire. If this is the last laser to fire, this method
               // will clean up.
               DoFire();
               // Continue tracking the time that has passed.
               mElapsedSeconds -= mFiringPauseInSeconds;
            }
         }
         else if (mShotsFired >= mNumberOfShots)
         {
            // If all the shots have been fired and this should be the last step.

            // Return the satellite where it was above the player.
            mSatellite.Sync(mOriginalSatellite);
            mIsDone = true;
         }
      }

      public override void OnStageCollision(Body aBody, Vector aPoint)
      {
         OnAnyCollision(aBody, aPoint);
      }

      public override void OnPlayerCollision(Body aBody, Vector aPoint, Player aPlayer)
      {
         OnAnyCollision(aBody, aPoint);
      }

      private void OnAnyCollision(Body aBody, Vector aPoint)
      {
         // The body's job was to illuminate the target. With its job done, it can go.
         Remove(aBody);

         // Set a couple variables that will be used by Step() going forward and...
         mIsFiring = true;
         mTarget = aPoint;
         // ...immediately fire a single laser.
         DoFire();
      }

      private void DoFire()
      {
         // Determine theta. Theta is the input for some upcoming sine and cosine calls that will move the
         // satellite slightly each time it fires.
         double theta;
         switch (mShotsFired)
         {
            case 0:
            case 4:
               // Above the target, a little to the right.
               theta = (Math.PI / 2.0) - mDeltaThetaInRadians;
               break;
            case 1:
            case 3:
            default:
               // Directly above the target.
               theta = Math.PI / 2.0;
               break;
            case 2:
               // Above the target, a little to the left.
               theta = (Math.PI / 2.0) + mDeltaThetaInRadians;
               break;
         }
         // Set the satellite's X position more or less above the target.
         mSatellite.X = mTarget.X + (Constants.BSeatSatellitePathRadiusInPixels * Math.Cos(theta)) -
                        (mSatellite.Radius / 2.0);
         // Set the satellite's Y position more or less at the typical altitude. At theta = pi/2,
         // the Y is equal to the constant. When theta != pi/2, the satellite is lowered slightly.
         mSatellite.Y = -Constants.BSeatSatelliteBaseAltitudeInPixels +
                        Constants.BSeatSatellitePathRadiusInPixels -
                        (Constants.BSeatSatellitePathRadiusInPixels * Math.Sin(theta)) -
                        (mSatellite.Radius / 2.0);

         // Add a "detonation" at the target the body hit. The World will treat this
         // detonation a bit differently and cast a beam towards it rather than perform
         // a shooting directly at the target.
         Detonations.Add(mTarget);
         mShotsFired += 1;

         // If we've fired all of the lasers.
         if (mShotsFired >= mNumberOfShots)
         {
            // Unflag the "is firing" variable.
            mIsFiring = false;
            // Step() will do one last thing now: returning the satellite to its original position.
         }
      }

      private BSeatProjectileSS(BSeatProjectileSS aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mFiringPauseInSeconds = aOther.mFiringPauseInSeconds;
         mDeltaThetaInRadians = aOther.mDeltaThetaInRadians;
         mOriginalSatellite = aWorldState.BodiesMap[mOriginalSatellite._id];
         mSatellite = aWorldState.BodiesMap[mSatellite._id];
         mIsFiring = aOther.mIsFiring;
         mIsDone = aOther.mIsDone;
         mShotsFired = aOther.mShotsFired;
         mTarget = aOther.mTarget != null ? new Vector(aOther.mTarget) : null;
         mElapsedSeconds = aOther.mElapsedSeconds;
      }
   }
}
