﻿using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class LarvaProjectileSS : Projectile
   {
      private bool mIsBouncing = false;
      private double mBounceCountdown = 0.0;
      private double mTriggerCountdown = 0.0;

      public LarvaProjectileSS(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new LarvaProjectileSS(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         base.Launch(aWorld, aPosition, aVelocity);

         // This projectile consists of a single ball that bounces around while continually
         // performing mini detonations. To that end, we'll just reuse the base method with
         // the only change to the body being a change in behavior.
         Focus.Behavior = DynamicBehaviorEnum.BALL;
      }

      public override void Step(double aDT, Vector aWind)
      {
         if (mIsBouncing == true)
         {
            // Continue the "waiting to hurt someone" countdown.
            mTriggerCountdown -= aDT;

            // If enough time has passed and another mini detonation should happen.
            if (mTriggerCountdown <= 0.0)
            {
               // Do the detonation.
               Detonations.Add(Focus.Center);
               mTriggerCountdown += 1.0 / Constants.BurnFrequencyInHertz;
            }

            // Continue the "bouncing around the stage" countdown.
            mBounceCountdown -= aDT;

            // If the ball has expired after bouncing around the stage.
            if (mBounceCountdown <= 0.0)
            {
               Remove(Focus);
            }
         }
      }

      public override void OnStageCollision(Body aBody, Vector aPoint)
      {
         // If this is the first contact with the stage.
         if (mIsBouncing == false)
         {
            // Set a flag that causes Step() to harm nearby players.
            mIsBouncing = true;

            // Set some time-tracking variables that are used to 1) expire the projectile
            // after a few seconds, and 2) trigger continuous mini detonations while the
            // projectile is still active.
            mBounceCountdown = Constants.LarvaBounceDurationInMilliseconds / 1000.0;
            mTriggerCountdown = 1.0 / Constants.BurnFrequencyInHertz;
         }
      }

      public override void OnPlayerCollision(Body aBody, Vector aPoint, Player aPlayer)
      {
         // Do nothing.
      }

      private LarvaProjectileSS(LarvaProjectileSS aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mIsBouncing = aOther.mIsBouncing;
         mBounceCountdown= aOther.mBounceCountdown;
         mTriggerCountdown = aOther.mTriggerCountdown;
      }
   }
}
