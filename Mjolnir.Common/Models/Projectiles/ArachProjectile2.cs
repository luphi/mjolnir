﻿using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class ArachProjectile2 : Projectile
   {
      public ArachProjectile2(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new ArachProjectile2(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         base.Launch(aWorld, aPosition, aVelocity);

         Focus.Behavior = DynamicBehaviorEnum.SANDSHARK;
      }

      public override void OnStageEmergence(Body aBody)
      {
         OnStageCollision(aBody, aBody.Center);
      }

      private ArachProjectile2(ArachProjectile2 aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
      }
   }
}
