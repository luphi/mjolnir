﻿using System;
using System.Collections.Generic;
using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class DroidSlingProjectile2 : Projectile
   {
      private readonly double mMaxTargetDistance = Constants.WalkDistanceInPixels * 40.0;
      private Body mMine1 = null;
      private Body mMine2 = null;
      private World mWorld = null;
      private bool mIsDoingPreturn = false;
      private Body mWalkingMine = null;
      private Player mWalkingMineTarget = null;
      private double mElapsedSeconds = 0.0;

      public override bool HasPreTurn => true;

      public DroidSlingProjectile2(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new DroidSlingProjectile2(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         base.Launch(aWorld, aPosition, aVelocity);

         mMine1 = new Body(Focus)
         {
            Velocity = aVelocity * 1.05
         };
         mMine2 = new Body(Focus)
         {
            Velocity = aVelocity * 0.95
         };

         // This child Projectile uses the primary body as an unseen, non-corporeal body.
         // This ghost primary will proceed with a normal, unaltered trajectory and the actual bodies
         // will the same angle but slightly different speeds.

         // The Focus was already added to the progressed bodies list by the base method.
         ProgressedBodies.Add(mMine1);
         ProgressedBodies.Add(mMine2);

         // The Focus was added to the corporeal bodies list by the base method. We don't want that.
         CorporealBodies.Remove(Focus);
         CorporealBodies.Add(mMine1);
         CorporealBodies.Add(mMine2);

         mWorld = aWorld;
      }

      public override void Remove(Body aBody)
      {
         if (ReferenceEquals(aBody, mMine1) == true)
         {
            // If mine 1 is being removed, nullify it.

            mMine1 = null;
         }
         else if (ReferenceEquals(aBody, mMine2) == true)
         {
            // If mine 2 is being remoed, nullify it.

            mMine2 = null;
         }

         // If the mine being removed is currently walking during a pre-turn.
         if (ReferenceEquals(aBody, mWalkingMine) == true)
         {
            // The mine either fell off the stage or detonate. In any case, its turn is over.
            mWalkingMine = null;
            mWalkingMineTarget = null;
            mElapsedSeconds = 0.0;
            // Step() will take it from here.
         }

         base.Remove(aBody);
      }

      public override void Step(double aDT, Vector aWind)
      {
         if ((Focus == null) || (mWorld == null))
         {
            return;
         }

         if (mIsDoingPreturn == true)
         {
            mElapsedSeconds += aDT;

            // It's possible, and intended, that the walking mine will have come into contact with the
            // target player, detonated, and been removed.
            if (mWalkingMine == null)
            {
               // Reuse the pre-turn event logic to try to pick the next mine.
               OnPreTurn();
            }

            // If either mine is active and, after performing this step, it has chosen to stop of its own accord.
            if ((mWalkingMine != null) && (DoPreTurnStep(mWalkingMine) == false))
            {
               // Once again reuse the pre-turn event logic to try to pick the next mine.
               OnPreTurn();
            }

            // If there is still no active mine.
            if (mWalkingMine == null)
            {
               // At this point, the walking-related variables are nulled and the pre-turn is done. The only
               // thing left to do is reset the pre-turn flag to make this projectile idle.
               mIsDoingPreturn = false;
            }
         }
         else if ((Focus.IsSettled == false) && ((mMine1?.IsSettled == true) || (mMine2?.IsSettled == true)))
         {
            // If this is the initial fly out, the Focus is still on the move, and one of the mines settled (just now).

            // The Focus will fly adjacent to the two mines. As a way of indicating a mine has landed on the stage
            // and to allow the players to see this, we'll pause the Focus where it is.
            Focus.IsSettled = true;
         }

         if ((Focus.IsSettled == true) && (mIsDoingPreturn == false))
         {
            OnPreTurn();
         }
      }

      public override void OnPreTurn()
      {
         if (mWorld == null)
         {
            return;
         }

         // If neither mine is currently walking and mine 1 is still in play and settled.
         if ((mWalkingMine == null) && (mMine1?.IsSettled == true))
         {
            DoPreTurnInitialization(mMine1);
            // Mine 1 is now walking towards a target player.
         }

         // If neither mine is currently walking, namely mine 1 from the above case, and mine 2 is still
         // in play and settled.
         if ((mWalkingMine == null) && (mMine2?.IsSettled == true))
         {
            // We don't care about the return value for mine 2. It may find a player nearby to target
            // or nothing will be done resulting in this projectile simply being idle.
            DoPreTurnInitialization(mMine2);
         }
      }

      private void DoPreTurnInitialization(Body aMine)
      {
         // Loop through all players in the World and find the one nearest to this mine while also
         // remembering the distance to him/her.
         IEnumerable<Player> players = mWorld.Players;
         double nearestDistance = double.MaxValue;
         Player nearestPlayer = null;
         foreach (Player player in players)
         {
            double distance = Vector.Distance(player.Center, aMine.Center);
            if (distance < nearestDistance)
            {
               nearestDistance = distance;
               nearestPlayer = player;
            }
         }

         // If the player is near enough to be targeted.
         if (nearestDistance <= mMaxTargetDistance)
         {
            // Determine the direction to the player from the mine. Walk() will use this property.
            aMine.Direction = (nearestPlayer.Center.X < aMine.Center.X) ? Direction.LEFT : Direction.RIGHT;

            // If nothing has prevented the mine from walking and it is, therefore, now walking.
            if (mWorld.Walk(aMine) == true)
            {
               // Set some variables that will be used as walking continues.
               mIsDoingPreturn = true;
               mWalkingMine = aMine;
               mWalkingMineTarget = nearestPlayer;
               mElapsedSeconds = 0.0;

               mWorld.Start();
            }
         }
      }

      private bool DoPreTurnStep(Body aMine)
      {
         // If the mine is close enough to the target player to detonate.
         if (Math.Abs(mWalkingMineTarget.Center.X - aMine.Center.X) <= aMine.Radius)
         {
            // This can mean one of two things:
            // 1) the mine will detonate the next time the World does collision detections, or
            // 2) the mine has walked above or below the player.
            // In case 1, the mine can stop because it's already certain to detonate. In case 2, the
            // mine can stop because it can't reach the player. Either way, the mine should stop walking.
            aMine.IsWalking = false;
            return false;
         }

         // If the mine is too far from the target player.
         if (Vector.Distance(mWalkingMineTarget.Center, aMine.Center) > mMaxTargetDistance)
         {
            // We can't have mines walking across the whole distance of the stage all willy-nilly.
            aMine.IsWalking = false;
            return false;
         }

         // If the mine has been walking for the maximum allowed time.
         if (mElapsedSeconds >= Constants.DroidSlingMineTimeoutInSeconds)
         {
            // It's time to stop walking. The mine can try again on some other pre-turn.
            aMine.IsWalking = false;
            return false;
         }

         // If not walking and not falling.
         if ((aMine.IsWalking == false) && (aMine.IsSettled == true) && (mWorld.Walk(aMine) == true))
         {
            mWorld.Start();
         }

         // Update the focus so the camera follows.
         Focus.X = aMine.X;
         Focus.Y = aMine.Y;

         // Indicate to the caller, if true, that this mine is still continuing its turn.
         return aMine.IsWalking == true;
      }

      private DroidSlingProjectile2(DroidSlingProjectile2 aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mMaxTargetDistance = aOther.mMaxTargetDistance;
         mMine1 = aOther.mMine1 != null ? aWorldState.BodiesMap[aOther.mMine1._id] : null;
         mMine2 = aOther.mMine2 != null ? aWorldState.BodiesMap[aOther.mMine2._id] : null;
         mWorld = aWorldState.World;
         mIsDoingPreturn = aOther.mIsDoingPreturn;
         mWalkingMine = aOther.mWalkingMine != null ? aWorldState.BodiesMap[aOther.mWalkingMine._id] : null;
         mWalkingMineTarget = aOther.mWalkingMineTarget != null ?
                              aWorldState.PlayersMap[aOther.mWalkingMineTarget._id] :
                              null;
         mElapsedSeconds = aOther.mElapsedSeconds;
      }
   }
}
