﻿using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class AkudaProjectileSS : Projectile
   {
      private readonly TeamEnum mTeam;

      public AkudaProjectileSS(Player aPlayer) : base(aPlayer)
      {
         mTeam = aPlayer.Team;
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new AkudaProjectileSS(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         base.Launch(aWorld, aPosition, aVelocity);

         Focus.Behavior = DynamicBehaviorEnum.INCORPOREAL;
      }

      public override void OnPlayerCollision(Body aBody, Vector aPoint, Player aPlayer)
      {
         // This projectile causes a Thor shooting for each player on the other team
         // it collides with.
         if (aPlayer.Team != mTeam)
         {
            Detonations.Add(aPoint);
         }
      }

      private AkudaProjectileSS(AkudaProjectileSS aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mTeam = aOther.mTeam;
      }
   }
}
