﻿using Mjolnir.Common.Physics;
using System;
using System.Collections.Generic;

namespace Mjolnir.Common.Models.Projectiles
{
   public class KameProjectileSS : Projectile
   {
      private const int mNumberOfBodiesAfterSplit = 6;

      private readonly double mSplitTimeInSeconds = Constants.KameSplitTimeInMilliseconds / 1000.0;
      private readonly double mTrajectorySeparationInRadians = Constants.KameTrajectorySeparationInDegrees *
                                                               Math.PI / 180.0;
      private bool mIsFirstStage = true; // True if unsplit, false otherwise.
      private double mElapsedSeconds = 0.0;

      public KameProjectileSS(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new KameProjectileSS(this, aWorldState);
      }

      public override void Step(double aDT, Vector aWind)
      {
         mElapsedSeconds += aDT;

         // If still in the first stage but it's time for the second stage.
         if ((mIsFirstStage == true) && (mElapsedSeconds >= mSplitTimeInSeconds))
         {
            // The Focus is now splitting up into multiple bodies. These bodies have will
            // trajectories based on, but offset from, from the Focus.
            List<Body> splitBodies = new List<Body>();
            double theta = Focus.Velocity.AngleInRadians - (mTrajectorySeparationInRadians *
                           ((mNumberOfBodiesAfterSplit / 2.0) - 0.5));
            double speed = Focus.Velocity.Length;
            for (int i = 0; i < mNumberOfBodiesAfterSplit; i++)
            {
               splitBodies.Add(new Body(Focus)
               {
                  Radius = Focus.Radius * 0.75,
                  Velocity = Vector.FromRadians(theta) * speed,
               });

               theta += mTrajectorySeparationInRadians;
            }

            // Remove the focus from the list of corporeal bodies but don't remove it from the
            // list of progressed bodies. We still want it to control where the camera looks but
            // it should no longer collide with the stage or players.
            CorporealBodies.Remove(Focus);
            // Add all of the new bodies that the focus has split into. 
            CorporealBodies.AddRange(splitBodies);
            OnPropertyChanged(nameof(CorporealBodies));

            // Add all of the new bodies to the list of progressed bodies.
            ProgressedBodies.AddRange(splitBodies);
            OnPropertyChanged(nameof(ProgressedBodies));

            mIsFirstStage = false;
         }
      }

      public override void OnStageCollision(Body aBody, Vector aPoint)
      {
         OnAnyCollision();

         base.OnStageCollision(aBody, aPoint);
      }

      public override void OnPlayerCollision(Body aBody, Vector aPoint, Player aPlayer)
      {
         OnAnyCollision();

         base.OnPlayerCollision(aBody, aPoint, aPlayer);
      }

      private void OnAnyCollision()
      {
         // If the Focus, whether split or unsplit, is still moving.
         if (Focus.IsSettled == false)
         {
            // Keep the Focus at its current position so the camera stops there too.
            Focus.IsSettled = true;
         }
      }

      private KameProjectileSS(KameProjectileSS aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
         mSplitTimeInSeconds = aOther.mSplitTimeInSeconds;
         mTrajectorySeparationInRadians = aOther.mTrajectorySeparationInRadians;
         mIsFirstStage = aOther.mIsFirstStage;
         mElapsedSeconds = aOther.mElapsedSeconds;
      }
   }
}
