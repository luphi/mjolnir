﻿using Mjolnir.Common.Physics;

namespace Mjolnir.Common.Models.Projectiles
{
   public class ArachProjectileSS : Projectile
   {
      public ArachProjectileSS(Player aPlayer) : base(aPlayer)
      {
      }

      public override Projectile Clone(WorldStateData aWorldState)
      {
         return new ArachProjectileSS(this, aWorldState);
      }

      public override void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         base.Launch(aWorld, aPosition, aVelocity);

         Focus.Behavior = DynamicBehaviorEnum.INCORPOREAL;
      }

      public override void OnPlayerCollision(Body aBody, Vector aPoint, Player aPlayer)
      {
         // This projectile detonates on the first contact with a player.
         Detonations.Add(aPoint);

         Remove(aBody);
      }

      private ArachProjectileSS(ArachProjectileSS aOther, WorldStateData aWorldState)
         : base(aOther, aWorldState)
      {
      }
   }
}
