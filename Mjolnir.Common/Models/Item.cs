﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Mjolnir.Common.Models
{
   [DataContract]
   public class Item
   {
      #region Serialized properties

      [DataMember]
      public ItemEnum ID { get; set; }

      [DataMember]
      public string Name { get; set; }

      [DataMember]
      public uint Slots { get; set; }

      [DataMember]
      public uint Delay { get; set; }

      #endregion


      /// <summary>
      /// Helper getter that returns a list of all Item models. This list contains
      /// instances of the items' classes.
      /// </summary>
      public static List<Item> List
      {
         get
         {
            // Get a list all item types from the enumeration defining them.
            List<ItemEnum> types = Enum.GetValues(typeof(ItemEnum)).Cast<ItemEnum>().ToList();

            // Remove the NONE value. We don't want to create a model with this type.
            types.RemoveAt(0);

            // Generate a list of instantiations of all types.
            List<Item> list = new List<Item>();
            foreach (ItemEnum type in types)
            {
               list.Add(new Item(type));
            }

            // Finally, return the list.
            return list;
         }
      }

      /// <summary>
      /// Helper getter that returns an instance of a random Item.
      /// </summary>
      public static Item Random
      {
         get
         {
            List<Item> list = List;
            return list[new Random().Next(list.Count)];
         }
      }

      public Item(ItemEnum aID)
      {
         ID = aID;


         #region Item-specific values

         switch (aID)
         {
            case ItemEnum.NONE:
               Name = "None";
               break;
            case ItemEnum.BANDAID:
               Name = "Bandaid";
               Slots = 1;
               Delay = 150;
               break;
            case ItemEnum.BLOOD:
               Name = "Blood";
               Slots = 1;
               Delay = 0;
               break;
            case ItemEnum.DUAL:
               Name = "Dual";
               Slots = 2;
               Delay = 600;
               break;
            case ItemEnum.DUAL_PLUS:
               Name = "Dual+";
               Slots = 2;
               Delay = 250;
               break;
            case ItemEnum.HEALTH_KIT:
               Name = "Health Kit";
               Slots = 2;
               Delay = 200;
               break;
            case ItemEnum.POWER_UP:
               Name = "Power Up";
               Slots = 1;
               Delay = 75;
               break;
            case ItemEnum.SHOVEL:
               Name = "Shovel";
               Slots = 1;
               Delay = 50;
               break;
            case ItemEnum.TEAM_TELEPORT:
               Name = "Team Teleport";
               Slots = 2;
               Delay = 50;
               break;
            case ItemEnum.TELEPORT:
               Name = "Teleport";
               Slots = 2;
               Delay = 100;
               break;
            case ItemEnum.WIND_CHANGE:
               Name = "Wind Change";
               Slots = 1;
               Delay = 75;
               break;
         }

         #endregion
      }

      public Item(Item aOther) : this(aOther.ID)
      {
      }

      public override string ToString()
      {
         return $"{{ {nameof(ID)} = {ID}, {nameof(Name)} = \"{Name}\", {nameof(Slots)} = {Slots}, " +
                $"{nameof(Delay)} = {Delay} }}";
      }

      public override bool Equals(object aObject)
      {
         if (aObject is Item item)
         {
            return Equals(item);
         }

         return false;
      }

      public bool Equals(Item aItem)
      {
         return ID == aItem.ID;
      }

      public override int GetHashCode()
      {
         unchecked // Allows integer overflow.
         {
            return 157 ^ ID.GetHashCode();
         }
      }

      /// <summary>
      /// Comparer class for use with .NET collection operations. This causes comparisons to be
      /// performed based solely on the item's ID.
      /// </summary>
      public class Comparer : IEqualityComparer<Item>
      {
         public bool Equals(Item aItem1, Item aItem2)
         {
            return ((int?)aItem1?.ID ?? -1) == ((int?)aItem2?.ID ?? -1);
         }

         public int GetHashCode(Item aItem)
         {
            return aItem?.GetHashCode() ?? 0;
         }
      }
   }
}
