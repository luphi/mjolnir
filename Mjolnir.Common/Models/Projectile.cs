﻿using Mjolnir.Common.Collections;
using Mjolnir.Common.Physics;
using NLog;

namespace Mjolnir.Common.Models
{
   public class Projectile : PropertyChangedBase
   {
      protected readonly ILogger mLog = LogManager.GetCurrentClassLogger();

      public Player Player { get; }

      /// <summary>
      /// The focal body, or a representation thereof. This property has no effects  within the
      /// World. It exists for clients to have something for the camera look at.
      /// </summary>
      public virtual Body Focus
      {
         get => mFocus;
         protected set => SetProperty(ref mFocus, value);
      }
      protected Body mFocus = null;

      /// <summary>
      /// A list of bodies that have physics applied directly.
      /// Items in this list may or may not overlap with those in the other list.
      /// </summary>
      public ConcurrentList<Body> ProgressedBodies { get; } = new ConcurrentList<Body>();

      /// <summary>
      /// A list of bodies that are checked for collisions within the World.
      /// Items in this list may or may not overlap with those in the other list.
      /// </summary>
      public ConcurrentList<Body> CorporealBodies { get; } = new ConcurrentList<Body>();

      public ConcurrentList<Vector> Detonations { get; } = new ConcurrentList<Vector>();

      public virtual bool HasPreTurn => false;

      public bool IsLaunched
      {
         get => mIsLaunched;
         set => SetProperty(ref mIsLaunched, value);
      }
      protected bool mIsLaunched = false;

      public virtual bool IsSettled
      {
         get
         {
            bool isSettled = true;

            foreach (Body body in CorporealBodies)
            {
               isSettled &= (body.IsSettled == true) && (body.IsWalking == false);
            }

            return isSettled;
         }
      }

      public virtual bool IsDone => CorporealBodies.Count == 0;

      public Projectile(Player aPlayer)
      {
         Player = aPlayer;
      }

      public virtual Projectile Clone(WorldStateData aWorldState)
      {
         return new Projectile(this, aWorldState);
      }

      public virtual void Launch(World aWorld, Vector aPosition, Vector aVelocity)
      {
         Focus = new Body
         {
            Radius = Player.Shot.BodyRadius,
            Position = aPosition,
            Velocity = aVelocity,
            IsSettled = false
         };
         ProgressedBodies.Add(Focus);
         CorporealBodies.Add(Focus);
      }

      public virtual void Step(double aDT, Vector aWind)
      {
      }

      public virtual void Remove(Body aBody)
      {
         // If the property change event should be invoked on the progressed bodies due to an item removal.
         if (ProgressedBodies.Remove(aBody) == true)
         {
            // Invoke the event on the list property to indicate the list has changed.
            OnPropertyChanged(nameof(ProgressedBodies));
         }

         // If the property change event should be invoked on the corporeal bodies due to an item removal.
         if (CorporealBodies.Remove(aBody) == true)
         {
            aBody.IsSettled = true;
            // Invoke the event on the list property to indicate the list has changed.
            OnPropertyChanged(nameof(CorporealBodies));
         }
      }

      public virtual void OnPreTurn()
      {
      }

      public virtual void OnStageCollision(Body aBody, Vector aPoint)
      {
         Detonations.Add(aPoint);

         Remove(aBody);
      }

      public virtual void OnStageEmergence(Body aBody)
      {
      }

      public virtual void OnPlayerCollision(Body aBody, Vector aPoint, Player aPlayer)
      {
         Detonations.Add(aPoint);

         Remove(aBody);
      }

      public override string ToString()
      {
         return $"{{ {nameof(Player)}.ID = {Player.ID}, {nameof(Focus)} = {Focus?.ToString() ?? "null"}, " +
                $"{nameof(ProgressedBodies)}.Count = {ProgressedBodies.Count}, " +
                $"{nameof(CorporealBodies)}.Count = {CorporealBodies.Count}, {nameof(HasPreTurn)} = {HasPreTurn}, " +
                $"{nameof(IsLaunched)} = {IsLaunched}, {nameof(IsDone)} = {IsDone}, " +
                $"{nameof(Detonations)}.Count = {Detonations.Count} }}";
      }

      protected Projectile(Projectile aOther, WorldStateData aWorldState)
      {
         Player = aWorldState.PlayersMap[aOther.Player.ID];
         mFocus = aWorldState.BodiesMap[aOther.mFocus._id];
         mIsLaunched = aOther.mIsLaunched;
         Detonations.AddRange(aOther.Detonations);

         foreach (Body body in aOther.ProgressedBodies)
         {
            ProgressedBodies.Add(aWorldState.BodiesMap[body._id]);
         }

         foreach (Body body in aOther.CorporealBodies)
         {
            CorporealBodies.Add(aWorldState.BodiesMap[body._id]);
         }
      }
   }
}
