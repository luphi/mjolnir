﻿namespace Mjolnir.Common.Models
{
   public class Launch
   {
      public uint PlayerID { get; set; }

      public Projectile Projectile { get; set; }

      public int DelayInMilliseconds { get; set; }

      public override string ToString()
      {
         return $"{{ {nameof(PlayerID)} = {PlayerID}, " +
                $"{nameof(Projectile)} = {Projectile?.ToString() ?? "null"}, " +
                $"{nameof(DelayInMilliseconds)} = {DelayInMilliseconds} }}";
      }
   }
}
