﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace Mjolnir.Common.Models
{
   /// <summary>
   /// Base class to be used for models implementing the INotifyPropertyChanged interface.
   /// This class provides 1) the PropertyChanged event, and 2) a protected method for
   /// setting the value of a property, if the value is new, and invoking the event.
   /// </summary>
   [DataContract]
   [KnownType(typeof(Player))]
   public class PropertyChangedBase : INotifyPropertyChanged
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();

      public event PropertyChangedEventHandler PropertyChanged;

      protected void SetProperty<T>(ref T aProperty, T aValue, [CallerMemberName] string aPropertyName = null)
      {
         // If the property's value is already the value being set, do not change it and do
         // not invoke the event.
         if (EqualityComparer<T>.Default.Equals(aProperty, aValue))
         {
            return;
         }

         // Set the property's new value and invoke the event to notify any observers.
         aProperty = aValue;
         OnPropertyChanged(aPropertyName);
      }

      protected void OnPropertyChanged(string aPropertyName)
      {
         try
         {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(aPropertyName));
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a property change event for " +
                          $"property \"{aPropertyName}\"");
         }
      }
   }
}
