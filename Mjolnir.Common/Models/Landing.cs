﻿namespace Mjolnir.Common.Models
{
   public class Landing
   {
      public Player Player { get; set; } = null;

      public Vector Point { get; set; } = null;

      public override string ToString()
      {
         return $"{{ {nameof(Player)} = {Player?.ToString() ?? "null"}, " +
                $"{nameof(Point)} = {Point?.ToString() ?? "null"} }}";
      }

      public override int GetHashCode()
      {
         unchecked // Allows integer overflow.
         {
            int hash = 439;
            hash *= 797 ^ Player.GetHashCode();
            hash *= 191 ^ Point.GetHashCode();
            return hash;
         }
      }
   }
}
