﻿using Mjolnir.Common.Models.Projectiles;
using Mjolnir.Common.Physics;
using NLog;
using System.Collections.Generic;

namespace Mjolnir.Common.Models
{
   public class Shot
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly int mLaunchesPerFire;
      private readonly int mLaunchDelayPerProjectileInMilliseconds;

      public MobileEnum MobileID { get; }

      public ShotEnum ShotID { get; }

      public ProjectileBehaviorEnum Behavior { get; }

      public uint Delay { get; }

      public int MinDamage { get; }

      public int MaxDamage { get; }

      public int? AftershockDamage { get; }

      public uint HoleMajorAxis { get; }

      public uint HoleMinorAxis { get; }

      public int BodiesPerProjectile { get; }

      public double? ResistanceReduction { get; }

      public double BodyRadius { get; }

      public bool HasAreaOfEffect { get; }


      public Shot(MobileEnum aMobileID, ShotEnum aShotID)
      {
         MobileID = aMobileID;
         ShotID = aShotID;


         #region Shot-specific values

         switch ((aMobileID, aShotID))
         {
            case (MobileEnum.AKUDA, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 250;
               MinDamage = 65; // Aftershock will apply additional damage later.
               MaxDamage = 85; // Aftershock will apply additional damage later.
               AftershockDamage = 75;
               HoleMajorAxis = 50;
               HoleMinorAxis = 40;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 5.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.AKUDA, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.THOR;
               Delay = 400;
               MinDamage = 140; // TODO World damage calculations
               MaxDamage = 500; // TODO World damage calculations
               AftershockDamage = null;
               HoleMajorAxis = 50;
               HoleMinorAxis = 50;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 7.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 3;
               mLaunchDelayPerProjectileInMilliseconds = 100;
               break;
            case (MobileEnum.AKUDA, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.THOR;
               Delay = 800;
               MinDamage = 80; // TODO World damage calculations
               MaxDamage = 250; // TODO World damage calculations
               AftershockDamage = null;
               HoleMajorAxis = 50;
               HoleMinorAxis = 50;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 7.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.ARACH, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 250;
               MinDamage = 100;
               MaxDamage = 150;
               AftershockDamage = null;
               HoleMajorAxis = 70;
               HoleMinorAxis = 60;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 7.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.ARACH, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 400;
               MinDamage = 190;
               MaxDamage = 300;
               AftershockDamage = null;
               HoleMajorAxis = 90;
               HoleMinorAxis = 70;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 10.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.ARACH, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 800;
               MinDamage = 350;
               MaxDamage = 500;
               AftershockDamage = null;
               HoleMajorAxis = 70;
               HoleMinorAxis = 60;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 9.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.B_SEAT, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.SATELLITE;
               Delay = 250;
               MinDamage = 100;
               MaxDamage = 160;
               AftershockDamage = null;
               HoleMajorAxis = 45;
               HoleMinorAxis = 30;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 5.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.B_SEAT, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.SATELLITE;
               Delay = 400;
               MinDamage = 200 / 3; // Per laser. This shot fires three lasers.
               MaxDamage = 280 / 3; // Per laser. This shot fires three lasers.
               AftershockDamage = null;
               HoleMajorAxis = 40;
               HoleMinorAxis = 25;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 5.0;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.B_SEAT, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.SATELLITE;
               Delay = 800;
               MinDamage = 400 / 5; // Per laser. This shot fires five lasers.
               MaxDamage = 550 / 5; // Per laser. This shot fires five lasers.
               AftershockDamage = null;
               HoleMajorAxis = 40;
               HoleMinorAxis = 25;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.DJ, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 250;
               MinDamage = 100; // Aftershock will apply additional damage later.
               MaxDamage = 130; // Aftershock will apply additional damage later.
               AftershockDamage = 40;
               HoleMajorAxis = 45;
               HoleMinorAxis = 30;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.DJ, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.GRAVITY_PULL;
               Delay = 340;
               MinDamage = 150;
               MaxDamage = 200;
               AftershockDamage = null;
               HoleMajorAxis = 65;
               HoleMinorAxis = 55;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.DJ, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.GRAVITY_PUSH;
               Delay = 800;
               MinDamage = 250;
               MaxDamage = 320;
               AftershockDamage = null;
               HoleMajorAxis = 65;
               HoleMinorAxis = 55;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.DROID_SLING, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 250;
               MinDamage = 100;
               MaxDamage = 160;
               AftershockDamage = null;
               HoleMajorAxis = 45;
               HoleMinorAxis = 45;
               BodiesPerProjectile = 3;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.DROID_SLING, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.STICKY;
               Delay = 400;
               MinDamage = 140;
               MaxDamage = 180;
               AftershockDamage = null;
               HoleMajorAxis = 30;
               HoleMinorAxis = 20;
               BodiesPerProjectile = 2;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.DROID_SLING, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.STICKY;
               Delay = 800;
               MinDamage = 200; // TODO is this correct?
               MaxDamage = 300; // TODO is this correct?
               AftershockDamage = null;
               HoleMajorAxis = 50;
               HoleMinorAxis = 30;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 10.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.FROST, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 250;
               MinDamage = 120;
               MaxDamage = 160;
               AftershockDamage = null;
               HoleMajorAxis = 75;
               HoleMinorAxis = 65;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.FROST, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 400;
               MinDamage = 150;
               MaxDamage = 170;
               AftershockDamage = null;
               HoleMajorAxis = 35;
               HoleMinorAxis = 25;
               BodiesPerProjectile = 1;
               ResistanceReduction = 0.05; // 5% reduction per shooting, 50% maximum.
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.FROST, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 800;
               MinDamage = 200;
               MaxDamage = 220;
               AftershockDamage = null;
               HoleMajorAxis = 55;
               HoleMinorAxis = 40;
               BodiesPerProjectile = 1;
               ResistanceReduction = 0.1; // 10% reduction per shooting, 50% maximum.
               BodyRadius = 10.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.KAME, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 250;
               MinDamage = 130;
               MaxDamage = 160;
               AftershockDamage = null;
               HoleMajorAxis = 50;
               HoleMinorAxis = 50;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 7.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.KAME, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 480;
               MinDamage = 220;
               MaxDamage = 280;
               AftershockDamage = null;
               HoleMajorAxis = 40;
               HoleMinorAxis = 40;
               BodiesPerProjectile = 2;
               ResistanceReduction = null;
               BodyRadius = 5.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.KAME, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 800;
               MinDamage = 400;
               MaxDamage = 600;
               AftershockDamage = null;
               HoleMajorAxis = 45;
               HoleMinorAxis = 45;
               BodiesPerProjectile = 6;
               ResistanceReduction = null;
               BodyRadius = 10.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.LARVA, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 250;
               MinDamage = 80;
               MaxDamage = 130;
               AftershockDamage = null;
               HoleMajorAxis = 50;
               HoleMinorAxis = 40;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 7.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.LARVA, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 480;
               MinDamage = 180;
               MaxDamage = 220;
               AftershockDamage = null;
               HoleMajorAxis = 50;
               HoleMinorAxis = 40;
               BodiesPerProjectile = 4; // Four balls are fired at once with slightly different trajectories.
               ResistanceReduction = null;
               BodyRadius = 6.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.LARVA, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 800;
               MinDamage = 10;
               MaxDamage = 40;
               AftershockDamage = null;
               HoleMajorAxis = 0; // The ball harms players but has no effect on the stage.
               HoleMinorAxis = 0; // The ball harms players but has no effect on the stage.
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.SASQUATCH, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 250;
               MinDamage = 170 / 4; // Four Projectiles are launched with each containing one missile.
               MaxDamage = 230 / 4; // Four Projectiles are launched with each containing one missile.
               AftershockDamage = null;
               HoleMajorAxis = 75;
               HoleMinorAxis = 40;
               BodiesPerProjectile = 1; // Four Projectiles are launched with each containing one missile.
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 4;
               mLaunchDelayPerProjectileInMilliseconds = 150;
               break;
            case (MobileEnum.SASQUATCH, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 400;
               MinDamage = 200 / 6; // Two projectiles are launched with each containing three bombs.
               MaxDamage = 270 / 6; // Two projectiles are launched with each containing three bombs.
               AftershockDamage = null;
               HoleMajorAxis = 40;
               HoleMinorAxis = 20;
               BodiesPerProjectile = 3; // Two projectiles are launched with each containing three bombs.
               ResistanceReduction = null;
               BodyRadius = 4.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 2;
               mLaunchDelayPerProjectileInMilliseconds = 150;
               break;
            case (MobileEnum.SASQUATCH, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 800;
               MinDamage = 350 / 8; // Eight Projectiles are launched with each containing one missile.
               MaxDamage = 500 / 8; // Eight Projectiles are launched with each containing one missile.
               AftershockDamage = null;
               HoleMajorAxis = 75;
               HoleMinorAxis = 40;
               BodiesPerProjectile = 1; // Eight Projectiles are launched with each containing one missile.
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 8;
               mLaunchDelayPerProjectileInMilliseconds = 100;
               break;
            case (MobileEnum.SHIELD, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 250;
               MinDamage = 120;
               MaxDamage = 165;
               AftershockDamage = null;
               HoleMajorAxis = 60;
               HoleMinorAxis = 40;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 7.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.SHIELD, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 480;
               MinDamage = 230;
               MaxDamage = 290;
               AftershockDamage = null;
               HoleMajorAxis = 50;
               HoleMinorAxis = 40;
               // This is a special case. The Projectile essentially has two lives. Its
               // single body will continue as if nothing happened after the first collision.
               // We'll consider this two bodies although only one exists at a time.
               BodiesPerProjectile = 2;
               ResistanceReduction = null;
               BodyRadius = 7.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.SHIELD, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 800;
               MinDamage = 350; // During the powered-up second phase, halved otherwise.
               MaxDamage = 500; // During the powered-up second phase, halved otherwise.
               AftershockDamage = null;
               HoleMajorAxis = 65;
               HoleMinorAxis = 50;
               // This shot has two phases separated by time. The latter deals twice
               // the damage of the former. The Projectile achieves this by creating a second,
               // overlapping body when the time to power up comes and detonates both in a
               // collision. Hence two bodies per projectile.
               BodiesPerProjectile = 2;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.THUNDER, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.LIGHTNING;
               Delay = 250;
               MinDamage = 75; // Aftershock will apply additional damage later.
               MaxDamage = 175; // Aftershock will apply additional damage later.
               AftershockDamage = 75;
               HoleMajorAxis = 45;
               HoleMinorAxis = 30;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 5.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.THUNDER, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.LIGHTNING_V;
               Delay = 300;
               MinDamage = 75; // Aftershock will apply additional damage later.
               MaxDamage = 225; // Aftershock will apply additional damage later.
               AftershockDamage = 75;
               HoleMajorAxis = 45;
               HoleMinorAxis = 30;
               BodiesPerProjectile = 2; // Technically just one, but the behavior creates two strikes.
               ResistanceReduction = null;
               BodyRadius = 5.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.THUNDER, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.LIGHTNING;
               Delay = 800;
               MinDamage = 50; // Aftershock will apply additional damage later.
               MaxDamage = 50; // Aftershock will apply additional damage later.
               AftershockDamage = 150;
               HoleMajorAxis = 60;
               HoleMinorAxis = 45;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = true;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.TRIKE, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 250;
               MinDamage = 100;
               MaxDamage = 150;
               AftershockDamage = null;
               HoleMajorAxis = 55;
               HoleMinorAxis = 40;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 9.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.TRIKE, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 400;
               MinDamage = 170;
               MaxDamage = 310;
               AftershockDamage = null;
               HoleMajorAxis = 40;
               HoleMinorAxis = 35;
               BodiesPerProjectile = 3;
               ResistanceReduction = null;
               BodyRadius = 7.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.TRIKE, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 800;
               MinDamage = 250;
               MaxDamage = 500;
               AftershockDamage = null;
               HoleMajorAxis = 75;
               HoleMinorAxis = 60;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.WIZARD, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 250;
               MinDamage = 100;
               MaxDamage = 160;
               AftershockDamage = null;
               HoleMajorAxis = 55;
               HoleMinorAxis = 45;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 6.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.WIZARD, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 400;
               MinDamage = 190;
               MaxDamage = 280;
               AftershockDamage = null;
               HoleMajorAxis = 45;
               HoleMinorAxis = 40;
               BodiesPerProjectile = 2;
               ResistanceReduction = null;
               BodyRadius = 5.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.WIZARD, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.SHIELD_STRIP;
               Delay = 800;
               MinDamage = 350;
               MaxDamage = 500;
               AftershockDamage = null;
               HoleMajorAxis = 70;
               HoleMinorAxis = 60;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = true;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.ZOOMER, ShotEnum.SHOT_1):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 250;
               MinDamage = 120;
               MaxDamage = 160;
               AftershockDamage = null;
               HoleMajorAxis = 50;
               HoleMinorAxis = 40;
               BodiesPerProjectile = 1;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
            case (MobileEnum.ZOOMER, ShotEnum.SHOT_2):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 400;
               MinDamage = 230 / 4; // Four Projectiles are launched with each containing one boomerang.
               MaxDamage = 290 / 4; // Four Projectiles are launched with each containing one boomerang.
               AftershockDamage = null;
               HoleMajorAxis = 50;
               HoleMinorAxis = 40;
               BodiesPerProjectile = 1; // Four Projectiles are launched with each containing one boomerang.
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 4;
               mLaunchDelayPerProjectileInMilliseconds = 75;
               break;
            case (MobileEnum.ZOOMER, ShotEnum.SPECIAL_SHOT):
               Behavior = ProjectileBehaviorEnum.STANDARD;
               Delay = 800;
               MinDamage = 350; // During the powered-up second phase, one third otherwise.
               MaxDamage = 900; // During the powered-up second phase, one third otherwise.
               AftershockDamage = null;
               HoleMajorAxis = 65;
               HoleMinorAxis = 50;
               // This shot has two phases separated by time. The latter deals three times
               // the damage of the former. The Projectile achieves this by creating two more,
               // overlapping bodies when the time to power up comes and detonates all three in
               // a collision. Hence three bodies per projectile.
               BodiesPerProjectile = 3;
               ResistanceReduction = null;
               BodyRadius = 8.0;
               HasAreaOfEffect = false;
               mLaunchesPerFire = 1;
               mLaunchDelayPerProjectileInMilliseconds = 0;
               break;
         }

         #endregion
      }

      /// <summary>
      /// Get a list of 1+ Projectile(s) that are being fired as part of this shot.
      /// Alongside each projectile is a delay, in milliseconds, to be applied before firing the
      /// associated Projectile. Delays may, and often will, be zero.
      /// 
      /// </summary>
      /// <returns></returns>
      public virtual List<Launch> Fire(World aWorld, Player aPlayer, Vector aPosition, Vector aVelocity)
      {
         List<Launch> launches = new List<Launch>();

         for (int i = 0; i < mLaunchesPerFire; i++)
         {
            Projectile projectile;
            switch ((MobileID, ShotID))
            {
               case (MobileEnum.AKUDA, ShotEnum.SPECIAL_SHOT):
                  projectile = new AkudaProjectileSS(aPlayer);
                  break;
               case (MobileEnum.ARACH, ShotEnum.SHOT_2):
                  projectile = new ArachProjectile2(aPlayer);
                  break;
               case (MobileEnum.ARACH, ShotEnum.SPECIAL_SHOT):
                  projectile = new ArachProjectileSS(aPlayer);
                  break;
               case (MobileEnum.B_SEAT, ShotEnum.SHOT_2):
                  projectile = new BSeatProjectile2(aPlayer);
                  break;
               case (MobileEnum.B_SEAT, ShotEnum.SPECIAL_SHOT):
                  projectile = new BSeatProjectileSS(aPlayer);
                  break;
               case (MobileEnum.DJ, ShotEnum.SPECIAL_SHOT):
                  projectile = new BurnProjectile(aPlayer, Constants.LocalizedGravityDurationInMilliseconds);
                  break;
               case (MobileEnum.DROID_SLING, ShotEnum.SHOT_1):
                  projectile = new DroidSlingProjectile1(aPlayer);
                  break;
               case (MobileEnum.DROID_SLING, ShotEnum.SHOT_2):
                  projectile = new DroidSlingProjectile2(aPlayer);
                  break;
               case (MobileEnum.DROID_SLING, ShotEnum.SPECIAL_SHOT):
                  projectile = new DroidSlingProjectileSS(aPlayer);
                  break;
               case (MobileEnum.KAME, ShotEnum.SHOT_2):
                  projectile = new KameProjectile2(aPlayer);
                  break;
               case (MobileEnum.KAME, ShotEnum.SPECIAL_SHOT):
                  projectile = new KameProjectileSS(aPlayer);
                  break;
               case (MobileEnum.LARVA, ShotEnum.SHOT_2):
                  projectile = new LarvaProjectile2(aPlayer);
                  break;
               case (MobileEnum.LARVA, ShotEnum.SPECIAL_SHOT):
                  projectile = new LarvaProjectileSS(aPlayer);
                  break;
               case (MobileEnum.SASQUATCH, ShotEnum.SHOT_1):
               case (MobileEnum.SASQUATCH, ShotEnum.SPECIAL_SHOT):
                  projectile = new SasquatchProjectile1(aPlayer);
                  break;
               case (MobileEnum.SASQUATCH, ShotEnum.SHOT_2):
                  projectile = new SasquatchProjectile2(aPlayer);
                  break;
               case (MobileEnum.SHIELD, ShotEnum.SHOT_2):
                  projectile = new ShieldProjectile2(aPlayer);
                  break;
               case (MobileEnum.SHIELD, ShotEnum.SPECIAL_SHOT):
                  projectile = new ShieldProjectileSS(aPlayer);
                  break;
               case (MobileEnum.TRIKE, ShotEnum.SHOT_2):
                  projectile = new TrikeProjectile2(aPlayer);
                  break;
               case (MobileEnum.TRIKE, ShotEnum.SPECIAL_SHOT):
                  projectile = new BurnProjectile(aPlayer, Constants.TrikeBurnTimeInMilliseconds);
                  break;
               case (MobileEnum.WIZARD, ShotEnum.SHOT_2):
                  projectile = new WizardProjectile2(aPlayer);
                  break;
               case (MobileEnum.ZOOMER, ShotEnum.SHOT_1):
               case (MobileEnum.ZOOMER, ShotEnum.SHOT_2):
                  projectile = new ZoomerProjectile12(aPlayer);
                  break;
               case (MobileEnum.ZOOMER, ShotEnum.SPECIAL_SHOT):
                  projectile = new ZoomerProjectileSS(aPlayer);
                  break;
               default:
                  projectile = new Projectile(aPlayer);
                  break;
            }

            projectile.Launch(aWorld, aPosition, aVelocity);

            launches.Add(new Launch
            {
               PlayerID = aPlayer.ID,
               Projectile = projectile,
               DelayInMilliseconds = mLaunchDelayPerProjectileInMilliseconds * i
            });
         }

         return launches;
      }

      public override string ToString()
      {
         return $"{{ {nameof(MobileID)} = {MobileID}, {nameof(ShotID)} = {ShotID}, {nameof(Delay)} = {Delay}, " +
                $"{nameof(Behavior)} = {Behavior}, {nameof(MinDamage)} = {MinDamage}, " +
                $"{nameof(MaxDamage)} = {MaxDamage}, {nameof(HoleMajorAxis)} = {HoleMajorAxis}, " +
                $"{nameof(HoleMinorAxis)} = {HoleMinorAxis}, " +
                $"{nameof(BodiesPerProjectile)} = {BodiesPerProjectile} }}";
      }
   }
}
