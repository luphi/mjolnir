﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Mjolnir.Common.Models
{
   [DataContract]
   [KnownType(typeof(Bot))]
   public class Player : Body
   {
      private const double mSatelliteRadius = 12.0;

      protected readonly ILogger mLog = LogManager.GetCurrentClassLogger();


      #region Serialized properties

      /// <summary>
      /// Unique identifier assigned by the server.
      /// </summary>
      [DataMember]
      public uint ID { get; private set; }

      /// <summary>
      /// Display name chosen by the player. Not guaranteed to be unique.
      /// </summary>
      [DataMember]
      public string Name { get; private set; }

      /// <summary>
      /// Basic flag indicating if the server recognized this user as someone
      /// with administrative privileges.
      /// If another player is flagged as the veteran, these privileges overrule
      /// the veteran completely.
      /// </summary>
      [DataMember]
      public bool IsAdmin { get; private set; }

      /// <summary>
      /// Basic flag indicating if this user is the most veteran present in a
      /// server and, therefore, has the privilege to choose some server
      /// properties in the absence of an admin.
      /// If an admin is present, he/she overrules the veteran completely.
      /// </summary>
      [DataMember]
      public bool IsVeteran
      {
         get => mIsVeteran;
         set => SetProperty(ref mIsVeteran, value);
      }
      protected bool mIsVeteran = false;

      [DataMember]
      public bool IsReady
      {
         get => mIsReady;
         set => SetProperty(ref mIsReady, value);
      }
      protected bool mIsReady = false;

      [DataMember]
      public bool IsCharging
      {
         get => mIsCharging;
         set => SetProperty(ref mIsCharging, value);
      }
      protected bool mIsCharging = false;

      [DataMember]
      public bool IsFiring
      {
         get => mIsFiring;
         set => SetProperty(ref mIsFiring, value);
      }
      protected bool mIsFiring = false;

      [DataMember]
      public bool IsDead
      {
         get => mIsDead;
         set => SetProperty(ref mIsDead, value);
      }
      protected bool mIsDead = false;

      /// <summary>
      /// The team the player is currently on. Either A xor B.
      /// </summary>
      [DataMember]
      public TeamEnum Team
      {
         get => mTeam;
         set => SetProperty(ref mTeam, value);
      }
      protected TeamEnum mTeam = TeamEnum.A;

      [DataMember]
      public ShotEnum ShotID
      {
         get => mShotID;
         set => SetProperty(ref mShotID, value);
      }
      protected ShotEnum mShotID = ShotEnum.SHOT_1;

      [DataMember]
      public int? SelectedItem
      {
         get => mSelectedItem;
         set => SetProperty(ref mSelectedItem, value);
      }
      private int? mSelectedItem = null;

      /// <summary>
      /// Value used to determine turn order. This will increment as during a player's
      /// turn based on his/her actions, or lack thereof. A higher (relative) value
      /// increases the player's time between turns.
      /// </summary>
      [DataMember]
      public uint Delay
      {
         get => mDelay;
         set => SetProperty(ref mDelay, value);
      }
      protected uint mDelay = 0;

      /// <summary>
      /// Current HP. The initial value at spawn will depend on the mobile.
      /// </summary>
      [DataMember]
      public int HP
      {
         get => mHP;
         set => SetProperty(ref mHP, value);
      }
      protected int mHP = 0;

      /// <summary>
      /// Current shield health, equivalent to HP. The initial value at spawn will
      /// depend on the mobile.
      /// </summary>
      [DataMember]
      public int ShieldHP
      {
         get => mShield;
         set => SetProperty(ref mShield, value);
      }
      protected int mShield = 0;

      /// <summary>
      /// The player's current (or recent, in the case of the post-game) score.
      /// Here, "score" is a euphemism for the player's number of kills.
      /// </summary>
      [DataMember]
      public uint Score
      {
         get => mScore;
         set => SetProperty(ref mScore, value);
      }
      protected uint mScore = 0;

      /// <summary>
      /// The player's remaining lives. Each player begins with three lives and
      /// is out of the game when this value is zero.
      /// </summary>
      [DataMember]
      public uint Lives
      {
         get => mLives;
         set => SetProperty(ref mLives, value);
      }
      protected uint mLives = 3;

      /// <summary>
      /// A multiplier applied to damage the player receives. This value is
      /// increased by certain projectiles that have a resistance reduction effect.
      /// </summary>
      [DataMember]
      public double DamageMultiplier
      {
         get => mDamageMultiplier;
         set
         {
            // The damage multiplier (resistance reduction) should not exceed 50% (1.5x).
            if (value <= 1.5)
            {
               SetProperty(ref mDamageMultiplier, value);
            }
         }
      }
      protected double mDamageMultiplier = 1.0;

      [DataMember]
      public Item[] Items
      {
         get => mItems;
         set => SetProperty(ref mItems, value);
      }
      protected Item[] mItems = new Item[6];

      /// <summary>
      /// The mobile chosen by the player before the game (in the lobby).
      /// </summary>
      [DataMember]
      public MobileEnum MobileID
      {
         get => mMobileID;
         set => SetProperty(ref mMobileID, value);
      }
      protected MobileEnum mMobileID = MobileEnum.NONE;

      /// <summary>
      /// The player's current aim, in radians, *independent of any other any potential rotation and
      /// unaffected by the direction the player is facing*. In other words, should the player walk
      /// along the stage, this value will not be affected.
      /// 
      /// This value will can broadly be thought of as being in the 0 to pi/2 range, although the
      /// mobile will limit that range in practice (e.g. pi/6 to 5*pi/6 degrees). Lower values mean
      /// the player is aiming lower, or closer to the stage.
      /// 
      /// The ultimate aim at the time of firing a shot, the trajectory, will take other rotation(s)
      /// into account.
      /// </summary>
      [DataMember]
      public double AimInRadians
      {
         get => mAimInRadians;
         set => SetProperty(ref mAimInRadians, value);
      }
      protected double mAimInRadians = CenterAimInRadians;

      #endregion Serialized properties


      #region Helper getters

      public double MinAimInRadians => CenterAimInRadians - (TrueAngleRangeInRadians / 2.0);

      public double MaxAimInRadians => CenterAimInRadians + (TrueAngleRangeInRadians / 2.0);

      public double TrueAngleRangeInRadians => Mobile?.TrueAngleRangeInRadians ?? 0.0;

      public bool HasShield => Mobile?.ShieldHP > 0;

      public virtual bool IsBot => false;

      public Vector Trajectory
      {
         get
         {
            // There are three or four steps here.
            // Step 1: Create a vector from the aim. The aim is indepedent of any other angles and
            // will be in the 0 to pi/2 quadrant.
            Vector trajectory = Vector.FromRadians(AimInRadians);

            // Step 2: If the player is facing left, mirror the vector across the Y axis so it also
            // faces left.
            if (Direction == Direction.LEFT)
            {
               trajectory.X = -trajectory.X;
            }

            // Step 3: Rotate the vector to account for the angle at which the player is standing.
            // That angle is the difference from what would be considered standing up straight.
            trajectory = Vector.Rotate(trajectory, Vector.UpInRadians - Pitch);

            // Step 4: Flip the Y axis. The math up to this point has been done in cartesian coordinates
            // whereas the World and graphics use computer coordinates with -Y being up.
            trajectory.Y = -trajectory.Y;

            return trajectory;
         }
      }

      public Mobile Mobile
      {
         get
         {
            if (MobileID == MobileEnum.NONE)
            {
               mLog.Warn($"{Name}'s mobile ID has not been set - returning a null {nameof(Mobile)}");
               return null;
            }

            if (MobileID != mInstantiatedMobileID)
            {
               mMobile = new Mobile(MobileID);
               mInstantiatedMobileID = MobileID;
            }

            return mMobile;
         }
      }
      private Mobile mMobile = null;
      private MobileEnum mInstantiatedMobileID = MobileEnum.NONE;

      public Shot Shot
      {
         get
         {
            // Use the Mobile getter to get the reference. The private variable should not
            // be referenced directly as it may be null. The getter sets it when needed.
            Mobile mobile = Mobile;
            if (mobile == null)
            {
               mLog.Warn($"{Name}'s mobile ID has not been set - returning a null {nameof(Shot)}");
               return null;
            }

            return mShotID switch
            {
               ShotEnum.SHOT_1 => mobile?.Shot1,
               ShotEnum.SHOT_2 => mobile?.Shot2,
               _ => mobile?.SpecialShot
            };
         }
      }

      /// <summary>
      /// Position to place the current shot's projectile(s) at such that the projectile body is in
      /// line with the curent aim and just outside the player's and body's collision radius.
      /// </summary>
      public Vector ProjectilePosition
      {
         get
         {
            // Try to get the shot. It's possible for it to be null if no mobile has been set.
            Shot shot = Shot;
            if (shot == null)
            {
               return null;
            }

            Vector position =
               // Begin with the center of the player's body and add...
               Center +
               // ...the radiuses of both the player and the projectile's body, plus one, then...
               Trajectory * (Radius + shot.BodyRadius + 1.0) -
               // ...move up and left to account for bodies being positioned by their top-left corners.
               new Vector(shot.BodyRadius, shot.BodyRadius);

            return position;
         }
      }

      public Body Satellite { get; } = new Body
      {
         Radius = mSatelliteRadius
      };

      #endregion Helper getters


      #region Constant(s)

      public const double CenterAimInRadians = Math.PI / 4.0;

      #endregion Constant(s)


      #region Static(s)

      public static IEqualityComparer<Player> Comparer { get; } = new PlayerComparer();

      #endregion Static(s)


      public Player(uint aID, string aName, bool aIsAdmin) : base()
      {
         ID = aID;
         Name = aName;
         IsAdmin = aIsAdmin;

         PropertyChanged += OnSelfPropertyChanged;
      }

      public Player(Player aOther) : base(aOther)
      {
         if (aOther == null)
         {
            return;
         }

         ID = aOther.ID;
         Name = aOther.Name;
         IsAdmin = aOther.IsAdmin;
         IsVeteran = aOther.IsVeteran;
         IsReady = aOther.IsReady;
         IsCharging = aOther.IsCharging;
         IsFiring = aOther.IsFiring;
         IsDead = aOther.IsDead;
         Team = aOther.Team;
         ShotID = aOther.ShotID;
         SelectedItem = aOther.SelectedItem;
         Delay = aOther.Delay;
         HP = aOther.HP;
         ShieldHP = aOther.ShieldHP;
         Score = aOther.Score;
         Lives = aOther.Lives;
         DamageMultiplier = aOther.DamageMultiplier;
         for (int i = 0; i < Items.Length; i++)
         {
            Items[i] = aOther.Items[i] != null ? new Item(aOther.Items[i]) : null;
         }
         MobileID = aOther.MobileID;
         AimInRadians = aOther.AimInRadians;
      }

      public override Body Clone()
      {
         return new Player(this);
      }

      public bool Sync(Player aUpdate, SyncFlags aFlags = SyncFlags.ALL)
      {
         if (aUpdate == null)
         {
            return false;
         }

         int initialHash = GetHashCode();

         // ID, Name, and IsAdmin should never change so skip them.
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.IS_VETERAN) == true))
         {
            IsVeteran = aUpdate.IsVeteran;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.IS_READY) == true))
         {
            IsReady = aUpdate.IsReady;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.IS_CHARGING) == true))
         {
            IsCharging = aUpdate.IsCharging;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.IS_FIRING) == true))
         {
            IsFiring = aUpdate.IsFiring;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.IS_DEAD) == true))
         {
            IsDead = aUpdate.IsDead;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.TEAM) == true))
         {
            Team = aUpdate.Team;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.SELECTED_SHOT) == true))
         {
            ShotID = aUpdate.ShotID;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.SELECTED_ITEM) == true))
         {
            SelectedItem = aUpdate.SelectedItem;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.DELAY) == true))
         {
            Delay = aUpdate.Delay;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.HP) == true))
         {
            HP = aUpdate.HP;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.SHIELD) == true))
         {
            ShieldHP = aUpdate.ShieldHP;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.SCORE) == true))
         {
            Score = aUpdate.Score;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.LIVES) == true))
         {
            Lives = aUpdate.Lives;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.DAMAGE_MULTIPLIER) == true))
         {
            DamageMultiplier = aUpdate.DamageMultiplier;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.ITEMS) == true))
         {
            Items = aUpdate.Items;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.MOBILE_ID) == true))
         {
            MobileID = aUpdate.MobileID;
         }
         if ((aFlags.HasFlag(SyncFlags.ALL) == true) || (aFlags.HasFlag(SyncFlags.AIM) == true))
         {
            AimInRadians = aUpdate.AimInRadians;
         }

         base.Sync(aUpdate);

         return GetHashCode() != initialHash;
      }

      public override string ToString()
      {
         string baseString = base.ToString();
         return $"{{ {nameof(ID)} = {ID}, {nameof(Name)} = \"{Name}\", {nameof(IsAdmin)} = {IsAdmin}, " +
                $"{nameof(IsVeteran)} = {IsVeteran}, {nameof(IsReady)} = {IsReady}, " +
                $"{nameof(IsCharging)} = {IsCharging}, {nameof(IsFiring)} = {IsFiring}, " +
                $"{nameof(IsDead)} = {IsDead}, {nameof(Team)} = {Team}, " +
                $"{nameof(ShotID)} = {ShotID}, " +
                $"{nameof(SelectedItem)} = {SelectedItem?.ToString() ?? "null"}, {nameof(Delay)} = {Delay}, " +
                $"{nameof(HP)} = {HP}, {nameof(ShieldHP)} = {ShieldHP}, {nameof(Score)} = {Score}, " +
                $"{nameof(Lives)} = {Lives}, {nameof(DamageMultiplier)} = {DamageMultiplier}, " +
                $"{nameof(Items)} = {string.Join(", ", (object[])Items)}, {nameof(MobileID)} = {MobileID}, " +
                $"{nameof(AimInRadians)} = {AimInRadians}, {baseString.Substring(2, baseString.Length - 4)} }}";
      }

      public override bool Equals(object aObject)
      {
         return (aObject is Player other) && (other.ID == ID);
      }

      //// TODO: Figure out if implementing IEquality still has a purpose.
      //public bool Equals(Player aPlayer)
      //{
      //   return Equals(aPlayer as object);
      //}

      public override int GetHashCode()
      {
         unchecked // Allows integer overflow.
         {
            int hash = 877;
            hash *= 353 ^ (int)ID;
            hash *= 967 ^ (Name?.GetHashCode() ?? 1);
            hash *= IsAdmin ? 311 : 79;
            hash *= IsVeteran ? 641 : 59;
            hash *= IsReady ? 19 : 857;
            hash *= IsCharging ? 439 : 401;
            hash *= IsFiring ? 101 : 683;
            hash *= IsDead ? 433 : 829;
            hash *= 619 ^ Team.GetHashCode();
            hash *= 251 ^ ShotID.GetHashCode();
            hash *= 443 ^ SelectedItem?.GetHashCode() ?? 521;
            hash *= 523 ^ (int)Delay;
            hash *= 569 ^ HP;
            hash *= 211 ^ ShieldHP;
            hash *= 389 ^ (int)Score;
            hash *= 743 ^ (int)Lives;
            hash *= 191 ^ DamageMultiplier.GetHashCode();
            foreach (Item item in Items)
            {
               hash *= 479 ^ (item?.GetHashCode() ?? 0);
            }
            hash *= 701 ^ (int)MobileID;
            hash *= 631 ^ AimInRadians.GetHashCode();
            hash *= 281 ^ base.GetHashCode();
            return hash;
         }
      }

      private void OnSelfPropertyChanged(object aSender, PropertyChangedEventArgs aArgs)
      {
         switch (aArgs.PropertyName)
         {
            case nameof(AimInRadians):
            case nameof(Direction):
            case nameof(Pitch):
               OnPropertyChanged(nameof(Trajectory));
               break;
            case nameof(X):
            case nameof(ShotID):
               // Update the satellite. It's positioned above the player with an altitude (above
               // the stage) determined by the shot. Shot 2 is three times the altitude of shot
               // 1 and the special shot.
               // This satellite is only used by B.Seat.
               double satelliteAltitutde = Constants.BSeatSatelliteBaseAltitudeInPixels;
               if (ShotID == ShotEnum.SHOT_2)
               {
                  satelliteAltitutde *= 3.0;
               }
               Satellite.X = X - (Radius / 2.0);
               Satellite.Y = 0.0 - (Radius / 2.0) - satelliteAltitutde;

               // If the player's shot (1, 2, or SS) changed and a Shot object is available.
               if ((aArgs.PropertyName == nameof(ShotID)) && (Shot != null))
               {
                  OnPropertyChanged(nameof(Shot));
               }
               break;
            case nameof(MobileID):
               Mobile mobile = Mobile;
               if (mobile != null)
               {
                  HP = mobile.HP;
                  ShieldHP = mobile.ShieldHP;
                  Radius = mobile.Diameter / 2.0;
                  OnPropertyChanged(nameof(Mobile));
               }
               break;
         }
      }

      /// <summary>
      /// Comparer class that allows for Players to be compared by their IDs in some standard
      /// library methods as opposed to more, let's say, voltage measurements. For example,
      /// unless this comparer is specified, Enumerable.Except() will use GetHashCode().
      /// </summary>
      private class PlayerComparer : IEqualityComparer<Player>
      {
         public bool Equals(Player aPlayer1, Player aPlayer2)
         {
            return aPlayer1?.ID == aPlayer2?.ID;
         }

         public int GetHashCode(Player aPlayer)
         {
            if (aPlayer != null)
            {
               return (int)aPlayer.ID;
            }

            return -1;
         }
      }
   }
}
