﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Models
{
   [DataContract]
   public class Shooting
   {
      #region Serialized properties

      [DataMember]
      public Vector Center { get; set; } = Vector.Zero;

      [DataMember]
      public ProjectileBehaviorEnum Behavior { get; set; } = ProjectileBehaviorEnum.STANDARD;

      [DataMember]
      public int MinDamage { get; set; } = 0;

      [DataMember]
      public int MaxDamage { get; set; } = 0;

      [DataMember]
      public int? AftershockDamage { get; set; } = null;

      [DataMember]
      public double HoleMajorAxis { get; set; } = 0.0;

      [DataMember]
      public double HoleMinorAxis { get; set; } = 0.0;

      [DataMember]
      public double? ResistanceReduction { get; set; } = null;

      [DataMember]
      public bool HasAreaOfEffect { get; set; } = false;

      #endregion


      public Shooting()
      {
      }

      public Shooting(Vector aCenter, Shot aShot)
      {
         Center = new Vector(aCenter);
         Behavior = aShot.Behavior;
         MinDamage = aShot.MinDamage / aShot.BodiesPerProjectile;
         MaxDamage = aShot.MaxDamage / aShot.BodiesPerProjectile;
         AftershockDamage = aShot.AftershockDamage;
         HoleMajorAxis = aShot.HoleMajorAxis;
         HoleMinorAxis = aShot.HoleMinorAxis;
         ResistanceReduction = aShot.ResistanceReduction;
         HasAreaOfEffect = aShot.HasAreaOfEffect;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Center)} = {Center}, {nameof(Behavior)} = {Behavior} , " +
                $"{nameof(MinDamage)} = {MinDamage}, {nameof(MaxDamage)} = {MaxDamage}, " +
                $"{nameof(AftershockDamage)} = {AftershockDamage?.ToString() ?? "null"}, " +
                $"{nameof(HoleMajorAxis)} = {HoleMajorAxis}, {nameof(HoleMinorAxis)} = {HoleMinorAxis}, " +
                $"{nameof(ResistanceReduction)} = {ResistanceReduction?.ToString() ?? "null"}, " +
                $"{nameof(HasAreaOfEffect)} = {HasAreaOfEffect} }}";
      }

        public override int GetHashCode()
      {
         unchecked // Allows integer overflow.
         {
            int hash = 877;
            hash *= 569 ^ Center.GetHashCode();
            hash *= 463 ^ Behavior.GetHashCode();
            hash *= 877 ^ MinDamage.GetHashCode();
            hash *= 557 ^ MaxDamage.GetHashCode();
            hash *= 107 ^ (AftershockDamage?.GetHashCode() ?? 1);
            hash *= 431 ^ HoleMajorAxis.GetHashCode();
            hash *= 311 ^ HoleMinorAxis.GetHashCode();
            hash *= 79 ^ (ResistanceReduction?.GetHashCode() ?? 1);
            hash *= HasAreaOfEffect ? 643 : 967;
            return hash;
         }
      }
    }
}
