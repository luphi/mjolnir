﻿using Mjolnir.Common.Disk;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mjolnir.Common.Models
{
   public class Stage : PropertyChangedBase
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private bool[,] mOriginalMask = null;

      public StageEnum ID { get; }

      /// <summary>
      /// Human-readable name and a unique identifier for the stage.
      /// </summary>
      public string Name { get; }

      /// <summary>
      /// Basename of a file, without an extension, that indicates the image file
      /// to be used as the stage's background.
      /// </summary>
      public string Background { get; }

      /// <summary>
      /// Basename of a file, without an extension, that indicates the image file
      /// to be used as the stage's foreground and, therefore, terrain.
      /// </summary>
      public string Foreground { get; }

      /// <summary>
      /// List of four (X, Y) coordinates within the stage where players on team A
      /// will be placed when a game is starting.
      /// </summary>
      public List<Vector> SpawnPointsA { get; } = new List<Vector>();

      /// <summary>
      /// List of four (X, Y) coordinates within the stage where players on team B
      /// will be placed when a game is starting.
      /// </summary>
      public List<Vector> SpawnPointsB { get; } = new List<Vector>();

      /// <summary>
      /// X position of the stage. Always zero. Included for code readability.
      /// </summary>
      public int X => 0;

      /// <summary>
      /// Y position of the stage. Always zero. Included for code readability.
      /// </summary>
      public int Y => 0;

      public int Width => mMask?.GetLength(0) ?? 0;

      public int Height => mMask?.GetLength(1) ?? 0;

      public bool IsLoaded
      {
         get => mIsLoaded;
         private set => SetProperty(ref mIsLoaded, value);
      }
      private bool mIsLoaded = false;

      public bool[,] Mask
      {
         get => mMask;
         private set => SetProperty(ref mMask, value);
      }
      private bool[,] mMask = null;

      /// <summary>
      /// Helper getter that returns a list of all Stage models, excluding "Random."
      /// This list contains instances of the models' classes.
      /// </summary>
      public static List<Stage> List
      {
         get
         {
            // Get a list all stage types from the enumeration defining them.
            List<StageEnum> types = Enum.GetValues(typeof(StageEnum)).Cast<StageEnum>().ToList();

            // Remove the NONE value. We don't want to create a model with this type.
            types.RemoveAt(0);

            // Remove the DEV value. The development stage is only used in development screens.
            types.RemoveAt(types.Count - 1);

            // Generate a list of instantiations of all types.
            List<Stage> list = new List<Stage>();
            foreach (StageEnum type in types)
            {
               list.Add(new Stage(type));
            }

            // Finally, return the list.
            return list;
         }
      }

      /// <summary>
      /// Helper getter that returns an instance of a random Stage, excluding the
      /// partially-complete Stage called "Random."
      /// </summary>
      public static Stage Random
      {
         get
         {
            List<Stage> list = List;
            return list[new Random().Next(list.Count)];
         }
      }

      public Stage(StageEnum aID)
      {
         ID = aID;


         #region Stage-specific values

         switch (aID)
         {
            case StageEnum.NONE:
            default:
               Name = "Random";
               Background = null;
               Foreground = "random_fg";
               break;
            case StageEnum.CAPITAL:
               Name = "Capital";
               Background = "capital_bg";
               Foreground = "capital_fg";
               SpawnPointsA.Add(new Vector(224.0f, 93.0f));
               SpawnPointsA.Add(new Vector(450.0f, 129.0f));
               SpawnPointsA.Add(new Vector(660.0f, 344.0f));
               SpawnPointsA.Add(new Vector(808.0f, 374.0f));
               SpawnPointsB.Add(new Vector(995.0f, 379.0f));
               SpawnPointsB.Add(new Vector(1131.0f, 349.0f));
               SpawnPointsB.Add(new Vector(1359.0f, 127.0f));
               SpawnPointsB.Add(new Vector(1575.0f, 93.0f));
               break;
            case StageEnum.COMFY_SPIRE:
               Name = "Comfy Spire";
               Background = "comfy_bg";
               Foreground = "comfy_fg";
               SpawnPointsA.Add(new Vector(138.0f, 26.0f));
               SpawnPointsA.Add(new Vector(415.0f, 70.0f));
               SpawnPointsA.Add(new Vector(553.0f, 128.0f));
               SpawnPointsA.Add(new Vector(715.0f, 105.0f));
               SpawnPointsB.Add(new Vector(941.0f, 106.0f));
               SpawnPointsB.Add(new Vector(1093.0f, 128.0f));
               SpawnPointsB.Add(new Vector(1226.0f, 70.0f));
               SpawnPointsB.Add(new Vector(1500.0f, 26.0f));
               break;
            case StageEnum.DUNCE_HILL:
               Name = "Dunce Hill";
               Background = "dunce_bg";
               Foreground = "dunce_fg";
               SpawnPointsA.Add(new Vector(45.0f, 21.0f));
               SpawnPointsA.Add(new Vector(255.0f, 78.0f));
               SpawnPointsA.Add(new Vector(439.0f, 135.0f));
               SpawnPointsA.Add(new Vector(625.0f, 98.0f));
               SpawnPointsB.Add(new Vector(842.0f, 88.0f));
               SpawnPointsB.Add(new Vector(1014.0f, 118.0f));
               SpawnPointsB.Add(new Vector(1163.0f, 89.0f));
               SpawnPointsB.Add(new Vector(1337.0f, 1.0f));
               break;
            case StageEnum.ENLIGHTENMENT:
               Name = "Enlightenment";
               Background = "enlightenment_bg";
               Foreground = "enlightenment_fg";
               SpawnPointsA.Add(new Vector(104.0f, 64.0f));
               SpawnPointsA.Add(new Vector(286.0f, 109.0f));
               SpawnPointsA.Add(new Vector(463.0f, 55.0f));
               SpawnPointsA.Add(new Vector(608.0f, 53.0f));
               SpawnPointsB.Add(new Vector(926.0f, 48.0f));
               SpawnPointsB.Add(new Vector(1178.0f, 94.0f));
               SpawnPointsB.Add(new Vector(1378.0f, 18.0f));
               SpawnPointsB.Add(new Vector(1535.0f, 14.0f));
               break;
            case StageEnum.MIMARO_VILLAGE:
               Name = "Mimaro Village";
               Background = "mimaro_bg";
               Foreground = "mimaro_fg";
               SpawnPointsA.Add(new Vector(95.0f, 85.0f));
               SpawnPointsA.Add(new Vector(222.0f, 69.0f));
               SpawnPointsA.Add(new Vector(502.0f, 66.0f));
               SpawnPointsA.Add(new Vector(641.0f, 30.0f));
               SpawnPointsB.Add(new Vector(1078.0f, 41.0f));
               SpawnPointsB.Add(new Vector(1256.0f, 36.0f));
               SpawnPointsB.Add(new Vector(1447.0f, 55.0f));
               SpawnPointsB.Add(new Vector(1687.0f, 45.0f));
               break;
            case StageEnum.OCEAN_OF_EXEMPLAR:
               Name = "Ocean of Exemplar";
               Background = "exemplar_bg";
               Foreground = "exemplar_fg";
               SpawnPointsA.Add(new Vector(123.0f, 133.0f));
               SpawnPointsA.Add(new Vector(300.0f, 114.0f));
               SpawnPointsA.Add(new Vector(484.0f, 273.0f));
               SpawnPointsA.Add(new Vector(665.0f, 261.0f));
               SpawnPointsB.Add(new Vector(949.0f, 261.0f));
               SpawnPointsB.Add(new Vector(1110.0f, 270.0f));
               SpawnPointsB.Add(new Vector(1301.0f, 107.0f));
               SpawnPointsB.Add(new Vector(1472.0f, 105.0f));
               break;
            case StageEnum.PIDGIN_ROOT:
               Name = "Pidgin Root";
               Background = "pidgin_bg";
               Foreground = "pidgin_fg";
               SpawnPointsA.Add(new Vector(74.0f, 66.0f));
               SpawnPointsA.Add(new Vector(239.0f, 135.0f));
               SpawnPointsA.Add(new Vector(450.0f, 172.0f));
               SpawnPointsA.Add(new Vector(717.0f, 210.0f));
               SpawnPointsB.Add(new Vector(1031.0f, 210.0f));
               SpawnPointsB.Add(new Vector(1300.0f, 161.0f));
               SpawnPointsB.Add(new Vector(1536.0f, 128.0f));
               SpawnPointsB.Add(new Vector(1702.0f, 40.0f));
               break;
            case StageEnum.RECURSE_QUARRY:
               Name = "Recurse Quarry";
               Background = "recurse_bg";
               Foreground = "recurse_fg";
               SpawnPointsA.Add(new Vector(152.0f, 19.0f));
               SpawnPointsA.Add(new Vector(252.0f, 39.0f));
               SpawnPointsA.Add(new Vector(435.0f, 10.0f));
               SpawnPointsA.Add(new Vector(631.0f, 56.0f));
               SpawnPointsB.Add(new Vector(777.0f, 56.0f));
               SpawnPointsB.Add(new Vector(954.0f, 10.0f));
               SpawnPointsB.Add(new Vector(1072.0f, 39.0f));
               SpawnPointsB.Add(new Vector(1260.0f, 19.0f));
               break;
            case StageEnum.STELLAR_CLOUD:
               Name = "Stellar Cloud";
               Background = "stellar_bg";
               Foreground = "stellar_fg";
               SpawnPointsA.Add(new Vector(88.0f, 169.0f));
               SpawnPointsA.Add(new Vector(206.0f, 97.0f));
               SpawnPointsA.Add(new Vector(359.0f, 39.0f));
               SpawnPointsA.Add(new Vector(508.0f, 12.0f));
               SpawnPointsB.Add(new Vector(832.0f, 12.0f));
               SpawnPointsB.Add(new Vector(977.0f, 39.0f));
               SpawnPointsB.Add(new Vector(1145.0f, 100.0f));
               SpawnPointsB.Add(new Vector(1259.0f, 168.0f));
               break;
            case StageEnum.WYVERN:
               Name = "Wyvern";
               Background = "wyvern_bg";
               Foreground = "wyvern_fg";
               SpawnPointsA.Add(new Vector(157.0f, 28.0f));
               SpawnPointsA.Add(new Vector(474.0f, 115.0f));
               SpawnPointsA.Add(new Vector(697.0f, 160.0f));
               SpawnPointsA.Add(new Vector(881.0f, 253.0f));
               SpawnPointsB.Add(new Vector(1154.0f, 252.0f));
               SpawnPointsB.Add(new Vector(1349.0f, 162.0f));
               SpawnPointsB.Add(new Vector(1550.0f, 124.0f));
               SpawnPointsB.Add(new Vector(1787.0f, 44.0f));
               break;
            case StageEnum.DEV:
               Name = "Dev";
               Background = "dev_bg";
               Foreground = "dev_fg";
               SpawnPointsA.Add(new Vector(236.0f, 321.0f));
               SpawnPointsA.Add(new Vector(458.0f, 321.0f));
               SpawnPointsA.Add(new Vector(585.0f, 312.0f));
               SpawnPointsA.Add(new Vector(1252.0f, 320.0f));
               SpawnPointsB.Add(new Vector(1414.0f, 99.0f));
               SpawnPointsB.Add(new Vector(1735.0f, 120.0f));
               SpawnPointsB.Add(new Vector(1650.0f, 150.0f));
               SpawnPointsB.Add(new Vector(1509.0f, 213.0f));
               break;
         }

         #endregion
      }

      public Stage(Stage aOther) : this(aOther.ID)
      {
         mIsLoaded = aOther.mIsLoaded;
         mMask = aOther.mMask;
      }

      /// <summary>
      /// Return the stage to its initial state. In other words, undo any "digging"
      /// done to the terrain/foreground.
      /// </summary>
      public virtual void Reset()
      {
         Mask = (bool[,])mOriginalMask.Clone();
      }

      /// <summary>
      /// Load the foreground image from disk and generate the terrain mask from this image.
      /// </summary>
      /// <returns>True if loading the foreground image and generating a terrain mask
      /// was successful, false if it failed for any reason.</returns>
      public bool Load(IContentWrapper aContenWrapper)
      {
         // If the stage has already been loaded (it's mask will be non-null).
         if (mIsLoaded == true)
         {
            // Reset the stage so it appears as if it were just loaded and return true.
            Reset();
            return true;
         }

         // Load the mask from disk by reading and decoding the foreground image and determining
         // which pixels therein are non-transparent.
         mOriginalMask = aContenWrapper.LoadStage(this);

         // If loading the mask failed for any reason.
         if (mOriginalMask == null)
         {
            return false;
         }

         // Copy the private mask to the public mask. This private mask will remain unmodified
         // from its original state leaving hte public one to be freely modified (e.g. holes
         // dug out of it).
         Mask = (bool[,])mOriginalMask.Clone();
         IsLoaded = true;

         return true;
      }

      public void Dig(Vector aCenter, double aMajorAxis, double aMinorAxis)
      {
         double majorRadius = aMajorAxis / 2.0;
         double minorRadius = aMinorAxis / 2.0;

         // For each pixel in the square formed around the given center coordinate with a
         // width and height equal to the radius plus the border thickness.
         for (int x = (int)(aCenter.X - majorRadius); x < aCenter.X + majorRadius; x++)
         {
            for (int y = (int)(aCenter.Y - minorRadius); y < aCenter.Y + minorRadius; y++)
            {
               // The equation for an ellipse with center C and major and minor axes a and b, respectively:
               //   (x - C_x)^2   (y - C_y)^2
               //   ----------- + ----------- = 1
               //       a^2           b^2
               // meaning that (x, y) is within the ellipse if the resulting value is <= 1.
               bool isInside = (Math.Pow(x - aCenter.X, 2.0) / Math.Pow(majorRadius, 2.0) +
                               (Math.Pow(y - aCenter.Y, 2.0) / Math.Pow(minorRadius, 2.0))) <= 1;

               // If this (x, y) coordinate is within the area of the ellipse and within the bounds
               // of the texture.
               if ((isInside == true) && (x >= 0) && (y >= 0) && (x < Width) && (y < Height))
               {
                  // Clear this pixel and mark it as not solid.
                  mMask[x, y] = false;
               }
            }
         }

         // Rather than using the setter to trigger a property changed event, directly call on it
         // in order to avoid unnecessarily copying the whole array.
         OnPropertyChanged(nameof(Mask));
      }

      public bool IsSolid(Vector aPoint)
      {
         if (aPoint == null)
         {
            return false;
         }

         return IsSolid((int)aPoint.X, (int)aPoint.Y);
      }

      public bool IsSolid(int aX, int aY)
      {
         if ((aX < X) || (aY < Y) || (aX >= X + Width) || (aY >= Y + Height))
         {
            return false;
         }

         return mMask[aX, aY];
      }

      public Vector NormalAt(Vector aPoint, int aRadius = 10)
      {
         if (aPoint == null)
         {
            return Vector.Zero;
         }

         return NormalAt((int)aPoint.X, (int)aPoint.Y, aRadius);
      }

      public Vector NormalAt(int aX, int aY, int aRadius = 10)
      {
         Vector sum = Vector.Zero;

         // Cycle through the nearby pixels and calculate a vector in the direction of the solid,
         // adjacent pixels. This vector's magnitude will have no meaning but its direction will
         // be the average of all vectors pointing to solid pixels in the region.
         // (This loop will search a quad with the given coordinate at the center.)
         for (int i = -aRadius; i <= aRadius; i++)
         {
            for (int j = -aRadius; j <= aRadius; j++)
            {
               if (IsSolid(aX + i, aY + j) == true)
               {
                  sum.X += i;
                  sum.Y += j;
               }
            }
         }

         // If there were no solid pixels from which to calculate a stage normal.
         if (sum == Vector.Zero)
         {
            // Return a unit vector pointing straight up.
            return new Vector(0.0, -1.0);
         }

         // The vector is currently pointing toward the stage. Negate the vector so it's pointing away
         // from the stage, thus being the normal.
         return sum * -1.0;
      }

      public override string ToString()
      {
         return $"{{ {nameof(ID)} = {ID}, {nameof(Name)} = \"{Name}\", " +
                $"{nameof(Background)} = \"{Background}\", " +
                $"{nameof(Foreground)} = \"{Foreground}\", " +
                $"{nameof(SpawnPointsA)} = {string.Join(", ", SpawnPointsA)}, " +
                $"{nameof(SpawnPointsB)} = {string.Join(", ", SpawnPointsB)}, " +
                $"{nameof(IsLoaded)} = {IsLoaded}, " +
                $"{nameof(X)} = {X}, {nameof(Y)} = {Y}, {nameof(Width)} = {Width}, " +
                $"{nameof(Height)} = {Height} }}";
      }
   }
}
