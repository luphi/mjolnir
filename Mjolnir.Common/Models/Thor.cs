﻿using Mjolnir.Common.Physics;
namespace Mjolnir.Common.Models
{
   public class Thor : Body
   {
      private const double mThorRadius = 35.0;
      private const uint mMaxLevel = 7;
      private const int mDamagePerLevelUp = 300;
      private const int mBaseDamagePerLightning = 50;
      private const int mDamagePerLightningGain = 30;

      private readonly World mWorld;
      private int mDamageAccumulator = 0;
      private double mCooldownCountdown = 0.0;

      public int Level
      {
         get => mLevel;
         private set => SetProperty(ref mLevel, value);
      }
      private int mLevel = 1;

      public int DamagePerLightning => mBaseDamagePerLightning + (mDamagePerLightningGain * (Level - 1));

      public Thor(World aWorld) : base(mThorRadius)
      {
         mWorld = aWorld;
         Y = (aWorld?.Y ?? 0.0) - Constants.ThorAltitudeInPixels;
      }

      public void Reset()
      {
         X = 0.0;
         Pitch = Vector.UpInRadians;
         mDamageAccumulator = 0;
         mCooldownCountdown = 0.0;
         Level = 1;
      }

      public void Step(double aDT)
      {
         if (mWorld == null)
         {
            return;
         }

         // Prepare a "delta time" variable to be applied to the left/right movement.
         double dt = aDT;
         // If the Thor is cooling down from a recent firing.
         if (mCooldownCountdown > 0.0)
         {
            // It should be stationary during the cooldown. Set "delta time" to zero to make "delta X" zero.
            dt = 0.0;
            mCooldownCountdown -= aDT;
            // However, if the cooldown countdown is now less than zero, another case will be triggered.
         }

         // If the cooldown period has ended paused Thor longer than just the remaining time.
         if (mCooldownCountdown < 0.0)
         {
            // Apply the excess pause time to the movement.
            // The counterdown will be negative so this is a subtraction.
            dt = aDT + mCooldownCountdown;
            // Reset the countdown so that the cooldown is now done.
            mCooldownCountdown = 0.0;
         }
         double dX = Constants.ThorSpeedInPixelsPerSecond * dt * (Direction == Direction.RIGHT ? 1.0 : -1.0);

         if (X + dX <= mWorld.X)
         {
            dX = X - mWorld.X;
            Direction = Direction.RIGHT;
         }
         else if (X + dX >= mWorld.X + mWorld.Width)
         {
            dX = mWorld.X + mWorld.Width - X;
            Direction = Direction.LEFT;
         }

         X += dX;
      }

      public void Charge(int aDamage)
      {
         if (Level == mMaxLevel)
         {
            return;
         }

         mDamageAccumulator += aDamage;
         if (mDamageAccumulator >= mDamagePerLevelUp)
         {
            Level += 1;
            mDamageAccumulator -= mDamagePerLevelUp;
         }
      }

      public void Fire()
      {
         mCooldownCountdown = Constants.ThorCooldownDurationInMilliseconds / 1000.0;
      }
   }
}
