﻿using Mjolnir.Common.Messages;
using Mjolnir.Common.Models;
using Mjolnir.Common.Network;
using Mjolnir.Common.Physics;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Mjolnir.Common.Server.States
{
   public class IngameState : IState
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly TurnList mTurnList = new TurnList();
      private readonly Server mServer;

      // Game status variables.
      private uint mTeamARemainingLives = 4; // Remaining lives for team A. Ranges 0 to 4. If 0, the game is over.
      private uint mTeamBRemainingLives = 4; // Remaining lives for team B. Ranges 0 to 4. If 0, the game is over.

      // Turn monitoring variables.
      private readonly Stopwatch mTurnStopwatch = new Stopwatch();
      private uint mCurrentTurnID = 0; // ID number of the player currently taking his/her turn.
      private uint mTurnCount = 0; // Continuously incremented value that tracks the number of turns.

      // Bot monitoring variables.
      private CancellationTokenSource mBotTurnTokenSource = null;

      private WalkMessage mPendingWalkMessage = null;
      private IPEndPoint mPendingWalkEndPoint = null;

      // World variables.
      private World mWorld = null;

      public event EventHandler<StateEnum> TransitionEvent;

      public string Name => "In-game";

      public IngameState(Server aServer)
      {
         mServer = aServer;
      }

      public void Enter()
      {
         mServer.LeaveEvent += OnPlayerLeave;
         mServer.Socket.ReceiveEvent += OnReceive;

         mWorld = new World(mServer.Stage); // The Stage is always set by the pre-game state. It won't be null.
         mWorld.LandingEvent += OnLandingEvent;
         mWorld.HaltEvent += OnHaltedEvent;
         mWorld.DeathEvent += OnDeathEvent;
         mWorld.ShootingEvent += OnShootingEvent;
         mWorld.SettledEvent += OnSettledEvent;

         mTeamARemainingLives = 4;
         mTeamBRemainingLives = 4;
         mServer.Socket.SendAll(Message.Serialize(new LivesMessage(mTeamARemainingLives, mTeamBRemainingLives)));

         List<Player> players = new List<Player>(mServer.Players);
         Player luphi = players.FirstOrDefault(p => p.Name.Equals("luphi"));

         // If the player named "luphi" (used by the developer of the same name) was found.
         if (luphi != null)
         {
            // A pseudo dev mode has been triggered.
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////
            Player firstPlayer = players.First();
            Player lastPlayer = players.Last();
            //// Local player goes first:
            //string placement = "start";
            //players[players.IndexOf(luphi)] = firstPlayer;
            //players[0] = luphi;
            // Local player goes last:
            string placement = "end";
            players[players.IndexOf(luphi)] = lastPlayer;
            players[players.Count - 1] = luphi;
            mLog.Debug($"Placed the dev player at the {placement} of the turn list");
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////
         }

         foreach (Player player in players)
         {
            player.Delay = 0;
            mServer.Socket.SendAll(Message.Serialize(new PlayerMessage(player)));
            mWorld.Add(player);
            mTurnList.Add(player);
         }

         mServer.Socket.SendAll(Message.Serialize(new GameBeginMessage(mTurnList.Shuffle())));

         NextTurn();
      }

      public void Exit()
      {
         mServer.LeaveEvent -= OnPlayerLeave;
         mServer.Socket.ReceiveEvent -= OnReceive;

         if (mWorld != null)
         {
            mWorld.LandingEvent -= OnLandingEvent;
            mWorld.HaltEvent -= OnHaltedEvent;
            mWorld.DeathEvent -= OnDeathEvent;
            mWorld.ShootingEvent -= OnShootingEvent;
            mWorld.Clear();
            mWorld = null;
         }

         mTurnList.Clear();
      }

      private void OnPlayerLeave(object aSender, ServerEventArgs aArgs)
      {
         mTurnList.Remove(aArgs.Player);
         mWorld.Remove(aArgs.Player);

         // Get the number of players/bots now on the two teams.
         List<Player> players = mServer.Players;
         int teamACount = players.Where(p => p.Team == TeamEnum.A).Count();
         int teamBCount = players.Count - teamACount;

         if (teamACount == 0)
         {
            // TODO announce a B team win
         }
         else if (teamBCount == 0)
         {
            // TODO announce an A team win
         }

         if (aArgs.Player.ID == mCurrentTurnID)
         {

         }
         // TODO
      }

      private void NextTurn()
      {
         mTurnCount += 1;

         Random random = new Random();
         // If a random integer in the [0, 9] range is zero (10% chance).
         if (random.Next(10) == 0)
         {
            // Change the wind with a randomized direction and speed.
            mWorld.Wind =
                // Get a unit vector in a random direction to be the wind and scale it by...
                Vector.FromRadians(random.NextDouble() * 2.0 * Math.PI) *
                // ...a random (0, 1] coefficient in order to give it a speed up to...
                (1.0 - random.NextDouble()) *
                // ...the constant, maximum wind acceleration.
                Constants.MaxWindAccelerationInPixelsPerSecondSquared;

            mServer.Socket.SendAll(Message.Serialize(new WindMessage(mWorld.Wind)));
         }

         Player player = mTurnList.Head;
         mCurrentTurnID = player.ID;
         mLog.Info($"Proceeding to the next turn for player \"{player.Name}\" ({player.ID})");
         mServer.Socket.SendAll(Message.Serialize(new TurnMessage(mCurrentTurnID)));

         mTurnStopwatch.Restart();

         if (player.IsBot == true)
         {
            Bot bot = player as Bot;
            IEnumerable<Player> targets = mServer.Players.Where(p => (p.Team != bot.Team) &&
                                                                     (p.IsDead == false));

            mBotTurnTokenSource = new CancellationTokenSource();

            Task.Run(() => bot.OnTurnAsync(mBotTurnTokenSource.Token, mWorld, targets));
         }

         Task.Run(async () =>
         {
            uint turnCountAtLaunch = mTurnCount;
            Player timedPlayer = player; // A reference just for this task's scope.
            await Task.Delay(Constants.TurnTimeoutInSeconds * 1000);

            // If, after the delay, the game has no proceeded to the next player's turn and
            // the player taking his/her turn is neither charging nor firing (i.e. still moving
            // or stationary).
            if ((mTurnCount == turnCountAtLaunch) &&
                (timedPlayer.IsCharging == false) &&
                (timedPlayer.IsFiring == false))
            {
               ///////////////////////////////////////////////////////////////////////////////////////////////////
               mTeamARemainingLives = (uint)random.Next(1, 5);
               mTeamBRemainingLives = (uint)random.Next(1, 5);
               mServer.Socket.SendAll(Message.Serialize(new LivesMessage(mTeamARemainingLives, mTeamBRemainingLives)));
               ///////////////////////////////////////////////////////////////////////////////////////////////////

               // Times up. Forcibly move on to the next player's turn.
               mTurnStopwatch.Stop();

               if (timedPlayer.IsBot == true)
               {
                  mLog.Info($"Bot \"{timedPlayer.Name}\" ({timedPlayer.ID}) took longer than " +
                            $"{Constants.TurnTimeoutInSeconds} seconds to decide on a move. Its " +
                            $"turn is being skipped.");
               }
               else
               {

                  mLog.Info($"Player \"{timedPlayer.Name}\" ({timedPlayer.ID}) took longer than " +
                            $"{Constants.TurnTimeoutInSeconds} seconds to begin firing. Continuing " +
                            $"to the next turn.");
               }

               EndTurn(aWasSkipped: true);
            }
         });
      }

      private void EndTurn(bool aWasSkipped = false)
      {
         // TODO check for remaining lives == 0?

         // Stop any ongoing bot tasks. If the turn ending was that of a human player, this
         // will do nothing.
         mBotTurnTokenSource?.Cancel();
         mBotTurnTokenSource = null;

         // Attempt to get the player model for the player whose turn is ending.
         // Note: if the player left the game during his/her turn, this will be null.
         Player player = mServer.Players.FirstOrDefault(p => p.ID == mCurrentTurnID);
         Mobile mobile = player?.Mobile;
         if (mobile != null)
         {
            // Calculate the delay for this turn. Begin by applying the time penalty.
            uint turnDelay = mobile.TurnDelay * (uint)Math.Round(mTurnStopwatch.Elapsed.TotalSeconds);

            if (aWasSkipped == true)
            {
               // If the player's turn ended as a result of timing out or voluntarily skipping the turn.

               // Apply only the mobile's base delay.
               turnDelay += mobile.BaseDelay;
            }
            else
            {
               // If the player fired a shot.

               // Apply the shot's delay.
               turnDelay += player.Shot.Delay;

               // If the player used an item.
               if ((player.SelectedItem >= 0) &&
                   (player.SelectedItem < 6) &&
                   (player.Items[player.SelectedItem.Value] != null))
               {
                  Item item = player.Items[player.SelectedItem.Value];

                  // Apply the item's delay penalty in addition to the shot's.
                  turnDelay += item.Delay;

                  // Remove the item from the player's list by setting the indexes that the
                  // item occupied to null.
                  player.Items[player.SelectedItem.Value] = null;
                  if (item.Slots == 2)
                  {
                     player.Items[player.SelectedItem.Value + 1] = null;
                  }

                  // Deselect the now-removed item by setting the selected item property to null.
                  player.SelectedItem = null;
               }
            }
            player.Delay += turnDelay;

            mServer.Socket.SendAll(Message.Serialize(new PlayerMessage(player)));
         }

         NextTurn();
      }

      private void OnFire(Player aPlayer, FireMessage aFireMessage)
      {
         // The stopwatch should already have been stopped when the player began charging
         // but we'll stop it (probably redundantly) just in case.
         mTurnStopwatch.Stop();

         if (aPlayer != null)
         {
            // Flag the player as not charging but firing. This essentially means that the
            // player is idle while the shot takes place.
            aPlayer.IsCharging = false;
            aPlayer.IsFiring = true;


            mServer.Socket.SendAll(Message.Serialize(new PlayerMessage(aPlayer)));

            Shot shot = aPlayer.Shot;

            // TODO use the player's Shot to fire Projectiles

            // TODO call EndTurn with the FireMessage after the world settles
         }
      }


      #region World event handlers

      private void OnLandingEvent(object aSender, Landing aLanding)
      {
         mLog.Debug("OnLandingEvent");
         // TODO
      }

      private void OnHaltedEvent(object aSender, Body aBody)
      {
         if (aBody is Player player)
         {
            if ((mPendingWalkMessage != null) &&
                (mPendingWalkEndPoint != null) &&
                (player.ID == mPendingWalkMessage.PlayerID))
            {
               if (mWorld?.Walk(aBody) == true)
               {
                  // Relay the message to all clients except the one that sent it.
                  mServer.Socket.SendAll(Message.Serialize(mPendingWalkMessage), mPendingWalkEndPoint);

                  mWorld.Start();
               }

               mPendingWalkMessage = null;
               mPendingWalkEndPoint = null;
            }
            else
            {
               // If the walking player *appears to* have stopped walking (i.e. there are no pending
               // walk messages).

               // The client and this server *should* be in sync in regards to the player's position
               // as walking occurs but we will alert the client to the player's official position
               // (this server is the final authority) at the earliest opportunity. However, network
               // delay exists and another WalkMessage may be in route right now. To avoid needlessly
               // asserting dominance, we'll wait a very short time before alerting the client(s).
               Task.Run(async () =>
               {
                  Player localPlayer = player; // A reference just for this task's scope.

                  // Wait half a second.
                  await Task.Delay(500);

                  // If the player is still not walking.
                  if (localPlayer.IsWalking == false)
                  {
                     // Alert all clients that the player has come halted. For now.
                     mServer.Socket.SendAll(Message.Serialize(new PlayerMessage(localPlayer)));
                  }
               });
            }
         }
      }

      private void OnDeathEvent(object aSender, Player aBody)
      {
         mLog.Debug("OnDeathEvent");
         // TODO update the score
      }

      private void OnShootingEvent(object aSender, Shooting aDetonation)
      {
         mLog.Debug("OnDetonationEvent");
         // TODO
      }

      private void OnSettledEvent(object aSender, Events aEvents)
      {
         mLog.Debug("OnSettledEvent");
         // TODO send an EventsMessage
         // TODO end the player's turn
      }

      #endregion World event handlers


      #region Reception/message handling

      private void OnReceive(object aSender, Reception aReception)
      {
         try
         {
            Message message = Message.Deserialize(aReception.Data);
            mLog.Debug($"server {message}");

            // Get the player model associated with this client.
            Player player = mServer.GetPlayerByEndpoint(aReception.EndPoint);

            // Determine if the message is coming from the active player. This information will be
            // used to ignore certain messages and/or properties.
            bool isSendersTurn = player?.ID == mCurrentTurnID;

            switch (message.Op)
            {
               case Message.Opcode.CHAT:
                  if ((message is ChatMessage chatMessage) &&
                      (mServer.IsEndPointOfID(aReception.EndPoint, chatMessage.PlayerID, chatMessage) == true))
                  {
                     if (chatMessage.IsPrivate && (player != null))
                     {
                        // Relay the message only to players on the same team.
                        IEnumerable<Player> team = mServer.Players.Where(p => p.Team == player.Team);
                        foreach (Player teammate in team)
                        {
                           mServer.Socket.SendTo(aReception.Data, mServer.GetEndpointByPlayerID(teammate.ID));
                        }
                     }
                     else
                     {
                        // Relay the message to everyone.
                        mServer.Socket.SendAll(aReception.Data);
                     }
                  }
                  break;
               case Message.Opcode.FIRE:
                  if ((message is FireMessage fireMessage) &&
                      (isSendersTurn == true) &&
                      (mServer.IsEndPointOfID(aReception.EndPoint, fireMessage.PlayerID, fireMessage) == true))
                  {
                     OnFire(player, fireMessage);
                  }
                  break;
               case Message.Opcode.PLAYER:
                  if ((message is PlayerMessage playerStatusMessage) &&
                      (player != null) &&
                      (mServer.IsEndPointOfID(aReception.EndPoint,
                                              playerStatusMessage.Player?.ID ?? uint.MaxValue,
                                              playerStatusMessage) == true))
                  {
                     // Determine the properties that the client of the player is currently allowed to change.
                     SyncFlags syncFlags;
                     if (isSendersTurn == true)
                     {
                        // If the client sending the message is 

                        if ((player.IsCharging == true) || (player.IsFiring == true))
                        {
                           // If the player has begun charging the shot or is idle after the shot.

                           // Limit the player to just changing aim and direction at the last second.
                           // This server will change the charging and firing flags and alert clients.
                           // Shot and item selection are prohibited to avoid the possibility of changing
                           // either after usage, thus affecting the delay calculation at the end of turn.
                           syncFlags = SyncFlags.AIM | SyncFlags.DIRECTION;
                        }
                        else
                        {
                           // Allow the player to change a handeful of things related to moving,
                           // aiming, and the upcoming shot.
                           syncFlags = SyncFlags.AIM | SyncFlags.DIRECTION | SyncFlags.IS_CHARGING |
                                       SyncFlags.SELECTED_SHOT | SyncFlags.SELECTED_ITEM;

                        }
                     }
                     else
                     {
                        // Outside of a player's turn, he/she is only allowed to change a subset
                        // of things.
                        syncFlags = SyncFlags.AIM | SyncFlags.DIRECTION |
                                    SyncFlags.SELECTED_SHOT | SyncFlags.SELECTED_ITEM;
                     }

                     // Sync this server's model of the player with the updated model.
                     // If syncing caused anything to be changed within the given list of properties
                     // (sync flags) allowed to change.
                     if (player.Sync(playerStatusMessage.Player, syncFlags) == true)
                     {
                        // If the stopwatch is still tracking time AND the player is now charging.
                        if ((mTurnStopwatch.IsRunning == true) && (player.IsCharging == true))
                        {
                           // The time a player take's to make a move adds to his/her delay. Charging is
                           // the point of no return for a shot so no further time penalty should be added.
                           mTurnStopwatch.Stop();

                           // The player will be allowed time to charge the shot. That is, allowed to
                           // determine the shots power and resulting initial velocities. However,
                           // there's a time limit equal to three charge periods, where the charge
                           // period is defined as a constant. So, spawn a task to check for a timeout.
                           Task.Run(async () =>
                           {
                              Player timedPlayer = player; // A reference just for this task's scope.
                              Stopwatch stopwatch = new Stopwatch();
                              uint cycles = 0;
                              stopwatch.Start();

                              // While the player is not firing and we have not waited three cycles.
                              while ((timedPlayer.IsFiring == false) && (cycles < 3))
                              {
                                 // Wait very briefly, 50 ms, before checking again on how long we've
                                 // been waiting for the player.
                                 await Task.Delay(50);
                                 
                                 TimeSpan cycleTimespan = stopwatch.Elapsed -
                                    TimeSpan.FromMilliseconds(cycles * Constants.ChargePeriodInMilliseconds);

                                 if (cycleTimespan.TotalMilliseconds >= Constants.ChargePeriodInMilliseconds)
                                 {
                                    cycles += 1;
                                 }
                              }

                              // If three cycles passed without the player firing.
                              if (cycles >= 3)
                              {
                                 // Times up. Fire a shot with last known charged power (100%).
                                 OnFire(timedPlayer, new FireMessage(timedPlayer.ID, 1.0));
                              }
                           });
                        }

                        // Relay the updated player status to all clients.
                        mServer.Socket.SendAll(Message.Serialize(new PlayerMessage(player)));
                     }
                  }
                  break;
               case Message.Opcode.SKIP: // Note: there is no dedicated SkipMessage, only a SKIP opcode.
                  if (isSendersTurn == true)
                  {
                     EndTurn(aWasSkipped: true);
                  }
                  break;
               case Message.Opcode.WALK:
                  if ((message is WalkMessage walkMessage) &&
                      (isSendersTurn == true) &&
                      (mServer.IsEndPointOfID(aReception.EndPoint, walkMessage.PlayerID, walkMessage) == true))
                  {
                     // The player's direction should have already been updated through a
                     // status message but just in case.
                     player.Direction = walkMessage.Direction;

                     if (player.IsWalking == true)
                     {
                        // If the player is already walking.

                        // Remember the message so that it can be applied when the current walk ends.
                        mPendingWalkMessage = walkMessage;
                        mPendingWalkEndPoint = aReception.EndPoint;
                     }
                     else if (mWorld.Walk(player) == true)
                     {
                        // If the player is able to walk (i.e. not obstructed by the stage).
                        
                        // Relay the message to all clients except the one that sent it.
                        mServer.Socket.SendAll(Message.Serialize(walkMessage), aReception.EndPoint);
                        // The various operations of the world are deterministic so clients and this
                        // server shoud be in agreement regarding the player's position. That said,
                        // status messages will be sent when charging begins and/or when the player's
                        // turn is complete which will contain the position known to this server.

                        mWorld.Start();
                     }
                  }
                  break;
            }
         }
         catch (Exception e)
         {
            mLog.Warn(e, $"{aReception.EndPoint} sent something unknown/dangerous.");
         }
      }

      #endregion Reception/message handling
   }
}
