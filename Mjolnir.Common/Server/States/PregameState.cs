﻿using Mjolnir.Common.Messages;
using Mjolnir.Common.Models;
using Mjolnir.Common.Network;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Mjolnir.Common.Server.States
{
   public class PregameState : IState
   {
      private const int DELAY = 3; // Delay before transitioning to the in-game state, in seconds.

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly Server mServer;
      private CancellationTokenSource mTokenSource = null;

      public event EventHandler<StateEnum> TransitionEvent;

      public string Name => "Pre-game";

      public PregameState(Server aServer)
      {
         mServer = aServer;
      }

      public void Enter()
      {
         mServer.Socket.ReceiveEvent += OnReceive;

         // If no stage was selected, it's treated as random. Select one now and tell the clients.
         if (mServer.Settings.StageID == StageEnum.NONE)
         {
            mServer.Stage = Stage.Random;
            mServer.Settings.StageID = mServer.Stage.ID;
            mServer.Socket.SendAll(Message.Serialize(mServer.Settings));
         }

         // Give the players initial positions based on the chosen stage's starting locations.
         AssignSpawnPoints();

         // Spawn a task to perform a second-by-second countdown that will 1) send updates to clients
         // and 2) trigger a transition to the in-game state when the countdown finishes.
         mTokenSource = new CancellationTokenSource();

         int countdown = DELAY * 2;
         Task.Run(async () =>
         {
            while (countdown >= 0)
            {
               // Wait half a second.
               // Note: there's some room for error here as Delay() doesn't guarantee it will delay
               // exactly 500 milliseconds but does guarantee a minimum delay of 500 milliseconds.
               await Task.Delay(500);

               // Continue the countdown.
               countdown -= 1;

               // Update the clients so they can display the progress.
               mServer.Socket.SendAll(Message.Serialize(new CountdownMessage((float)countdown / (DELAY * 2))));
            }

            try
            {
               // Trigger a transition to the in-game state.
               TransitionEvent?.Invoke(this, StateEnum.IN_GAME);
            }
            catch (Exception e)
            {
               mLog.Error(e, "Exception thrown while propagating a transition event");
            }
         }, mTokenSource.Token);

         // With this state essentially idle and the countdown happening in a task, this is a good time
         // to (try to) load the stage.
         // If the stage failed to load.
         if (!mServer.Stage.Load(mServer.ContentWrapper))
         {
            mLog.Error($"Failed to load stage {mServer.Stage.Name} from disk. The game cannot continue.");
            try
            {
               TransitionEvent?.Invoke(this, StateEnum.ERROR);
            }
            catch (Exception e)
            {
               // If this catch block is ever reached, something is seriously wrong.
               mLog.Error(e, "Exception thrown while propagating a transition event due to a stage load failure");
            }
         }
      }

      public void Exit()
      {
         mServer.Socket.ReceiveEvent -= OnReceive;

         mTokenSource?.Cancel();
         mTokenSource = null;
      }

      /// <summary>
      /// Assign initial positions to this server's players based on the available starting locations
      /// of the stage selected in the previous state.
      /// </summary>
      private void AssignSpawnPoints()
      {
         // Cycle through team A's players and the stage's starting locations for
         // that team.
         List<Player> teamA = mServer.Players.Where(p => p.Team == TeamEnum.A).ToList();
         for (int i = 0; i < teamA.Count; i++)
         {
            // Team size and bounds check for what's hopefully an impossible case.
            if (i >= 4)
            {
               mLog.Warn($"The size of team A ({teamA.Count}) exceeded the greatest possible size.");
               break;
            }

            // The starting location is a player's would-be center location.
            Vector spawnPoint = mServer.Stage.SpawnPointsA[i];
            // Calculate the player's actual position (origin), which is to the top-left.
            Player player = teamA[i];
            player.X = spawnPoint.X - player.Radius;
            player.Y = spawnPoint.Y - (2.0 * player.Radius);
         }

         // Do the same for team B.
         List<Player> teamB = mServer.Players.Where(p => p.Team == TeamEnum.B).ToList();
         for (int i = 0; i < teamB.Count; i++)
         {
            if (i >= 4)
            {
               mLog.Warn($"The size of team B ({teamB.Count}) exceeded the greatest possible size.");
               break;
            }

            // The starting location is a player's would-be center location.
            Vector location = mServer.Stage.SpawnPointsB[i];
            // Calculate the player's actual position (origin), which is to the top-left.
            Player player = teamB[i];
            player.X = location.X - player.Radius;
            player.Y = location.Y - (2.0 * player.Radius);
         }

         // Update the clients with these new positions.
         mServer.Socket.SendAll(Message.Serialize(new PlayersMessage(mServer.Players)));
      }

      private void OnReceive(object aSender, Reception aReception)
      {
         try
         {
            Message message = Message.Deserialize(aReception.Data);
            switch (message.Op)
            {
               case Message.Opcode.CHAT:
                  if ((message is ChatMessage chatMessage) &&
                      mServer.IsEndPointOfID(aReception.EndPoint, chatMessage.PlayerID, chatMessage))
                  {
                     // Relay the message.
                     mServer.Socket.SendAll(aReception.Data);
                  }
                  break;
            }
         }
         catch (Exception e)
         {
            mLog.Warn(e, $"{aReception.EndPoint} sent something unknown/dangerous.");
         }
      }
   }
}
