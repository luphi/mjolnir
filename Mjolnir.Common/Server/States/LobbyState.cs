﻿using Mjolnir.Common.Messages;
using Mjolnir.Common.Models;
using Mjolnir.Common.Network;
using NLog;
using System;
using System.Linq;

namespace Mjolnir.Common.Server.States
{
   public class LobbyState : IState
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly Server mServer;

      public event EventHandler<StateEnum> TransitionEvent;

      public string Name => "Lobby";

      public LobbyState(Server aServer)
      {
         mServer = aServer;
      }

      public void Enter()
      {
         mServer.JoinEvent += OnPlayerJoin;
         mServer.Socket.ReceiveEvent += OnReceive;
      }

      public void Exit()
      {
         mServer.JoinEvent -= OnPlayerJoin;
         mServer.Socket.ReceiveEvent -= OnReceive;
      }

      private void OnPlayerJoin(object aSender, ServerEventArgs aArgs)
      {
         mServer.Socket.SendTo(Message.Serialize(mServer.Settings), aArgs.EndPoint);
      }


      #region Reception/message handling

      private void OnReceive(object aSender, Reception aReception)
      {
         try
         {
            Message message = Message.Deserialize(aReception.Data);
            switch (message?.Op)
            {
               case Message.Opcode.CHAT:
                  if ((message is ChatMessage chatMessage) &&
                      (mServer.IsEndPointOfID(aReception.EndPoint, chatMessage.PlayerID, chatMessage) == true))
                  {
                     // Relay the message.
                     mServer.Socket.SendAll(aReception.Data);
                  }
                  break;
               case Message.Opcode.PLAYER:
                  if ((message is PlayerMessage playerStatusMessage) &&
                      (mServer.IsEndPointOfID(aReception.EndPoint,
                                             playerStatusMessage.Player?.ID ?? uint.MaxValue,
                                             playerStatusMessage) == true))
                  {
                     // Get a reference to player's model as this server currently recognizes it.
                     Player player = mServer.Players.FirstOrDefault(p => p.ID == playerStatusMessage.Player.ID);
                     if (player != null)
                     {
                        // Remember some specific values in order to know if they've changed after the sync.
                        TeamEnum oldTeam = player.Team;
                        Item[] oldItems = player.Items;

                        // Sync this server's model of the player with the updated model.
                        // If syncing caused anything to be changed within the given list of properties
                        // (sync flags) allowed to change.
                        if (player.Sync(playerStatusMessage.Player, SyncFlags.IS_READY |
                                                                    SyncFlags.ITEMS |
                                                                    SyncFlags.MOBILE_ID |
                                                                    SyncFlags.TEAM) == true)
                        {
                           // If the player is requesting a team change.
                           if (oldTeam != player.Team)
                           {
                              // Make sure there's room on the other team for him/her.
                              // Get the number of players/bots currently on the two teams.
                              int teamACount = mServer.Players.Where(p => p.Team == TeamEnum.A).Count();
                              int teamBCount = mServer.Players.Count - teamACount;

                              int maxPerTeam = (int)(mServer.Settings.MaxPlayerCount / 2);
                              // If the team the player is switching to is full.
                              if (((player.Team == TeamEnum.A) && (teamACount >= maxPerTeam)) ||
                                  ((player.Team == TeamEnum.B) && (teamBCount >= maxPerTeam)))
                              {
                                 mLog.Info($"Player {player.Name} asked to join team {player.Team} but " +
                                     "the team is full.");
                                 // Deny the team switch by assigning the original value.
                                 player.Team = oldTeam;
                              }
                           }

                           // If the player's list of selected items has changed.
                           if (Enumerable.SequenceEqual(oldItems, player.Items) == false)
                           {
                              // Determine if the updated player's total number of item slots in use
                              // exceeds the maximum allowed (six).
                              uint updatedItemSlotSum = 0;
                              for (int i = 0; (i < 6) && (i < player.Items.Length); i++)
                              {
                                 Item item = player.Items[i];
                                 if (item != null)
                                 {
                                    // The slot counts will be derived from a newly-instantiated model
                                    // in order to avoid relying on the deserialized property. A
                                    // malicious client could have modified it.
                                    updatedItemSlotSum += new Item(item).Slots;
                                    i += (int)item.Slots - 1; // Increments i an extra index if the item uses two slots.
                                 }
                              }

                              if (updatedItemSlotSum > 6)
                              {
                                 // Deny the item list update by assigning the original value.
                                 mLog.Warn($"Player {player.Name} attempted to select items totalling " +
                                           $"{updatedItemSlotSum} slots, exceeding the maximum of 6.");
                                 player.Items = oldItems;
                              }
                           }

                           // Relay the updated player status to all players, including the one that
                           // sent the message.
                           mServer.Socket.SendAll(Message.Serialize(new PlayerMessage(player)));

                           // If all of the players are flagged as ready (to begin the game).
                           if (mServer.Players.Where(p => p.IsReady == true).Count() == mServer.Players.Count)
                           {
                              try
                              {
                                 // Transition to the brief pre-game state. (Which will, in turn,
                                 // transition to the in-game state.)
                                 TransitionEvent?.Invoke(this, StateEnum.PRE_GAME);
                              }
                              catch (Exception e)
                              {
                                 mLog.Error(e, "Exception thrown while propagating a transition event");
                              }
                           }
                        }
                     }
                  }
                  break;
               case Message.Opcode.SERVER_SETTINGS:
                  if (message is ServerSettingsMessage serverSettingsMessage)
                  {
                     // Try to get the player associated with this client.
                     Player player = mServer.GetPlayerByEndpoint(aReception.EndPoint);

                     if ((player != null) && ((player.IsAdmin == true) || (player.IsVeteran == true)))
                     {
                        // If the player was found and is either an admin or veteran.

                        // This player has the privilege to change server settings. Change 'em.
                        mServer.Settings = serverSettingsMessage;

                        // Update the stage with the model matching the stage's ID.
                        // Note: the "Random" stage will result in a null assignment. An actual
                        // model will be selected when transitioning out of this state.
                        mServer.Stage = Stage.List.FirstOrDefault(s => s.ID == serverSettingsMessage.StageID);

                        // Relay the new settings to all clients.
                        mServer.Socket.SendAll(aReception.Data);
                     }
                     else if (player != null)
                     {
                        // If we know the player that just tried to change server setings without
                        // sufficient privilege.

                        mLog.Warn($"Player {player.Name} attempted to change this server's settings " +
                                  "but is neither an admin nor a veteran.");
                     }
                     else
                     {
                        // If we don't even know what this client is.

                        mLog.Warn($"The client at {aReception.EndPoint} attempted to change this server's " +
                                  "settings but could not be associated with a player.");
                     }
                  }
                  break;
               case Message.Opcode.START_GAME: // Note: there is no dedicated message, only a START_GAME opcode.
                  {
                     // Try to get the player associated with this client.
                     Player player = mServer.GetPlayerByEndpoint(aReception.EndPoint);

                     if ((player != null) && ((player.IsAdmin == true) || (player.IsVeteran == true)))
                     {
                        // If the player was found and is either an admin or veteran.

                        // If all of the players are flagged as ready (to begin the game).
                        if (mServer.Players.Where(p => p.IsReady == true).Count() == mServer.Players.Count)
                        {
                           try
                           {
                              // Transition to the brief pre-game state. (Which will, in turn,
                              // transition to the in-game state.)
                              TransitionEvent?.Invoke(this, StateEnum.PRE_GAME);
                           }
                           catch (Exception e)
                           {
                              mLog.Error(e, "Exception thrown while propagating a transition event");
                           }
                        }
                     }
                     else if (player != null)
                     {
                        mLog.Warn($"Player {player.Name} attempted to start the game but is neither " +
                                  "an admin nor a veteran.");
                     }
                     else
                     {
                        mLog.Warn($"The client at {aReception.EndPoint} attempted to start game but " +
                                  "could not be associated with a player.");
                     }
                  }
                  break;
            }
         }
         catch (Exception e)
         {
            mLog.Warn(e, $"{aReception.EndPoint} sent something unknown/dangerous.");
         }
      }

      #endregion Reception/message handling
   }
}
