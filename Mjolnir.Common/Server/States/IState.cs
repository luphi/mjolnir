﻿using System;

namespace Mjolnir.Common.Server.States
{
   public interface IState
   {
      /// <summary>
      /// Event observed by the server. When invoked, the server will transition to
      /// the state represented by the given state code.
      /// </summary>
      event EventHandler<StateEnum> TransitionEvent;

      /// <summary>
      /// A human-readable name for the state. This does not affect behavior.
      /// </summary>
      string Name { get; }

      /// <summary>
      /// Prepare the state as needed. This method will be called when the state
      /// is entered, either for the first or a subsequent time.
      /// </summary>
      void Enter();

      /// <summary>
      /// Clean up the state as needed. This method will be called after the state
      /// invokes a transition event.
      /// </summary>
      void Exit();
   }
}
