﻿using Mjolnir.Common.Messages;
using Mjolnir.Common.Network;
using NLog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Mjolnir.Common.Server.States
{
   public class PostgameState : IState
   {
      private const int DELAY = 15; // Delay before transitioning to the lobby state, in seconds.

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly Server mServer;
      private CancellationTokenSource mTokenSource = null;

      public event EventHandler<StateEnum> TransitionEvent;

      public string Name => "Post-game";

      public PostgameState(Server aServer)
      {
         mServer = aServer;
      }

      public void Enter()
      {
         mServer.Socket.ReceiveEvent += OnReceive;

         // Spawn a task to perform a second-by-second countdown that will 1) send updates to clients
         // and 2) trigger a transition to the lobby state when the countdown finishes.
         mTokenSource = new CancellationTokenSource();

         int countdown = DELAY * 2;
         Task.Run(async () =>
         {
            while (countdown >= 0)
            {
               // Wait half a second.
               // Note: there's some room for error here as Delay() doesn't guarantee it will delay
               // exactly 500 milliseconds but does guarantee a minimum delay of 500 milliseconds.
               await Task.Delay(500);
               // Continue the countdown.
               countdown--;
               // Update the clients so they can display the progress.
               mServer.Socket.SendAll(Message.Serialize(new CountdownMessage((float)countdown / (DELAY * 2))));
            }

            try
            {
               // Trigger a transition to the lobby state.
               TransitionEvent?.Invoke(this, StateEnum.LOBBY);
            }
            catch (Exception e)
            {
               mLog.Error(e, "Exception thrown while propagating a transition event");
            }
         }, mTokenSource.Token);
      }

      public void Exit()
      {
         mServer.Socket.ReceiveEvent -= OnReceive;

         mTokenSource?.Cancel();
         mTokenSource = null;
      }

      private void OnReceive(object aSender, Reception aReception)
      {
         try
         {
            Message message = Message.Deserialize(aReception.Data);
            switch (message.Op)
            {
               case Message.Opcode.CHAT:
                  // Relay the message.
                  mServer.Socket.SendAll(aReception.Data);
                  break;
            }
         }
         catch (Exception e)
         {
            mLog.Warn(e, $"{aReception.EndPoint} sent something unknown/dangerous.");
         }
      }
   }
}
