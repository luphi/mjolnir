﻿using Mjolnir.Common.Disk;
using Mjolnir.Common.Messages;
using Mjolnir.Common.Models;
using Mjolnir.Common.Network;
using Mjolnir.Common.Server.States;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Mjolnir.Common.Server
{
   public class Server : IDisposable
   {
      private const int JOIN_TIMEOUT = 30000; // Milliseconds.

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly bool mIsSingleplayer;
      private readonly bool mIsPrivate;
      private readonly bool mAreBotsEnabled;
      private readonly string mServerName;
      private readonly string mMOTD;
      private readonly string mAdminPassword;
      private readonly string mJoinPassword;
      private readonly List<string> mBanList;
      private readonly ConcurrentDictionary<IPEndPoint, Player> mPlayerMap = new ConcurrentDictionary<IPEndPoint, Player>();
      private readonly ConcurrentQueue<Bot> mBots = new ConcurrentQueue<Bot>();
      private readonly ConcurrentBag<IPEndPoint> mPendingJoins = new ConcurrentBag<IPEndPoint>();
      private static uint sID = 0;

      ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
      private CancellationTokenSource mDevTokenSource;
      private readonly string mLoremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
          "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim " +
          "veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo " +
          "consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore " +
          "eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa " +
          "qui officia deserunt mollit anim id est laborum";
      ///////////////////////////////////////////////////////////////////////////////////////////////////////////////


      #region State code and instances

      // State management and status properties/variable.
      private StateEnum mStateCode;

      private LobbyState LobbyState
      {
         get
         {
            mLobbyState ??= new LobbyState(this); // Assign if null.
            return mLobbyState;
         }
      }
      private LobbyState mLobbyState = null;
      private PregameState PregameState
      {
         get
         {
            mPregameState ??= new PregameState(this); // Assign if null.
            return mPregameState;
         }
      }
      private PregameState mPregameState = null;
      private IngameState IngameState
      {
         get
         {
            mIngameState ??= new IngameState(this); // Assign if null.
            return mIngameState;
         }
      }
      private IngameState mIngameState = null;
      private PostgameState PostgameState
      {
         get
         {
            mPostgameState ??= new PostgameState(this); // Assign if null.
            return mPostgameState;
         }
      }
      private PostgameState mPostgameState = null;

      #endregion State code and instances


      // Public properties made available to the boostrap and states.
      public IContentWrapper ContentWrapper { get; }

      public TcpServerSocket Socket { get; }

      public IState State { get; private set; }

      public StateEnum StateCode => mStateCode;

      public Stage Stage { get; set; } = null;

      public List<Player> Players => mPlayerMap.Values.ToList();

      public List<Bot> Bots => mBots.ToList();

      public int PlayerCount => mPlayerMap.Count - mBots.Count;

      /// <summary>
      /// Persistant settings configured during the lobby state that should be applied to
      /// future rounds too. The message allocated here is effectively the game's default settings.
      /// </summary>
      public ServerSettingsMessage Settings
      {
         get => mSettings;
         set
         {
            if (value != null)
            {
               mSettings = value;
            }
         }
      }
      private ServerSettingsMessage mSettings = new ServerSettingsMessage(StageEnum.NONE,
                                                                          8,
                                                                          SuddenDeathModeEnum.DOUBLE);

      public event EventHandler<StateEnum> TransitionEvent;
      public event EventHandler<ServerEventArgs> JoinEvent;
      public event EventHandler<ServerEventArgs> LeaveEvent;

      public Server(IContentWrapper aContentWrapper,
                    TcpServerSocket aServerSocket,
                    bool aIsSingleplayer,
                    bool aIsPrivate,
                    bool aAreBotsEnabled,
                    string aServerName,
                    string aMOTD,
                    string aAdminPassword,
                    string aJoinPassword,
                    List<string> aBanList)
      {
         mIsSingleplayer = aIsSingleplayer;
         mIsPrivate = aIsPrivate;
         mAreBotsEnabled = aAreBotsEnabled;
         mServerName = aServerName;
         mMOTD = aMOTD;
         mAdminPassword = aAdminPassword;
         mJoinPassword = aJoinPassword;
         mBanList = aBanList;

         ContentWrapper = aContentWrapper;

         Socket = aServerSocket;
         Socket.ListenEvent += OnOpen;
         Socket.CloseEvent += OnClose;
         Socket.ConnectEvent += OnClientConnect;
         Socket.DisconnectEvent += OnClientDisconnect;
         Socket.ReceiveEvent += OnReceive;

         mStateCode = StateEnum.LOBBY;
         State = LobbyState;
      }

      public void Dispose()
      {
         Socket.ListenEvent -= OnOpen;
         Socket.CloseEvent -= OnClose;
         Socket.ConnectEvent -= OnClientConnect;
         Socket.DisconnectEvent -= OnClientDisconnect;
         Socket.ReceiveEvent -= OnReceive;
      }

      /// <summary>
      /// Helper method intended for states to associate a player model with a client endpoint.
      /// </summary>
      /// <param name="aEndPoint"></param>
      /// <returns></returns>
      public Player GetPlayerByEndpoint(IPEndPoint aEndPoint)
      {
         // Try to get the player model owned by the given endpoint.
         if (mPlayerMap.TryGetValue(aEndPoint, out Player player) == true)
         {
            // Found it.
            return player;
         }

         // Didn't find it. Return a null.
         return null;
      }

      /// <summary>
      /// Helper method intended for states to associate a client endpoint with a player ID.
      /// </summary>
      /// <param name="aID"></param>
      /// <returns></returns>
      public IPEndPoint GetEndpointByPlayerID(uint aID)
      {
         // Try to get the key-value pair for the endpoint-player pair with the matching player ID.
         KeyValuePair<IPEndPoint, Player> pair = mPlayerMap.FirstOrDefault(p => p.Value.ID == aID);

         // If the pair matches the type's default.
         if (pair.Equals(default(KeyValuePair<IPEndPoint, Player>)) == true)
         {
            // FirstOrDefault() returned a default instance meaning it was not found. Return a null.
            return null;
         }

         // Found it. Return the endpoint of the player.
         return pair.Key;
      }

      public bool IsEndPointOfID(IPEndPoint aEndPoint, uint aPlayerID, Message aMessage)
      {
         if (aEndPoint == null)
         {
            return false;
         }

         // Try to get the player owned by the client at the given endpoint.
         Player sourcePlayer = GetPlayerByEndpoint(aEndPoint);
         // Try to get the player matching this player ID.
         Player targetPlayer = mPlayerMap.Values.FirstOrDefault(p => p.ID == aPlayerID);

         // Prepare a human-readable name for the player being spoken for.
         string playerName;
         if (targetPlayer != null)
         {
            playerName = $"player \"{targetPlayer.Name}\" ({targetPlayer.ID}";
         }
         else
         {
            playerName = $"a non-existent player with ID {aPlayerID}";
         }

         // If the client at the given endpoint does not have a player associated with it.
         if (sourcePlayer == null)
         {
            // The client is connected, in the socket sense, but does not own any player.
            mLog.Warn($"A client at endpoint {aEndPoint} attempted to speak for {playerName} but " +
                      $"has no player associated with it: {aMessage}");
            return false;
         }

         // If no player with the given player ID exists.
         if ((targetPlayer == null) || (sourcePlayer.ID != targetPlayer.ID))
         {
            // The client owns a player but not the one it identified.
            mLog.Warn($"The client associated with player \"{sourcePlayer.Name}\" ({sourcePlayer.ID}) " +
                      $"attempted to speak for {playerName}: {aMessage}");
            return false;
         }

         // Everything checks out. The client owns the player with the given ID.
         return true;
      }

      /// <summary>
      /// Invoked when the server socket begins listening for clients.
      /// This is the entry point for the "state machine" that is this server, although
      /// it's just a linear, cyclical series of only four states.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnOpen(object aSender, EventArgs aArgs)
      {
         if (mAreBotsEnabled == true)
         {
            // Fill all available player slots with bots.
            while (mPlayerMap.Count < Settings.MaxPlayerCount)
            {
               AddBot();
            }
         }

         if (mStateCode != StateEnum.LOBBY)
         {
            mStateCode = StateEnum.LOBBY;
            State.Exit();
            State = LobbyState;
         }

         State.TransitionEvent += OnStateTransition;
         State.Enter();

         ///////////////////////////////////////////////////////////////////////////////////////////////////////////
         mDevTokenSource = new CancellationTokenSource();
         Task.Run(async () =>
         {
            Random random = new Random();
            while (!mDevTokenSource?.Token.IsCancellationRequested ?? false)
            {
               await Task.Delay(random.Next(500, 3000));
               Player player = Players[random.Next(Players.Count)];
               List<string> loremIpsum = new List<string>(mLoremIpsum.Split(' '));
               ChatMessage message = new ChatMessage(player.ID,
                      (mStateCode == StateEnum.IN_GAME) && (random.Next() % 2 == 0),
                      string.Join(" ", loremIpsum.OrderBy(s => random.Next()).Take(random.Next(5, 15))));
               Socket.SendAll(Message.Serialize(message));
            }
         });
         ///////////////////////////////////////////////////////////////////////////////////////////////////////////
      }

      /// <summary>
      /// Invoked when the server socket closes.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnClose(object aSender, EventArgs aArgs)
      {
         if (State != null)
         {
            State.Exit();
            State.TransitionEvent -= OnStateTransition;
         }

         ///////////////////////////////////////////////////////////////////////////////////////////////////////////
         mDevTokenSource?.Cancel();
         mDevTokenSource = null;
         ///////////////////////////////////////////////////////////////////////////////////////////////////////////
      }

      private void OnStateTransition(object aSender, StateEnum aNextState)
      {
         // Begin the transition by remembering the new state (code).
         mStateCode = aNextState;

         // Let the current state perform its cleanup.
         State.Exit();
         State.TransitionEvent -= OnStateTransition;

         // Select the IState instance that implements this state.
         switch (mStateCode)
         {
            case StateEnum.LOBBY:
               State = LobbyState;
               break;
            case StateEnum.PRE_GAME:
               State = PregameState;
               break;
            case StateEnum.IN_GAME:
               State = IngameState;
               break;
            case StateEnum.POST_GAME:
               State = PostgameState;
               break;
            case StateEnum.ERROR:
               // The server is inoperable so just leave it in limbo.
               State = null;
               break;
         }

         // Let the bots switch contexts as needed.
         foreach (Bot bot in mBots)
         {
            bot.OnStateTransition(mStateCode);
         }

         if (State != null)
         {
            mLog.Info($"Transitiong to state {State.Name}");

            // Alert the clients to the state transition. They will respond by loading the
            // associated screen.
            Socket.SendAll(Message.Serialize(new ServerStateMessage(mStateCode)));

            State.TransitionEvent += OnStateTransition;
            // Tell the new state to configure itself.
            State.Enter();
         }

         try
         {
            // Notify any hooked handlers of the new state.
            TransitionEvent?.Invoke(this, mStateCode);
         }
         catch (Exception e)
         {
            mLog.Error(e, "Exception thrown while propagating a transition event");
         }
      }

      /// <summary>
      /// Invoked when a client socket connects.
      /// This method begins the process of allowing a new player to join if all of the
      /// conditions are met.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aEndPoint">Information about hte endpoint that connected.</param>
      private void OnClientConnect(object aSender, IPEndPoint aEndPoint)
      {
         // If this client is banned.
         if (mBanList?.Contains(aEndPoint.Address.ToString()) == true)
         {
            // Bluntly tell the client that is is banned.
            Socket.SendTo(Message.Serialize(new KickMessage(KickReasonEnum.BAN)), aEndPoint);
            // Fully ignore the client by closing its socket.
            Socket.Remove(aEndPoint);
            return;
         }

         // If the server has moved on from the lobby state, it's no longer accepting new players.
         if (mStateCode != StateEnum.LOBBY)
         {
            // Tell the player he/she missed the chance to join.
            Socket.SendTo(Message.Serialize(new KickMessage(KickReasonEnum.NOT_NOW)), aEndPoint);
            Socket.Remove(aEndPoint);
            return;
         }

         // Give the client some context. Specifically, this server's name, message of
         // the day, and the private/public status. If private, the client will ask the
         // user for a password.
         Socket.SendTo(Message.Serialize(new AhoyMessage(mServerName, mMOTD, mIsPrivate)), aEndPoint);

         // Add this client to the list of clients that are in the process of joining.
         mPendingJoins.Add(aEndPoint);

         // Spawn a task to handle a timeout. That is, what should be done if the client
         // fails to send a "join" message in time.
         // In practice, this will most likely be caused by a user failing to enter a
         // password fast enough but may also indicate a modified and/or rogue client.
         Task.Run(async () =>
         {
            // Give the user some time. This delay allows for network latency to be
            // accounted for but, mainly, gives the user time to type a password.
            await Task.Delay(JOIN_TIMEOUT);

            // If the client is still in the list (i.e. not removed after receiving the
            // "join" message we were waiting for), it should be removed by force.
            if (mPendingJoins.Contains(aEndPoint) == true)
            {
               // Notify the would-be player that he/she is being kicked from the server
               // (even before technically joining) and provide the reason.
               Socket.SendTo(Message.Serialize(new KickMessage(KickReasonEnum.TIMEOUT)), aEndPoint);
               // Forget this client socket.
               Socket.Remove(aEndPoint);
            }
         });
      }

      /// <summary>
      /// Invoked when a client socket disconnects.
      /// Initiates a "leave" event which ultimately leads to the registry and other player(s)
      /// being notified.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aEndPoint">Information about the endpoint that disconnected.</param>
      private void OnClientDisconnect(object aSender, IPEndPoint aEndPoint)
      {
         // If this client had joined as a player.
         if (mPlayerMap.TryRemove(aEndPoint, out Player player) == true)
         {
            // Let them leave.
            mLog.Info($"Removed player {player.Name} due to a lost connection");
            // Tell everyone about the parting player by telling them about all current players.
            Socket.SendAll(Message.Serialize(new PlayersMessage(Players)));

            // If there's at least one player left.
            if (PlayerCount > 0)
            {
               // Find the player that has been around the longest (lowest ID) and make him/her the veteran.
               Player oldestPlayer = Players.Where(p => p.IsBot == false).OrderBy(p => p.ID).First();
               oldestPlayer.IsVeteran = true;
            }

            try
            {
               LeaveEvent?.Invoke(this, new ServerEventArgs(aEndPoint, player));
            }
            catch (Exception e)
            {
               mLog.Error(e, "Exception thrown while propagating a player leave event");
            }

            // If bots are being used to fill unused player slots, add one to take up the player's slot.
            if (mAreBotsEnabled == true)
            {
               AddBot();
            }
         }
      }

      /// <summary>
      /// Invoked by the socket when data is received from any client.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aReception">Object holding the data and information about the
      /// endpoint that sent it.</param>
      private void OnReceive(object aSender, Reception aReception)
      {
         try
         {
            Message message = Message.Deserialize(aReception.Data);

            if (message is AhoyHoyMessage joinmessage)
            {
               if (mPendingJoins.TryTake(out IPEndPoint endPoint) == true)
               {
                  OnAhoyHoy(endPoint, joinmessage);
               }
            }
         }
         catch (Exception e)
         {
            mLog.Warn(e, $"{aReception.EndPoint} sent something unknown/dangerous.");
         }
      }

      /// <summary>
      /// Called when a "I want to join" message comes in from a new client/player.
      /// Determines if said player can join and if the player is an admin or veteran.
      /// If the player cannot join, the player is kicked and disconnected.
      /// </summary>
      /// <param name="aEndPoint"></param>
      /// <param name="aMessage"></param>
      private void OnAhoyHoy(IPEndPoint aEndPoint, AhoyHoyMessage aMessage)
      {
         // If this server is configured to be private (i.e. requires users to enter a password) AND
         // the password is incorrect.
         if ((mIsPrivate == true) && (aMessage.JoinPassword != mJoinPassword))
         {
            // Notify the would-be player that he/she is being kicked from the server (even before
            // technically joining) and provide the reason.
            Socket.SendTo(Message.Serialize(new KickMessage(KickReasonEnum.INCORRECT_PASSWORD)), aEndPoint);

            // Finish kicking 'em.
            Socket.Remove(aEndPoint);
            return;
         }

         // If bots are being used to fill unused player slots, remove one to make room.
         if (mAreBotsEnabled == true)
         {
            RemoveABot();
         }

         // Create the model to represent the player and give him/her a unique ID.
         bool isAdmin = (aMessage.AdminPassword == mAdminPassword) || (mIsSingleplayer == true);
         Player player = new Player(sID++, aMessage.Name, isAdmin);

         // If this is the only player.
         if (PlayerCount == 0)
         {
            // He/she has been here the longest and is, therefore, the veteran.
            player.IsVeteran = true;
         }

         // Determine the player's default team.
         player.Team = GetLeastPopulatedTeam();
         mPlayerMap[aEndPoint] = player;

         // Give the new player some information it will need.
         Socket.SendTo(Message.Serialize(new WelcomeMessage(player.ID, isAdmin, mStateCode)), aEndPoint);

         // Tell everyone about the new player by telling them about all current players.
         Socket.SendAll(Message.Serialize(new PlayersMessage(Players)));

         try
         {
            JoinEvent?.Invoke(this, new ServerEventArgs(aEndPoint, player));
         }
         catch (Exception e)
         {
            mLog.Error(e, "Exception thrown while propagating a player join event");
         }
      }

      private TeamEnum GetLeastPopulatedTeam()
      {
         // Get the number of players/bots currently on the two teams.
         int teamACount = Players.Where(p => p.Team == TeamEnum.A).Count();
         int teamBCount = Players.Count - teamACount;

         // Choose one based on the lower value, defaulting to A when they're equal.
         return teamBCount < teamACount ? TeamEnum.B : TeamEnum.A;
      }

      private void AddBot()
      {
         Bot bot = new Bot(sID++)
         {
            Team = GetLeastPopulatedTeam()
         };

         // Generate a fake endpoint for the bot.
         IPEndPoint endPoint = GetEndPointByBot(bot);

         // Add the bot to the player map as if it's a real boy.
         mPlayerMap[endPoint] = bot;
         mBots.Enqueue(bot);
         mLog.Info($"Added bot player {bot.Name}");

         try
         {
            JoinEvent?.Invoke(this, new ServerEventArgs(endPoint, new Player(bot))); // TODO: use Clone() instead?
         }
         catch (Exception e)
         {
            mLog.Error(e, "Exception thrown while propagating a bot join event");
         }
      }

      private void RemoveABot()
      {
         if (mBots.TryDequeue(out Bot bot) == true)
         {
            // Get the bot's fake endpoint.
            IPEndPoint endPoint = GetEndPointByBot(bot);

            if (mPlayerMap.TryRemove(endPoint, out Player player) == true)
            {
               try
               {
                  LeaveEvent?.Invoke(this, new ServerEventArgs(endPoint, new Player(bot))); // TODO: use Clone() instead?
               }
               catch (Exception e)
               {
                  mLog.Error(e, "Exception thrown while propagating a bot leave event");
               }

               // Dipose of the bot (i.e. unreserve the bot's name so it can be used again).
               bot.Dispose();

               mLog.Info($"Removed bot player {bot.Name}");
            }
         }
      }

      private IPEndPoint GetEndPointByBot(Bot aBot)
      {
         // Generate a fake endpoint for the bot. It doesn't need to actually point at
         // something but does need to be unique (to be used as a map key).
         return new IPEndPoint(IPAddress.Any, (int)aBot.ID);
      }
   }
}
