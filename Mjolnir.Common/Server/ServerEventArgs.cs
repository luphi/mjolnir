﻿using Mjolnir.Common.Models;
using System.Net;

namespace Mjolnir.Common.Server
{
   public class ServerEventArgs
   {
      public IPEndPoint EndPoint { get; }

      public Player Player { get; }

      public ServerEventArgs(IPEndPoint aEndPoint, Player aPlayer)
      {
         EndPoint = aEndPoint;
         Player = aPlayer;
      }
   }
}
