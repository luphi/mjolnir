﻿using Mjolnir.Common.Disk;
using Mjolnir.Common.Messages;
using Mjolnir.Common.Network;
using NLog;
using System;
using System.Collections.Generic;
using System.Net;

namespace Mjolnir.Common.Server
{
   public class ServerBootstrap
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly IContentWrapper mContentWrapper;
      private readonly string mHostAddress;
      private readonly int mHostPort;
      private readonly string mRegistryAddress;
      private readonly int mRegistryPort;
      private readonly bool mIsSingleplayer;
      private readonly bool mIsPrivate;
      private readonly bool mIsVoiceEnabled;
      private readonly bool mAreBotsEnabled;
      private readonly string mServerName;
      private readonly string mMOTD;
      private readonly string mAdminPassword;
      private readonly string mJoinPassword;
      private readonly List<string> mBanList;
      private TcpServerSocket mServerSocket = null;
      private RegistryClient mRegistryClient = null;
      private Server mServer = null;

      /// <summary>
      /// Singleplayer constructor. Configures the server for localhost.
      /// </summary>
      public ServerBootstrap(IContentWrapper aContentWrapper)
      {
         mContentWrapper = aContentWrapper;
         mHostAddress = IPAddress.Loopback.ToString();
         mHostPort = Constants.DefaultServerPort;
         mRegistryAddress = null;
         mRegistryPort = 0;
         mIsSingleplayer = true;
         mIsPrivate = false;
         mIsVoiceEnabled = false;
         mAreBotsEnabled = true;
         mServerName = "Singleplayer";
         mMOTD = "Singleplayer";
         mAdminPassword = null;
         mJoinPassword = null;
         mBanList = null;
      }

      /// <summary>
      /// Multiplayer constructor. Configures the server for public usage.
      /// </summary>
      /// <param name="aHostPort"></param>
      /// <param name="aRegistryAddress"></param>
      /// <param name="aRegistryPort"></param>
      /// <param name="aIsPrivate"></param>
      /// <param name="aIsVoiceEnabled"></param>
      /// <param name="aAreBotsEnabled"></param>
      /// <param name="aServerName"></param>
      /// <param name="aMOTD"></param>
      /// <param name="aAdminPassword"></param>
      /// <param name="aJoinPassword"></param>
      /// <param name="aBanList"></param>
      public ServerBootstrap(IContentWrapper aContentWrapper,
                             int aHostPort,
                             string aRegistryAddress,
                             int aRegistryPort,
                             bool aIsPrivate,
                             bool aIsVoiceEnabled,
                             bool aAreBotsEnabled,
                             string aServerName,
                             string aMOTD,
                             string aAdminPassword = null,
                             string aJoinPassword = null,
                             List<string> aBanList = null)
      {
         mContentWrapper = aContentWrapper;
         mHostAddress = IPAddress.Any.ToString();
         mHostPort = aHostPort;
         mRegistryAddress = aRegistryAddress;
         mRegistryPort = aRegistryPort;
         mIsSingleplayer = false;
         mIsPrivate = aIsPrivate;
         mIsVoiceEnabled = aIsVoiceEnabled;
         mAreBotsEnabled = aAreBotsEnabled;
         mServerName = aServerName;
         mMOTD = aMOTD;
         mAdminPassword = aAdminPassword;
         mJoinPassword = aJoinPassword;
         mBanList = aBanList;
      }

      public int Start()
      {
         // If this bootstrap was constructed with a valid registry hostname and port.
         if ((string.IsNullOrEmpty(mRegistryAddress) == false) && (mRegistryPort != 0))
         {
            // Create a registry client. This is used to both register and unregister a
            // server with a registry (i.e. the master list of servers) and to send updates
            // about the player count and state.
            // Important note: if no valid registry address nor port is provided, the
            // registry client will act as a dummy. It won't even attempt to communicate
            // with a registry. This is the case for servers running as a local, single-
            // player server.
            mRegistryClient = new RegistryClient(mRegistryAddress, mRegistryPort);
         }

         // Create and attempt to open the server socket. This socket will be the
         // root source for all events, including the event that will trigger a shutdown.
         // With the socket listening, register its instance.
         mServerSocket = new TcpServerSocket(mHostAddress, mHostPort);
         mServerSocket.ListenEvent += OnSocketListen;
         mServerSocket.CloseEvent += OnSocketClose;

         // Create the finite state machine that will manage the various states the
         // server will operate in. This object also contains the main time step logic.
         mServer = new Server(mContentWrapper,
                              mServerSocket,
                              mIsSingleplayer,
                              mIsPrivate,
                              mAreBotsEnabled,
                              mServerName,
                              mMOTD,
                              mAdminPassword,
                              mJoinPassword,
                              mBanList);
         mServer.TransitionEvent += OnStateTransition;
         mServer.JoinEvent += OnPlayerJoinOrLeave;
         mServer.LeaveEvent += OnPlayerJoinOrLeave;

         // If the socket cannot listen on the given port.
         if (mServerSocket.Listen(out int boundPort) == false)
         {
            // It would be impossible to act as a server without the socket listening.
            // Therefore, we will log an error and return 0 to notify the caller.
            mLog.Error($"Failed to bind socket {mHostPort}. Cannot continue.");
            mServerSocket = null;
            mRegistryClient = null;
            return 0;
         }

         mLog.Info($"Started a game server - bound port {boundPort}");
         return boundPort;
      }

      public void Stop(string aShutdownReason = null)
      {
         if (mServerSocket != null)
         {
            // Tell all clients that this server is going down and provide them with a reason.
            // If null, the reason will not be displayed and simply treated as a smooth shutdown.
            // In other cases, it may report an error. In all cases, the clients will exit.
            mServerSocket.SendAll(Message.Serialize(new ServerShutdownMessage(aShutdownReason)));

            // Unhook the event handlers, close the socket, and let garbage collection have it.
            mServerSocket.ListenEvent -= OnSocketListen;
            mServerSocket.CloseEvent -= OnSocketClose;
            mServerSocket.Close();
            mServerSocket = null;
         }

         if (mServer != null)
         {
            // Unhook the event handlers, let it do its cleanup, and then throw it away.
            mServer.TransitionEvent -= OnStateTransition;
            mServer.JoinEvent -= OnPlayerJoinOrLeave;
            mServer.LeaveEvent -= OnPlayerJoinOrLeave;
            mServer.Dispose();
            mServer = null;
         }

         mRegistryClient = null;

         mLog.Info("Stopped a game server");
      }

      /// <summary>
      /// Invoked when the server socket begins listening.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnSocketListen(object aSender, EventArgs aArgs)
      {
         // For the first time, register this server with the registry.
         mRegistryClient?.Register(mServerName,
                                   mHostPort,
                                   mServer.StateCode,
                                   mServer.PlayerCount,
                                   mIsPrivate,
                                   mIsVoiceEnabled);
      }

      /// <summary>
      /// Invoked when the server socket closes. In theory, this should only happen when
      /// the server is properly stopped but may also result from an error.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnSocketClose(object aSender, EventArgs aArgs)
      {
         // Tell the registry to forget about this server.
         mRegistryClient?.Unregister();
      }


      private void OnStateTransition(object aSender, StateEnum aNextState)
      {
         if (aNextState == StateEnum.ERROR)
         {
            // If the server ran into an unrecoverable error.

            mLog.Error($"The server ran into an unrecoverable error and is now offline.");
            // Stop the server while providing a reason to the clients for the sudden shutdown.
            Stop("The server ran into an unrecoverable error.");
         }
         else
         {
            // If the server is smoothly transitioning to a new state.

            // Notify the registry of the new state.
            mRegistryClient?.Register(mServerName,
                                      mHostPort,
                                      mServer.StateCode,
                                      mServer.PlayerCount,
                                      mIsPrivate,
                                      mIsVoiceEnabled);
         }
      }

      /// <summary>
      /// Invoked when a player joins or leaves. Notifies the registry, if connected, of the change.
      /// </summary>
      /// <param name="aSender"></param>
      /// <param name="aArgs"></param>
      private void OnPlayerJoinOrLeave(object aSender, object aArgs)
      {
         // Notify the registry of the new player count.
         mRegistryClient?.Register(mServerName,
                                   mHostPort,
                                   mServer.StateCode,
                                   mServer.PlayerCount,
                                   mIsPrivate,
                                   mIsVoiceEnabled);
      }
   }
}
