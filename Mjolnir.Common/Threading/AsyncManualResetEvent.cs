﻿using System.Threading;
using System.Threading.Tasks;

namespace Mjolnir.Common.Threading
{
   /// <summary>
   /// Minimal, awaitable equivalent of System.Threading.ManualResetEvent which can be found
   /// in the standard library. In contrast, this class may be used to block a task rather
   /// than a thread.
   /// </summary>
   public class AsyncManualResetEvent
   {
      private const int POLLING_PERIOD_IN_MILLISECONDS = 50;

      private CancellationTokenSource mTokenSource = new CancellationTokenSource();

      /// <summary>
      /// When true, indicates that the "event" has been signaled. At this time, this
      /// object will no longer delay tasks calling WaitAsync().
      /// </summary>
      public bool IsSet { get; private set; }

      public AsyncManualResetEvent(bool aInitialState = false)
      {
         IsSet = aInitialState;
      }

      /// <summary>
      /// Unsignal the "event" such that tasks awaiting this object with WaitAsync()
      /// will be delayed.
      /// </summary>
      public void Reset()
      {
         mTokenSource = new CancellationTokenSource();
         IsSet = false;
      }

      /// <summary>
      /// Signal the "event" such that tasks awaiting this object with WaitSync()
      /// will no longer be delayed and execution will proceed.
      /// </summary>
      public void Set()
      {
         mTokenSource.Cancel();
         IsSet = true;
      }

      /// <summary>
      /// Asynchronously await until the "event" signal is set.
      /// </summary>
      /// <returns>Awaitable task that may be used to block tasks</returns>
      /// <param name="aExternalToken">Optional cancellation token that may be used
      /// to end this wait but does not set the "event"</param>
      public Task WaitAsync(CancellationToken? aExternalToken = null)
      {
         // Grab a token from the existing token source. This token source instance will be
         // forgotten when reset.
         CancellationToken internalToken = mTokenSource.Token;

         return Task.Run(async () =>
         {
            // The general method here is a bit ugly but .NET does not yet provide any better
            // alternatives. Continuously and frequently poll the cancellation token to determine
            // if the "event" has been signaled. If signaled, this task will no longer await.
            while ((IsSet == false) &&
                   (internalToken.IsCancellationRequested == false) &&
                   (aExternalToken?.IsCancellationRequested == false))
            {
               await Task.Delay(POLLING_PERIOD_IN_MILLISECONDS);
            }
         });
      }
   }
}
