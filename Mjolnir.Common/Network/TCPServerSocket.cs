﻿using NLog;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace Mjolnir.Common.Network
{
   public class TcpServerSocket
   {
      /// <summary>
      /// Effectively the greatest amount of data that can be received in one read.
      /// Largely unimportant as most messages are much smaller than this buffer and
      /// concatenating 2+ reads is fine.
      /// </summary>
      private const int BUFFER_LENGTH = 4096;

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly Socket mSocket;
      private readonly ConcurrentDictionary<Socket, IPEndPoint> mClients = new ConcurrentDictionary<Socket, IPEndPoint>();
      private readonly ConcurrentDictionary<Socket, bool> mIsSendingTo = new ConcurrentDictionary<Socket, bool>();
      private readonly ConcurrentDictionary<Socket, Queue<byte[]>> mSendQueues =
                   new ConcurrentDictionary<Socket, Queue<byte[]>>();
      private bool mIsListening = false;

      public string Interface { get; }

      public int Port { get; }

      public int? BoundPort { get; private set; } = null;

      public string Url { get; private set; }


      /// <summary>
      /// Return the current number of connected clients.
      /// </summary>
      public int ClientCount => mClients.Count;

      /// <summary>
      /// Invoked when a new connection is accepted. Passes the end
      /// point (address and port as an IPEndPoint).
      /// </summary>
      public event EventHandler<IPEndPoint> ConnectEvent;

      /// <summary>
      /// Invoked when a client disconnects. Passes the end point
      /// (address and port as an IPEndPoint).
      /// </summary>
      public event EventHandler<IPEndPoint> DisconnectEvent;

      /// <summary>
      /// Invoked when a complete message is received. The argument passed
      /// to the handler is an object holding the data received and endpoint metadata.
      /// </summary>
      public event EventHandler<Reception> ReceiveEvent;

      /// <summary>
      /// Invoked when the socket begins listening and, therefore, this socket is
      /// actively operating as a server.
      /// </summary>
      public event EventHandler ListenEvent;

      /// <summary>
      /// Invoked when the socket closes.
      /// </summary>
      public event EventHandler CloseEvent;

      public TcpServerSocket(string aInterface, int aPort)
      {
         Interface = aInterface;
         Port = aPort;
         Url = $"{Interface}:{Port}";
         mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
      }

      public bool Listen(out int aBoundPort, int aMaxBacklog = 4)
      {
         // Default output value in case of an early return.
         aBoundPort = 0;

         if (mIsListening == true)
         {
            return true;
         }

         // If the IP/address cannot be parsed as a correctly-formatted IP address string.
         if (IPAddress.TryParse(Interface, out IPAddress ipAddress) == false)
         {
            mLog.Warn($"Cannot listen on network interface \"{Interface}\" because it is not " +
                      $"formatted correctly)");
            return false;
         }

         try
         {
            // Bind the network interface and port passed into the constructor so that incoming
            // connection requests from that/those interace(s) on that port will be routed to
            // this socket. Note that port zero is treated as "just give me one that's available."
            mSocket.Bind(new IPEndPoint(ipAddress, Port));

            // Begin listening, meaning that the socket is willing to accept new connections, with
            // the given number of backlogged connections when congested.
            mSocket.Listen(aMaxBacklog);

            // If no exception was thrown by this point, everything was successful.
            mIsListening = true;
            BoundPort = ((IPEndPoint)mSocket.LocalEndPoint).Port;
            Url = $"{Interface}:{BoundPort}";
            aBoundPort = BoundPort.Value;
            mLog.Debug($"Bound port {BoundPort} of interface {Interface}");
            OnListenEvent();

            // With no exceptions thrown, this method will continue outside this try block.
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Failed to bind port {Port} of interface {Interface}");
            return false;
         }

         // We're clear to accept connections. To that end, launch a task to wait for them.
         Task.Run(async () =>
         {
            while (mIsListening == true)
            {
               try
               {
                  // Wait for incoming socket connections and accept them.
                  Socket clientSocket = await mSocket.AcceptAsync();

                  // Remember the socket and its endpoint.
                  IPEndPoint endPoint = (IPEndPoint)clientSocket.RemoteEndPoint;
                  mClients[clientSocket] = endPoint;

                  // Give the socket entries in a couple of send-related maps.
                  mIsSendingTo[clientSocket] = false;
                  mSendQueues[clientSocket] = new Queue<byte[]>();

                  mLog.Debug($"Accepted a connection from {endPoint}");
                  OnConnectEvent(endPoint);

                  // With the client connected, begin receiving data from it. This will result in a
                  // task dedicated to this connection that handles the receptions.
                  Receive(clientSocket);
               }
               catch (Exception e)
               {
                  SocketUtils.LogException(mLog, e, Url);

                  // Exceptions thrown while accepting connections almost certain indicates an
                  // unrecoverable error.
                  Close();
               }
            }
         });

         return true;
      }

      /// <summary>
      /// Close the socket. This begins by shutting down all known client sockets.
      /// </summary>
      public void Close()
      {
         if (mIsListening == false)
         {
            return;
         }

         try
         {
            foreach (Socket clientSocket in mClients.Keys)
            {
               Remove(clientSocket);
            }

            mSocket.Close();

            mLog.Debug($"Server socket bound to {Url} closed smoothly");
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while closing the server of bound to {Url}");
         }

         mIsListening = false;
         BoundPort = null;
         Url = $"{Interface}:{Port}";
      }

      public void SendTo(byte[] aData, IPEndPoint aEndPoint)
      {
         // The 'any' address is invalid so it's ignored.
         // (It's also used as the endpoint for bots which have no remote endpoint.)
         if ((aEndPoint.Address.Equals(IPAddress.Any) == true) ||
             (aEndPoint.Address.Equals(IPAddress.IPv6Any) == true))
         {
            return;
         }

         // Search the socket-to-endpoint map for an endpoint matching the one given
         KeyValuePair<Socket, IPEndPoint> socketEndPointPair = mClients.FirstOrDefault(p => p.Value.Equals(aEndPoint));

         // If the pair returned is not a default instance, AKA if the endpoint was found.
         if (socketEndPointPair.Equals(default(KeyValuePair<Socket, IPEndPoint>)) == false)
         {
            SendTo(aData, socketEndPointPair.Key);
         }
      }

      /// <summary>
      /// If connected, writes the given data to all known clients.
      /// </summary>
      /// <param name="aData">Data to write in its entirety.</param>
      /// <param name="aEndPointToExclude">A single, optional IPEndPoint to exclude.</param>
      public void SendAll(byte[] aData, IPEndPoint aEndPointToExclude = null)
      {
         if (aData?.Length == 0)
         {
            // This may indicate some problems upstream so it's good to log it.
            mLog.Warn($"Attempted to send null or empty data");

            return;
         }

         // Prefix the length of the data being sent to the data. The equivalent
         // server socket will read this length.
         byte[] prefixedData = new byte[aData.Length + sizeof(int)];
         Buffer.BlockCopy(BitConverter.GetBytes(aData.Length), 0, prefixedData, 0, sizeof(int));
         Buffer.BlockCopy(aData, 0, prefixedData, sizeof(int), aData.Length);

         // Send the same data to all sockets (other than the potential end point to exclude).
         foreach (KeyValuePair<Socket, IPEndPoint> socketEndPointPair in mClients)
         {
            // If this socket is not to be excluded.
            if (socketEndPointPair.Value.Equals(aEndPointToExclude) == false)
            {
               SendTo(aData, socketEndPointPair.Key);
            }
         }
      }

      /// <summary>
      /// Shutdown and close the connection of a client at the given endpoint if a client at
      /// this endpoint is currently connected.
      /// </summary>
      /// <param name="aEndPoint"></param>
      public void Remove(IPEndPoint aEndPoint)
      {
         if (aEndPoint == null)
         {
            return;
         }

         // Search the socket-to-endpoint map for an endpoint matching the one given
         KeyValuePair<Socket, IPEndPoint> socketEndPointPair = mClients.FirstOrDefault(p => p.Value.Equals(aEndPoint));

         // If the pair returned is not a default instance, AKA if the endpoint was found.
         if (socketEndPointPair.Equals(default(KeyValuePair<Socket, IPEndPoint>)) == false)
         {
            Remove(socketEndPointPair.Key);
         }
      }


      #region Private helper methods

      private void Receive(Socket aClientSocket)
      {
         Task.Run(async () =>
         {
            // Create a buffer that will hold accumulated data and that will be resized as needed.
            byte[] rollingBuffer = new byte[0];

            while (mIsListening == true)
            {
               try
               {
                  // Create a temporary buffer to hold receptions. It will be used for one call to ReceiveAsync()
                  // and may contain the sent data in full or an incomplete portion.
                  byte[] buffer = new byte[BUFFER_LENGTH];
                  int lengthReceivedInBytes = await aClientSocket.ReceiveAsync(new ArraySegment<byte>(buffer),
                                                                               SocketFlags.None);

                  if (lengthReceivedInBytes > 0)
                  {
                     // If some non-zero amount of data was recevied, in full or not.

                     // Append the newly-received data to the rolling buffer.
                     int destinationIndex = rollingBuffer.Length;
                     Array.Resize(ref rollingBuffer, rollingBuffer.Length + lengthReceivedInBytes);
                     Array.Copy(buffer, 0, rollingBuffer, destinationIndex, lengthReceivedInBytes);

                     // While enough data has been received to allow us to remove a full message from
                     // the rolling buffer.
                     // Note: This method also extracts the message and invokes the reception event.
                     while (SocketUtils.TryGetMessage(rollingBuffer, out byte[] messageAsBytes) == true)
                     {
                        OnReceiveEvent(new Reception(mClients[aClientSocket], messageAsBytes));

                        // We want to remove the message but, remember, every message is framed with a prefixed
                        // number indicating the length of the message. We want to remove that number too.
                        int lengthInBytesToTruncate = messageAsBytes.Length + SocketUtils.LengthPrefixLengthInBytes;

                        if (SocketUtils.TryGetSegment(rollingBuffer,
                                                      lengthInBytesToTruncate,
                                                      rollingBuffer.Length - lengthInBytesToTruncate,
                                                      out byte[] truncatedBuffer) == true)
                        {
                           rollingBuffer = truncatedBuffer;
                        }
                        else
                        {
                           mLog.Warn($"Could not truncate the rolling buffer ({rollingBuffer.Length}) " +
                                     $"by {lengthInBytesToTruncate} bytes");
                           rollingBuffer = new byte[0];
                        }
                     }
                  }
                  else
                  {
                     // If zero bytes were received, it indicates the remote socket shutdown.

                     Remove(aClientSocket);
                  }
               }
               catch (Exception e)
               {
                  SocketUtils.LogException(mLog, e, Url);
                  Remove(aClientSocket);
                  break;
               }
            }
         });
      }

      private void SendTo(byte[] aData, Socket aClientSocket)
      {
         if ((aData?.Length == 0) || (aClientSocket?.Connected != true))
         {
            // If the input was null or an empty array.
            if (aData?.Length == 0)
            {
               // This may indicate some problems upstream so it's good to log it.
               mLog.Warn($"Attempted to send null or empty data");
            }

            return;
         }

         // Prefix the length of the data being sent to the data. The equivalent server socket will
         // read this length.
         byte[] framedData = new byte[aData.Length + SocketUtils.LengthPrefixLengthInBytes];
         Array.Copy(BitConverter.GetBytes(aData.Length),
                    0,
                    framedData,
                    0,
                    SocketUtils.LengthPrefixLengthInBytes);
         Array.Copy(aData, 0, framedData, SocketUtils.LengthPrefixLengthInBytes, aData.Length);

         // Push the data to the thread-safe send queue. 
         mSendQueues[aClientSocket].Enqueue(framedData);

         // If the socket is not currently sending anything.
         if ((mIsSendingTo.TryGetValue(aClientSocket, out bool isSending) == true) &&
             (isSending == false))
         {
            // Data is sent from a task which has a couple benefits:
            // it allows the calling thread to continue immediately, regardless of how large the data
            // may be, and it prevents the error case in which Socket.SendAsync() is called while it
            // is in the process of sending previous data.

            // Set the "socket is being used to send data" flag.
            mIsSendingTo[aClientSocket] = true;

            Task.Run(async () =>
            {
               // While there's queued data to be sent to this client.
               while ((mSendQueues.TryGetValue(aClientSocket, out Queue<byte[]> queue) == true) &&
                      (queue.TryDequeue(out byte[] data) == true))
               {
                  try
                  {
                     // Send the data to the remote socket.
                     await aClientSocket.SendAsync(new ArraySegment<byte>(data), SocketFlags.None);
                  }
                  catch (Exception e)
                  {
                     SocketUtils.LogException(mLog, e, Url);
                     Remove(aClientSocket);
                  }
               }

               mIsSendingTo[aClientSocket] = false;
            });
         }
      }

      /// <summary>
      /// Helper method to remove a client from the list of known clients. This
      /// will shutdown the socket, if needed, and invoke the Disconnected event.
      /// </summary>
      /// <param name="aClientSocket">The client socket to remove</param>
      private void Remove(Socket aClientSocket)
      {
         if (aClientSocket?.Connected == true)
         {
            aClientSocket.Shutdown(SocketShutdown.Both);
            aClientSocket.Close();
         }

         if (mClients.TryRemove(aClientSocket, out IPEndPoint endPoint) == true)
         {
            try
            {
               DisconnectEvent?.Invoke(this, endPoint);
            }
            catch (Exception e)
            {
               mLog.Error(e, "Exception thrown while propagating a disconnect event");
            }

            mIsSendingTo.TryRemove(aClientSocket, out _);
            mSendQueues.TryRemove(aClientSocket, out _);
         }
      }


      #region Event invokation helpers

      private void OnConnectEvent(IPEndPoint aEndPoint)
      {
         try
         {
            ConnectEvent?.Invoke(this, aEndPoint);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(ConnectEvent)} event");
         }
      }

      private void OnDisconnectEvent(IPEndPoint aEndPoint)
      {
         try
         {
            DisconnectEvent?.DynamicInvoke(this, aEndPoint);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(DisconnectEvent)} event");
         }
      }

      private void OnReceiveEvent(Reception aReception)
      {
         try
         {
            ReceiveEvent?.Invoke(aReception, aReception);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(ReceiveEvent)} event");
         }
      }

      private void OnListenEvent()
      {
         try
         {
            ListenEvent?.Invoke(this, EventArgs.Empty);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(ListenEvent)} event");
         }
      }

      private void OnCloseEvent()
      {
         try
         {
            CloseEvent?.Invoke(this, EventArgs.Empty);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(CloseEvent)} event");
         }
      }

      #endregion Event invokation helpers


      #endregion Private helper methods
   }
}
