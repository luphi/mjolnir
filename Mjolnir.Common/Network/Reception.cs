﻿using System.Net;

namespace Mjolnir.Common.Network
{
   public class Reception
   {
      public IPEndPoint EndPoint { get; }

      public byte[] Data { get; }

      public Reception(IPEndPoint aEndPoint, byte[] aData)
      {
         EndPoint = aEndPoint;
         Data = aData;
      }
   }
}
