﻿using NLog;
using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Mjolnir.Common.Network
{
   public class TcpClientSocket
   {
      /// <summary>
      /// Effectively the greatest amount of data that can be received in one read.
      /// Largely unimportant as most messages are much smaller than this buffer and
      /// concatenating 2+ reads is fine.
      /// </summary>
      private const int BUFFER_LENGTH = 4096;

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly ConcurrentQueue<byte[]> mSendQueue = new ConcurrentQueue<byte[]>();
      private Socket mSocket;
      private bool mIsOpen = false;
      private bool mIsSending = false;

      public string Address { get; }

      public int Port { get; }

      private string Url { get; }


      #region Events

      /// <summary>
      /// Invoked when a connection is made to the remote socket pointed to by
      /// the address and port given to the constructor.
      /// </summary>
      public event EventHandler ConnectEvent;

      /// <summary>
      /// Invoked when a connection attempt is refused.
      /// </summary>
      public event EventHandler ConnectRefusalEvent;

      /// <summary>
      /// Invoked when a previously-connected socket disconnects, including
      /// when the Disconnect() method is called.
      /// </summary>
      public event EventHandler DisconnectEvent;

      /// <summary>
      /// Invoked when a complete message is received. The argument passed
      /// to the handler is the data received as a byte array.
      /// </summary>
      public event EventHandler<byte[]> ReceiveEvent;

      #endregion Events


      /// <summary>
      /// True if the socket is currently connected to the given remote endpoint, false otherwise.
      /// </summary>
      public bool IsConnected => mSocket.Connected == true;

      public TcpClientSocket(string aAddress, int aPort)
      {
         Address = aAddress;
         Port = aPort;
         mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
         Url = $"{aAddress}:{aPort}";
      }

      /// <summary>
      /// Attempt a connection to the remote endpoint indicated by the address and port
      /// given to the constructor. If a connection cannot be established or is lost, it
      /// will be reattempted until Disconnect() is called.
      /// </summary>
      public void Connect()
      {
         Connect(aIsInitialAttempt: true);
      }

      /// <summary>
      /// Disconnect from, or cease attempting to connect to, the remote endpoint.
      /// </summary>
      public void Disconnect()
      {
         // If the socket is not trying to connect.
         if (mIsOpen == false)
         {
            // There's nothing to disconnect.
            return;
         }

         mIsOpen = false;

         try
         {
            bool wasConnected = false;

            // If the socket is connected at this time.
            if (IsConnected == true)
            {
               // Remember that it was connected so we can notify observers of the disconnection.
               wasConnected = true;

               // Finish any sending and/or receiving and disconnect. This should only be done if
               // the connection had been established.
               mSocket.Shutdown(SocketShutdown.Both);
            }

            // Close (and consequently Dispose()) the socket, then create a new one.
            // This is done in order to guarantee the socket's resources are freed and that this
            // new socket can be (re)used to connect to the remote socket another time.
            mSocket.Close();
            mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // If the socket was connected before shutting down.
            if (wasConnected == true)
            {
               // Notify observers that it is, or is being, disconnected.
               OnDisconnectEvent();
            }
         }
         catch (Exception e)
         {
            SocketUtils.LogException(mLog, e, Url);
         }
      }

      /// <summary>
      /// If connected, writes the given data to the remote socket.
      /// </summary>
      /// <param name="aData">Data to write in its entirety.</param>
      public void Send(byte[] aData)
      {
         if ((aData?.Length == 0) || (IsConnected == false))
         {
            // If the input was null or an empty array.
            if (aData?.Length == 0)
            {
               // This may indicate some problems upstream so it's good to log it.
               mLog.Warn($"Attempted to send null or empty data");
            }

            return;
         }

         // Prefix the length of the data being sent to the data. The equivalent server socket will
         // read this length.
         byte[] framedData = new byte[aData.Length + SocketUtils.LengthPrefixLengthInBytes];
         Array.Copy(BitConverter.GetBytes(aData.Length),
                    0,
                    framedData,
                    0,
                    SocketUtils.LengthPrefixLengthInBytes);
         Array.Copy(aData, 0, framedData, SocketUtils.LengthPrefixLengthInBytes, aData.Length);

         // Push the data to the thread-safe send queue. 
         mSendQueue.Enqueue(framedData);

         // If the socket is not currently sending anything.
         if (mIsSending == false)
         {
            // Data is sent from a task which has a couple benefits:
            // it allows the calling thread to continue immediately, regardless of how large the data
            // may be, and it prevents the error case in which Socket.SendAsync() is called while it
            // is in the process of sending previous data.

            // Set the "socket is being used to send data" flag.
            mIsSending = true;

            Task.Run(async () =>
            {
               // While there's queued data to be sent.
               while (mSendQueue.TryDequeue(out byte[] data) == true)
               {
                  try
                  {
                     // Send the data to the remote socket.
                     await mSocket.SendAsync(new ArraySegment<byte>(data), SocketFlags.None);
                  }
                  catch (Exception e)
                  {
                     SocketUtils.LogException(mLog, e, Url);
                     TryReconnect(aWasDisconnected: true);
                  }
               }

               mIsSending = false;
            });
         }
      }


      #region Private helper methods


      private void Connect(bool aIsInitialAttempt)
      {
         // If this is an initial attempt to connect the socket from outside of this class
         // (i.e. not a reconnect attempt) and the socket hasn't already been opened.
         if ((aIsInitialAttempt == true) && (mIsOpen == true))
         {
            // Attempting an initial connection multiple times may indicate some problems upstream
            // so it's good to log it.
            mLog.Warn($"Attempted to connect to {Url} redundantly");
            return;
         }

         // If the IP property cannot be parsed as a correctly-formatted IP address string, OR it
         // is an address string but it's either of the 'any' addresses.
         if ((IPAddress.TryParse(Address, out IPAddress ipAddress) == false) ||
             (ipAddress.Equals(IPAddress.Any) == true) ||
             (ipAddress.Equals(IPAddress.IPv6Any) == true))
         {
            mLog.Warn($"Cannot connect to IP address \"{Address}\" because it is either " +
                      $"formatted incorrect or is an 'any' address " +
                      $"(\"{IPAddress.Any}\" or \"{IPAddress.IPv6Any}\")");
            return;
         }

         // Set the "socket is connected or trying to connect" flag.
         mIsOpen = true;

         Task.Run(async () =>
         {
            try
            {
               await mSocket.ConnectAsync(ipAddress, Port);

               // If this line is reached, a connection was made without throwing an exception.
               // Notify observers of the connection.
               OnConnectEvent();

               Receive();
            }
            catch (Exception e)
            {
               SocketUtils.LogException(mLog, e, Url);

               // If this exception is the result of SocketException error code 10061.
               // (Error code 10061 is the same as Windows Sockets error code WSAECONNREFUSED, or
               // "connection refused" meaning the connection could not be made.)
               if ((e as SocketException)?.ErrorCode == 10061)
               {
                  OnConnectRefusalEvent();
               }

               TryReconnect(aWasDisconnected: false);
            }
         });
      }

      /// <summary>
      /// Continue attempts to re-establish the connection if the socket should
      /// connect.
      /// </summary>
      /// <param name="aWasDisconnected">When true, indicates that the socket was just
      /// disconnected and, therefore, a Disconnect event should be invoked.</param>
      private void TryReconnect(bool aWasDisconnected)
      {
         // If the socket has been closed.
         if (mIsOpen == false)
         {
            // Do no try to reconnect.
            return;
         }

         // If the socket just lost its connection
         if (aWasDisconnected == true)
         {
            // Notify observers of the disconnection.
            OnDisconnectEvent();

            // Due to a shortcoming of the underlying provider, connecting has some quirks.
            // To work around this, dispose of the current Socket object and create a new one.
            mSocket.Dispose();
            mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
         }

         // Attempt to connect again.
         Connect(aIsInitialAttempt: false);
      }

      /// <summary>
      /// Perform receptions in the background, invoking the reception event as data arrives.
      /// </summary>
      private void Receive()
      {
         Task.Run(async () =>
         {
            // Create a buffer that will hold accumulated data and that will be resized as needed.
            byte[] rollingBuffer = new byte[0];

            // While the socket has not been voluntarily disconnected.
            while (mIsOpen == true)
            {
               try
               {
                  // Create a temporary buffer to hold receptions. It will be used for one call to ReceiveAsync()
                  // and may contain the sent data in full or an incomplete portion.
                  byte[] buffer = new byte[BUFFER_LENGTH];
                  int lengthReceivedInBytes = await mSocket.ReceiveAsync(new ArraySegment<byte>(buffer),
                                                                         SocketFlags.None);

                  if (lengthReceivedInBytes > 0)
                  {
                     // If some positive number of bytes were received, in full or not.

                     // Append the newly-received data to the rolling buffer.
                     int destinationIndex = rollingBuffer.Length;
                     Array.Resize(ref rollingBuffer, rollingBuffer.Length + lengthReceivedInBytes);
                     Array.Copy(buffer, 0, rollingBuffer, destinationIndex, lengthReceivedInBytes);

                     // While enough data has been received to allow us to remove a full message from
                     // the rolling buffer.
                     // Note: This method also extracts the message and invokes the reception event.
                     while (SocketUtils.TryGetMessage(rollingBuffer, out byte[] messageAsBytes) == true)
                     {
                        OnReceiveEvent(messageAsBytes);

                        // We want to remove the message but, remember, every message is framed with a prefixed
                        // number indicating the length of the message. We want to remove that number too.
                        int lengthInBytesToTruncate = messageAsBytes.Length + SocketUtils.LengthPrefixLengthInBytes;

                        if (SocketUtils.TryGetSegment(rollingBuffer,
                                                      lengthInBytesToTruncate,
                                                      rollingBuffer.Length - lengthInBytesToTruncate,
                                                      out byte[] truncatedBuffer) == true)
                        {
                           rollingBuffer = truncatedBuffer;
                        }
                        else
                        {
                           mLog.Warn($"Could not truncate the rolling buffer ({rollingBuffer.Length}) " +
                                     $"by {lengthInBytesToTruncate} bytes");
                           rollingBuffer = new byte[0];
                        }
                     }
                  }
                  else
                  {
                     // If zero bytes were received, it indicates the remote socket shutdown.

                     // Exit this loop, leading to repeated reconnection attempts.
                     break;
                  }
               }
               catch (Exception e)
               {
                  SocketUtils.LogException(mLog, e, Url);
                  break;
               }
            }

            TryReconnect(aWasDisconnected: true);
         });
      }


      #region Event invokation helpers

      private void OnConnectEvent()
      {
         try
         {
            ConnectEvent?.Invoke(this, EventArgs.Empty);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(ConnectEvent)} event");
         }
      }

      private void OnConnectRefusalEvent()
      {
         try
         {
            ConnectRefusalEvent?.Invoke(this, EventArgs.Empty);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(ConnectRefusalEvent)} event");
         }
      }

      private void OnDisconnectEvent()
      {
         try
         {
            DisconnectEvent?.Invoke(this, EventArgs.Empty);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(DisconnectEvent)} event");
         }
      }

      private void OnReceiveEvent(byte[] aData)
      {
         try
         {
            ReceiveEvent?.Invoke(this, aData);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(ReceiveEvent)} event");
         }
      }

      #endregion Event invokation helpers


      #endregion Private helper methods
   }
}
