﻿using Mjolnir.Common.Messages;
using NLog;
using System;
using System.Collections.Concurrent;

namespace Mjolnir.Common.Network
{
   public class GameClient
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly ConcurrentQueue<Message> mQueue = new ConcurrentQueue<Message>();
      private TcpClientSocket mSocket = null;

      public bool HasMessage => mQueue.IsEmpty == false;
      public Message Head => mQueue.TryDequeue(out Message message) == true ? message : new Message(Message.Opcode.NONE);

      public event EventHandler ConnectEvent;

      public event EventHandler ConnectRefusalEvent;

      public event EventHandler DisconnectEvent;

      public event EventHandler MessageEvent;

      public void Connect(string aHostname, int aPort)
      {
         // Disconnect from the current server before trying to connect to another.
         // This will do nothing if we're not connected to another server, which is
         // most likely the case.
         Disconnect();

         mSocket = new TcpClientSocket(aHostname, aPort);
         mSocket.ConnectEvent += OnConnect;
         mSocket.ConnectRefusalEvent += OnConnectRefusal;
         mSocket.DisconnectEvent += OnDisconnect;
         mSocket.ReceiveEvent += OnReceive;
         mSocket.Connect();
      }

      public void Disconnect()
      {
         if (mSocket != null)
         {
            mSocket.ConnectEvent -= OnConnect;
            mSocket.ConnectRefusalEvent -= OnConnectRefusal;
            mSocket.DisconnectEvent -= OnDisconnect;
            mSocket.ReceiveEvent -= OnReceive;
            mSocket.Disconnect();
            mSocket = null;
         }

         // Clear the queue.
         while (mQueue.IsEmpty == false)
         {
            mQueue.TryDequeue(out Message _);
         }
      }

      public void Send(Message aMessage)
      {
         if (aMessage != null)
         {
            mSocket?.Send(Message.Serialize(aMessage));
         }
      }

      private void OnConnect(object aSender, EventArgs aArgs)
      {
         try
         {
            ConnectEvent?.Invoke(this, EventArgs.Empty);
         }
         catch (Exception e)
         {
            mLog.Error(e, "Exception thrown while propagating a connect event");
         }
      }

      private void OnConnectRefusal(object aSender, EventArgs aArgs)
      {
         try
         {
            ConnectRefusalEvent?.Invoke(this, EventArgs.Empty);
         }
         catch (Exception e)
         {
            mLog.Error(e, "Exception thrown while propagating a connect failed event");
         }
      }

      private void OnDisconnect(object aSender, EventArgs aArgs)
      {
         try
         {
            DisconnectEvent?.Invoke(this, EventArgs.Empty);
         }
         catch (Exception e)
         {
            mLog.Error(e, "Exception thrown while propagating a disconnect event");
         }
      }

      private void OnReceive(object aSender, byte[] aData)
      {
         Message message = Message.Deserialize(aData);

         if (message != null)
         {
            mQueue.Enqueue(message);

            try
            {
               MessageEvent?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception e)
            {
               mLog.Error(e, "Exception thrown while propagating a message event");
            }
         }
         else
         {
            mLog.Warn($"Unable to receive {aData.Length} bytes as a message");
         }
      }
   }
}
