﻿using NLog;
using System;
using System.Net.Sockets;
using System.Runtime.CompilerServices;

namespace Mjolnir.Common.Network
{
   /// <summary>
   /// Contains static methods used by both the server and client sockets.
   /// </summary>
   internal static class SocketUtils
   {
      internal const int LengthPrefixLengthInBytes = sizeof(int);

      internal static void LogException(ILogger aLog,
                                        Exception aException,
                                        string aUrl,
                                        [CallerMemberName] string aMethodName = null)
      {
         SocketException socketException = aException as SocketException; // May be null.

         // If the exception is a complete non-problem.
         if ((socketException?.ErrorCode == 995) ||
             (socketException?.ErrorCode == 10038) ||
             (socketException?.ErrorCode == 10053) ||
             (socketException?.ErrorCode == 10054) ||
             (socketException?.ErrorCode == 10060) ||
             (socketException?.ErrorCode == 10061) ||
             (aException is ObjectDisposedException))
         {
            // These ones are unimportant and frequent enough that we don't want to log anything.
            return;
         }

         if (socketException != null)
         {
            // If one of the remaining socket exceptions was raised.

            // These are likely more serious and should be logged as errors.
            aLog.Error(aException, $"Socket exception (error code {socketException.ErrorCode}) " +
                                   $"in {aMethodName} with endpoint {aUrl}");
         }
         else
         {
            // If any other exception was raised.

            // It's definitely serious, so log it as an error.
            aLog.Error(aException, $"Exception caught in {aMethodName} with endpoint {aUrl}");
         }

         // Notes:
         // Error code 995 is "overlapped operation aborted" essentially meaning the socket was
         // doing something on another socket when it was closed.
         // Error code 10038 is "socket operation on nonsocket" meaning that the object that was
         // once a valid socket is no longer valid following the closing process.
         // Error code 10053 is "software caused connection abort" and happens when the local
         // socket aborts/removes a connection (by shutting down).
         // Error code 10054 is "connection reset by peer" and happens when the other end of the
         // connection closes voluntarily.
         // Error code 10060 is "connection timed out" and simply means the remote computer
         // hasn't responded. This can mean it's a server we're looking for but haven't heard
         // from, or a socket that died before it could politely alert us.
         // Error code 10061 is "connection refused" which just means there was no socket at the
         // server's address able to accept the connection.
      }

      internal static bool TryGetMessage(byte[] aBuffer, out byte[] aMessageAsBytes)
      {
         // If the length prefix can be extracted from the buffer AND the buffer has enough data
         // to hold that message AND the byte segment of the message was successfully extracted.
         if ((TryGetLength(aBuffer, out int messageLengthInBytes) == true) &&
             (aBuffer?.Length >= messageLengthInBytes + LengthPrefixLengthInBytes))
         {
            return TryGetSegment(aBuffer, LengthPrefixLengthInBytes, messageLengthInBytes, out aMessageAsBytes);
         }

         aMessageAsBytes = new byte[0];
         return false;
      }

      internal static bool TryGetLength(byte[] aBuffer, out int aLengthInBytes)
      {
         // The length is prepended to every message, at index 0, and is an 'int' type.
         // If that small, prepended array of bytes can be extracted.
         if (TryGetSegment(aBuffer, 0, LengthPrefixLengthInBytes, out byte[] lengthAsBytes) == true)
         {
            // Take the byte array and turn it back into the 'int' it was originally.
            aLengthInBytes = BitConverter.ToInt32(lengthAsBytes, 0);
            return true;
         }

         aLengthInBytes = 0;
         return false;
      }

      internal static bool TryGetSegment(byte[] aBuffer, int aIndex, int aLength, out byte[] aSegment)
      {
         if (aLength > aBuffer?.Length - aIndex)
         {
            aSegment = null;
            return false;
         }

         aSegment = new byte[aLength];
         Array.Copy(aBuffer, aIndex, aSegment, 0, aLength);
         return true;
      }
   }
}
