﻿using Mjolnir.Common.Messages;
using NLog;
using System;
using System.Collections.Generic;

namespace Mjolnir.Common.Network
{
   public class RegistryClient
   {
      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly TcpClientSocket mSocket;
      private ServerRegistrationMessage mMostRecent = null;
      private bool mWasConnectionLost = false;

      public event EventHandler<List<ServerRegistrationMessage>> RetrievalEvent;

      public RegistryClient(string aHostname, int aPort)
      {
         mSocket = new TcpClientSocket(aHostname, aPort);
         mSocket.ConnectEvent += OnConnect;
         mSocket.DisconnectEvent += OnDisconnect;
         mSocket.ReceiveEvent += OnReceive;

         mSocket.Connect();
      }

      public void Register(string aName,
                           int aPort,
                           StateEnum aState,
                           int aPlayerCount,
                           bool aIsPrivate,
                           bool aIsVoiceEnabled)
      {
         ServerRegistrationMessage message = new ServerRegistrationMessage
         {
            Port = aPort,
            Name = aName,
            State = aState,
            PlayerCount = aPlayerCount,
            IsPrivate = aIsPrivate,
            IsVoiceEnabled = aIsVoiceEnabled
         };

         if (message.Equals(mMostRecent) == false)
         {
            mMostRecent = message;
            mSocket.Send(Message.Serialize(message));
         }
      }

      public void Unregister()
      {
         mSocket.Send(Message.Serialize(new Message(Message.Opcode.SERVER_UNREGISTER)));
      }

      public void Retrieve()
      {
         mSocket.Send(Message.Serialize(new Message(Message.Opcode.SERVER_LIST_REQUEST)));
      }

      private void OnConnect(object aSender, EventArgs aArgs)
      {
         if (mWasConnectionLost == true)
         {
            mLog.Info($"Reconnected to the registry at {mSocket.Address}:{mSocket.Port}");
         }

         if (mMostRecent != null)
         {
            mSocket.Send(Message.Serialize(mMostRecent));
         }
      }

      private void OnDisconnect(object aSender, EventArgs aArgs)
      {
         mLog.Warn($"Disconnected from the registry at {mSocket.Address}:{mSocket.Port}");

         mWasConnectionLost = true;
      }

      private void OnReceive(object aSender, byte[] aData)
      {
         Message message = Message.Deserialize(aData);

         if (message?.Op == Message.Opcode.SERVER_LIST)
         {
            ServerListMessage serverListMessage = message as ServerListMessage;
            try
            {
               RetrievalEvent?.Invoke(this, serverListMessage.Servers);
            }
            catch (Exception e)
            {
               mLog.Error(e, $"Exception thrown while propagating a retrievel event");
            }
         }
         else
         {
            mLog.Warn($"Received data that could not be deserialized to a {nameof(ServerListMessage)}");
         }
      }
   }
}
