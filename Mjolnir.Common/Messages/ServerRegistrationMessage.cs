﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class ServerRegistrationMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public string Address { get; set; }

      [DataMember]
      public int Port { get; set; }

      [DataMember]
      public string Name { get; set; }

      [DataMember]
      public StateEnum State { get; set; }

      [DataMember]
      public int PlayerCount { get; set; }

      [DataMember]
      public bool IsPrivate { get; set; }

      [DataMember]
      public bool IsVoiceEnabled { get; set; }

      #endregion Serialized properties


      public ServerRegistrationMessage()
      {
         Op = Opcode.SERVER_REGISTER;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, " +
                $"{nameof(Address)} = {(Address == null ? "null" : $"\"{Address}\"")}, " +
                $"{nameof(State)} = \"{State}\", {nameof(Port)} = {Port}, " +
                $"{nameof(Name)} = {(Name == null ? "null" : $"\"{Name}\"")}, " +
                $"{nameof(PlayerCount)} = {PlayerCount}, {nameof(IsPrivate)} = {IsPrivate}, " +
                $"{nameof(IsVoiceEnabled)} = {IsVoiceEnabled} }}";
      }

      public override bool Equals(object aObject)
      {
         return (aObject is ServerRegistrationMessage other) && (other.Address == Address) &&
                (other.Port == Port) && (other.Name == Name) && (other.State == State) &&
                (other.PlayerCount == PlayerCount) && (other.IsPrivate == IsPrivate) &&
                (other.IsVoiceEnabled == IsVoiceEnabled);
      }

      public override int GetHashCode()
      {
         unchecked // Allows integer overflow.
         {
            int hash = 163;
            hash *= 569 ^ (Address != null ? Address.GetHashCode() : 1);
            hash *= 223 ^ Port.GetHashCode();
            hash *= 11 ^ (Name != null ? Name.GetHashCode() : 1);
            hash *= 739 ^ State.GetHashCode();
            hash *= 487 ^ PlayerCount.GetHashCode();
            hash *= IsPrivate ? 907 : 97;
            hash *= IsVoiceEnabled ? 761 : 587;
            return hash;
         }
      }
   }
}
