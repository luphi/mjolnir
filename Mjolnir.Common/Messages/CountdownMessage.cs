﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class CountdownMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public float CountdownAsPercent { get; set; }

      #endregion Serialized properties


      public CountdownMessage(float aCountdownAsPercent)
      {
         Op = Opcode.COUNTDOWN;
         CountdownAsPercent = aCountdownAsPercent;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(CountdownAsPercent)} = {CountdownAsPercent} }}";
      }
   }
}
