﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class WelcomeMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public uint PlayerID { get; set; }

      [DataMember]
      public bool IsAdmin { get; set; }

      [DataMember]
      public StateEnum StateCode { get; set; }

      #endregion Serialized properties


      public WelcomeMessage(uint aPlayerID, bool aIsAdmin, StateEnum aStateCode)
      {
         Op = Opcode.WELCOME;
         PlayerID = aPlayerID;
         IsAdmin = aIsAdmin;
         StateCode = aStateCode;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(PlayerID)} = {PlayerID}, {nameof(IsAdmin)} = {IsAdmin}, " +
                $"{nameof(StateCode)} = {StateCode} }}";
      }
   }
}
