﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class AhoyHoyMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public string Name { get; set; }
      
      [DataMember]
      public string JoinPassword { get; set; }
      
      [DataMember]
      public string AdminPassword { get; set; }

      #endregion Serialized properties


      public AhoyHoyMessage(string aName, string aJoinPassword, string aAdminPassword)
      {
         Op = Opcode.AHOY_HOY;
         Name = aName;
         JoinPassword = aJoinPassword;
         AdminPassword = aAdminPassword;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(Name)} = \"{(Name == null ? "null" : $"\"{Name}\"")}\", " +
                $"{nameof(JoinPassword)} = {(JoinPassword == null ? "null" : $"\"{JoinPassword}\"")}, " +
                $"{nameof(AdminPassword)} = {(AdminPassword == null ? "null" : $"\"{AdminPassword}\"")} }}";
      }
   }
}
