﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class LivesMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public uint TeamARemainingLives { get; set; } = 0;

      [DataMember]
      public uint TeamBRemainingLives { get; set; } = 0;

      #endregion Serialized properties


      public LivesMessage(uint aTeamARemainingLives, uint aTeamBRemainingLives)
      {
         Op = Opcode.LIVES;
         TeamARemainingLives = aTeamARemainingLives;
         TeamBRemainingLives = aTeamBRemainingLives;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(TeamARemainingLives)} = {TeamARemainingLives}, " +
                $"{nameof(TeamBRemainingLives)} = {TeamBRemainingLives} }}";
      }
   }
}
