﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class ChatMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public uint PlayerID { get; set; }

      [DataMember]
      public bool IsPrivate { get; set; }

      [DataMember]
      public string Message { get; set; }

      #endregion Serialized properties


      public ChatMessage(uint aPlayerID, bool aIsPrivate, string aMessage)
      {
         Op = Opcode.CHAT;
         PlayerID = aPlayerID;
         IsPrivate = aIsPrivate;
         Message = aMessage;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(PlayerID)} = {PlayerID}, " +
                $"{nameof(IsPrivate)} = {IsPrivate}, " +
                $"{nameof(Message)} = \"{(Message == null ? "null" : $"\"{Message}\"")}\" }}";
      }
   }
}
