﻿using Mjolnir.Common.Models;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class PlayersMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public List<Player> Players { get; set; } = new List<Player>();

      #endregion Serialized properties


      public PlayersMessage(List<Player> aPlayers)
      {
         Op = Opcode.PLAYERS;
         Players.AddRange(aPlayers);
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(Players)} = {string.Join(", ", Players)} }}";
      }
   }
}
