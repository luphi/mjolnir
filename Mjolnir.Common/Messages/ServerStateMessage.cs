﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class ServerStateMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public StateEnum State { get; set; }

      #endregion Serialized properties


      public ServerStateMessage(StateEnum aState)
      {
         Op = Opcode.SERVER_STATE;
         State = aState;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(State)} = {State} }}";
      }
   }
}
