﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class GameBeginMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public uint[] TurnList { get; set; }

      #endregion Serialized properties


      public GameBeginMessage(uint[] aTurnList)
      {
         Op = Opcode.GAME_BEGIN;
         TurnList = aTurnList;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(TurnList)} = {string.Join(", ", TurnList)} }}";
      }
   }
}
