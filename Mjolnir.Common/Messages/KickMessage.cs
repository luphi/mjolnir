﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class KickMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public KickReasonEnum Reason { get; set; }

      #endregion Serialized properties


      public KickMessage(KickReasonEnum aReason)
      {
         Op = Opcode.KICK;
         Reason = aReason;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(Reason)} = {Reason} }}";
      }
   }
}
