﻿using NLog;
using System;
using System.IO;
using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   [KnownType(typeof(AhoyHoyMessage))]
   [KnownType(typeof(AhoyMessage))]
   [KnownType(typeof(ChatMessage))]
   [KnownType(typeof(CountdownMessage))]
   [KnownType(typeof(DevMessage))]
   [KnownType(typeof(EventsMessage))]
   [KnownType(typeof(FireMessage))]
   [KnownType(typeof(GameBeginMessage))]
   [KnownType(typeof(GameEndMessage))]
   [KnownType(typeof(KickMessage))]
   [KnownType(typeof(LivesMessage))]
   [KnownType(typeof(PlayerMessage))]
   [KnownType(typeof(PlayersMessage))]
   [KnownType(typeof(PostgameMessage))]
   [KnownType(typeof(ServerListMessage))]
   [KnownType(typeof(ServerRegistrationMessage))]
   [KnownType(typeof(ServerSettingsMessage))]
   [KnownType(typeof(ServerShutdownMessage))]
   [KnownType(typeof(ServerStateMessage))]
   [KnownType(typeof(TurnMessage))]
   [KnownType(typeof(WalkMessage))]
   [KnownType(typeof(WelcomeMessage))]
   [KnownType(typeof(WindMessage))]
   public class Message
   {
      private static readonly ILogger sLog = LogManager.GetCurrentClassLogger();
      private static DataContractSerializer sSerializer = null;

      public enum Opcode
      {
         // Reserved for this base class. This opcode should never be seen in practice. If it
         // is, something went wrong.
         NONE,
         // Opcode reserved for development. Will likely be reused for many purposes.
         DEV,
         // Register, or update, information on a game server with a registry.
         SERVER_REGISTER,
         // Unregister, or remove, a server that has registered itself with a registry.
         SERVER_UNREGISTER,
         // Sent by a client to a registry to request a full list of registered game servers.
         SERVER_LIST_REQUEST, // Op only. No child type.
                              // Sent by a registry to a client, contains a list of registered game servers.
         SERVER_LIST,
         // Sent by a server when a client joins and/or when the server transitions states.
         SERVER_STATE,
         // Sent by a server to a client when a client first connects.
         AHOY,
         // Sent by a client to a server in response to an "ahoy." Contains the player's name
         // and zero to two passwords. (A join password is included for private servers and an
         // admin password is included if the client registered one for the server.)
         AHOY_HOY,
         // Sent by a server to a client when the client first joins. Assigns the player an ID.
         WELCOME,
         // Sent by a server to a client when the client is denied access to the server or
         // removed by force at any other time.
         KICK,
         // A simple text message sent by a client and relayed to other clients by a server.
         CHAT,
         // A full status of a single player. Sent by both clients and servers to indicate a
         // requested update (when sent by a player) and approved updates (when sent by a server).
         PLAYER,
         // A full status of all players in a server. Sent by a server to all connected clients.
         PLAYERS,
         // Sent by a server to indicate the current settings, configurable in the lobby
         // state/screen. Sent by a client, with sufficient privilege (veteran or admin) to
         // change these setting, to a server.
         SERVER_SETTINGS,
         // Sent by a veteran or admin client to a server indicating the game should start. This
         // requires all present players be ready and that there be availble slots for new
         // players. If, instead, all slots were filled and players ready, the game would begin
         // without any additional input.
         START_GAME,
         COUNTDOWN,
         POSTGAME,
         GAME_BEGIN,
         LIVES,
         GAME_END,
         WALK,
         FIRE,
         TURN,
         WIND,
         SKIP,
         SERVER_SHUTDOWN,
         EVENTS
      }

      /// <summary>
      /// Operation code. Essentially, an identifier for the message type.
      /// </summary>
      [DataMember]
      public Opcode Op { get; set; }

      public Message(Opcode aOp = Opcode.NONE)
      {
         Op = aOp;
      }

      /// <summary>
      /// Take a Message or child thereof and serialize it into a byte array.
      /// </summary>
      /// <param name="aMessage">A message to serialize.</param>
      /// <returns>The message as a byte array or null if it could not be serialized.</returns>
      public static byte[] Serialize(Message aMessage)
      {
         if (aMessage == null)
         {
            return null;
         }

         if (sSerializer == null)
         {
            sSerializer = new DataContractSerializer(typeof(Message));
         }

         try
         {
            using (MemoryStream stream = new MemoryStream())
            {
               sSerializer.WriteObject(stream, aMessage);
               return stream.ToArray();
            }
         }
         catch (Exception e)
         {
            sLog.Error(e, $"Exception thrown during {aMessage.GetType().Name} serialization");
            return null;
         }
      }

      /// <summary>
      /// Turn a byte array into an instance of a Message. Although cast to a Message, it may be,
      /// and almost certainly is, a child type.
      /// </summary>
      /// <param name="aData">The byte array to deserialize.</param>
      /// <returns>The data as a Message or null if it could not be deserialized.</returns>
      public static Message Deserialize(byte[] aData)
      {
         if (aData == null)
         {
            return null;
         }

         if (sSerializer == null)
         {
            sSerializer = new DataContractSerializer(typeof(Message));
         }

         try
         {
            using (MemoryStream stream = new MemoryStream(aData))
            {
               return sSerializer.ReadObject(stream) as Message;
            }
         }
         catch (Exception e)
         {
            sLog.Error(e, "Exception thrown during Message deserialization");
            return null;
         }
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op} }}";
      }
   }
}
