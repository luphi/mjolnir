﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class TurnMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public uint ActivePlayerID { get; set; }

      #endregion Serialized properties


      public TurnMessage(uint aActivePlayerID)
      {
         Op = Opcode.TURN;
         ActivePlayerID = aActivePlayerID;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(ActivePlayerID)} = {ActivePlayerID} }}";
      }
   }
}
