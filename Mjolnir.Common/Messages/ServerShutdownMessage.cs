﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class ServerShutdownMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public string Reason { get; set; }

      #endregion Serialized properties


      public ServerShutdownMessage(string aReason)
      {
         Op = Opcode.SERVER_SHUTDOWN;
         Reason = aReason;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, " +
                $"{nameof(Reason)} = \"{(Reason == null ? "null" : $"\"{Reason}\"")}\" }}";
      }
   }
}
