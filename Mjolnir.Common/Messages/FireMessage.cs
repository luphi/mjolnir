﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class FireMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public uint PlayerID { get; set; }

      [DataMember]
      public double PowerAsPercentage { get; set; }

      #endregion Serialized properties


      public FireMessage(uint aPlayerID, double aPowerAsPercentage)
      {
         Op = Opcode.FIRE;
         PlayerID = aPlayerID;
         PowerAsPercentage = aPowerAsPercentage;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(PlayerID)} = {PlayerID}, " +
                $"{nameof(PowerAsPercentage)} = {PowerAsPercentage} }}";
      }
   }
}
