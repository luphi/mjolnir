﻿using Mjolnir.Common.Models;
using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class WindMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public Vector Wind { get; set; }

      #endregion Serialized properties


      public WindMessage(Vector aWind)
      {
         Op = Opcode.WIND;
         Wind = aWind;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(Wind)} = {Wind} }}";
      }
   }
}
