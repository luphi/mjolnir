﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class ServerSettingsMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public StageEnum StageID { get; set; }

      [DataMember]
      public uint MaxPlayerCount { get; set; }

      [DataMember]
      public SuddenDeathModeEnum SuddenDeathMode { get; set; }

      #endregion Serialized properties


      public ServerSettingsMessage(StageEnum aStage, uint aMaxPlayerCount, SuddenDeathModeEnum aSuddenDeathMode)
      {
         Op = Opcode.SERVER_SETTINGS;
         StageID = aStage;
         MaxPlayerCount = aMaxPlayerCount;
         SuddenDeathMode = aSuddenDeathMode;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(StageID)} = \"{StageID}\", " +
               $"{nameof(MaxPlayerCount)} = {MaxPlayerCount}, {nameof(SuddenDeathMode)} = {SuddenDeathMode} }}";
      }
   }
}
