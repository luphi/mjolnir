﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class AhoyMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public string ServerName { get; set; }

      [DataMember]
      public string Motd { get; set; }

      [DataMember]
      public bool IsPrivate { get; set; }

      #endregion Serialized properties


      public AhoyMessage(string aServerName, string aMotd, bool aIsPrivate)
      {
         Op = Opcode.AHOY;
         ServerName = aServerName;
         Motd = aMotd;
         IsPrivate = aIsPrivate;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, " +
                $"{nameof(ServerName)} = \"{(ServerName == null ? "null" : $"\"{ServerName}\"")}\", " +
                $"{nameof(Motd)} = \"{(Motd == null ? "null" : $"\"{Motd}\"")}\", " +
                $"{nameof(IsPrivate)} = {IsPrivate} }}";
      }
   }
}
