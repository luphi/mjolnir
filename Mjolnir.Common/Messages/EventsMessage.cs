﻿using Mjolnir.Common.Models;
using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class EventsMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public Events Events { get; set; }

      #endregion Serialized properties


      public EventsMessage(Events aEvents)
      {
         Op = Opcode.EVENTS;
         Events = aEvents;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(Events)} = {Events} }}";
      }
   }
}
