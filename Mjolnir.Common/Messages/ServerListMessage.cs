﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class ServerListMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public List<ServerRegistrationMessage> Servers { get; set; } = new List<ServerRegistrationMessage>();

      #endregion Serialized properties


      public ServerListMessage()
      {
         Op = Opcode.SERVER_LIST;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(Servers)} = {string.Join(", ", Servers)} }}";
      }
   }
}
