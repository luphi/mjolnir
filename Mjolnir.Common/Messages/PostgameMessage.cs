﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class PostgameMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public uint Delay { get; set; }

      #endregion Serialized properties


      public PostgameMessage(uint aDelay)
      {
         Op = Opcode.POSTGAME;
         Delay = aDelay;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(Delay)} = {Delay} }}";
      }
   }
}
