﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class WalkMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public uint PlayerID { get; set; }

      [DataMember]
      public Direction Direction { get; set; }

      #endregion Serialized properties


      public WalkMessage(uint aPlayerID, Direction aDirection)
      {
         Op = Opcode.WALK;
         PlayerID = aPlayerID;
         Direction = aDirection;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(PlayerID)} = {PlayerID}, " +
                $"{nameof(Direction)} = \"{Direction}\" }}";
      }
   }
}
