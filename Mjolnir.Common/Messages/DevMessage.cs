﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class DevMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public string String { get; set; }

      #endregion Serialized properties


      public DevMessage()
      {
         Op = Opcode.DEV;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, " +
                $"{nameof(String)} = \"{(String == null ? "null" : $"\"{String}\"")}\" }}";
      }
   }
}
