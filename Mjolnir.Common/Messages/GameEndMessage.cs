﻿using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class GameEndMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public TeamEnum WinningTeam { get; set; }

      #endregion Serialized properties


      public GameEndMessage(TeamEnum aWinningTeam)
      {
         Op = Opcode.GAME_END;
         WinningTeam = aWinningTeam;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(WinningTeam)} = {WinningTeam} }}";
      }
   }
}
