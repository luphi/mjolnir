﻿using Mjolnir.Common.Models;
using System.Runtime.Serialization;

namespace Mjolnir.Common.Messages
{
   [DataContract]
   public class PlayerMessage : Message
   {
      #region Serialized properties

      [DataMember]
      public Player Player { get; set; }

      #endregion Serialized properties


      public PlayerMessage(Player aPlayer)
      {
         Op = Opcode.PLAYER;
         Player = aPlayer;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Op)} = {Op}, {nameof(Player)} = {Player} }}";
      }
   }
}
