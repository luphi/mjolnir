﻿using System;

namespace Mjolnir.Common
{
   /// <summary>
   /// Identifier for specific stages, derived from the stages' names.
   /// Stage models take this value as the sole parameter and set some values
   /// (foreground image, width, height, etc.) based on the value.
   /// </summary>
   public enum StageEnum
   {
      NONE,
      CAPITAL,
      COMFY_SPIRE,
      DUNCE_HILL,
      ENLIGHTENMENT,
      MIMARO_VILLAGE,
      OCEAN_OF_EXEMPLAR,
      PIDGIN_ROOT,
      RECURSE_QUARRY,
      STELLAR_CLOUD,
      WYVERN,
      DEV
   }

   /// <summary>
   /// Identifier for specific mobiles, derived from the mobiles' names.
   /// Mobile models take this value as the sole parameter and set some values
   /// (body radius, shots [weapons], initial HP, etc.) based on the value.
   /// </summary>
   public enum MobileEnum
   {
      NONE,
      AKUDA,
      ARACH,
      B_SEAT,
      DJ,
      DROID_SLING,
      FROST,
      KAME,
      LARVA,
      SASQUATCH,
      SHIELD,
      THUNDER,
      TRIKE,
      WIZARD,
      ZOOMER
   }

   /// <summary>
   /// Identifier for specific items, derived from the items' names.
   /// Item models take this value as the sole parameter and set some values
   /// (name, slots used, delay) based on the value.
   /// </summary>
   public enum ItemEnum
   {
      NONE,
      BANDAID,
      BLOOD,
      DUAL,
      DUAL_PLUS,
      HEALTH_KIT,
      POWER_UP,
      SHOVEL,
      TEAM_TELEPORT,
      TELEPORT,
      WIND_CHANGE
   }

   /// <summary>
   /// Dynamic/physical behavior applied to bodies within the world that allow some
   /// bodies to, for example, bounce off the stage, 'swim' through the stage, and other
   /// affect how these bodies move.
   /// </summary>
   public enum DynamicBehaviorEnum
   {
      // The body acts as if its movement creates upward thrust equal, but opposite, to
      // gravity. Acceleration from wind is still applied.
      AIRFOIL,
      // The body will bounce off the stage when they come in contact. These collisions
      // will be inelastic, resulting in a pre-determined velocity each bounce.
      BALL,
      // The body acts as if it has high air resistance causing the acceleration from
      // gravity to be halved. Acceleration from wind is unaffected.
      FEATHER,
      // The body will not collide with the stage but will collide with players.
      INCORPOREAL,
      // The body will 'swim' through the stage after becoming submerged with behavior
      // similar to all others in the open air. A body will be considered submerged
      // if or when its center, as an individual point, enters any solid portions of
      // the stage. After this, it will retain its velocities and acceleration due to
      // wind will still apply but there will be two changes: 1) gravity will be
      // inverted (upward), and 2) the body will be considered in contact with the
      // stage, for a second time, and detonate if the body's center exits the stage
      // (i.e. if the individual center point is no longer overlapping a solid point
      // of the stage).
      SANDSHARK,
      // The most common/typical behavior for players and projectils. Physics will be
      // applied while the body is still in the air but cease once in contact with the
      // stage or, for projectile bodies, a player.
      // In this context, "cease" will either mean a player sitting/standing on the
      // stage or a projectile body detonating.
      STANDARD
   }

   /// <summary>
   /// Specific detonation, or lack thereof, behavior applied to bodies within the world
   /// that allows them to, for example, act as a target for a laser, summon lightning,
   /// or stick to the stage without any additional action (at that time).
   /// </summary>
   public enum ProjectileBehaviorEnum
   {
      // The projectile body pulls players towards the location on the stage or a player
      // at which it lands. This gravitational pull has a finite reach but extends
      // beyond the standard blast radius.
      GRAVITY_PULL,
      // The projectile body pushes players away from the location on the stage or a
      // player at which it lands. This gravitational pull has a finite reach but
      // extends beyond the standard blast radius.
      GRAVITY_PUSH,
      // The projectile body summons a single lightning bolt from directly above the
      // location on the stage at which it lands. Does not collide with players.
      LIGHTNING,
      // The projectile body summons two lightning bolts from both sides, diagonally
      // at 45 degrees, striking the location on the stage at which it lands. Does not
      // collide with players.
      LIGHTNING_V,
      // The projectile body illuminates a target for the satellite floating above the
      // player that fired the projectile. Only B.Seat uses this.
      SATELLITE,
      // The projectile has a wide-range area of effect property that reduces the shields
      // of all players in range to zero.
      SHIELD_STRIP,
      // The projectile body detonates and causes damage directly. This can be thought
      // of as a bullet, bomb, missile, and similar things.
      STANDARD,
      // The projectile body lands on the stage in the same way a player would. However,
      // these bodies will detonate as standard if they collide with a player.
      STICKY,
      // The projectile body illuminates a target for the Thor floating above the stage.
      // Only Akuda uses this.
      THOR
   }

   public enum SuddenDeathModeEnum
   {
      NONE,
      DOUBLE,
      SS,
      BIGBOMB
   }

   public enum TeamEnum
   {
      A,
      B
   }

   public enum Direction
   {
      LEFT,
      RIGHT
   }

   public enum ShotEnum
   {
      SHOT_1,
      SHOT_2,
      SPECIAL_SHOT
   }

   public enum StateEnum
   {
      /// <summary>
      /// The lobby state acts as the "waiting room" for players that wish
      /// to join as well as the time for players to choose their mobile and,
      /// potentailly, the stage they will play on.
      /// </summary>
      LOBBY,
      /// <summary>
      /// The pre-game state is a brief one in which players are shown their
      /// starting locations within the stage. The server will determine these
      /// locations but do little more. This state will transition to the
      /// in-game state in a matter of seconds.
      /// </summary>
      PRE_GAME,
      /// <summary>
      /// The in-game state is exactly what its name suggests. All logic for
      /// actual gameplay is contained within this state.
      /// </summary>
      IN_GAME,
      /// <summary>
      /// The post-game state is a brief one in which players are shown a
      /// final scoreboard along with winner/loser assignments. This state
      /// will transition to the lobby state in a matter of seconds.
      /// </summary>
      POST_GAME,
      /// <summary>
      /// Indicates the server ran into an unrecoverable error. This does not
      /// represent a state but the lack of a state. This exists to provide a
      /// reason for unexpected server shutdowns.
      /// </summary>
      ERROR
   }

   public enum KickReasonEnum
   {
      // The initial attempt to join a server failed because 1) the server
      // is private, AND 2) the user did not provide a correct password.
      INCORRECT_PASSWORD,
      // The initial attempt to join a server failed because the client did
      // not send a join message in time.
      TIMEOUT,
      // At some point after joining a server, a veteran or admin chose to
      // remove the player.
      KICK,
      // The player is banned from this particular server. This reason may
      // be given either at an initial attempt to join or at some point after.
      BAN,
      // The server is in a state that no longer accepts new players, namely
      // any one other than the lobby state.
      NOT_NOW,
   }

   /// <summary>
   /// Flags used to selectively sync properties of a Player or Body model.
   /// </summary>
   [Flags]
   public enum SyncFlags
   {
      ALL = 1,
      // Player properties.
      IS_VETERAN = 2 << 0,
      IS_READY = 2 << 1,
      IS_CHARGING = 2 << 2,
      IS_FIRING = 2 << 3,
      IS_DEAD = 2 << 4,
      TEAM = 2 << 5,
      SELECTED_SHOT = 2 << 6,
      SELECTED_ITEM = 2 << 7,
      DELAY = 2 << 8,
      HP = 2 << 9,
      SHIELD = 2 << 10,
      SCORE = 2 << 11,
      LIVES = 2 << 12,
      DAMAGE_MULTIPLIER = 2 << 13,
      ITEMS = 2 << 14,
      MOBILE_ID = 2 << 15,
      AIM = 2 << 16,
      // Body properties.
      X = 2 << 32,
      Y = 2 << 33,
      RADIUS = 2 << 34,
      PITCH = 2 << 35,
      DIRECTION = 2 << 36,
      VELOCITY = 2 << 37,
      ACCELERATION = 2 << 38,
      IS_SETTLED = 2 << 39,
      IS_WALKING = 2 << 40,
      IS_SUBMERGED = 2 << 41,
      BEHAVIOR = 2 << 42
   }
}
