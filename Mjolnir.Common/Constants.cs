﻿namespace Mjolnir.Common
{
   public static class Constants
   {
      public const string Name = "Mjolnir";

      #region Network (default) constants

      /// <summary>
      /// Default port for a game server to use.
      /// </summary>
      public const int DefaultServerPort = 51509;

      /// <summary>
      /// Default port for a registry to use.
      /// </summary>
      public const int DefaultRegistryPort = 51510;

      #endregion Network (default) constants


      #region World constants

      /// <summary>
      /// Frequency, in hertz, of the world loop (physics).
      /// </summary>
      public const int GameLoopFrequencyInHertz = 60;

      /// <summary>
      /// Acceleration due to gravity, in pixels per second^2, on the Y axis.
      /// </summary>
      public const int Gravity = 300;

      /// <summary>
      /// The amount of shield health, equivalent to HP, that is potentially added to players'
      /// shield at the beginning of each turn. This applies only to players whose mobiles
      /// have shields and this regeneration will not exceed their initial shield health.
      /// </summary>
      public const int ShieldHealthRegenerationPerRound = 20;

      /// <summary>
      /// The speed, in pixels per second, at which a mobile (player) walks along the stage.
      /// </summary>
      public const int WalkSpeedInPixelsPerSecond = 25;

      /// <summary>
      /// The distance, in pixels, that a single walk will attempt to advance.
      /// In other words, this is the maximum number of pixels a mobile will be translated,
      /// on the X axis, each time a user presses an arrow key.
      /// </summary>
      public const int WalkDistanceInPixels = 5;

      /// <summary>
      /// The greatest Y distance, in pixels, that a player can move in a walk step in a couple
      /// senses. When walking uphill, players that would walk a Y distance greater than this
      /// value will have that step denied. When walking downhill, players will be treated as
      /// if they are falling (i.e. unsettled).
      /// </summary>
      public const int WalkMaxClimbInPixels = 10;

      /// <summary>
      /// The speed of a projectile when fired at full power independent of the mobile or shot.
      /// When fired at less than full power, the speed will correlate directly to this value
      /// meaning half power will result in a speed of FullPowerAsSpeed / 2.
      /// </summary>
      public const int FullPowerAsSpeedInPixelsPerSecond = 550;

      /// <summary>
      /// The greatest possible magnitude of the wind acceleration vector. The magnitude
      /// in-game will range from zero to this value.
      /// </summary>
      public const int MaxWindAccelerationInPixelsPerSecondSquared = 50;

      /// <summary>
      /// The maximum time, in seconds, that a player has to perform actions during his/her
      /// turn. The turn ends when this timeout is reached or when the player begins charging
      /// a shot, whichever happens first.
      /// </summary>
      public const int TurnTimeoutInSeconds = 20;

      /// <summary>
      /// The time, in seconds, between 0% charged and 100% charged when a player is preparing
      /// to fire a shot. In other words, if the space key is held for half of this period, the
      /// player will have charged 50% of the maximum power.
      /// Charging is periodic in that it wraps around when 100% is exceeded, allowing for three
      /// attempts at getting the desired power. Wrapping around, here, means that the charge
      /// returns to 0% and begins increasing again from there.
      /// </summary>
      public const int ChargePeriodInMilliseconds = 2000;

      /// <summary>
      /// Maximum distance, in pixels, from a shooting center point to a player's center point
      /// at which the player will be harmed. Subsequently, this value is used to calculate the
      /// exact damage done. Shooting in direct contact with a player do maximum damage whereas
      /// those at this distance do minimum damage with distances inbetween doing damage on a
      /// linear scale inbween the minimum and maximum.
      /// </summary>
      public const int BlastRadiusInPixels = 50;

      /// <summary>
      /// Maximum distance, in pixels, fro ma shooting center point to a player's center point
      /// at which a shot's "area of effect" property applies. In practice, this is limited to
      /// a couple special shots that 1) strip players of their shields or 2) summon lightning.
      /// This value should always exceed the blast radius.
      /// </summary>
      public const int AreaOfEffectRadiusInPixels = 100;

      /// <summary>
      /// A delay, in milliseconds, to wait before applying aftershock damage. Aftershock is an
      /// additional damage dealt by some projectiles following the initial detonation damage.
      /// </summary>
      public const int AftershockDelayInMilliseconds = 1000;

      /// <summary>
      /// The duration, in milliseconds, that the localized gravity effects of D.J.'s shots are
      /// last. This value applies to both push and pull shots.
      /// </summary>
      public const int LocalizedGravityDurationInMilliseconds = 1000;

      /// <summary>
      /// The speed, in pixels per second, applied to bodies with the ball dynamic behavior
      /// following a stage collision. Bodies with this behavior have uniquely inelastic
      /// collisions with the stage in which they bounce off the stage at this speed independent
      /// of their incidental speed.
      /// </summary>
      public const int BallSpeedInPixelPerSeconds = 75;

      #endregion World constants


      #region Projectile constants

      /// <summary>
      /// The height, in pixels, above the stage that a B.Seat's satellite will hover at. This
      /// value applies directly to B.Seat's shot 1 and special shot. For shot 2, the
      /// satellite will be at three times this altitude.
      /// </summary>
      public const int BSeatSatelliteBaseAltitudeInPixels = 500;

      /// <summary>
      /// The rate, in hertz, at which a couple of B.Seat's shots will cause their satellites to
      /// fire. The number of shots depends on the shot but the rate is the same. This value
      /// applies to B.Seat's shot 2 and special shot.
      /// </summary>
      public const int BSeatFiringFrequencyInHertz = 10;

      /// <summary>
      /// The radius, in pixels, of a B.Seat satellite's path when a special shot causes the
      /// satellite to fire from above the projectile. This radius is from a center point
      /// directly above the projectile.
      /// </summary>
      public const int BSeatSatellitePathRadiusInPixels = 35;

      /// <summary>
      /// The change, in degrees, along a B.Seat satellite's path when a special shot causes
      /// the satellite to fire from above the projectile.
      /// </summary>
      public const int BSeatSatelliteMovementDThetaInDegrees = 30;

      /// <summary>
      /// A frequency, in hertz, that applies to the continuous, mini detonations of some
      /// projectiles. These projectiles (e.g. Larva's SS or Trike's SS) cause more damage as time
      /// progresses with this value determinig the discrete rate of it.
      /// </summary>
      public const int BurnFrequencyInHertz = 5;

      /// <summary>
      /// A time, in seconds, at which a Droid Sling's robotic mine projectile will give up
      /// what it's trying to do. For shot 2, the mine will pause where it is and remain idle
      /// until a player comes near enough. For the special shot, the mine will detonate.
      /// </summary>
      public const int DroidSlingMineTimeoutInSeconds = 6;

      /// <summary>
      /// A maximum distance, in pixels, that the streams of a Kame's shot 2 will be
      /// offset by. This is the offset distance from a sort of center point the two
      /// streams rotate around meaning that the maximum distance separating the two
      /// streams will be twice this value.
      /// </summary>
      public const int KameStreamMaxOffsetInPixels = 25;

      /// <summary>
      /// A duration, in milliseconds, after which a Kame's special shot's projectile will
      /// split from one body into multiple smaller ones.
      /// </summary>
      public const int KameSplitTimeInMilliseconds = 2200;

      /// <summary>
      /// The angle, in degrees, separating the trajectories of the multiple, smaller balls
      /// that result from a Kame's initial special shot projectile's splitting.
      /// </summary>
      public const int KameTrajectorySeparationInDegrees = 2;

      /// <summary>
      /// The duration, in milliseconds, that Larva's ball projectiles will bounce around the
      /// stage before detonating. This value applies to Larva's shot 2 and special shot.
      /// </summary>
      public const int LarvaBounceDurationInMilliseconds = 3000;

      /// <summary>
      /// A maximum distance, in pixels, that the balls fired by a Larva using shot 2 are
      /// separated. This value is the distance between each ball.
      /// </summary>
      public const int LarvaBallMaxTetherDistanceInPixels = 25;

      /// <summary>
      /// A maximum distance, in pixels, that a Sasquatch's missiles will be offset by from
      /// a non-existent, ideal trajectory. This value applies to the missiles of Sasquatch's
      /// shot 1 and special shot.
      /// </summary>
      public const int SasquatchMaxMissileOffsetInPixels = 10;

      /// <summary>
      /// A maximum distance, in pixels, that a Sasquatch's bombs will be offset by from the
      /// center, focus body. This value applies to the bombs of Sasquatch's shot 2.
      /// </summary>
      public const int SasquatchMaxBombOffsetInPixels = 25;

      /// <summary>
      /// The duration, in milliseconds, that Shield's special shot projectile waits before
      /// it powers up. The projectile's timer starts at launch and counts down until reaching
      /// this value. After that time, the projectile does more damage.
      /// </summary>
      public const int ShieldPowerUpTimeInMilliseconds = 1800;

      /// <summary>
      /// The height, in pixels, above the stage that the Thor will hover at. Its X position
      /// will change as it floats left or right but its Y position will remain fixed at this.
      /// </summary>
      public const int ThorAltitudeInPixels = 1000;

      /// <summary>
      /// The speed, in pixels per second, at which Thor (the satellite floating above the
      /// stage) moves left or right.
      /// </summary>
      public const int ThorSpeedInPixelsPerSecond = 50;

      /// <summary>
      /// A duration, in milliseconds, that the Thor will remain stationary following a firing.
      /// During this period, the Thor will pause its left/right movement and thew associated
      /// view will display a beam to the target.
      /// This duration does not affect the actual time between firings. The Thor will fire
      /// when a projectile body collides with the stage without restriction.
      /// </summary>
      public const int ThorCooldownDurationInMilliseconds = 750;

      /// <summary>
      /// A maximum distance, in pixels, that the oribitting bodies of a Trike's shot 2 will
      /// orbit around its center, focus body.
      /// </summary>
      public const int TrikeMaxOrbitRadiusInPixels = 25;

      /// <summary>
      /// The duration, in milliseconds, a Trike's special shot projectile will do continuous
      /// damage to players within the blast radius.
      /// </summary>
      public const int TrikeBurnTimeInMilliseconds = 1500;

      /// <summary>
      /// A maximum distance, in pixels, that the lasers of a Wizard's shot 2 will be
      /// offset by. This is the offset distance from a sort of center point the two
      /// lasers rotate around meaning that the maximum distance separating the two
      /// lasers will be twice this value.
      /// </summary>
      public const int WizardMaxLaserOffsetInPixels = 15;

      /// <summary>
      /// A coefficient used to calculate the distance at which a Zoomer's projectile will
      /// begin to descend when the "hook" dynamic behavior is used. This value is not the
      /// distance but directly correlates with it.
      /// </summary>
      public const int ZoomerDistanceCoefficient = 350;

      /// <summary>
      /// A logistic function growth rate used to calculate the weights of some inputs.
      /// This value applies to Zoomer projectiles that use the "hook" dynamic behavior and
      /// is part of determining the degree to which the projectile's current velocity and/
      /// or the wind affect the resulting velocity.
      /// </summary>
      public const int ZoomerGrowthRate = 25;

      /// <summary>
      /// The duration, in milliseconds, that Zoomer's special shot projectile waits before
      /// it powers up. The projectile's timer starts at launch and counts down until reaching
      /// this value. After that time, the projectile does more damage.
      /// </summary>
      public const int ZoomerPowerUpTimeInMilliseconds = 1800;

      #endregion Projectile constants
   }
}
