﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Mjolnir.Common.Collections
{
   /// <summary>
   /// Partial implementation of a thread-safe System.Collections.Generic.List<T> that
   /// System.Collections.Concurrent is lacking.
   /// </summary>
   /// <typeparam name="T"></typeparam>
   public class ConcurrentList<T> : ICollection<T>,
                                    IEnumerable<T>,
                                    IList<T>,
                                    IReadOnlyCollection<T>,
                                    IReadOnlyList<T>,
                                    IList
   {
      private readonly List<T> mList = new List<T>();
      private int mCount = 0;

      public T this[int aIndex]
      {
         get
         {
            BoundsCheck(nameof(aIndex), aIndex);

            return mList[aIndex];
         }
         set
         {
            BoundsCheck(nameof(aIndex), aIndex);

            mList[aIndex] = value;
         }
      }

      object IList.this[int aIndex]
      {
         get => this[aIndex];
         set => this[aIndex] = (T)value;
      }

      public int Count => mCount;

      public bool IsReadOnly => false;

      public bool IsFixedSize => false;

      public bool IsSynchronized => true;

      public object SyncRoot => new object();

      public void Add(T aItem)
      {
         if (aItem == null)
         {
            throw new ArgumentNullException(nameof(aItem));
         }

         lock (SyncRoot)
         {
            mList.Add(aItem);
            mCount = mList.Count;
         }
      }

      public int Add(object aItem)
      {
         TypeCheck(nameof(aItem), aItem);

         Add((T)aItem);

         return mCount;
      }

      public void AddRange(IEnumerable<T> aItems)
      {
         if (aItems == null)
         {
            throw new ArgumentNullException(nameof(aItems));
         }

         lock (SyncRoot)
         {
            mList.AddRange(aItems);
            mCount = mList.Count;
         }
      }

      public void Clear()
      {
         if (mCount > 0)
         {
            lock (SyncRoot)
            {
               mList.Clear();
               mCount = 0;
            }
         }
      }

      public bool Contains(T aItem)
      {
         lock (SyncRoot)
         {
            return mList.Contains(aItem) == true;
         }
      }

      public bool Contains(object aItem)
      {
         TypeCheck(nameof(aItem), aItem);

         return Contains((T)aItem) == true;
      }

      public void CopyTo(T[] aArray, int aArrayIndex)
      {
         if (aArray == null)
         {
            throw new ArgumentNullException(nameof(aArray));
         }

         if (aArray.Length - aArrayIndex < mCount)
         {
            throw new ArgumentException("The destination array is not long enough");
         }

         lock (SyncRoot)
         {
            mList.CopyTo(aArray, aArrayIndex);
         }
      }

      public void CopyTo(Array aArray, int aIndex)
      {
         CopyTo((T[])aArray, aIndex);
      }

      public IEnumerator<T> GetEnumerator()
      {
         lock (SyncRoot)
         {
            List<T> copy = new List<T>();
            copy.AddRange(mList);
            return copy.GetEnumerator();
         }
      }

      public int IndexOf(T aItem)
      {
         lock (SyncRoot)
         {
            return mList.IndexOf(aItem);
         }
      }

      public int IndexOf(object aItem)
      {
         TypeCheck(nameof(aItem), aItem);

         return IndexOf((T)aItem);
      }

      public void Insert(int aIndex, T aItem)
      {
         BoundsCheck(nameof(aIndex), aIndex);

         lock (SyncRoot)
         {
            mList.Insert(aIndex, aItem);
            mCount = mList.Count;
         }
      }

      public void Insert(int aIndex, object aItem)
      {
         BoundsCheck(nameof(aIndex), aIndex);
         TypeCheck(nameof(aItem), aItem);

         Insert(aIndex, (T)aItem);
      }

      public bool Remove(T aItem)
      {
         lock (SyncRoot)
         {
            if (mList.Remove(aItem) == true)
            {
               mCount = mList.Count;
               return true;
            }

            return false;
         }
      }

      public void Remove(object aItem)
      {
         TypeCheck(nameof(aItem), aItem);

         Remove((T)aItem);
      }

      public void RemoveAt(int aIndex)
      {
         BoundsCheck(nameof(aIndex), aIndex);

         lock (SyncRoot)
         {
            mList.RemoveAt(aIndex);
            mCount = mList.Count;
         }
      }

      IEnumerator IEnumerable.GetEnumerator()
      {
         return GetEnumerator();
      }

      private void BoundsCheck(string aParamName, int aIndex, [CallerMemberName] string aMethodName = null)
      {
         if ((aIndex < 0) || (aIndex >= mCount))
         {
            throw new ArgumentOutOfRangeException($"Parameter {aParamName} of {aMethodName} was outside " +
                                                  $"the bounds [{0}, {mCount}]");
         }
      }

      private void TypeCheck(string aParamName, object aObject, [CallerMemberName] string aMethodName = null)
      {
         if ((aObject is T) == false)
         {
            throw new ArgumentException($"Parameter {aParamName} of {aMethodName} was not of type {typeof(T)}");
         }
      }
   }
}
