﻿using Mjolnir.Common.Models;
using System.Collections.Generic;

namespace Mjolnir.Common.Physics
{
   /// <summary>
   /// Internal class that allows World's incorporeal overlaps dictionary to better identify keys.
   /// </summary>
   public class BodyComparer : IEqualityComparer<Body>
   {
      public bool Equals(Body aBody1, Body aBody2)
      {
         return ReferenceEquals(aBody1, aBody2);
      }

      public int GetHashCode(Body aBody)
      {
         return 0;
      }
   }
}
