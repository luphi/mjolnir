﻿using Mjolnir.Common.Models;
using System;
using System.Collections.Generic;

namespace Mjolnir.Common.Physics
{
   /// <summary>
   /// Results object that holds the events that occurred during a run of the world
   /// (where a run is the period between a Start() call and the world settling or a
   /// call to Stop()).
   /// </summary>
   public class WorldRunData
   {
      public List<Landing> Landings { get; } = new List<Landing>();

      public List<Body> Halts { get; } = new List<Body>();

      public List<Player> Deaths { get; } = new List<Player>();

      public List<Shooting> Shootings { get; } = new List<Shooting>();

      public List<Damage> Damages { get; } = new List<Damage>();

      public ulong Steps { get; set; } = 0;

      public DateTime? StartTime { get; set; } = null;
   }
}
