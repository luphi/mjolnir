﻿using Mjolnir.Common.Models;
using System;
using System.Collections.Generic;

namespace Mjolnir.Common.Physics
{
    public class Collisions
    {
      #region Collision detection

      internal static bool AreColliding(Body aBody1, Body aBody2)
      {
         // The goal here is to check the distance between the bodies' centers against the sum of
         // their radiuses. If that distance is less than that sum, they're overlapping (colliding).
         Vector center1 = aBody1.Center;
         Vector center2 = aBody2.Center;
         double dX = center2.X - center1.X; // Delta X. X distance between the bodies' (circles') centers.
         double dY = center2.Y - center1.Y; // Delta Y. Y distance between the bodies' (circles') centers.
         double eR = aBody1.Radius + aBody2.Radius; // Epsilon R. Sum of the radiuses of the two bodies (circles).

         // This is an application of the Pythagorean theorem without the square root step.
         // (We want to avoid square roots becuase computers aren't especially fast with them due to the
         // underlying math being iterative [probably Newton's method]).
         return dX * dX + dY * dY < eR * eR;
      }

      internal static bool AreColliding(Stage aStage, Body aBody)
      {
         if (aBody is Player)
         {
            // If the body is a Player or a child class of it (Bot).

            return aStage.IsSolid(aBody.Footing);
         }
         else
         {
            // If the body is an instance of the Body class.

            // Calculate the area formed by the overlapping portions of the two entities. In other
            // words, the area they both share. This area will be searched for overlapping pixels.
            int x1 = (int)Math.Round(Math.Max(aStage.X, aBody.X));
            int x2 = (int)Math.Round(Math.Min(aStage.X + aStage.Width, aBody.X + aBody.Radius * 2.0));
            int y1 = (int)Math.Round(Math.Max(aStage.Y, aBody.Y));
            int y2 = (int)Math.Round(Math.Min(aStage.Y + aStage.Height, aBody.Y + aBody.Radius * 2.0));

            // For each pixel in the overlapping area.
            for (int y = y1; y < y2; ++y)
            {
               for (int x = x1; x < x2; ++x)
               {
                  // If the pixel in the stage is solid and the same point is within the body.
                  if ((aStage.IsSolid(x, y) == true) && (IsInside(aBody, x, y) == true))
                  {
                     // The stage's pixel is inside the body. That's a collision.
                     return true;
                  }
               }
            }

            // If the above loops did not return true, there was no collision.
            return false;
         }
      }

      internal static bool AreCollidingOverhang(Stage aStage, Body aBody)
      {
         // Calculate the area formed by the overlapping portions of the stage and the upper
         // quadrant of the body's circle in the direction it is facing. This area will be
         // searched for overlapping pixels.
         double quadrantX = aBody.Direction == Direction.LEFT ? aBody.X : aBody.X + aBody.Radius;
         int x1 = (int)Math.Round(Math.Max(aStage.X, quadrantX));
         int x2 = (int)Math.Round(Math.Min(aStage.X + aStage.Width, quadrantX + aBody.Radius));
         int y1 = (int)Math.Round(Math.Max(aStage.Y, aBody.Y));
         int y2 = (int)Math.Round(Math.Min(aStage.Y + aStage.Height, aBody.Y + aBody.Radius));

         // For each pixel in the overlapping area.
         for (int y = y1; y < y2; ++y)
         {
            for (int x = x1; x < x2; ++x)
            {
               // If the pixel in the stage is solid and the same point is within the body's quadrant.
               if ((aStage.IsSolid(x, y) == true) && (IsInside(aBody, x, y) == true))
               {
                  // That's a collision.
                  return true;
               }
            }
         }

         // If the above loops did not return true, there was no collision.
         return false;
      }

      internal static bool IsInside(Body aBody, double aX, double aY)
      {
         // Calculate the point's distance from the center of the body (circle). If that distance
         // is less than the radius, it's within the body.
         Vector center = aBody.Center;
         double dX = aX - center.X; // Delta X. X distance between the point and body's center.
         double dY = aY - center.Y; // Delta Y. Y distance between the point and body's center.

         return Math.Pow(dX, 2.0) + Math.Pow(dY, 2.0) < Math.Pow(aBody.Radius, 2.0);
      }

      internal static bool IsInside(Stage aStage, double aX, double aY)
      {
         return (aX >= aStage.X) && (aY >= aStage.Y) &&
                (aX < aStage.X + aStage.Width) && (aY < aStage.Y + aStage.Height);
      }

      #endregion Collision detection


      #region Collision response

      internal static Vector GetMinimumTranslationVector(Stage aStage, Body aBody)
      {
         // Error case check. The loop below will be infinite if the velocity is zero.
         if ((aBody == null) || (aBody.Velocity == Vector.Zero))
         {
            return Vector.Zero;
         }

         // The method:
         // Negate the body's velocity to get a vector pointing in the reverse direction.
         // Normalize this reverse vector to get a minimal offset by which to translate.
         // Continually translate the body by this minimal offset until the body and
         // stage are no longer colliding. This total offset is the minimum translation vector.

         Vector step = aBody.Velocity.Normalized();
         step *= -1.0;

         Body translated;
         if (aBody is Player bodyAsPlayer)
         {
            // If the body is actually a child Player type.

            // Allocate a new object so the position of the original is not modified.
            translated = new Player(bodyAsPlayer);

            // The player's inexspensive collision point will be used to check collision with
            // the terrain. This is done because, outside of walking that has its own logic,
            // the only time a player will be moving is when falling. This means we know
            // exactly where the collision with the stage will happen.

            // While the player is still standing in, not on, the stage.
            while (aStage.IsSolid(((Player)translated).Footing) == true)
            {
               translated.Position += step; // Move backwards one step.
            }
         }
         else
         {
            // If the body is an instance of the base Body type.

            // Allocate a new object so the position of the original is not modified.
            translated = new Body(aBody);

            // While the body is still colliding with the stage.
            while (AreColliding(aStage, translated) == true)
            {
               translated.Position += step; // Move backwards one step.
            }
         }

         return new Vector(translated.X - aBody.X, translated.Y - aBody.Y);

         // Alternative method to consider:
         // Use the body's center point and a raycast toward's the stage's edge,
         // use this cast to calculate the penetration vector (P), and
         // return -Vector.Dot(P, aBody.Velocity) / aBody.Velocity.Length
         //      (A dot B) / |B| is the projection of A onto B
         //      the negation reverses the direction (opposite the velocity)
         // Calculating the penetration vector could introduce some error but this method
         // would almost certainly be faster.
      }

      internal static Vector GetStageCollisionPoint(Stage aStage, IEnumerable<Player> aPlayers, Body aBody)
      {
         // Error case check. The behavior below will be undefiend for a velocity vector without
         // a direction.
         if ((aBody == null) || (aBody.Velocity == Vector.Zero))
         {
            return Vector.Zero;
         }

         // The method:
         // Cast a ray from the body's center towards the edge of the stage. The direction
         // of the raycast will be equal to or opposite of the body's current velocity,
         // depending on whether or not the body's center is currently in the ground. This
         // raycast will return locations of pixels along the edge: one solid and one not.

         Vector center = aBody.Center;
         Vector direction = aBody.Velocity;

         // If the body translated so far in the last step that it is submerged, reverse
         // the direction vector.
         if (aStage.IsSolid((int)center.X, (int)center.Y) == true)
         {
            direction *= -1.0;
         }

         // Cast the ray and, if the ray hits the stage, return the point at which is collides.
         if (CastRay(aStage, aPlayers, center, direction, out Vector collisionPoint) == true)
         {
            return collisionPoint;
         }

         return Vector.Zero;
      }

      internal static Vector GetBodyCollisionPoint(Body aMoving, Body aStatic)
      {
         // Error case check. The behavior below will be undefiend for a velocity vector without
         // a direction.
         if ((aMoving == null) || (aStatic == null) || (aMoving.Velocity == Vector.Zero))
         {
            return Vector.Zero;
         }

         // The method:
         // Translate the moving body backward, opposite its current velocity vector,
         // until it is no longer in contact with the static body. Using this position,
         // get a vector difference of the two bodies' centers. Use this vector and the
         // static body's radius to calculate the point on the static body's outer edge
         // that overlaps with this vector. This is where we assume the collision happened.

         Vector step = aMoving.Velocity.Normalized();
         step *= -1.0;

         // Allocate a new object so the position of the original is not modified.
         Body translated = new Body(aMoving);
         // While the moving body is still colliding with the static body.
         while (AreColliding(aStatic, translated) == true)
         {
            translated.Position += step; // Move backwards one step.
         }

         // Calculate the difference between the centers of the static body and translated
         // moving body. (This can be thought of as a line drawn from one center to the other.)
         Vector centerDifference = translated.Center - aStatic.Center;

         // Set the vector's length equal to the static body's radius resulting in a
         // vector that is in the direction of the other body and just long enough to
         // touch the outer edge of the static body.
         centerDifference.Length = aStatic.Radius;

         // Return that point on the static body's outer edge.
         Vector point = aStatic.Center + centerDifference;
         point = new Vector(point.X, point.Y);
         return point;
      }

      internal static bool CastRay(Stage aStage,
                                   IEnumerable<Player> aPlayers,
                                   Vector aOrigin,
                                   Vector aDirection,
                                   out Vector aCollisionPoint)
      {
         // If no direction in which to cast the ray is provided, there will be a division by zero error.
         if (aDirection == Vector.Zero)
         {
            // There's no right answer here. Nullify the output parameter and give up.
            aCollisionPoint = null;
            return false;
         }

         double maxLength = 2.0 * (aStage.Width >= aStage.Height ? aStage.Width : aStage.Height);

         // This is an implementation of what's known as a digital differential analysis (DDA).
         // In contrast to another popular method, Bresenham's line algorithm, this offers real
         // accuracy with regards to the cast direction. Bresenham's use of integer arithmetic
         // limits it substantially.
         Vector scaledDirection = new Vector(aDirection)
         {
            Length = maxLength
         };
         double dx = scaledDirection.X;
         double dy = scaledDirection.Y;
         double step = Math.Abs(Math.Abs(dx) >= Math.Abs(dy) ? dx : dy);
         dx /= step;
         dy /= step;
         double x = aOrigin.X;
         double y = aOrigin.Y;

         for (int i = 0; i <= step; i++)
         {
            foreach (Player player in aPlayers)
            {
               if (IsInside(player, x, y) == true)
               {
                  aCollisionPoint = new Vector(x, y);
                  return true;
               }
            }

            if (aStage.IsSolid((int)x, (int)y) == true)
            {
               aCollisionPoint = new Vector(x, y);
               return true;
            }

            x += dx;
            y += dy;
         }

         // If still executing, the ray does not collide with the stage.
         aCollisionPoint = null;
         return false;
      }

      #endregion Collision response
   }
}
