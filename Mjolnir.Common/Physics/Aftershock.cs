﻿using Mjolnir.Common.Models;
using System.Collections.Generic;
using System.Linq;

namespace Mjolnir.Common.Physics
{
   /// <summary>
   /// Object for holding contextual information for a single detonation's aftershock.
   /// </summary>
   public class Aftershock
   {
      public List<Player> Players { get; } = new List<Player>();

      public int Damage { get; set; }

      public double CountdownInSeconds { get; set; }

      public Aftershock Clone(WorldStateData aWorldState)
      {
         Aftershock clone = new Aftershock
         {
            Damage = Damage,
            CountdownInSeconds = CountdownInSeconds
         };

         foreach (Player player in Players)
         {
            clone.Players.Add(aWorldState.PlayersMap[player.ID]);
         }

         return clone;
      }

      public override string ToString()
      {
         return $"{{ {nameof(Players)} = {string.Join(", ", Players.Select(p => p.Name))}, " +
                $"{nameof(Damage)} = {Damage}, {nameof(CountdownInSeconds)} = {CountdownInSeconds} }}";
      }
   }
}
