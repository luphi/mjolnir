﻿using Mjolnir.Common.Collections;
using Mjolnir.Common.Models;
using Mjolnir.Common.Threading;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Mjolnir.Common.Physics
{
   public class World
   {
      /// <summary>
      /// Acceleration due to gravity, as a Vector.
      /// To be clear, this is g, not G. This variable is capitalized for naming conventions.
      /// </summary>
      private readonly static Vector sG = new Vector(0.0, Constants.Gravity);

      /// <summary>
      /// Discrete time step (delta time), in seconds, applied to integration.
      /// In other words, the time between changes to bodies' positions and velocities.
      /// </summary>
      private readonly static double sDT = 1.0 / Constants.GameLoopFrequencyInHertz;

      private readonly ILogger mLog = LogManager.GetCurrentClassLogger();
      private readonly Stage mStage;
      // List of all players within the physical world who are assumed to be subject to physics.
      private readonly ConcurrentList<Player> mPlayers = new ConcurrentList<Player>();
      // List of launches (projectile, player reference, and delay) that result from a shot firing
      // but who have a remaining delay before actually starting.
      private readonly ConcurrentList<Launch> mPendingLaunches = new ConcurrentList<Launch>();
      // List of launches that have already started and have not yet run their course (i.e. not all
      // projectiles of the launch have detonated or flown outside the world).
      private readonly ConcurrentList<Launch> mActiveLaunches = new ConcurrentList<Launch>();
      // List of post-detonation shocks (additional damage) from specifc projectiles to be applied,
      // after a delay, to players within range of said projectiles' detonations.
      private readonly ConcurrentList<Aftershock> mAftershocks = new ConcurrentList<Aftershock>();
      // List of post-detonation gravitational forces that result from specific projectiles to be
      // applied, for a short duration, to players within range of said projectiles' donations.
      private readonly ConcurrentList<LocalizedGravity> mLocalizedGravities =
                   new ConcurrentList<LocalizedGravity>();
      // Map of incorpeal projectile bodies, whose behavior dictates that they do not disppear when
      // colliding with a player, and the list of players which they are currently overlapping with.
      private readonly ConcurrentDictionary<Body, List<Player>> mIncorporealOverlaps =
                   new ConcurrentDictionary<Body, List<Player>>(new BodyComparer());
      // Map of bodies, players or specific projectiles, and the X-axis location they will walk to.
      private readonly ConcurrentDictionary<Body, double> mWalkGoals =
                   new ConcurrentDictionary<Body, double>(new BodyComparer());
      // A simple signal used to pause the run. The initial state, true, means the signal is set such
      // that a run will not be paused (i.e. it doesn't resume until it receives the signal).
      private readonly AsyncManualResetEvent mResumeSignal = new AsyncManualResetEvent(aInitialState: true);
      // Cancellation token source used to stop the world once running, and used to indicate if the
      // world is running or not.
      private CancellationTokenSource mTokenSource = null;


      #region Events

      /// <summary>
      /// Event invoked when a Player lands on the Stage.
      /// </summary>
      public event EventHandler<Landing> LandingEvent;

      /// <summary>
      /// Event invoked when a body, usually a player, has stopped walking for any reason.
      /// </summary>
      public event EventHandler<Body> HaltEvent;

      /// <summary>
      /// Event invoked when a player's HP has been reduced to zero or the player has fallen outside the
      /// bounds of the world.
      /// </summary>
      public event EventHandler<Player> DeathEvent;

      /// <summary>
      /// Event invoked when a projectile body collides with the Stage or a Player.
      /// </summary>
      public event EventHandler<Shooting> ShootingEvent;

      /// <summary>
      /// Event invoked when all bodies in this world have ceased moving. In other words, this indicates
      /// world simulations are complete.
      /// This event will be invoked after each call to Start(), but probably not immediately.
      /// </summary>
      public event EventHandler<Events> SettledEvent;

      #endregion Events


      #region Properties

      /// <summary>
      /// Current acceleration due to wind, as a Vector.
      /// </summary>
      public Vector Wind { get; set; } = Vector.Zero;

      public Vector Acceleration => sG + Wind;

      /// <summary>
      /// The sattelite hovering above the stage, used as a weapon by Akudas.
      /// </summary>
      public Thor Thor { get; set; }

      #endregion Properties


      #region Getters

      public double X => mStage.X;

      public double Y => mStage.Y;

      public double Width => mStage.Width;

      public double Height => mStage.Height;

      public bool IsRunning => mTokenSource != null;

      public IEnumerable<Player> Players => mPlayers;

      public IEnumerable<Projectile> Projectiles => mPendingLaunches.Select(l => l.Projectile)
                                                                    .Concat(mActiveLaunches.Select(l => l.Projectile));

      public IEnumerable<Launch> PreTurns => mActiveLaunches.Where(l => l.Projectile.HasPreTurn == true);

      #endregion Getters


      public World(Stage aStage)
      {
         mStage = aStage;
         Thor = new Thor(this);
      }

      /// <summary>
      /// Create an identical, idle world from this world. This includes active launches but
      /// pending but excludes pending launches and walking goals. In short, this will
      /// not produce a perfect clone if used between calls to Start() and Stop().
      /// </summary>
      /// <returns>An identical but idle world</returns>
      public World Clone()
      {
         //////////////////////////////////////////////////////////////////////////////////////////
         /// The first step is making clones of all contained objects in such a way that we can
         /// quickly find the *instance* we want. Many routines make the assumption they are
         /// working with the same instances other routines are.

         Dictionary<uint, Body> bodiesMap = new Dictionary<uint, Body>();
         CloneBodyList(ref bodiesMap, mPlayers); // Player is a child of Body.
         CloneBodyList(ref bodiesMap, mPlayers.Select(p => p.Satellite)); // Clone satellite bodies.
         CloneLaunchList(ref bodiesMap, mActiveLaunches);

         Dictionary<uint, Player> playersMap = new Dictionary<uint, Player>();
         foreach (Body body in bodiesMap.Values)
         {
            if (body is Player player)
            {
               playersMap[player.ID] = player;
            }
         }

         //////////////////////////////////////////////////////////////////////////////////////////
         /// The second step is making clones of the lists, maps, and individual models where the
         /// collections will need to reference the same instances.

         World clone = new World(new Stage(mStage.ID))
         {
            Wind = new Vector(Wind)
         };
         clone.mPlayers.AddRange(playersMap.Values);

         WorldStateData worldState = new WorldStateData(clone, playersMap, bodiesMap);

         foreach (Launch launch in mActiveLaunches)
         {
            clone.mActiveLaunches.Add(new Launch
            {
               PlayerID = launch.PlayerID,
               Projectile = launch.Projectile.Clone(worldState),
               DelayInMilliseconds = launch.DelayInMilliseconds
            });
         }

         return clone;
      }

      public void Add(Player aPlayer)
      {
         if (aPlayer != null)
         {
            mPlayers.Add(aPlayer);
         }
      }

      public void Remove(Player aPlayer)
      {
         if (aPlayer != null)
         {
            mPlayers.Remove(aPlayer);
         }
      }

      public void Clear()
      {
         mPlayers.Clear();
         mActiveLaunches.Clear();
         mAftershocks.Clear();
         mLocalizedGravities.Clear();
         mIncorporealOverlaps.Clear();
      }


      #region Actions

      public void Start()
      {
         // If the world is not already running and the stage is, which is necessary, is available.
         if ((IsRunning == false) && (mStage != null))
         {
            // Allocate a cancellation token source in order to cancel the background procress(es)
            // of the world if needed.
            mTokenSource = new CancellationTokenSource();

            // Fire and forget the task. It will still invoke events as time progresses.
            Task.Run(() => RunAsync(mTokenSource?.Token));
         }
         else if (IsRunning == true)
         {
            mLog.Warn($"Cannot start because a run is currently in progress");
         }
         else
         {
            mLog.Warn($"Cannot start because the provided {nameof(Stage)} is null");
         }
      }

      /// <summary>
      /// Awaitable method for starting the world. The task will complete once the world has settled.
      /// </summary>
      /// <param name="aToken">Optional cancellation token which, when not null, does two things:
      /// 1) indicates that this run of the world should be in real-time, and 2) allows this task
      /// to be cancelled</param>
      /// <returns></returns>
      public async Task<Events> StartAsync(CancellationToken? aToken = null)
      {
         // If the world is not already running and the stage is, which is necessary, is available.
         if ((IsRunning == false) && (mStage != null))
         {
            // When a null cancellation token is passed to Run(), it executes immediately, or as fast
            // as the CPU will allow. When given a cancellation token, it executes in real-time.
            return await RunAsync(aToken);
         }
         else if (IsRunning == true)
         {
            mLog.Warn($"Cannot start because a run is currently in progress");
         }
         else
         {
            mLog.Warn($"Cannot start because the provided {nameof(Stage)} is null");
         }

         return null;
      }

      public void Pause()
      {
         mResumeSignal.Reset();
      }

      public void Resume()
      {
         mResumeSignal.Set();
      }

      public void Stop()
      {
         mTokenSource?.Cancel();
         mTokenSource = null;

         mPendingLaunches.Clear();
         mAftershocks.Clear();
         mLocalizedGravities.Clear();
         mIncorporealOverlaps.Clear();
         mWalkGoals.Clear();
      }

      public bool Walk(Body aBody)
      {
         if (CanWalk(aBody) == true)
         {
            aBody.IsWalking = true;
            mWalkGoals[aBody] = aBody.X + (Constants.WalkDistanceInPixels *
                                           (aBody.Direction == Direction.RIGHT ? 1.0 : -1.0));

            return true;
         }

         return false;
      }

      public bool Fire(Player aPlayer, double aPowerAsPercent, out List<Projectile> aProjectiles)
      {
         // Output an empty list of projectiles as a default in case of an error. This list will be populated
         // if everything continues smoothly.
         aProjectiles = new List<Projectile>();

         if (aPlayer == null)
         {
            mLog.Warn($"{nameof(Fire)}() - parameter {nameof(aPlayer)} was null");
            return false;
         }

         // Try to get the Shot being fired. Getting a null be impossible in practice as the client
         // and server force mobile selection on the user...
         Shot shot = aPlayer?.Shot;

         // ... and try to place the body, or bodies, just outside the collision radius of the player.
         Vector position = aPlayer?.ProjectilePosition;

         // If the shot or position (or possibly player) is null, or the power is outside the possible range.
         if ((shot == null) || (position == null) || (aPowerAsPercent < 0.0) || (aPowerAsPercent > 1.0))
         {
            if (shot == null)
            {
               mLog.Warn($"{nameof(Fire)}() - '{nameof(shot)}' was null");
            }

            if (position == null)
            {
               mLog.Warn($"{nameof(Fire)}() - '{nameof(position)}' was null");
            }

            if (aPowerAsPercent < 0.0)
            {
               mLog.Warn($"{nameof(Fire)}() - parameter {nameof(aPowerAsPercent)} was less than 0.0");
            }
            else if (aPowerAsPercent > 1.0)
            {
               mLog.Warn($"{nameof(Fire)}() - parameter {nameof(aPowerAsPercent)} was greater than 1.0");
            }

            return false;
         }

         // Calculate the velocity of the body, or bodies, based on where the player is aiming and
         // the power of this firing.
         Vector velocity = aPlayer.Trajectory * Constants.FullPowerAsSpeedInPixelsPerSecond * aPowerAsPercent;

         // The goal here is to create a list of Launches from the player's Shot where a Launch is simply a
         // projectile and a 0+ delay before the projectile leaves the barrel.

         // Clear any lingering launches to be safe.
         mPendingLaunches.Clear();

         // Simulatenously get the Launches from the Shot and iterate through them after giving the Shot
         // the center trajectory (direction vector) to launch projectiles at.
         foreach (Launch launch in shot.Fire(this, aPlayer, position, velocity))
         {
            if ((mPlayers.Any(p => p.ID == launch.PlayerID) == false) || (launch.Projectile == null))
            {
               continue;
            }

            foreach (Body body in launch.Projectile.ProgressedBodies)
            {
               // Flag the body as active so the other routines in this World apply to it.
               body.IsSettled = false;
            }

            // Remember the Launch, making it available for the next call to Start().
            mPendingLaunches.Add(launch);
         }

         // Replace the temporarily-empty list of projectiles with the real one.
         aProjectiles = mPendingLaunches.Select(l => l.Projectile).ToList();

         return true;
      }

      #endregion Actions


      #region ToString()

      public override string ToString()
      {
         return $"{{ Stage = \"{mStage.Name}\", \"{nameof(Wind)}\" = {Wind}, \"{nameof(Thor)}\" = {Thor}, " +
                $"\"{nameof(IsRunning)}\" = {IsRunning}, # Players = {mPlayers.Count}, " +
                $"# Pending Launches = {mPendingLaunches.Count}, # Active Launches = {mActiveLaunches.Count}, " +
                $"# Aftershocks = {mAftershocks.Count}, # Localized Gravities = {mLocalizedGravities.Count}, " +
                $"# Incorporeal Overlaps = {mIncorporealOverlaps.Count}, # Walk Goals = {mWalkGoals.Count}, " +
                $"# Preturns = {PreTurns.Count()} }}";
      }

      #endregion ToString()


      #region Physics

      private async Task<Events> RunAsync(CancellationToken? aToken)
      {
         // The absence of a cancellation token is used to indicate this run should be completed
         // as fast as the CPU allows. To that end, this flag is used to either loop through every
         // step at full speed or to add artificial delays.
         bool isRealTime = aToken.HasValue == true;

         // Prepare a couple objects to hold the events that may occur as time passes: one
         // to hold all events from this step and one to hold all events since the run began.
         WorldRunData stepData = new WorldRunData();
         WorldRunData runData = new WorldRunData
         {
            StartTime = DateTime.Now,
         };

         // Declare the model/serializable version of the run data. It will be assigned
         // once the world has settled.
         Events events = null;

         // Define some temporal variables for tracking time and managing the number of steps.
         double fixedStepDurationInMilliseconds = sDT * 1000.0;
         double currentElapsedInMilliseconds = 0.0;
         double fixedElapsedTimeInMilliseconds = 0.0;
         double accumulatedTimeInMilliseconds = 0.0;
         Stopwatch stopwatch = Stopwatch.StartNew();

         bool isDone = false;

         while (isDone == false)
         {
            if (isRealTime == true)
            {
               // If simulating in real-time, in sync with an IRL clock.

               // Pause the appropriate amount of times between steps.
               await Task.Delay((int)(fixedStepDurationInMilliseconds - accumulatedTimeInMilliseconds));

               // If the world has been paused.
               if (mResumeSignal.IsSet == false)
               {
                  // Pause the stopwatch so we don't resume and immediately perform 1267032 loops
                  // as if that amount of time has actually passed.
                  stopwatch.Stop();

                  // When unset/unsignaled, this will block the task from continuing until set.
                  await mResumeSignal.WaitAsync(aToken);

                  // Resume the stopwatch so that the real-time steps can match the fixed time step.
                  stopwatch.Start();
               }

               // Calculate how much time passed since the last iteration and sum it with the
               // debt from that iteration.
               double previousElapsedInMilliseconds = currentElapsedInMilliseconds;
               currentElapsedInMilliseconds = stopwatch.ElapsedMilliseconds;
               accumulatedTimeInMilliseconds += currentElapsedInMilliseconds - previousElapsedInMilliseconds;
            }
            else
            {
               // If simulating as fast as possible.

               // Just assume millenia have passed. This way, the upcoming loop will effectively
               // continue until the world settles.
               accumulatedTimeInMilliseconds = double.MaxValue;
            }

            // Advance in time by 1+ step(s). This will result in only one loop in most
            // cases but may exceed one if the last iteration was unusually slow to
            // process. In such cases, we perform multiple iterations as fast as the
            // system will allow.
            while ((accumulatedTimeInMilliseconds >= fixedStepDurationInMilliseconds) && (isDone == false))
            {
               runData.Steps += 1;

               fixedElapsedTimeInMilliseconds += sDT * 1000.0;

               bool areAllBodiesSettled = true;
               Thor.Step(sDT);

               foreach (Launch launch in mPendingLaunches)
               {
                  if (launch.DelayInMilliseconds <= fixedElapsedTimeInMilliseconds)
                  {
                     mActiveLaunches.Add(launch);
                     launch.Projectile.IsLaunched = true;
                     mPendingLaunches.Remove(launch);
                  }
               }

               // Apply changes over time to each player.
               foreach (Player player in mPlayers)
               {
                  IntegrationStep(player, stepData);
               }

               // Apply changes over time to each projectile.
               foreach (Launch launch in mActiveLaunches)
               {
                  foreach (Body body in launch.Projectile.ProgressedBodies)
                  {
                     // Progress the bodies directly.
                     IntegrationStep(body, stepData);
                  }

                  // Allow the Projectile to do any custom steps. This may be method
                  // may or may not be implemented. It varies.
                  launch.Projectile.Step(sDT, Wind);
               }

               // Perform some checks specific to projectile bodies.
               areAllBodiesSettled &= ProjectilePhase(stepData);

               // Check for interactions between players and projectiles.
               // AKA see if any players were shot this iteration.
               areAllBodiesSettled &= PlayerProjectilePhase(stepData);

               // Perform some checks specific to player bodies.
               // This phase must be performed last due to the possiblity of other phases
               // digging holes under players and unsettling them.
               areAllBodiesSettled &= PlayerPhase(stepData);

               foreach (Aftershock aftershock in mAftershocks.ToList())
               {
                  aftershock.CountdownInSeconds -= sDT;

                  if (aftershock.CountdownInSeconds <= 0.0)
                  {
                     foreach (Player player in aftershock.Players)
                     {
                        HarmPlayer(player, aftershock.Damage);

                        // If the damage from the aftershock killed the player.
                        if (player.IsDead == true)
                        {
                           stepData.Deaths.Add(player);
                        }
                     }

                     mAftershocks.Remove(aftershock);
                  }
               }

               foreach (LocalizedGravity localizedGravity in mLocalizedGravities.ToList())
               {
                  localizedGravity.CountdownInSeconds -= sDT;

                  if (localizedGravity.CountdownInSeconds <= 0.0)
                  {
                     mLocalizedGravities.Remove(localizedGravity);
                  }
               }

               // Shootings may have been found but the effects of them have not. That is,
               // projectiles may have collided with the stage and/or players but they have
               // not yet applied damage to the stage and/or players. So, for each shooting:
               foreach (Shooting shooting in stepData.Shootings)
               {
                  // 
                  Detonate(shooting, stepData);
               }

               // Add all events from this run to the lists of events for the whole run.
               runData.Landings.AddRange(stepData.Landings);
               runData.Halts.AddRange(stepData.Halts);
               runData.Deaths.AddRange(stepData.Deaths);
               runData.Shootings.AddRange(stepData.Shootings);
               runData.Damages.AddRange(stepData.Damages);

               if ((areAllBodiesSettled == true) &&
                   (mPendingLaunches.Count <= 0) &&
                   (mAftershocks.Count <= 0) &&
                   (mLocalizedGravities.Count <= 0))
               {
                  Stop();
                  isDone = true;

                  events = new Events(runData.Landings.ToArray(),
                                      runData.Halts.ToArray(),
                                      runData.Deaths.ToArray(),
                                      runData.Shootings.ToArray(),
                                      runData.Damages.ToArray(),
                                      runData.Steps,
                                      (DateTime.Now - runData.StartTime)?.TotalSeconds ?? 0.0);

                  OnSettledEvent(events);
               }

               if (isRealTime == true)
               {
                  foreach (Landing landing in stepData.Landings)
                  {
                     OnLandingEvent(landing);
                  }

                  foreach (Body body in stepData.Halts)
                  {
                     OnHaltEvent(body);
                  }

                  foreach (Player player in stepData.Deaths)
                  {
                     OnDeathEvent(player);
                  }

                  foreach (Shooting shooting in stepData.Shootings)
                  {
                     OnShootingEvent(shooting);
                  }
               }

               // Clear the lists of the step events to reuse them in the next iteration.
               stepData.Landings.Clear();
               stepData.Halts.Clear();
               stepData.Deaths.Clear();
               stepData.Shootings.Clear();
               stepData.Damages.Clear();

               accumulatedTimeInMilliseconds -= fixedStepDurationInMilliseconds;
            }

            isDone |= aToken?.IsCancellationRequested == true;
         }

         // We use this cancellation token source to indicate whether or not the world is
         // running. To indicate this run is complete, set it to null.
         mTokenSource = null;

         return events;
      }

      private void IntegrationStep(Body aBody, WorldRunData aEvents)
      {
         if (aBody.IsSettled == false)
         {
            Vector acceleration;
            if (aBody.Behavior == DynamicBehaviorEnum.AIRFOIL)
            {
               // If the body should be treated like an airfoil.

               // The body's "lift" cancels gravity and only the wind applies.
               acceleration = new Vector(Wind);
            }
            else if (aBody.Behavior == DynamicBehaviorEnum.FEATHER)
            {
               // If the body should be treated as if it's lightweight and/or with high air resistance.

               // Air resistance halves the effect of gravity.
               acceleration = sG / 2.0 + Wind;
            }
            else if (aBody.IsSubmerged == true)
            {
               // If the body is currently swimming through the stage (due to sandshark behavior).

               // Apply strong buoyancy, or a fictional solid version of it, by reversing
               // gravity. Wind does not apply to something underground.
               acceleration = new Vector(sG.X, -sG.Y) * 5.0;
            }
            else
            {
               // If the body is just some generic rigid body.

               // Forces apply normally. Gravity and wind accelerations are applied unchanged.
               acceleration = sG + Wind;
            }

            // The following is Velocity Verlet-style integration. (RK4 is more accurate but might be overkill.)

            // Position progression. We'll use a separate position vector to avoid triggering
            // property change events twice per step.
            Vector position = new Vector(aBody.Position);
            position += aBody.Velocity * sDT;
            position += 0.5 * acceleration * sDT * sDT;
            aBody.Position = position;

            // Velocity progression. The acceleration vector will vary with the body's behavior.
            aBody.Velocity += acceleration * sDT;
         }

         if (aBody.IsWalking == true)
         {
            WalkStep(aBody, aBody.Direction, aEvents);
         }
      }

      private void WalkStep(Body aBody, Direction aDirection, WorldRunData aEvents)
      {
         // Try to get the X coordinate the body is walking towards. This is assigned when walking
         // begins and should only be nulled when walking ends.
         if (mWalkGoals.ContainsKey(aBody) == false)
         {
            return;
         }

         // Allocate a copy of the body that we'll apply changes to. If or when the changes are
         // deemed acceptable, the original model will be synced with this one.
         Body wouldBeBody = aBody is Player player ? new Player(player) : new Body(aBody);

         // The goal with these variables is to control the size of a single "step." We want the
         // body's (X) translations to be no more than one pixel so nothing like a single-pixel
         // hole or spike in the stage is skipped over.
         // dX (delta X) is the total, absolute (positive) distance to move.
         // stepX is the distance to move each iteration below. It will have a sign appropriate for the
         // direction of movement and never exceed one.
         // sumX is the total distance moved during the following loop.
         double dX = Constants.WalkSpeedInPixelsPerSecond * sDT;
         double stepX = (dX <= 1.0 ? dX : 1.0) * (aDirection == Direction.LEFT ? -1.0 : 1.0);
         double sumX = 0.0;
         double goalX = mWalkGoals[aBody];

         // Flag to help control the flow of this method. If true, the body will stop wherever
         // it is, whether the full distance was covered or not, and the halted event is invoked.
         bool isHalted = false;

         do
         {
            // Move the would-be body along the X axis.
            double originalX = wouldBeBody.X;
            wouldBeBody.X += stepX;

            // If the body has reached or passed the goal X position.
            // (This is a sort of sign comparison. If the original X and current X are on the
            // same side of the goal, these subtractions will have the same sign. Therefore,
            // if they are both, say, less than the goal X, multiplying these differences will
            // result in a positive (> 0.0) value. If on different sides, it will be negative.)
            if ((wouldBeBody.X - goalX) * (originalX - goalX) < 0.0)
            {
               // Reached the goal.
               wouldBeBody.X = goalX;
               isHalted = true;
               // The loop will continue in order to do the remaining checks (overhang
               // collision, steep climb, etc.) for this would-be final position.
            }

            // Now we must check the Y position at the new X position and respond according.
            // If the needed change in Y is too far in either direction, the body won't
            // simply have its Y position adjusted; it will either halt at the last X or
            // begin falling. In most cases, though, the body will simply be moved to the
            // stage's height at the new X.
            double originalY = wouldBeBody.Y;

            if (Collisions.AreColliding(mStage, wouldBeBody) == true)
            {
               // If the collision point, centered on the body's bottom edge, is in the stage.

               // The body will be climbing or remainig at the same Y.
               Vector footing = wouldBeBody.Footing;
               while ((Collisions.AreColliding(mStage, wouldBeBody) == true) &&
                      (Collisions.IsInside(mStage, footing.X, footing.Y) == true))
               {
                  wouldBeBody.Y -= 1.0; // Move up by one pixel.
                  footing = wouldBeBody.Footing;
               }
            }
            else
            {
               // If the body is temporarily hovering over some amount of empty air.

               // The body will be walking downward or outright falling.
               Vector footing = wouldBeBody.Footing;
               while ((Collisions.AreColliding(mStage, wouldBeBody) == false) &&
                      (Collisions.IsInside(mStage, footing.X, footing.Y) == true))
               {
                  wouldBeBody.Y += 1.0; // Move down by one pixel.
                  footing = wouldBeBody.Footing;
               }

               wouldBeBody.Y -= 1.0; // Reverse by one to return to the last transparent pixel.
            }

            // If the Y change in either direction is too great.
            if (Math.Abs(wouldBeBody.Y - originalY) > Constants.WalkMaxClimbInPixels)
            {
               isHalted = true;
               if (wouldBeBody.Y > originalY)
               {
                  // If the change in Y is too far down.

                  // Let 'em fall by flagging the body as unsettled, causing the regular
                  // step method to apply forces (e.g. gravity) to it.
                  wouldBeBody.IsSettled = false;
                  // Return the body to the last Y position, at the edge of the cliff.
                  wouldBeBody.Y = originalY;
               }
               else
               {
                  // If the change in Y is too far up.

                  // Break here so the body remains at its last position.
                  break;
               }
            }

            // If the would-be body at the new position is colliding with some overhang or similar
            // portion of the stage.
            // (This method limits collision checks to just one quadrant of the body's collision
            // circle: the upper quadrant in the direction the body is facing.)
            if (Collisions.AreCollidingOverhang(mStage, wouldBeBody) == true)
            {
               // Halt and break here so the body remains at its last position.
               isHalted = true;
               break;
            }

            // Update the would-be body's rotation at the new, acceptable position.
            wouldBeBody.Pitch = mStage.NormalAt(wouldBeBody.Footing).AngleInRadians;

            // Update the would-be body's velocity from the distance it just travelled this step.
            wouldBeBody.Velocity = new Vector(wouldBeBody.X - aBody.X, wouldBeBody.Y - aBody.Y);

            // Sync the actual model with the would-be model.
            aBody.Sync(wouldBeBody, SyncFlags.X | SyncFlags.Y | SyncFlags.PITCH |
                                    SyncFlags.VELOCITY | SyncFlags.IS_SETTLED);

            sumX += Math.Abs(stepX);
         }
         while ((isHalted == false) && (sumX <= dX));
         // NOTE: the above are the conditions of a do-while loop, not a new while loop.

         // If the body should cease walking for any reason.
         if (isHalted == true)
         {
            // Remove the goal X position, flag the body as not walking, and invoke the event to
            // alert any observers.
            mWalkGoals.TryRemove(aBody, out double _);
            aBody.IsWalking = false;
            aEvents.Halts.Add(aBody);
         }
      }

      private bool PlayerPhase(WorldRunData aEvents)
      {
         bool areAllPlayersSettled = true;

         Dictionary<Player, Vector> netGravityVectors = new Dictionary<Player, Vector>();
         List<LocalizedGravity> localizedGravities = mLocalizedGravities.ToList();

         // For every player in the world.
         foreach (Player player in mPlayers)
         {
            if (player.IsSettled == false)
            {
               // Players are considered to be settled (i.e. done having physics applied, for now)
               // if their bottom edge is currently touching the stage.
               player.IsSettled = mStage.IsSolid(player.Footing) == true;

               // If the player was moving until just now (i.e. the player landed on the stage).
               if (player.IsSettled == true)
               {
                  // Add the player to the list of landed bodies and pass along the exact location
                  // at which the player touched down.
                  // NOTE: Calculating the collision point and minimum translation vector
                  // require the player's velocity for calculations. It must not be changed
                  // until after these methods have returned.
                  aEvents.Landings.Add(new Landing
                  {
                     Player = player,
                     Point = Collisions.GetStageCollisionPoint(mStage, mPlayers, player)
                  });

                  // Move the player back just enough for it to be right on top top of the stage.
                  player.Position += Collisions.GetMinimumTranslationVector(mStage, player);

                  // Set the player's rotation based on the stage's slope at this position.
                  player.Pitch = mStage.NormalAt(player.Footing).AngleInRadians;

                  // Zeroize the player's physical properties now that he/she is settled.
                  player.Acceleration = Vector.Zero;
                  player.Velocity = Vector.Zero;
                  player.IsSettled = true;
               }

               // If the player has fallen into the void, never to return.
               if (player.Y > mStage.Height)
               {
                  // Add the player to the list of deaths.
                  aEvents.Deaths.Add(player);

                  // Zeroize the player's physical properties now that he/she is settled.
                  player.Acceleration = Vector.Zero;
                  player.Velocity = Vector.Zero;
                  player.IsSettled = true;
                  player.IsDead = true;
               }
            }

            netGravityVectors[player] = Vector.Zero;
            foreach (LocalizedGravity localizedGravity in localizedGravities)
            {
               // TODO prevent it from jittering near the gravity center
               Vector gravityVector = localizedGravity.Center - player.Center;
               if ((gravityVector.Length <= Constants.BlastRadiusInPixels * 5) &&
                   (Math.Abs(gravityVector.X) >= Constants.WalkDistanceInPixels))
               {
                  netGravityVectors[player] += gravityVector * (localizedGravity.IsPull ? 1.0 : -1.0);
               }
            }

            areAllPlayersSettled &= player.IsSettled && (player.IsWalking == false);
         }

         foreach (KeyValuePair<Player, Vector> playerVectorPair in netGravityVectors)
         {
            Player player = playerVectorPair.Key;
            Vector netGravityVector = playerVectorPair.Value;
            if ((netGravityVector != Vector.Zero) && (CanWalk(player) == true))
            {
               Direction direction = netGravityVector.X < 0.0 ? Direction.LEFT : Direction.RIGHT;
               mWalkGoals[player] = player.X + Constants.WalkDistanceInPixels *
                                              (direction == Direction.RIGHT ? 1.0 : -1.0);

               // Localized gravity pulls the player(s) along the X axis and, because regular gravity
               // still applies, the resulting movement is just like walking. For that reason, the
               // walk step is reused but we want to ignore "halt" event(s) since this is a pre-turn.
               // To ignore them, we just create a new data object and forget it ever existed.
               WalkStep(player, direction, new WorldRunData());
            }
         }

         return areAllPlayersSettled;
      }

      private bool ProjectilePhase(WorldRunData aEvents)
      {
         bool areAllProjectilesSettled = true;

         // For every projectile in the world.
         foreach (Launch launch in mActiveLaunches.ToList())
         {
            Projectile projectile = launch.Projectile;
            Shot shot = projectile.Player.Shot;

            foreach (Body body in projectile.CorporealBodies)
            {
               if ((body.X + 2.0 * body.Radius < mStage.X) ||
                   (body.X > mStage.X + mStage.Width) ||
                   (body.Y > mStage.Y + mStage.Height))
               {
                  // If the body has been shot past the left or right ends of the stage, or the
                  // body has fallen into the void below the stage.

                  // It won't be returning. Forget about it.
                  projectile.Remove(body);
               }
               else if ((Collisions.AreColliding(mStage, body) == true) &&
                        (body.Behavior != DynamicBehaviorEnum.INCORPOREAL))
               {
                  // If the projectile is contacting the stage to any degree and the body's behavior
                  // isn't the one that allows it to phase through the stage.

                  if (body.Behavior == DynamicBehaviorEnum.SANDSHARK)
                  {
                     // If the body "swims" through the stage.

                     if ((body.IsSubmerged == true) && (mStage.IsSolid(body.Center) == false))
                     {
                        // If the body is, or has been, submerged in the stage but its center has just
                        // now exited the stage.

                        // Sandshark bodies have unique behavior when they emerge from the stage.
                        projectile.OnStageEmergence(body);
                     }
                     else if ((body.IsSubmerged == false) && (mStage.IsSolid(body.Center) == true))
                     {
                        // If the body has not yet been submerged in the stage but its center has just
                        // now entered the stage.

                        // Flag the body as submerged. This will change the acceleration applied to
                        // it in Step().
                        body.IsSubmerged = true;
                     }
                  }
                  else if (body.Behavior == DynamicBehaviorEnum.BALL)
                  {
                     Vector normal = mStage.NormalAt(Collisions.GetStageCollisionPoint(mStage, mPlayers, body));
                     Vector penetrationVector = Collisions.GetMinimumTranslationVector(mStage, body);
                     body.Position += penetrationVector;
                     Vector normalizedReflectionVector = Vector.Reflect(body.Velocity, normal).Normalized();
                     body.Position += normalizedReflectionVector * penetrationVector.Length;
                     body.Velocity = normalizedReflectionVector * Constants.BallSpeedInPixelPerSeconds;

                     projectile.OnStageCollision(body, Collisions.GetStageCollisionPoint(mStage, mPlayers, body));
                  }
                  else if (shot.Behavior == ProjectileBehaviorEnum.STICKY)
                  {
                     // If the body behaves something like a mine that sticks to the stage.

                     // This does not count as a shooting. Instead, place the body on the stage
                     // similar to the way a player would placed.
                     body.Position += Collisions.GetMinimumTranslationVector(mStage, body);
                     body.Pitch = mStage.NormalAt(body.Footing).AngleInRadians;
                     body.Acceleration = Vector.Zero;
                     body.Velocity = Vector.Zero;
                     body.IsSettled = true;

                     // The body is now on the stage and settled but it doesn't end there.
                     projectile.Step(sDT, Wind);
                  }
                  else
                  {
                     // If the projectile uses the Thor to fire a beam.
                     if (shot.Behavior == ProjectileBehaviorEnum.THOR)
                     {
                        // Alert the Thor to the firing. It will pause while it cools down.
                        Thor.Fire();
                     }

                     projectile.OnStageCollision(body, Collisions.GetStageCollisionPoint(mStage, mPlayers, body));
                  }
               }
            }

            CheckForDetonations(launch, shot, projectile, aEvents);

            areAllProjectilesSettled &= projectile.IsSettled;
         }

         return areAllProjectilesSettled;
      }

      private bool PlayerProjectilePhase(WorldRunData aEvents)
      {
         bool areAllProjectilesSettled = true;

         // For every player in the world.
         foreach (Player player in mPlayers)
         {
            // For every projectile in the world.
            foreach (Launch launch in mActiveLaunches)
            {
               Projectile projectile = launch.Projectile;
               Shot shot = projectile.Player.Shot;
               foreach (Body projectileBody in projectile.CorporealBodies)
               {
                  // If the body's behavior is the "swim through the stage" type.
                  if (projectileBody.Behavior == DynamicBehaviorEnum.SANDSHARK)
                  {
                     // Sandshark bodies don't collide with players. They only detonate after submerging
                     // and subsequently hitting the open air again.
                     continue;
                  }

                  if (Collisions.AreColliding(player, projectileBody) == true)
                  {
                     // GetBodyCollisionPoint() assumes the first body parameter is moving (i.e. has a
                     // non-zero velocity). Determine which body is moving with preference given to the
                     // projectile's body. The vast majority of the time, the projectile will be moving.
                     Body movingBody = projectileBody.Velocity != Vector.Zero ? projectileBody : player;
                     Body staticBody = ReferenceEquals(movingBody, projectileBody) ? player : projectileBody;

                     if (projectileBody.Behavior == DynamicBehaviorEnum.INCORPOREAL)
                     {
                        // If this body is not already having its overlaps monitored.
                        if (mIncorporealOverlaps.ContainsKey(projectileBody) == false)
                        {
                           // Create a list for this body. When the body collides with a player, it will
                           // be added to this list so we can trigger a collision event just once with
                           // that player.
                           mIncorporealOverlaps[projectileBody] = new List<Player>();
                        }

                        // If this body has not already overlapped with this player.
                        if (mIncorporealOverlaps[projectileBody].Contains(player) == false)
                        {
                           // Remember this collision so we don't respond to it more than once.
                           mIncorporealOverlaps[projectileBody].Add(player);

                           // Now, have the projectile respond to the collision between the body and player.
                           projectile.OnPlayerCollision(projectileBody,
                                                        Collisions.GetBodyCollisionPoint(movingBody, staticBody),
                                                        player);
                        }
                     }
                     else
                     {
                        // If the projectile uses the Thor to fire a beam.
                        if (shot.Behavior == ProjectileBehaviorEnum.THOR)
                        {
                           // Alert the Thor to the firing. It will pause while it cools down.
                           Thor.Fire();
                        }

                        projectile.OnPlayerCollision(projectileBody,
                                                     Collisions.GetBodyCollisionPoint(movingBody, staticBody),
                                                     player);
                     }
                  }
                  else if ((projectileBody.Behavior == DynamicBehaviorEnum.INCORPOREAL) &&
                           (mIncorporealOverlaps.ContainsKey(projectileBody) == true) &&
                           (mIncorporealOverlaps[projectileBody].Contains(player) == true))
                  {
                     // Incorporeal bodies don't interact with the stage but do recognize player collisions.
                     // If the body has incorpeal behavior and was colliding with the player until just now.

                     // Remove this player from that body's list of players it overlaps.
                     mIncorporealOverlaps[projectileBody].Remove(player);
                  }
               }

               CheckForDetonations(launch, shot, projectile, aEvents);

               areAllProjectilesSettled &= projectile.IsSettled;
            }
         }

         return areAllProjectilesSettled;
      }

      #endregion Physics


      #region Helper methods

      private bool CanWalk(Body aBody)
      {
         return (aBody == null) || (aBody.IsWalking == true) || (aBody.IsSettled == false) ||
                (mStage == null) || (Collisions.AreCollidingOverhang(mStage, aBody) == false);
      }

      private void Detonate(Shooting aShooting, WorldRunData aEvents)
      {
         // Dig a hole in the stage where the detonation happened.
         mStage.Dig(aShooting.Center, aShooting.HoleMajorAxis, aShooting.HoleMinorAxis);

         // Potentially prepare an Aftershock (holds information needed to apply aftershock damage
         // after a delay). If the shooting has an aftershock effect, the players within the blast
         // radius will have it applied to them.
         Aftershock aftershock = null;
         if (aShooting.AftershockDamage.HasValue == true)
         {
            aftershock = new Aftershock();
         }

         // Figure out the blast radius. In most cases, this is just the constant value.

         foreach (Player player in mPlayers)
         {
            // Calculate the distance, in pixels, between the player's center and the center of
            // the detonation.
            double distance = (player.Center - aShooting.Center).Length;

            // If the distance is within the detonation blast radius.
            // (The detonation blast radius is smaller than the AoE radius.)
            if (distance <= Constants.BlastRadiusInPixels)
            {
               CheckAreaEffects(aShooting, player, aEvents);

               // At least some damage will be done to this player.
               // Clamp the distance value so that it's in the [player radius, blast radius] range.
               distance = Math.Clamp(distance, player.Radius, Constants.BlastRadiusInPixels);

               // Calculate a coefficient for an upcoming equation. This coefficient effectively
               // determines if max damage is done to the player, or min damage.
               // This coefficient will be in the [0.0, 1.0] range.
               double coefficient = (distance - player.Radius) /
                                    (Constants.BlastRadiusInPixels - player.Radius);

               // Calculate the damage such that the damage to the player is between the detonation's
               // min and max damage values (derived from the Shot) with greater distance resulting
               // in less damage, at a linear rate.
               int damage = aShooting.MaxDamage -
                            (int)Math.Round((aShooting.MaxDamage - aShooting.MinDamage) * coefficient);

               // Apply the player's damage multiplier. This 1.0+ multiplier can increase due to some
               // projectiles with a resistance reduction effect. This value is usually just 1.0.
               damage = (int)Math.Round(damage * player.DamageMultiplier);

               // Harm the player with the calculated damage. This method returns the amount of damage
               // done to the player which may be less than the calculated value depending on the
               // player's remaining HP (shield and/or regular HP).
               damage = HarmPlayer(player, damage);

               // Damage to players is important so log it into the run data for later review.
               aEvents.Damages.Add(new Damage
               {
                  PlayerID = player.ID,
                  Amount = damage
               });

               if (player.IsDead == true)
               {
                  // If the damage from the detonation killed the player.

                  aEvents.Deaths.Add(player);
               }
               else
               {
                  // If the player is still alive after taking damage.

                  // If the shot causes a damaging aftershock shortly after detonation.
                  if (aShooting.AftershockDamage.HasValue == true)
                  {
                     // Remember the player so aftershock damage can be applied later.
                     aftershock.Players.Add(player);
                  }

                  // If the shot causes a reduction in the player's resistance.
                  if (aShooting.ResistanceReduction.HasValue == true)
                  {
                     // Increase the damage done to the player by all future shootings.
                     // The DamageMultiplier setter will limit its value to 50% (1.5x).
                     player.DamageMultiplier += aShooting.ResistanceReduction.Value;
                  }
               }

               // If the damage is the result of a big ol' laser beam from Thor.
               if (aShooting.Behavior == ProjectileBehaviorEnum.THOR)
               {
                  // Thor grows stronger.
                  Thor.Charge(damage);
               }
            }

            if ((player.IsDead == false) && (distance <= Constants.AreaOfEffectRadiusInPixels))
            {
               CheckAreaEffects(aShooting, player, aEvents);
            }

            // Since a hole was just dug in the stage, it's possible the player is currently hovering
            // in the air roadrunner vs. coyote style. Unsettle the player if he/she is no longer
            // on the stage.
            Vector footing = player.Footing;

            // Players are actually kept one pixel above the stage so that they don't constantly
            // collide and trigger various things. For that reason, we want to check one pixel below
            // the Footing getter's value.
            footing.Y += 1.0;
            player.IsSettled = mStage.IsSolid(footing) == true;
         }

         // If the Aftershock context object isn't null (meaning this shooting has an aftershock effect)
         // and the list of players to apply it to is not empty.
         if (aftershock?.Players?.Count > 0)
         {
            // Apply an additional, constant damage after a short delay.
            aftershock.Damage = aShooting.AftershockDamage.Value;
            aftershock.CountdownInSeconds = Constants.AftershockDelayInMilliseconds / 1000.0;
            // The players to apply the damage to were added to a list in the loop above.

            mAftershocks.Add(aftershock);
         }

         if ((aShooting.Behavior == ProjectileBehaviorEnum.GRAVITY_PULL) ||
             (aShooting.Behavior == ProjectileBehaviorEnum.GRAVITY_PUSH))
         {
            mLocalizedGravities.Add(new LocalizedGravity
            {
               Center = aShooting.Center,
               IsPull = aShooting.Behavior == ProjectileBehaviorEnum.GRAVITY_PULL,
               CountdownInSeconds = Constants.LocalizedGravityDurationInMilliseconds / 1000.0
            });
         }
      }

      private void CheckForDetonations(Launch aLaunch,
                                       Shot aShot,
                                       Projectile aProjectile,
                                       WorldRunData aEvents)
      {
         // For the 0+ detonations that the projectile is reporting.
         foreach (Vector point in aProjectile.Detonations)
         {
            if ((aShot.Behavior == ProjectileBehaviorEnum.THOR) ||
                (aShot.Behavior == ProjectileBehaviorEnum.SATELLITE) ||
                (aShot.Behavior == ProjectileBehaviorEnum.LIGHTNING))
            {
               // If the behavior involves casting a single ray towards the detonation point.

               Vector origin = null;
               if (aShot.Behavior == ProjectileBehaviorEnum.THOR)
               {
                  // Thor will fire a beam where the body collided with the stage. To that end,
                  // cast a ray from the Thor towards that point. "Towards" is important as the
                  // ray may collide with the stage elsewhere and detonate at that point rather
                  // than the target point.
                  origin = Thor.Center;
               }
               else if (aShot.Behavior == ProjectileBehaviorEnum.SATELLITE)
               {
                  // Same concept as the Thor laser but from a satellite above a player.
                  // TODO: Is it possible for the Sallite to be null here due to the user switching
                  // mobiles, or moving the satellite by switching the shot, since firing?
                  origin = aProjectile.Player.Satellite.Center;
               }
               else if (aShot.Behavior == ProjectileBehaviorEnum.LIGHTNING)
               {
                  // Lightning strikes from directly above with the same general idea as the other
                  // cases. We'll choose an origin far above the detonation point to cast the ray from.
                  origin = point + new Vector(0.0, -mStage.Height);
               }

               // If the ray hit the stage or a player.
               if ((origin != null) && (Collisions.CastRay(mStage,
                                                           mPlayers,
                                                           origin,
                                                           point - origin,
                                                           out Vector beamCollisionPoint) == true))
               {
                  aEvents.Shootings.Add(new Shooting(beamCollisionPoint, aShot));
               }
            }
            else if (aShot.Behavior == ProjectileBehaviorEnum.LIGHTNING_V)
            {
               // If the behavior involves casting two rays towards the detonation point.

               // For i = -1 and i = 1.
               for (int i = -1; i < 2; i += 2)
               {
                  // Choose an origin that produces a 45-degree ray. Given i is -1 or 1,
                  // this means one will be from the left and one from the right.
                  Vector origin = point + new Vector(i * mStage.Height, -mStage.Height);

                  // If the ray hit the stage or a player.
                  if (Collisions.CastRay(mStage,
                                         mPlayers,
                                         origin,
                                         point - origin,
                                         out Vector beamCollisionPoint) == true)
                  {
                     aEvents.Shootings.Add(new Shooting(beamCollisionPoint, aShot));
                  }
               }
            }
            else
            {
               // The projectile body itself detonates therefore a shooting occurred at this point.
               aEvents.Shootings.Add(new Shooting(point, aShot));
            }
         }
         aProjectile.Detonations.Clear();

         // If the projectile is done, depleted, or otherwise finished (this varies among Projectiles).
         if (aProjectile.IsDone == true)
         {
            // No need to keep this projectile around anymore. Toss it.
            mActiveLaunches.Remove(aLaunch);
         }
      }

      private void CheckAreaEffects(Shooting aShooting, Player aPlayer, WorldRunData aEvents)
      {
         if (aShooting.HasAreaOfEffect == true)
         {
            if (aShooting.Behavior == ProjectileBehaviorEnum.LIGHTNING)
            {
               // If the shooting's projectile has an area effect that summons lightning.

               Vector origin = aShooting.Center + new Vector(0.0, -mStage.Height);
               if (Collisions.CastRay(mStage,
                                      mPlayers,
                                      origin,
                                      aShooting.Center - origin,
                                      out Vector beamCollisionPoint) == true)
               {
                  aEvents.Shootings.Add(new Shooting
                  {
                     Center = beamCollisionPoint,
                     MinDamage = aShooting.MinDamage,
                     MaxDamage = aShooting.MinDamage
                  });
               }
            }
            else if (aShooting.Behavior == ProjectileBehaviorEnum.SHIELD_STRIP)
            {
               // If the shooting's projectile had an area of effect property that nullifies
               // the shields of players in range.

               // Zeroize the player's shield.
               aPlayer.ShieldHP = 0;
            }
         }
      }

      /// <summary>
      /// Applies the given damage to the given player, accounting for the player's potential
      /// shield HP, and returns the amount of damage actually done.
      /// If the player's HP is reduced to zero, this method will also said its "is dead" property.
      /// </summary>
      /// <param name="aPlayer">The player to be harmed.</param>
      /// <param name="aDamage">The amount of damage to take from the player.</param>
      /// <returns>The amount of damage actually done to the player</returns>
      private static int HarmPlayer(Player aPlayer, int aDamage)
      {
         int damageRemaining = aDamage;
         int damageDone = 0;

         if (aPlayer.ShieldHP > 0)
         {
            if (aPlayer.ShieldHP > damageRemaining)
            {
               damageDone += damageRemaining;
               aPlayer.ShieldHP -= damageRemaining;
            }
            else
            {
               damageDone += aPlayer.ShieldHP;
               aPlayer.ShieldHP = 0;
            }

            damageRemaining = aDamage - damageDone;
         }

         if (aPlayer.HP <= damageRemaining)
         {
            damageDone += aPlayer.HP;
            aPlayer.HP = 0;
            aPlayer.IsDead = true;
         }
         else
         {
            damageDone += damageRemaining;
            aPlayer.HP -= damageRemaining;
         }

         return damageDone;
      }

      private void CloneBodyList(ref Dictionary<uint, Body> aBodiesMap, IEnumerable<Body> aBodies)
      {
         foreach (Body body in aBodies)
         {
            if (aBodiesMap.ContainsKey(body._id) == false)
            {
               aBodiesMap[body._id] = body.Clone();
            }
         }
      }

      private void CloneLaunchList(ref Dictionary<uint, Body> aBodiesMap, IEnumerable<Launch> aLaunchs)
      {
         foreach (Launch launch in aLaunchs)
         {
            if (launch.Projectile != null)
            {
               CloneBodyList(ref aBodiesMap, launch.Projectile.ProgressedBodies);
               CloneBodyList(ref aBodiesMap, launch.Projectile.CorporealBodies);
            }
         }
      }


      #region Event wrappers

      private void OnLandingEvent(Landing aLanding)
      {
         try
         {
            LandingEvent?.Invoke(this, aLanding);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(LandingEvent)}");
         }
      }

      private void OnHaltEvent(Body aBody)
      {
         try
         {
            HaltEvent?.Invoke(this, aBody);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(HaltEvent)}");
         }
      }

      private void OnDeathEvent(Player aPlayer)
      {
         try
         {
            DeathEvent?.Invoke(this, aPlayer);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(DeathEvent)}");
         }
      }

      private void OnShootingEvent(Shooting aShooting)
      {
         try
         {
            ShootingEvent?.Invoke(this, aShooting);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(ShootingEvent)}");
         }
      }

      private void OnSettledEvent(Events aEvents)
      {
         try
         {
            SettledEvent?.Invoke(this, aEvents);
         }
         catch (Exception e)
         {
            mLog.Error(e, $"Exception thrown while propagating a {nameof(SettledEvent)}");
         }
      }

      #endregion Event wrappers


      #endregion Helper methods
   }
}
