﻿using Mjolnir.Common.Models;
namespace Mjolnir.Common.Physics
{
   /// <summary>
   /// Holds contextual information for the push or pull forces resulting from a D.J.'s shots (2 and SS).
   /// </summary>
   public class LocalizedGravity
   {
      public Vector Center { get; set; }

      public bool IsPull { get; set; }

      public double CountdownInSeconds { get; set; }

      public LocalizedGravity Clone()
      {
         return new LocalizedGravity
         {
            Center = new Vector(Center),
            IsPull = IsPull,
            CountdownInSeconds = CountdownInSeconds
         };
      }

      public override string ToString()
      {
         return $"{{ {nameof(Center)} = {Center}, {nameof(IsPull)} = {IsPull}, " +
                $"{nameof(CountdownInSeconds)} = {CountdownInSeconds} }}";
      }
   }
}
