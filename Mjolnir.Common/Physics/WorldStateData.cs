﻿using Mjolnir.Common.Models;
using System.Collections.Generic;

namespace Mjolnir.Common.Physics
{
   /// <summary>
   /// Contextual object that holds stateful information about a World instance. Essentially,
   /// this is passed around to represent a snapshot of a World for the purpose of making
   /// clones of Worlds.
   /// </summary>
   public class WorldStateData
   {
      public World World { get; }

      public Dictionary<uint, Player> PlayersMap { get; }

      public Dictionary<uint, Body> BodiesMap { get; }

      public WorldStateData(World aWorld,
                            Dictionary<uint, Player> aPlayersMap,
                            Dictionary<uint, Body> aBodiesMap)
      {
         World = aWorld;
         PlayersMap = aPlayersMap;
         BodiesMap = aBodiesMap;
      }
   }
}
