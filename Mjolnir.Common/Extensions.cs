﻿using System;

namespace Mjolnir.Common
{
   public static class Extensions
   {
      public static double WrapRadians(this double aAngle)
      {
         // Limit the angle to [0, 2*pi).
         // (Is this the most efficient method? It's easy to read if nothing else.)
         while (aAngle < 0.0)
         {
            aAngle += 2.0 * Math.PI;
         }

         while (aAngle >= 2.0 * Math.PI)
         {
            aAngle -= 2.0 * Math.PI;
         }

         return aAngle;
      }

      public static double DistanceInRadians(this double aAngle, double aOtherAngle)
      {
         //double normalizedThis = WrapRadians(aAngle);
         //double normalizedOther = WrapRadians(aOtherAngle);
         //double difference = normalizedThis > normalizedOther ? normalizedThis - normalizedOther :
         //                                                       normalizedOther - normalizedThis;

         double difference = aAngle > aOtherAngle ? aAngle - aOtherAngle : aOtherAngle - aAngle;
         difference %= 2.0 * Math.PI;
         
         if (difference <= Math.PI)
         {
            return difference;
         }

         return (2.0 * Math.PI) - difference;
      }

      public static double NextDouble(this Random aRandom, double aMinimum, double aMaximum)
      {
         return aRandom.NextDouble() * (aMaximum - aMinimum) + aMinimum;
      }

      public static T Next<T>(this T aElement) where T : struct
      {
         if (typeof(T).IsEnum == false)
         {
            throw new ArgumentException(string.Format($"{typeof(T).FullName} is not an enumeration"));
         }

         T[] values = (T[])Enum.GetValues(aElement.GetType());
         
         int index = Array.IndexOf(values, aElement) + 1;
         if (index == values.Length)
         {
            index = 0;
         }

         return values[index];
      }
   }
}
