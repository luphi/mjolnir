﻿using Mjolnir.Common.Models;

namespace Mjolnir.Common.Disk
{
   public interface IContentWrapper
   {
      bool[,] LoadStage(Stage aStage);
      string StageForegroundPath(Stage aStage);
      string StageBackgroundPath(Stage aStage);
   }
}
